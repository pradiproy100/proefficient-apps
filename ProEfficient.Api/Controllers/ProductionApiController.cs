﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProEfficient.Api.Controllers
{
    //[Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductionApiController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ProductionApiController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("ProductionApi")]
        public List<ProductionEntry> ProductionApi(String productionDate, String OrganizationId, String Token)
        {

            DateTime prodDate = new DateTime();
            if (productionDate.Contains("-") || (productionDate.Contains("/")))
            {
                prodDate = productionDate.ToDateTime();
            }

            else
            {
                prodDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                                .AddMilliseconds(Convert.ToDouble(productionDate))
                                .ToLocalTime();
            }


            int OrgId = OrganizationId.ToInteger0();
            var AllProductions = new ProductionService(_configuration).DataAccessLayer.GetProductionEntry(new ProductionEntry() { ProductionDate = prodDate });
            if (AllProductions != null && AllProductions.Count > 0)
            {

                foreach (var prod in AllProductions)
                {
                    prod.MachineName = new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = prod.MachineID.ToInteger0() }).MachineName;
                    prod.ProductName = new ProductService(_configuration).DataAccessLayer.GetMasterProduct(new MasterProduct() { ProductId = prod.ProductID.ToInteger0() }).ProductName;
                }
                return AllProductions.FindAll(item => item.OrganizationId == OrgId && item.ProductionDate.ToString("dd/MM/yyyy") == prodDate.ToString("dd/MM/yyyy"));
            }
            return AllProductions;
        }



        [HttpPost]
        [Route("saveProductionEntry")]
        public Boolean saveProductionEntry([FromBody] string ProductionEntrystr)
        {
            List<jsonProductionEntry> ProductionEntry = JsonConvert.DeserializeObject<List<jsonProductionEntry>>(ProductionEntrystr);
            bool save = false;
            //JObject json = JObject.Parse(data.ProductionEntry);
            // List<ProductionEntry> ProductionEntry= new List<ProductionEntry>();
            // string response = JsonConvert.SerializeObject(data.ProductionEntry);
            //var ProductionEntry = JsonConvert.DeserializeObject<List<jsonProductionEntry>>(data.ProductionEntry.ToString());
            string strDeletedEntries = "";
            if (ProductionEntry == null)
            {
                return save;
            }

            try
            {
                for (int k = 0; k < ProductionEntry.Count; k++)
                {
                    if (ProductionEntry[k].ProductionId != null && ProductionEntry[k].ProductionId != "0")
                    {
                        strDeletedEntries += ProductionEntry[k].ProductionId + ",";
                    }

                }
                if (strDeletedEntries.Length > 0)
                { strDeletedEntries = strDeletedEntries.Remove(strDeletedEntries.Length - 1, 1); }
                for (int j = 0; j < ProductionEntry.Count; j++)
                {

                    if (ProductionEntry[j].TotalProduction.ToDecimal0() == 0)
                        continue;
                    ProductionEntry objProdEntry = new ProductionEntry();


                    if (ProductionEntry[j].ProductionId != null && ProductionEntry[j].ProductionId != "0")
                    {
                        objProdEntry = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryById(new ProductionEntry() { ProductionId = ProductionEntry[j].ProductionId.ToInteger0() });
                        objProdEntry.ProductionId = ProductionEntry[j].ProductionId.ToInteger0();

                    }
                    else
                    {
                        DateTime prdate = new DateTime();
                        DateTime.TryParse(ProductionEntry[j].ProductionDate, out prdate);
                        objProdEntry.ProductionDate = prdate;
                        //objProdEntry.rejectionlist = JsonConvert.SerializeObject(ProductionEntry[j].RejectionReasonList, Formatting.Indented);

                    }
                    objProdEntry.TotalProduction = ProductionEntry[j].TotalProduction.ToDecimal0();
                    int sMinutePart = ProductionEntry[j].ProductionStartTime.ToDecimal0().GetFractionPart() == 0 ? 0 : (ProductionEntry[j].StartTime.ToDecimal0().GetFractionPart());
                    int sHourPart = ProductionEntry[j].ProductionStartTime.ToDecimal0().GetIntPart();

                    int eMinutePart = ProductionEntry[j].ProductionEndTime.ToDecimal0().GetFractionPart() == 0 ? 0 : (ProductionEntry[j].EndTime.ToDecimal0().GetFractionPart());
                    int eHourPart = ProductionEntry[j].ProductionEndTime.ToDecimal0().GetIntPart();

                    objProdEntry.StartTime = new DateTime(objProdEntry.ProductionDate.Year,
                        objProdEntry.ProductionDate.Month, objProdEntry.ProductionDate.Day,
                        sHourPart, sMinutePart, 0);

                    objProdEntry.EndTime = new DateTime(objProdEntry.ProductionDate.Year,
                      objProdEntry.ProductionDate.Month, objProdEntry.ProductionDate.Day,
                      eHourPart, eMinutePart, 0);

                    objProdEntry.UserID = ProductionEntry[j].UserID.ToInteger0();
                    objProdEntry.OperatorID = ProductionEntry[j].OperatorID.ToInteger0();
                    objProdEntry.ProductID = ProductionEntry[j].ProductID.ToInteger0();
                    objProdEntry.SupervisorID = ProductionEntry[j].SupervisorID.ToInteger0();
                    objProdEntry.TimeOfEntry = DateTime.Now;
                    objProdEntry.ShiftId = ProductionEntry[j].ShiftId.ToInteger0();
                    objProdEntry.ReWork = ProductionEntry[j].ReWork.ToInteger0();
                    objProdEntry.Rejection = ProductionEntry[j].Rejection.ToInteger0();
                    objProdEntry.MachineID = ProductionEntry[j].MachineID.ToInteger0();
                    objProdEntry.OrganizationId = ProductionEntry[j].OrganizationId.ToInteger0();
                    // objProdEntry.rejectionlist = ProductionEntry[j].RejectionList;
                    objProdEntry.rejectionlist = JsonConvert.SerializeObject(ProductionEntry[j].RejectionReasonList, Formatting.Indented);

                    objProdEntry.deletedEntries = strDeletedEntries;
                    if (new ProductionService(_configuration).DataAccessLayer.UpdateProductionEntryThroughApi(objProdEntry))
                    {
                        save = true;
                    }
                    else
                        save = false;

                    new ProductionService(_configuration).DataAccessLayer.UpdateOEECalculation(DateTime.Now);
                }


                //Insert Breakdown data from here
                var breakdownlists = ProductionEntry[ProductionEntry.Count - 1].breakdownentrylst;
                //var dynamicbreakdownlsit= JsonConvert.DeserializeObject<List<jsonProductionEntry>>(data.breakdownlist.ToString()); 
                bool breakdatasave = saveBreakdownEntry(breakdownlists);
                if (breakdownlists != null)
                    save = breakdatasave;



            }
            catch (Exception ex)
            {

                save = false;
            }

            return save;
        }

        [HttpPost]
        [Route("deleteOnlyEntry")]
        public Boolean deleteOnlyEntry(String productionDate, String ShiftId, String typ)
        {

            bool save = false;

            try
            {
                DateTime prdate = new DateTime();
                DateTime.TryParse(productionDate, out prdate);
                Int32 shiftidint = Convert.ToInt32(ShiftId);
                if (new BreakDownService(_configuration).DataAccessLayer.DeleteProdBreakEntryByshift(prdate, shiftidint, typ))
                {
                    save = true;
                }
                else
                    save = false;

            }
            catch (Exception ex)
            {

                save = false;
            }

            return save;
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        [NonAction]
        public Boolean saveBreakdownEntry(List<jsonBreakDownEntry> BreakdownEntry)
        {
            bool save = false;
            string strDeletedEntries = "";
            if (BreakdownEntry == null)
            {
                return save;
            }


            try
            {
                for (int k = 0; k < BreakdownEntry.Count; k++)
                {
                    if (BreakdownEntry[k].BreakDownID != null && BreakdownEntry[k].BreakDownID != "0")
                    {
                        strDeletedEntries += BreakdownEntry[k].BreakDownID + ",";
                    }

                }
                if (strDeletedEntries.Length > 0)
                { strDeletedEntries = strDeletedEntries.Remove(strDeletedEntries.Length - 1, 1); }
                for (int j = 0; j < BreakdownEntry.Count; j++)
                {
                    BreakDownEntry objBreakdowbEntry = new BreakDownEntry();

                    if (BreakdownEntry[j].BreakDownID != "" && BreakdownEntry[j].BreakDownID != "0" && BreakdownEntry[j].BreakDownID != null)
                    {
                        objBreakdowbEntry = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntry(new BreakDownEntry() { BreakDownID = BreakdownEntry[j].BreakDownID.ToInteger0() });
                    }
                    else
                    {
                        objBreakdowbEntry.ProductionDate = BreakdownEntry[j].ProductionDate.ToDateTime();
                    }

                    //int sMinutePart = BreakdownEntry[j].StartTime.ToDecimal0().GetFractionPart() == 0 ? 0 : (BreakdownEntry[j].StartTime.ToDecimal0().GetFractionPart());
                    //int sHourPart = BreakdownEntry[j].StartTime.ToDecimal0().GetIntPart();

                    //int eMinutePart = BreakdownEntry[j].EndTime.ToDecimal0().GetFractionPart() == 0 ? 0 : (BreakdownEntry[j].EndTime.ToDecimal0().GetFractionPart());
                    //int eHourPart = BreakdownEntry[j].EndTime.ToDecimal0().GetIntPart();

                    //objBreakdowbEntry.StartTime = new DateTime(objBreakdowbEntry.ProductionDate.Year,
                    //    objBreakdowbEntry.ProductionDate.Month, objBreakdowbEntry.ProductionDate.Day,
                    //    sHourPart, sMinutePart, 0);

                    //objBreakdowbEntry.EndTime = new DateTime(objBreakdowbEntry.ProductionDate.Year,
                    //  objBreakdowbEntry.ProductionDate.Month, objBreakdowbEntry.ProductionDate.Day,
                    //  eHourPart, eMinutePart, 0);
                    objBreakdowbEntry.StartTime = BreakdownEntry[j].StartTime.ToDateTime();
                    objBreakdowbEntry.EndTime = BreakdownEntry[j].EndTime.ToDateTime();
                    objBreakdowbEntry.TotalStoppageTime = BreakdownEntry[j].TotalStoppageTime.ToDecimal0() * 60;
                    objBreakdowbEntry.OperatorID = BreakdownEntry[j].OperatorID.ToInteger0();
                    objBreakdowbEntry.UserID = BreakdownEntry[j].UserID.ToInteger0();
                    objBreakdowbEntry.SupervisorID = BreakdownEntry[j].SupervisorID.ToInteger0();
                    objBreakdowbEntry.TimeOfEntry = DateTime.Now;
                    objBreakdowbEntry.IsActive = true;
                    objBreakdowbEntry.Remarks = BreakdownEntry[j].Remarks;
                    objBreakdowbEntry.ShiftId = BreakdownEntry[j].ShiftId.ToInteger0();
                    objBreakdowbEntry.MachineID = BreakdownEntry[j].MachineID.ToInteger0();
                    objBreakdowbEntry.OrganizationId = BreakdownEntry[j].OrganizationId.ToInteger0();
                    objBreakdowbEntry.ReasonId = BreakdownEntry[j].ReasonId.ToInteger0();
                    objBreakdowbEntry.SubReasonId = BreakdownEntry[j].SubReasonId.ToInteger0();

                    objBreakdowbEntry.deletedEntries = strDeletedEntries;
                    if (new BreakDownService(_configuration).DataAccessLayer.UpdateBreakDownEntry(objBreakdowbEntry))
                    {
                        save = true;
                    }
                    else
                        save = false;
                }
            }
            catch (Exception)
            {

                save = false;
            }
            return save;

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("getBreakDownEntryData")]
        public List<BreakDownEntry> getBreakDownEntryData(String productionDate, String OrganizationId, String Token)
        {

            DateTime prodDate = new DateTime();
            if (productionDate.Contains("-") || (productionDate.Contains("/")))
            {
                prodDate = productionDate.ToDateTime();
            }

            else
            {
                prodDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                                .AddMilliseconds(Convert.ToDouble(productionDate))
                                .ToLocalTime();
            }


            int OrgId = OrganizationId.ToInteger0();
            var AllBreakdowns = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryListDateTime(prodDate, OrgId);
            if (AllBreakdowns != null && AllBreakdowns.Count > 0)
            {

                foreach (var prod in AllBreakdowns)
                {
                    prod.MachineName = new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = prod.MachineID.ToInteger0() }).MachineName;

                }
                return AllBreakdowns.FindAll(item => item.OrganizationId == OrgId && item.ProductionDate.ToString("dd/MM/yyyy") == prodDate.ToString("dd/MM/yyyy"));
            }
            return AllBreakdowns;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("GetProductionEntry")]
        public jsonProductionEntry GetProductionEntry(string id)
        {
            jsonProductionEntry ProductionEntry = new jsonProductionEntry();
            var ProdEntry = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryById(new ProductionEntry() { ProductionId = id.ToInteger0() });


            if (ProdEntry != null)
            {

                ProductionEntry.ProductionId = ProdEntry.ProductionId.ToStringx();
                ProductionEntry.MachineID = ProdEntry.MachineID.ToStringx();
                ProductionEntry.ProductID = ProdEntry.ProductID.ToStringx();
                ProductionEntry.UserID = ProdEntry.UserID.ToStringx();
                ProductionEntry.OperatorID = ProdEntry.OperatorID == 0 ? ProdEntry.UserID.ToStringx() : ProdEntry.OperatorID.ToStringx();
                ProductionEntry.SupervisorID = ProdEntry.SupervisorID.ToStringx();
                ProductionEntry.OrganizationId = ProdEntry.OrganizationId.ToStringx();
                ProductionEntry.TimeOfEntry = ProdEntry.TimeOfEntry.ToStringx();
                ProductionEntry.ShiftId = ProdEntry.ShiftId.ToStringx();
                ProductionEntry.ProductionStartTime = ProdEntry.ProductionStartTime.ToStringx();
                ProductionEntry.ProductionEndTime = ProdEntry.ProductionEndTime.ToStringx();
                ProductionEntry.StartTime = ProdEntry.StartTime.ToDateTime().ToString("HH.mm");
                ProductionEntry.EndTime = ProdEntry.EndTime.ToDateTime().ToString("HH.mm");
                ProductionEntry.TotalProduction = ProdEntry.TotalProduction.ToStringx();
                ProductionEntry.ReWork = ProdEntry.ReWork.ToStringx();
                ProductionEntry.Rejection = ProdEntry.Rejection.ToStringx();
                ProductionEntry.ProductionDate = ProdEntry.ProductionDate.ToStringx();
                ProductionEntry.IsSaved = ProdEntry.IsSaved.ToStringx();
                ProductionEntry.ProdDate = ProdEntry.ProdDate.ToStringx();
                ProductionEntry.MachineName = ProdEntry.MachineName.ToStringx();
                ProductionEntry.ProductName = ProdEntry.ProductName.ToStringx();

            }

            return ProductionEntry;

        }

        [HttpGet]
        [Route("getProductionEntryDataByShiftAndDate")]
        public List<GetProductionEntryByShiftAndDateVM> getProductionEntryDataByShiftAndDate([FromQuery] string ProdDate, [FromQuery] string ProdToDate, string Shiftid, string organizationID)
        {

            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            DateTime ProductionToDate = new DateTime();
            DateTime.TryParse(ProdToDate, out ProductionToDate);
            List<GetProductionEntryByShiftAndDateVM> AllProductions = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryByShiftAndDate(new GetProBreakDataParamsVM()
            { ProductionDate = ProductionDate, ProductionToDate = ProductionToDate, ShiftId = shiftid, OrganizationID = OrgId });

            List<RejectionEntryByShiftAndDateVM> rejList = new ProductionService(_configuration).DataAccessLayer.GetRejectionEntryByShiftAndDate(new GetProBreakDataParamsVM()
            { ProductionDate = ProductionDate, ProductionToDate = ProductionToDate, ShiftId = shiftid, OrganizationID = OrgId });

            if (rejList?.Count > 0)
            {
                foreach (GetProductionEntryByShiftAndDateVM p in AllProductions)
                {
                    p.Rejection = rejList.Where(x => x.ProdEntryId == p.ProductionId || x.MachineID == p.MachineID) == null ? 0
                            : rejList.Where(x => x.ProdEntryId == p.ProductionId || x.MachineID == p.MachineID).Sum(x => x.ProdCount);
                    p.RejectionReasonList = new List<RejectionEntryByShiftAndDateVM>();
                    foreach (RejectionEntryByShiftAndDateVM c in rejList)
                    {
                        if (c.ProdEntryId > 0)
                        {
                            if (p.ProductionId == c.ProdEntryId)
                            {
                                p.RejectionReasonList.Add(c);
                            }
                        }
                        else if (p.MachineID == c.MachineID)
                        {

                            p.RejectionReasonList.Add(c);
                        }
                    }

                }
            }
            return AllProductions;
            // return new JsonResult(AllProductions);
        }

        [HttpGet]
        [Route("getBreakDownEntryDataByShiftAndDate")]
        public List<GetBreakDownEntryByShiftAndDateVM> getBreakDownEntryDataByShiftAndDate([FromQuery] string ProdDate, [FromQuery] string ProdToDate, string Shiftid, string organizationID)
        {

            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            DateTime ProductionToDate = new DateTime();
            DateTime.TryParse(ProdToDate, out ProductionToDate);
            List<GetBreakDownEntryByShiftAndDateVM> AllBreakdowns = new ProductionService(_configuration).DataAccessLayer.GetBreakDownEntryByShiftAndDateVM(new GetProBreakDataParamsVM()
            { ProductionDate = ProductionDate, ProductionToDate = ProductionToDate, ShiftId = shiftid, OrganizationID = OrgId });

            return AllBreakdowns;
            // return new JsonResult(AllBreakdowns);
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("GetBreakdownEntry/{id}")]
        public jsonBreakDownEntry GetBreakdownEntry(string id)
        {
            jsonBreakDownEntry BreakDownEntry = new jsonBreakDownEntry();
            var BreakEntry = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntry(new BreakDownEntry() { BreakDownID = id.ToInteger0() });


            if (BreakEntry != null)
            {

                BreakDownEntry.BreakDownID = BreakEntry.BreakDownID.ToStringx();
                BreakDownEntry.MachineID = BreakEntry.MachineID.ToStringx();
                BreakDownEntry.ReasonId = BreakEntry.ReasonId.ToStringx();
                BreakDownEntry.UserID = BreakEntry.UserID.ToStringx();
                BreakDownEntry.OperatorID = ((BreakEntry.OperatorID == null) || (BreakEntry.OperatorID == 0)) ? BreakEntry.UserID.ToStringx() : BreakEntry.OperatorID.ToStringx();
                BreakDownEntry.SupervisorID = BreakEntry.SupervisorID.ToStringx();
                BreakDownEntry.TimeOfEntry = BreakEntry.TimeOfEntry.ToStringx();
                BreakDownEntry.ShiftId = BreakEntry.ShiftId.ToStringx();
                BreakDownEntry.BreakDownStartTime = BreakEntry.StartTime.ToDateTime().ToString("HH.mm");
                BreakDownEntry.StartTime = BreakEntry.StartTime.ToDateTime().ToString("HH.mm");
                BreakDownEntry.Remarks = BreakEntry.Remarks.ToStringx();
                BreakDownEntry.BreakDownEndTime = BreakEntry.EndTime.ToDateTime().ToString("HH.mm");
                BreakDownEntry.OrganizationId = BreakEntry.OrganizationId.ToStringx();
                BreakDownEntry.EndTime = BreakEntry.EndTime.ToDateTime().ToString("HH.mm");
                BreakDownEntry.TotalStoppageTime = BreakEntry.TotalStoppageTime.ToStringx();
                BreakDownEntry.IsActive = BreakEntry.IsActive.ToStringx();
                BreakDownEntry.AddBy = BreakEntry.AddBy.ToStringx();
                BreakDownEntry.EdittedBy = BreakEntry.EdittedBy.ToStringx();
                BreakDownEntry.ProductionDate = BreakEntry.ProductionDate.ToString("dd/MM/yyyy");
                BreakDownEntry.IsSaved = BreakEntry.IsSaved.ToStringx();

            }




            return BreakDownEntry;

        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("getProductionEntryDataNew")]
        public List<ProductionEntry> getProductionEntryDataNew([FromQuery] string Shiftidstr, string organizationID)
        {

            int shiftid = Shiftidstr.ToInteger();
            int OrgId = organizationID.ToInteger();
            List<ProductionEntry> AllProductions = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryNew(new ProductionEntry()
            { ShiftId = shiftid, OrganizationId = OrgId });
            return AllProductions;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("DeleteProductionEntry")]
        public bool DeleteProductionEntry([FromQuery] string productionID)
        {
            bool result = false;
            int prodid = productionID.ToInteger();
            if (prodid > 0)
                result = new ProductionService(_configuration).DataAccessLayer.DeleteProductionEntry(prodid);
            return result;
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("getMachineProductMapping")]
        public List<ProductMachineVM> getMachineProductMapping([FromQuery] string ProdDate, string organizationID)
        {
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            int OrgId = organizationID.ToInteger();

            List<ProductMachineVM> AllMachineProduct = new MachineProductService(_configuration).DataAccessLayer.GetProductMachineList(OrgId, ProductionDate);
            //string strJson = JsonConvert.SerializeObject(AllMachineProduct, Formatting.None,
            //new JsonSerializerSettings()
            //{
            //    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            //});
            // return strJson;
            return AllMachineProduct;
            // return new JsonResult(AllProductions);
        }
        [HttpGet]
        [Route("getProductionEntryUI")]
        public ProductionPageVM getProductionEntryUI([FromQuery] string ProdDate, [FromQuery] string ProdToDate, string organizationID)
        {
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            DateTime ProductionToDate = new DateTime();
            DateTime.TryParse(ProdToDate, out ProductionToDate);
            int OrgId = organizationID.ToInteger();

            ProductionPageVM productionPageVM = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryUI(OrgId, ProductionDate, ProductionToDate);

            return productionPageVM;
            // return new JsonResult(AllProductions);
        }

    }
}
