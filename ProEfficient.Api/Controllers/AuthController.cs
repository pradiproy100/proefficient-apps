using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ProEfficient.Core.Auth.DTO;
using ProEfficient.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ProEfficient.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AuthController( IConfiguration config,
            IMapper mapper, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _config = config;
            _mapper = mapper;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegistrationDTO userForRegistration)
        {
           
            var userToCreate = _mapper.Map<User>(userForRegistration);
            var result =await _userManager.CreateAsync(userToCreate, userForRegistration.Password);            
            var userToReturn = _mapper.Map<UserForDetailedDTO>(userToCreate);
            if(result.Succeeded)
            {
               // await _userManager.AddToRoleAsync(userToCreate, userToCreate.UserTypeId.ToString());
                // return CreatedAtRoute("GetUser", new { Controller = "users", id = userToCreate.Id }, userToReturn);
                //var appUser = _mapper.Map<UserForListDTO>(userToReturn);

                return Ok(new
                    {                      
                        user = userToReturn
                    });
                
            }
            return BadRequest(result.Errors);
            
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDTO userForLogin)
        {
            var user = await _userManager.FindByEmailAsync(userForLogin.Email);
           
            if (user != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, userForLogin.Password, false);
                if (result.Succeeded)
                {
                    
                   
                    var appUser = _mapper.Map<UserForListDTO>(user);
                    if (appUser.IsActive == true)
                    {
                        return Ok(new
                        {
                            token = GenerateJwtToken(user).Result,
                            user = appUser
                        });
                    }

                }
            }
           
            return Unauthorized();
           
        }
        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(ClaimTypes.Name,user.UserName)
            };
            var roles=await _userManager.GetRolesAsync(user);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role,role));
                
            }
            var key = new SymmetricSecurityKey(Encoding.UTF8
            .GetBytes(_config.GetSection("AppSettings:Token").Value));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = System.DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);

        }
    }
}