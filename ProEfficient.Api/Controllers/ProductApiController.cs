﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Api.Controllers
{
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[Controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ProductController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet("GetAllProduct/{organizationId}")]
        public List<MasterProduct> GetAllProductByOrganization([FromRoute][Required] int organizationId)
        {
            return new ProductService(_configuration).DataAccessLayer.GetMasterProductList().
                                        FindAll(i => i.OrganizationId == organizationId && i.IsActive.ToBoolean());
        }
        [HttpGet("GetProduct/{productId}")]
        public MasterProduct GetProductById([FromRoute][Required] int productId)
        {
            return new ProductService(_configuration).DataAccessLayer.GetMasterProduct(new MasterProduct() { ProductId = productId });
        }
    }
}