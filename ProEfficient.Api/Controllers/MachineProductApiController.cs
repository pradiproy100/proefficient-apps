﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;

namespace ProEfficient.Api.Controllers
{
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[Controller]")]
    [ApiController]
    public class MachineProductController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public MachineProductController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost("GetMachineProductMappingData")]
        public IActionResult GetMachineProductMappingData([Required] ProductMachineMachine model)
        {
            if (model == null)
                return BadRequest(nameof(model));
            if (string.IsNullOrEmpty(model.ProductIds))
                return BadRequest("Products");
            if (string.IsNullOrEmpty(model.MachineIds))
                return BadRequest("MachineIds");

            ProductMachineVMSelect productMachineVMSelect = new ProductMachineVMSelect
            {
                MachineIds = model.MachineIds,
                ProductIds = model.ProductIds,
                OrganizationId = model.OrganizationId
            };

            var objMachineProduct = new MachineProductService(_configuration).DataAccessLayer;
            List<ProductMachineVM> productMachineLst = objMachineProduct.GetMachineProductList(productMachineVMSelect).ToList();
            if (productMachineLst.Count > 0)
            {
                productMachineLst.ForEach(x =>
                {
                    x.IsMapped = true;
                });
                return Ok(productMachineLst);
            }
            else
                return NotFound("Record not found.");
        }

        [HttpPost("{userId}/SaveMachineProductMapping")]
        public ActionResult SaveMachineProductMapping([FromRoute] int userId, List<ProductMachineRequestModel> model)
        {
            try
            {
                DataTable UDT = new DataTable();
                UDT.Columns.Add("MachineId", typeof(int));
                UDT.Columns.Add("ProductId", typeof(int));
                UDT.Columns.Add("CycleTime", typeof(decimal));
                UDT.Columns.Add("DemandTypeText", typeof(string));
                for (int i = 0; i < model.Count; i++)
                {
                    if (model[i].IsMapped == true)
                    {
                        DataRow DR = UDT.NewRow();
                        DR["MachineId"] = model[i].MachineID;
                        DR["ProductId"] = model[i].ProductId;
                        DR["CycleTime"] = model[i].ProductCycleTime;
                        DR["DemandTypeText"] = "";
                        UDT.Rows.Add(DR);
                    }
                }
                var result = new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT, userId, "mapping");
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}