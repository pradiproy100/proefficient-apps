﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ProEfficient.Api.Controllers
{
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[Controller]")]
    [ApiController]
    public class MachineController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public MachineController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("GetAllMachineByOrganizationid/{organizationId}")]
        public List<MasterMachine> GetAllMachineByOrganization([FromRoute][Required] int organizationId)
        {
            return new MachineService(_configuration).DataAccessLayer.GetMasterMachineList().
                    FindAll(i => i.OrganizationId == organizationId && i.IsActive.ToBoolean());
        }

        [HttpGet("GetAllMachineByMachineId/{machineId}")]
        public MasterMachine GetAllMachineById([FromRoute][Required] int machineId)
        {                                                                  
            return new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = machineId });
        }
        [HttpGet]
        public List<SelectListItem> MachineFromMappingList(int Organizationid)
        {

            List<SelectListItem> list = new List<SelectListItem>();
            var AllMachine = new List<ProductMachineVM>();

            AllMachine = new MachineProductService(_configuration).DataAccessLayer.GetProductMachineList(Organizationid, System.DateTime.Now);
            var distinct = AllMachine.GroupBy(x => x.MachineID).Select(x => x.FirstOrDefault());


            foreach (var Type in distinct)
            {

                list.Add(new SelectListItem()
                {
                    Text = Type.MachineName,
                    Value = Type.MachineID.ToStringWithNullEmpty()
                });


            }
            return list;

        }
    }
}