﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System.Collections.Generic;
using System.Linq;

namespace ProEfficient.Api.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class RejectionApiController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public RejectionApiController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("getRejectionReasonQuantityList/{id}")]
        public List<RejectionReasonModel> getRejectionReasonQuantityList(int id)
        {

            var ReasonList = new RejectionReasonService(_configuration).DataAccessLayer.GetRejectionReasonList();
            var SavedRejectionList = new RejectionReasonService(_configuration).DataAccessLayer.GetProductionRejectionReasonList(id);
            if (SavedRejectionList == null)
            {
                SavedRejectionList = new List<ProductionRejection>();
            }
            var RejectionReasonModelList = new List<RejectionReasonModel>();
            foreach (var reason in ReasonList)
            {
                var foundsaved = SavedRejectionList.FirstOrDefault(i => i.RejectionReasonId == reason.ReasonId);
                var reasonmodel = new RejectionReasonModel();
                if (foundsaved != null)
                {
                    reasonmodel.ReasonQuantity = foundsaved.RejectionQuantity.ToInteger0();
                }
                else
                {
                    reasonmodel.ReasonQuantity = 0;
                }
                reasonmodel.ReasonId = reason.ReasonId;
                reasonmodel.ReasonName = reason.ReasonName;
                RejectionReasonModelList.Add(reasonmodel);

            }

            return RejectionReasonModelList;
        }

        [HttpGet("dummyval")]

        public List<RejectionReasonModel> dummyval()
        {
            return new List<RejectionReasonModel>()
            {
                new RejectionReasonModel(){
                    ReasonId=1,
                    ReasonName="Broken 1",
                    ReasonQuantity=5,
                    TotalCount=15
                },
                 new RejectionReasonModel(){
                    ReasonId=2,
                    ReasonName="Broken 2",
                    ReasonQuantity=5,
                    TotalCount=15
                },
               new RejectionReasonModel(){
                    ReasonId=3,
                    ReasonName="Broken 3",
                    ReasonQuantity=5,
                    TotalCount=15
                }
            };
        }
    }
}
