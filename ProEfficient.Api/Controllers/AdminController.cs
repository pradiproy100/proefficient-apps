using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ProEfficient.Core.Auth.DTO;
using ProEfficient.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using System;

namespace ProEfficient.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class AdminController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public AdminController( IConfiguration config, IMapper mapper, UserManager<User> userManager)
        {           
            _userManager = userManager;
            _config = config;
            _mapper = mapper;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegistrationDTO userForRegistration)
        {

            var userToCreate = _mapper.Map<User>(userForRegistration);
            var result = await _userManager.CreateAsync(userToCreate, userForRegistration.Password);
            var userToReturn = _mapper.Map<UserForDetailedDTO>(userToCreate);
            if (result.Succeeded)
            {
                // return CreatedAtRoute("GetUser", new { Controller = "users", id = userToCreate.Id }, userToReturn);
                //var appUser = _mapper.Map<UserForListDTO>(userToReturn);

                return Ok(new
                {
                    user = userToReturn
                });

            }
            return BadRequest(result.Errors);

        }
        [HttpPost("updateUser")]
        public async Task<IActionResult> updateUser(UserForRegistrationDTO userForRegistration)
        {
            var userToUpdate=await _userManager.FindByIdAsync(userForRegistration.Id.ToString());
            userToUpdate.UserTypeId = userForRegistration.UserTypeId;
           
            //  var userToUpdate = _mapper.Map<User>(userForRegistration);
            try
            {
                if (!string.IsNullOrWhiteSpace(userForRegistration.Password))
                {
                    userToUpdate.PasswordHash = _userManager.PasswordHasher.HashPassword(userToUpdate, userForRegistration.Password);
                  
                }
                var result = await _userManager.UpdateAsync(userToUpdate);
               // var result1 = await _userManager.UpdateSecurityStampAsync(userToUpdate);
                var userToReturn = _mapper.Map<UserForDetailedDTO>(userToUpdate);
                if (result.Succeeded)
                {
                    return Ok(new
                    {
                        user = userToReturn
                    });

                }
                return BadRequest(result.Errors);
            }catch(Exception ex)
            { throw ex; }
        }
        [HttpGet("deactivateUser")]
        public async Task<IActionResult> deactivateUser(string userId)
        {
            try
            {
                var userToUpdate = await _userManager.FindByIdAsync(userId);
            userToUpdate.IsActive = !userToUpdate.IsActive;


                var result = await _userManager.UpdateAsync(userToUpdate);
                var userToReturn = _mapper.Map<UserForDetailedDTO>(userToUpdate);
                return Ok();
            }
            catch (Exception ex)
            { throw ex; }
        }
        [HttpGet("getUserList")]
        public async Task<List<UserForListDTO>> GetUserList()
        {
            
            var userList = await _userManager.Users.ToListAsync();
            List<UserForListDTO> result=_mapper.Map<List<UserForListDTO>>(userList);
            return result; 

        }
        [HttpGet("getUserById")]
        public async Task<UserForRegistrationDTO> GetUserById(int userId)
        {

            var user = await _userManager.FindByIdAsync(userId.ToString());
            UserForRegistrationDTO result = _mapper.Map<UserForRegistrationDTO>(user);

           return result;

        }

    }
}