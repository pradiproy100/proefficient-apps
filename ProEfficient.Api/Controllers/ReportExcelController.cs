﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Api.Helpers;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportExcelController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ReportExcelController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        //[ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        [Route("SendReport")]
        public bool SendReport(int OrganizationId)
        {
            bool isSent = false;          
            string FileName = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/Report_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            var AllReports = new SendReportService(_configuration).DataAccessLayer.GetReportSentList(OrganizationId);
            string strReportType = string.Empty;
            if (AllReports != null && AllReports.Count > 0)
            {

                foreach (var report in AllReports)
                {
                    isSent = false;
                    TypeofData typOfData= GetMomentRange(report.DateRange);
                    var table = new DataTable();
                    var enumReport = (ReportType)report.ReportTypeId;
                    strReportType = enumReport.ToString();
                    if (strReportType == "OEE")
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime startDate = DateTime.ParseExact(typOfData.StartDate, "yyyy-MM-dd", provider);
                        DateTime endDate = DateTime.ParseExact(typOfData.EndDate, "yyyy-MM-dd", provider);
                        var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                        var resultList = objCategoryDAL.GetOEEReport(startDate, endDate, OrganizationId, report.UserMachineReportID);                       
                        var filteredList = resultList.Where(a => a.DataLevel == "DateLevel").ToList();
                        List<FinalARPRQROEEExcel> lstexcelRes = new List<FinalARPRQROEEExcel>();
                        foreach (FinalARPRQROEEHome m in filteredList)
                        {
                            FinalARPRQROEEExcel em = new FinalARPRQROEEExcel();
                            em.Date = m.RecordDate;
                            em.MachineName = m.MachineName;

                            if (!string.IsNullOrEmpty(m.tShiftA))
                            {
                                string[] ABC = m.tShiftA.Split(",");
                                em.ShiftAAR = ABC[0];
                                em.ShiftAPR = ABC[1];
                                em.ShiftAQR = ABC[2];
                            }
                            em.ShiftAOEE = m.ShiftA;

                            if (!string.IsNullOrEmpty(m.tShiftB))
                            {
                                string[] ABC = m.tShiftB.Split(",");
                                em.ShiftBAR = ABC[0];
                                em.ShiftBPR = ABC[1];
                                em.ShiftBQR = ABC[2];
                            }
                            em.ShiftBOEE = m.ShiftB;

                            if (!string.IsNullOrEmpty(m.tShiftC))
                            {
                                string[] ABC = m.tShiftC.Split(",");
                                em.ShiftCAR = ABC[0];
                                em.ShiftCPR = ABC[1];
                                em.ShiftCQR = ABC[2];
                            }
                            em.ShiftCOEE = m.ShiftC;

                            if (!string.IsNullOrEmpty(m.tOverAll))
                            {
                                string[] ABC = m.tOverAll.Split(",");
                                em.OverAllAR = ABC[0];
                                em.OverAllPR = ABC[1];
                                em.OverAllQR = ABC[2];
                            }
                            em.OverAllOEE = m.OverAll;
                            lstexcelRes.Add(em);

                        }
                        table = Core.Utility.UtilityAll.ToDataTable<FinalARPRQROEEExcel>(lstexcelRes);
                        FileName = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/"+report.ReportName + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
                    }
                    else if (strReportType == "BREAKDOWN")
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime startDate = DateTime.ParseExact(typOfData.StartDate, "yyyy-MM-dd", provider);
                        DateTime endDate = DateTime.ParseExact(typOfData.EndDate, "yyyy-MM-dd", provider);
                        var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                        var resultList = objCategoryDAL.GetLossReportRptDetails(startDate, endDate, OrganizationId, report.UserMachineReportID);                       
                        table = Core.Utility.UtilityAll.ToDataTable<LossReportDetailsModel>(resultList);
                        FileName = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/" + report.ReportName + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
                    }
                    else if (strReportType == "REJECTION")
                    {
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        DateTime startDate = DateTime.ParseExact(typOfData.StartDate, "yyyy-MM-dd", provider);
                        DateTime endDate = DateTime.ParseExact(typOfData.EndDate, "yyyy-MM-dd", provider);
                        var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                        var resultList = objCategoryDAL.GetRejectionReportRpt(startDate, endDate, OrganizationId, report.UserMachineReportID);
                        table = Core.Utility.UtilityAll.ToDataTable<RejectionReasonDetailsModel>(resultList);
                        FileName = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/" + report.ReportName + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
                    }
                   
                    if (table.Rows.Count > 0)
                    {
                        try
                        {
                           
                            ProEfficient.Api.Helpers.ExcelExporter.CreateExcel(table, FileName,strReportType);
                            bool ismailSent = new ProEfficient.Api.Helpers.SendEmail(_configuration).SendMail(report.EmailTo, "ProEfficient Report ("+ report.ReportName + ")", "Hi", FileName);
                            isSent = true;
                        }
                        catch(Exception ex)
                        {
                            isSent = false;
                            throw ex;
                        }

                    }
                    
                }
               
            }
            return isSent;
        }

       
        private TypeofData GetMomentRange(string preDefinedLabel)
        {

            bool isFound = false;
            DateTime now = DateTime.Now;
            DateTime startDate = now;
            DateTime endDate = now;

            if (preDefinedLabel == "Daily")
            {
                startDate = DateTime.Now.AddDays(-1);
                endDate = DateTime.Now.AddDays(-1);
                isFound = true;
            }

            else if (preDefinedLabel == "Weekly")
            {
                DateTime date = DateTime.Now.AddDays(-7);
                while (date.DayOfWeek != DayOfWeek.Monday)
                {
                    date = date.AddDays(-1);
                }

                startDate = date;
                endDate = date.AddDays(7);
                isFound = true;
            }

            else if (preDefinedLabel == "Monthly")
            {
                var month = new DateTime(now.Year, now.Month, 1);
                startDate = month.AddMonths(-1);
                endDate = month.AddDays(-1);
                isFound = true;
            }

            TypeofData typeData = new TypeofData();
            if (isFound == true)
            {

                typeData.StartDate = startDate.ToString("yyyy-MM-dd").Trim();
                typeData.EndDate = endDate.ToString("yyyy-MM-dd").Trim();

            }

            return typeData;
        }
    }
}
