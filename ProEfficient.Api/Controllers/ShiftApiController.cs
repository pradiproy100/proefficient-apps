﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;

namespace ProEfficient.Api.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class ShiftApiController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ShiftApiController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("GetAllShift/{OrganizationId}")]
        public List<Shift> GetAllShift(int OrganizationId, String Token)
        {
            return new ShiftService(_configuration).DataAccessLayer.GetShiftList().FindAll(i => i.OrganizationId == OrganizationId && i.IsActive.ToBoolean());
        }

        [HttpGet("GetShift/{ShiftId}")]
        public Shift GetShift(int ShiftId, String Token)
        {
            return new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = ShiftId });
        }
    }
}