﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProEfficient.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[Controller]")]
    [ApiController]
    public class UserApiController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public UserApiController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("GetAllUsers/{OrganizationId}")]
        public List<OrganizationUser> GetAllUsers(int OrganizationId, String Token)
        {
            return new UserManagementService(_configuration).DataAccessLayer.GetUserList().FindAll(i => i.OrganizationId == OrganizationId && i.IsActive.ToBoolean());
        }
        [HttpGet("GetUser/{UserId}")]
        public OrganizationUser GetUser(int UserId, String Token)
        {
            return new UserManagementService(_configuration).DataAccessLayer.GetUser(new OrganizationUser() { UserId = UserId });
        }
        [HttpGet("AuthenticateUser/{UserId}/{PassWord}/{Token}")]
        public OrganizationUser AuthenticateUser(String UserId, String PassWord, String Token)
        {
            if (new UserManagementService(_configuration).DataAccessLayer.GetUserList().FirstOrDefault(i => i.EmailId == UserId && i.Password == PassWord && i.IsActive.ToBoolean()) != null)
            {
                return new UserManagementService(_configuration).DataAccessLayer.GetUserList().FirstOrDefault(i => i.EmailId == UserId && i.Password == PassWord && i.IsActive.ToBoolean());
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        public Boolean SaveProdection(jsonProductionEntry ProductionEntry)
        {
            return true;
        }
        [HttpGet]
        public List<StopageReason> GetAllStoppageReason(String Token)
        {
            return new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList().FindAll(i => i.IsActive.ToBoolean());
        }

    }
}