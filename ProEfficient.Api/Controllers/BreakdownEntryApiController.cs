﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;

namespace ProEfficient.Api.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class BreakdownEntryApiController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public BreakdownEntryApiController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        /// <summary>
        /// Get Breakdowns
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetBreakdowns")]
        public IActionResult GetBreakdowns(BreakdownSearchVM model)
        {
            try
            {
                var objBreakdown = new BreakDownService(_configuration).DataAccessLayer;
                List<BreakdownEntryEditVM> objBreakdownlst = objBreakdown.GetBreakDownEntryEdit(model.ProductionDate, model.ShiftId).ToList();
                if (objBreakdownlst.Count > 0)
                {
                    List<BreakdownEntryEditVM> objBreakdownlstFilter = new List<BreakdownEntryEditVM>();
                    decimal.TryParse(_configuration["TotalStoppageTime"], out decimal totalDuration);
                    if (totalDuration > 0)
                    {
                        foreach (var item in objBreakdownlst)
                        {
                            string[] totalStoppage = item.TotalStoppageTime.Split(':');
                            if (totalStoppage.Length == 3 && Convert.ToDecimal(totalStoppage[1]) >= totalDuration)
                            {
                                objBreakdownlstFilter.Add(item);
                            }
                        }
                    }
                    return Ok(objBreakdownlstFilter);
                }
                else
                    return NotFound("No Record Found");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Edit Breakdown Entries
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("EditBreakdownEntries")]
        public ActionResult EditBreakdownEntries(List<BreakdownEntryEditVM> model)
        {
            try
            {
                DataTable UDT = new DataTable();
                UDT.Columns.Add("BreakdownId", typeof(int));
                UDT.Columns.Add("ReasonId", typeof(int));
                UDT.Columns.Add("SubReasonId", typeof(int));
                UDT.Columns.Add("Remarks", typeof(string));
                for (int i = 0; i < model.Count; i++)
                {
                    DataRow DR = UDT.NewRow();
                    DR["BreakdownId"] = model[i].BreakdownId;
                    DR["ReasonId"] = model[i].ReasonId;
                    DR["SubReasonId"] = model[i].SubReasonId;
                    DR["Remarks"] = model[i].Remarks;
                    UDT.Rows.Add(DR);
                }
                bool isSaved = new BreakDownService(_configuration).DataAccessLayer.EditBreakDownEntry(UDT);
                return Ok(isSaved);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Get All Active Reasons By OrganizationId
        /// </summary>
        /// <returns></returns>
        [Route("GetActiveReasons/{organizationId}")]
        [HttpGet]
        public IActionResult GetActiveReasons([FromRoute][Required] int organizationId)
        {
            try
            {
                return Ok(UserUtility.ActiveReasonsByOrganizationId(organizationId));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        /// <summary>
        /// Get All Sub Reason By reasonId
        /// </summary>
        /// <returns></returns>
        [Route("GetStoppageSubReason/{reasonId}")]
        [HttpGet]
        public IActionResult GetStoppageSubReason([Required][FromRoute] int reasonId = 0)
        {
            if (reasonId == 0)
                return StatusCode(StatusCodes.Status400BadRequest);
            try
            {
                var stopageSubReasons = new StopageSubReasonService(_configuration).DataAccessLayer.GetStopageSubReasonByReasonId(
                                  new StopageSubReason
                                  {
                                      ReasonId = reasonId.ToInteger0()
                                  });
                if (stopageSubReasons != null && stopageSubReasons.Any())
                    return Ok(stopageSubReasons);
                else
                    return NotFound("No record found");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        ///// <summary>
        ///// Get Substoppage Reasons
        ///// </summary>
        ///// <returns></returns>
        //[Route("GetSubstoppagereasonlst")]
        //[HttpGet]
        //public ActionResult GetSubstoppagereasonlst([FromRoute][Required] int reasonId)
        //{
        //    try
        //    {
        //        var subReasons = UserUtility.GetSubstoppagereasonlst().Where(x => x.ReasonID == reasonId);
        //        if (subReasons != null && subReasons.Any())
        //            return Ok(subReasons);
        //        else
        //            return NotFound("No record found");
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        //    }
        //}
    }
}