using System.Linq;
using AutoMapper;
using ProEfficient.Core.Auth.DTO;
using ProEfficient.Api.Models;

namespace ProEfficient.Api.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
           
            CreateMap<UserForRegistrationDTO,User>();
            CreateMap<User, UserForDetailedDTO>();
            CreateMap<User, UserForRegistrationDTO>();
            CreateMap<User, UserForListDTO>();

        }
    }
}