﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace ProEfficient.Api.Helpers
{
    public class SendEmail
    {

        private  IConfiguration Configuration;

        public SendEmail(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public static string script = @"< h1> Dear Sir < /h1>< p>Please Find attached the Production Report.< /p>";
        public static string sciptAfter = "<html>" + script + "</html>";

        public String FromEmailsmtpClient => Configuration.GetValue<string>("FromEmailsmtpClient");

        public String FromEmailId => Configuration.GetValue<string>("FromEmailId");

        public String FromEmailPassword => Configuration.GetValue<string>("FromEmailPassword");

        public int FromEmailsmtpClientPort => Configuration.GetValue<int>("FromEmailsmtpClientPort");
        public String BccEmail => Configuration.GetValue<string>("BccEmail");
        public String ReplaceToToBCC => Configuration.GetValue<string>("ReplaceToToBCC");
        


        public Boolean SendMail(String ToEmail, String Subject, String EmailMessage, String AttachmentPath)
        {
            try
            {
                var BackUppath = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/BackUpExcelPath/" + DateTime.Now.ToString("yyyyMMddHHmmssFFF") +".xlsx";
               // var BackUppath = (Email.BackupExcelPath + DateTime.Now.ToString("yyyyMMddHHmmssFFF")) + ".xlsx";


                var AttachmentFolder = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/";
                String attachmentPath = String.Empty;
                foreach (var fileInfo in new DirectoryInfo(AttachmentFolder).GetFiles())
                {
                    attachmentPath = fileInfo.FullName;
                   
                    break;
                }
                if (attachmentPath != String.Empty)
                {
                    File.Move(attachmentPath, BackUppath);

                    MailMessage Msg = new MailMessage();
                    // Sender e-mail address.
                    Msg.From = new MailAddress(FromEmailId);
                    // Recipient e-mail address.
                    if (ReplaceToToBCC == "YES")
                    {
                        Msg.To.Add(BccEmail);
                    }
                    else
                    {
                        Msg.To.Add(ToEmail);
                    }
                    if (!string.IsNullOrEmpty(BccEmail))
                    {
                        Msg.Bcc.Add(BccEmail);
                    }
                    Msg.Subject = Subject;
                    Msg.IsBodyHtml = true;
                    //Msg.Body = EmailMessage;
                    Msg.Body = "Hi, <br /> Please find attachement for <b>"+Subject+ "</b><br /><br />For any further information , Please contact to Administrator.";
                    // your remote SMTP server IP.
                    SmtpClient smtp = new SmtpClient();
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;                    
                    smtp.Host = FromEmailsmtpClient;
                    smtp.Port = FromEmailsmtpClientPort;
                    smtp.Credentials = new System.Net.NetworkCredential(FromEmailId, FromEmailPassword);
                    smtp.EnableSsl = true;
                    System.Net.Mail.Attachment attachment;
                    Thread.Sleep(1000);
                    attachment = new System.Net.Mail.Attachment(BackUppath);
                    Msg.Attachments.Add(attachment);
                    smtp.SendAsync(Msg, null);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
