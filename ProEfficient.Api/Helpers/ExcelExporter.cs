﻿using System;
using System.Data;
using System.Reflection;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Data.SqlClient;
using System.Globalization;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Drawing.Chart.Style;
using System.Linq;
using System.Drawing;

namespace ProEfficient.Api.Helpers
{
    public enum ReportType
    {
        OEE=1,
        BREAKDOWN=2,
        REJECTION=3
    }
    public class ExcelExporter
    {
        private static string EXPORT_FILE = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/Exported.xlsx";
        private static string ASSET_FILE = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/file_format.xlsx";

        //Export Entry point function
        public static void Export( DataSet ds,string FileName, string startdate,string enddate)
        {
            extractAsset();
            string dateStr = startdate + " TO " + enddate;
       // string FileName = AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/" + "Report_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            FileInfo excelFile = new FileInfo(EXPORT_FILE);
            if (ds.Tables.Count > 0)
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        System.Data.DataTable table = ds.Tables[i];
                        if (table.TableName.Contains("OEE"))
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[table.TableName];
                            loadData(worksheet, table, dateStr);

                        }
                        else if (table.TableName.Contains("Breakdown"))
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[table.TableName];
                            loadDataBreak(worksheet, table, dateStr);
                        }
                        else if (table.TableName.Contains("Value_Added_Time"))
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets[table.TableName];
                            loadDataWaterfall(worksheet, table, dateStr);
                        }
                        //ExcelWorksheet worksheet = package.Workbook.Worksheets[table.TableName];
                        //loadData(worksheet, table, dateStr);
                        //ExcelWorksheet worksheetMain = package.Workbook.Worksheets[table.TableName];
                        //worksheetMain.Name = "Report_Auto_" + DateTime.Now.ToString("ddMMyyyy");
                        //worksheet.Name = Library.SheetNameFromConfig;

                    }
                    package.Save();
                }
                System.IO.DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "ReportFiles/SampleAttachment/");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                File.Copy(EXPORT_FILE, FileName);

                using (FileStream fs = File.Open(FileName, FileMode.Open, FileAccess.Write, FileShare.None))
                {
                    fs.FlushAsync();
                }
                //try
                //{
                //    var xl = new Microsoft.Office.Interop.Excel.Application();
                //    var wb = xl.Workbooks.Open(FileName);
                //    wb.Save();
                //    wb.Close();
                //    xl.Quit();
                //    Marshal.ReleaseComObject(wb);
                //    Marshal.ReleaseComObject(xl);
                //}
                //catch (Exception ex)
                //{
                //    Utility.WriteLog("Error in Office Interop Excel", true);
                //}
            }
          
           
        }

        private static void loadData(ExcelWorksheet worksheet, System.Data.DataTable table,string datestr)
        {
            worksheet.Cells[1, 1].Value = "OEE Report From "+datestr;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                try
                {

                    for (int j = 0; j < table.Columns.Count; j++)
                    {

                        string strVal = table.Rows[i].ItemArray[j].ToString();
                        if (j > 0)
                        {
                            decimal val = validateValuedecimal(strVal);
                            worksheet.Cells[i + 4, j + 1].Value = val;
                        }                       
                        else { worksheet.Cells[i + 4, j + 1].Value = strVal; }
                        if (i == table.Rows.Count - 1)
                        {
                            worksheet.Cells[i + 4, j+1].Style.Font.Bold = true;
                            //worksheet.Cells[i + 4, j + 1].Style.Fill.PatternType = ExcelFillStyle.DarkHorizontal;
                            //worksheet.Cells[i + 4, j + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            //Add a bar chart
            var chart = worksheet.Drawings.AddBarChart("bar3dChart", eBarChartType.ColumnClustered3D);
           
            var ShiftAOEESeries = chart.Series.Add("E4:E20", "A4:A20");
            ShiftAOEESeries.Header = "Shift A OEE";
            var ShiftBOEESeries = chart.Series.Add("I4:I20", "A4:A20");
            ShiftBOEESeries.Header = "Shift B OEE";
            var ShiftCOEESeries = chart.Series.Add("M4:M20", "A4:A20");
            ShiftCOEESeries.Header = "Shift C OEE";
            var OEESeries = chart.Series.Add("Q4:Q20", "A4:A20");
            OEESeries.Header = "Over All OEE";
            chart.SetPosition(1, 0, 18, 0);
            chart.SetSize(1200, 400);
            chart.Title.Text = "OEE";
            //Set style 9 and Colorful Palette 3. 
            //chart.StyleManager.SetChartStyle(ePresetChartStyle.Column3dChartStyle9, ePresetChartColors.ColorfulPalette1);
            chart.StyleManager.SetChartStyle(ePresetChartStyle.Column3dChartStyle9, ePresetChartColors.ColorfulPalette3);

            //ExcelChart chart = worksheet.Drawings.AddChart("chart", eChartType.ColumnClustered); //To add chart to added sheet of type column clustered chart
            //chart.XAxis.Title.Text = "Machines"; //give label to x-axis of chart  
            //chart.XAxis.Title.Font.Size = 10;
            //chart.YAxis.Title.Text = "OEE"; //give label to Y-axis of chart  
            //chart.YAxis.Title.Font.Size = 10;
            //chart.SetSize(1500, 400);
            //chart.SetPosition(2, 0, 19, 0);

            //var ShiftAOEESeries = chart.Series.Add("E4:E20", "A4:A20");
            //ShiftAOEESeries.Header = "Shift A OEE";
            //var ShiftBOEESeries = chart.Series.Add("I4:I20", "A4:A20");
            //ShiftBOEESeries.Header = "Shift B OEE";
            //var ShiftCOEESeries = chart.Series.Add("M4:M20", "A4:A20");
            //ShiftCOEESeries.Header = "Shift C OEE";
            //var OEESeries = chart.Series.Add("Q4:Q20", "A4:A20");
            //OEESeries.Header = "Over All OEE";

            // worksheet.Calculate();
        }
        private static void loadDataBreak(ExcelWorksheet worksheet, System.Data.DataTable table, string datestr)
        {
            worksheet.Cells[1, 1].Value = "Break Down Report From " + datestr;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                try
                {

                    for (int j = 1; j < table.Columns.Count; j++)
                    {

                        string strVal = table.Rows[i].ItemArray[j].ToString();
                        
                       if (j == 2)
                        {
                            string val = validateValuedecimaltohhmm(strVal);
                            worksheet.Cells[i + 3, j].Value = val;
                            decimal valraw = validateValuedecimal(strVal);
                            worksheet.Cells[i + 3, j+4].Value = valraw/3600;
                        }
                        else if (j ==3)
                        {
                            decimal val = validateValuedecimal(strVal);
                            worksheet.Cells[i + 3, j ].Value = val;
                        }
                        else { worksheet.Cells[i + 3, j].Value = strVal; }
                        //if (i == table.Rows.Count - 1)
                        //{
                        //    worksheet.Cells[i + 3, j-7].Style.Font.Bold = true;
                        //    //worksheet.Cells[i + 4, j + 1].Style.Fill.PatternType = ExcelFillStyle.DarkHorizontal;
                        //    //worksheet.Cells[i + 4, j + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        //}
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            worksheet.Calculate();
            //Add a line chart with high/low Bars
            var chart = worksheet.Drawings.AddLineChart("LineChartWithDroplines", eLineChartType.Line3D);
            var serie1 = chart.Series.Add(worksheet.Cells[4, 6, 12, 6], worksheet.Cells[4, 1, 12, 1]);
            serie1.Header = "Duration";
            var serie2 = chart.Series.Add(worksheet.Cells[4, 3, 12, 3], worksheet.Cells[4, 1, 12, 1]);
            serie2.Header = "Freequency";

            chart.SetPosition(1, 0, 4, 0);
            chart.SetSize(800, 400);
            chart.Title.Text = "Breakdown Duration/Freequency";
            chart.AddHighLowLines();
            chart.StyleManager.SetChartStyle(ePresetChartStyle.Line3dChartStyle4);
            //Set the style using the Excel ChartStyle number. The chart style must exist in the ExcelChartStyleManager.StyleLibrary[]. 
            //Styles can be added and removed from this library. By default it is loaded with the styles for EPPlus supported chart types.
            // chart.StyleManager.SetChartStyle(237);
            // range.AutoFitColumns(0);
            //Add a bubble chart on the data with one serie per row. 
            //var wsChart = worksheet.Drawings.AddChart("Bubble Chart", eChartType.Bubble);
            //var chart = ((ExcelBubbleChart)wsChart);
            //for (int row = 4; row <= 12; row++)
            //{
            //    var serie = chart.Series.Add(worksheet.Cells[row, 3], worksheet.Cells[row, 4]);
            //    serie.HeaderAddress = worksheet.Cells[row, 1];
            //}
            //chart.DataLabel.Position = eLabelPosition.Center;
            ////chart.DataLabel.ShowSeriesName = true;
            ////chart.DataLabel.ShowBubbleSize = true;
            //chart.DataLabel.ShowPercent = true;
            //chart.Title.Text = "Breakdown";
            //chart.XAxis.Title.Text = "Duration";
            //chart.XAxis.Title.Font.Size = 12;
            //chart.XAxis.MajorGridlines.Width = 1;
            //chart.YAxis.Title.Text = "Freequency";
            //chart.YAxis.Title.Font.Size = 12;
            //chart.Legend.Position = eLegendPosition.Right;

            //chart.StyleManager.SetChartStyle(ePresetChartStyleMultiSeries.BubbleChartStyle10);
            //chart.SetSize(1000, 400);
            //chart.SetPosition(1, 0, 4, 0);

        }
        private static void loadDataWaterfall(ExcelWorksheet worksheet, System.Data.DataTable table, string datestr)
        {
            worksheet.Cells[1, 1].Value = "Value Added Time From " + datestr;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                try
                {

                    for (int j = 0; j < table.Columns.Count; j++)
                    {

                        string strVal = table.Rows[i].ItemArray[j].ToString();

                        if (j == 1)
                        {
                            //string val = validateValuedecimaltohhmm(strVal);
                            //worksheet.Cells[i + 3, j+1].Value = val;
                            decimal valraw = validateValuedecimal(strVal);
                            worksheet.Cells[i + 3, j + 1].Value = valraw ;
                        }
                        
                        else { worksheet.Cells[i + 3, j+1].Value = strVal; }
                        //if (i == table.Rows.Count - 1)
                        //{
                        //    worksheet.Cells[i + 3, j-7].Style.Font.Bold = true;
                        //    //worksheet.Cells[i + 4, j + 1].Style.Fill.PatternType = ExcelFillStyle.DarkHorizontal;
                        //    //worksheet.Cells[i + 4, j + 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);

                        //}
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            worksheet.Cells[table.Rows.Count+3, 1].Value = "Value Added Time";           
            decimal ValueAddedTime = table.AsEnumerable().Sum(row =>decimal.Parse(row.Field<string>(table.Columns[1].ColumnName)));
            worksheet.Cells[table.Rows.Count+3, 2].Value = ValueAddedTime;
            worksheet.Calculate();
            //Add a line chart with high/low Bars
            
            var waterfallChart = worksheet.Drawings.AddWaterfallChart("Waterfall1");
            waterfallChart.Title.Text = "Value Added Time";
            waterfallChart.SetPosition(1, 0, 4, 0);
            waterfallChart.SetSize(800, 400);
            var wfSerie = waterfallChart.Series.Add(worksheet.Cells[3, 2, 10, 2], worksheet.Cells[3, 1, 10, 1]);
            var dp = wfSerie.DataPoints.Add(0);
            dp.SubTotal = true;
            dp = wfSerie.DataPoints.Add(7);
            dp.SubTotal = true;

        }
        public static decimal validateValuedecimal(string strVal)
        {
            decimal decVal;
            double dVal;
            int iVal;
            if (decimal.TryParse(strVal, out decVal))
            {
                return decVal;
            }
            else if (double.TryParse(strVal, out dVal))
            {
                return Convert.ToDecimal(dVal);
            }
            else if (Int32.TryParse(strVal, out iVal))
            {
                return Convert.ToDecimal(iVal);
            }
            else
            {
                return 0;
            }

        }
        public static string validateValuedecimaltohhmm(string strVal)
        {
            double val;
           
            decimal decVal;
            double dVal;
            int iVal;
            if (decimal.TryParse(strVal, out decVal))
            {
                val =Convert.ToDouble(decVal);
            }
            else if (double.TryParse(strVal, out dVal))
            {
                val= Convert.ToDouble(dVal);
            }
            else if (Int32.TryParse(strVal, out iVal))
            {
                val= Convert.ToDouble(iVal);
            }
            else
            {
                val= 0;
            }
            //return val;
            TimeSpan timeSpan = TimeSpan.FromMinutes(val);

           return timeSpan.ToString(@"hh\:mm");

        }
     
        private static void extractAsset()
        {
            try
            {
                
                using (var resource = new FileStream(ASSET_FILE, FileMode.Open, FileAccess.Read))
                using (var file = new FileStream(EXPORT_FILE, FileMode.Create, FileAccess.Write))
                    resource.CopyTo(file);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void CreateExcel(DataTable table, string FileName,string ReportType)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                if (table.Rows.Count > 0)
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Report");
                    if (ReportType == "OEE")
                    {
                        loadData(worksheet, table);
                    }
                    else if (ReportType == "BREAKDOWN")
                    {
                        loadBreakData(worksheet, table);
                    }
                    else if (ReportType == "REJECTION")
                    { loadRejData(worksheet, table); }
                }

                //Write the file to the disk
                FileInfo fi = new FileInfo(FileName);
                package.SaveAs(fi);
            }
        }
        // Functions for write to Excel sheet/s
        private static void loadData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int productionDateIndex = 1;
            int machineNameIndex = 2;
            int ShiftAIndex = 3;
            int ShiftBIndex = 7;
            int ShiftCIndex = 11;
            int overAllIndex = 15;

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, productionDateIndex].Value = "Date";
            ws.Cells[rowIndex, machineNameIndex].Value = "Line";
            ws.Cells[rowIndex, ShiftAIndex].Value = "Shift A";
            ws.Cells[rowIndex, ShiftBIndex].Value = "Shift B";
            ws.Cells[rowIndex, ShiftCIndex].Value = "Shift C";
            ws.Cells[rowIndex, overAllIndex].Value = "Over All";
            ws.Cells["C1:F1"].Merge = true;
            ws.Cells["G1:J1"].Merge = true;
            ws.Cells["K1:N1"].Merge = true;
            ws.Cells["O1:R1"].Merge = true;
            ws.Cells["A1:R1"].Style.Font.Size = 12;
            ws.Cells["A1:R1"].Style.Font.Bold = true;


            ws.Column(2).Width = 35;
            using (ExcelRange Rng = ws.Cells[1, 1, 1, 18])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
            using (ExcelRange Rng = ws.Cells[2, 1, 2, 18])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
            // sub headers
            rowIndex = 2;
            string subHeaderVal = "AR";
            for (int subIncrement = 0; subIncrement < 4; subIncrement++)
            {
                if (subIncrement == 1) { subHeaderVal = "PR"; }
                else if (subIncrement == 2) { subHeaderVal = "QR"; }
                else if (subIncrement == 3) { subHeaderVal = "OEE"; }
                ws.Cells[rowIndex, ShiftAIndex + subIncrement].Value = subHeaderVal;
                ws.Cells[rowIndex, ShiftBIndex + subIncrement].Value = subHeaderVal;
                ws.Cells[rowIndex, ShiftCIndex + subIncrement].Value = subHeaderVal;
                ws.Cells[rowIndex, overAllIndex + subIncrement].Value = subHeaderVal;
            }
            ws.Cells["A3"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        private static void loadBreakData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Reason";
            ws.Cells[rowIndex, 4].Value = "Start";
            ws.Cells[rowIndex, 5].Value = "End";
            ws.Cells[rowIndex, 6].Value = "Duration";


            ws.Column(2).Width = 35;
            ws.Column(3).Width = 35;
            using (ExcelRange Rng = ws.Cells[1, 1, 1, 6])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        private static void loadRejData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Reason";
            ws.Cells[rowIndex, 4].Value = "Count";

            using (ExcelRange Rng = ws.Cells[1, 1, 1, 4])
            {

                Rng.Style.Font.Bold = true;

                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Column(2).Width = 35;
            ws.Column(3).Width = 35;

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }

    }
}
