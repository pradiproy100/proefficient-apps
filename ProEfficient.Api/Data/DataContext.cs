using ProEfficient.Api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ProEfficient.Api.Data
{
    public class DataContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>, UserRole
        ,IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public DataContext(DbContextOptions<DataContext> options):base(options){}
       

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.Entity<User>(entity =>
            //{
            //    entity.ToTable(name: "User");
            //    entity.Property(e => e.Id).HasColumnName("UserId");
            //    entity.Property(e => e.Email).HasColumnName("EmailId");

            //});
            builder.Entity<Role>().ToTable("User");
            builder.Entity<Role>().ToTable("Role");

            builder.Entity<UserRole>().ToTable("UserRole");
            builder.Entity<UserRole>(userRole => 
            {
                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();
                 userRole.HasOne(ur => ur.user)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

           

        }
    }
}