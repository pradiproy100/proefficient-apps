using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ProEfficient.Api.Models
{
    public class User : IdentityUser<int>
    {
        public int OrganizationId { get; set; }
        public int UserTypeId { get; set; }
        public bool IsActive { get; set; }   
        public int AddBy { get; set; }    
        public int EdittedBy { get; set; }
        


        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}