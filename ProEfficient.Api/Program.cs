using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProEfficient.Api.Data;
using ProEfficient.Api.Models;
using System;
using System.IO;

namespace ProEfficient.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IServiceProvider services = null;
            try
            {
                var host = CreateHostBuilder(args).Build();
                using (var scope = host.Services.CreateScope())
                {
                    services = scope.ServiceProvider;

                    var context = services.GetRequiredService<DataContext>();
                    var userManager = services.GetRequiredService<UserManager<User>>();
                    var roleManager = services.GetRequiredService<RoleManager<Role>>();
                    context.Database.Migrate();
                }
                host.Run();
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Proefficient\api\Logs\Error.log", ex.ToString() + Environment.NewLine);
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                // .UseMetrics()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

    }
}
