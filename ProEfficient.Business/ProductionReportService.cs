﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class ProductionReportService
    {
        private readonly IConfiguration _configuration;

        public ProductionReportService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ProductionReportDAL DataAccessLayer => new ProductionReportDAL(_configuration);
    }
}
