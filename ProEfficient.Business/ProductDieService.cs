﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Business
{
    public class ProductDieService
    {
        private readonly IConfiguration _configuration;

        public ProductDieService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ProductDieDAL DataAccessLayer => new ProductDieDAL(_configuration);
    }
}
