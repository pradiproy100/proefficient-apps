﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class ProductionReasonMappingService
    {
        private readonly IConfiguration _configuration;

        public ProductionReasonMappingService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ProductReasonMappingDAL DataAccessLayer => new ProductReasonMappingDAL(_configuration);
    }
}
