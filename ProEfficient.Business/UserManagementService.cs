﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class UserManagementService
    {
        private readonly IConfiguration _configuration;

        public UserManagementService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public UserManagementDAL DataAccessLayer => new UserManagementDAL(_configuration);
    }
}
