﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class RejectionReasonService
    {
        private readonly IConfiguration _configuration;

        public RejectionReasonService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public RejectionReasonDAL DataAccessLayer => new RejectionReasonDAL(_configuration);
    }
}
