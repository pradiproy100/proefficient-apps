﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Business
{
    public class WeekMonthReportService
    {
        private readonly IConfiguration _configuration;

        public WeekMonthReportService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public WeekMonthReportDAL DataAccessLayer => new WeekMonthReportDAL(_configuration);
    }
}
