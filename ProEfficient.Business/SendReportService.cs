﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Business
{
    public class SendReportService
    {
        private readonly IConfiguration _configuration;

        public SendReportService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SendReportDAL DataAccessLayer => new SendReportDAL(_configuration);
    }
}
