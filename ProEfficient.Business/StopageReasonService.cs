﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class StopageReasonService
    {
        private readonly IConfiguration _configuration;

        public StopageReasonService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public StopageReasonDAL DataAccessLayer => new StopageReasonDAL(_configuration);
    }
}
