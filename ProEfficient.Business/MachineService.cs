﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class MachineService
    {
        private readonly IConfiguration _configuration;

        public MachineService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public MachineDAL DataAccessLayer => new MachineDAL(_configuration);
    }
}
