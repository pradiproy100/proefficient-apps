﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class MachineProductService
    {
        private readonly IConfiguration _configuration;

        public MachineProductService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public MachineProductDAL DataAccessLayer => new MachineProductDAL(_configuration);
    }
}
