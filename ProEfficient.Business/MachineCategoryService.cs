﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class MachineCategoryService
    {
        private readonly IConfiguration _configuration;

        public MachineCategoryService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public MachineCategoryDAL DataAccessLayer => new MachineCategoryDAL(_configuration);
    }
}
