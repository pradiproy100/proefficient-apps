﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class ProductionService
    {
        private readonly IConfiguration _configuration;

        public ProductionService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ProductionDAL DataAccessLayer => new ProductionDAL(_configuration);
    }
}
