﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class SupervisorSchedulerService
    {
        private readonly IConfiguration _configuration;

        public SupervisorSchedulerService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SupervisorMachineDAL DataAccessLayer => new SupervisorMachineDAL(_configuration);
    }
}
