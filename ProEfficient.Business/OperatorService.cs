﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class OperatorService
    {
        private readonly IConfiguration _configuration;

        public OperatorService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public OperatorDAL DataAccessLayer => new OperatorDAL(_configuration);
    }
}
