﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Business
{
    public class ProductMouldService
    {
        private readonly IConfiguration _configuration;

        public ProductMouldService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ProductMouldDAL DataAccessLayer => new ProductMouldDAL(_configuration);
    }
}
