﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class OrganizationService
    {
        private readonly IConfiguration _configuration;

        public OrganizationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public OrganizationDAL DataAccessLayer => new OrganizationDAL(_configuration);
    }
}
