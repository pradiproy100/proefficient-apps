﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class ImportDataService
    {
        private readonly IConfiguration _configuration;

        public ImportDataService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ImportDAL DataAccessLayer => new ImportDAL(_configuration);
    }
}
