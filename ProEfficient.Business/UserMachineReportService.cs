﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class UserMachineReportService
    {
        private readonly IConfiguration _configuration;

        public UserMachineReportService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public UserMachineReportDAL DataAccessLayer => new UserMachineReportDAL(_configuration);
    }
}
