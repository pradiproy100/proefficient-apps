﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class SubStoopageResonService
    {

        private readonly IConfiguration _configuration;

        public SubStoopageResonService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SubStoopageResonDAL DataAccessLayer => new SubStoopageResonDAL(_configuration);
    }
}
