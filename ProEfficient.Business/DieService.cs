﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class DieService
    {
        private readonly IConfiguration _configuration;

        public DieService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public DieDAL DataAccessLayer => new DieDAL(_configuration);
    }
}
