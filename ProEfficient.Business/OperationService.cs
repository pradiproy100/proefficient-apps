﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class OperationService
    {
        private readonly IConfiguration _configuration;

        public OperationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public OperationDAL DataAccessLayer => new OperationDAL(_configuration);
    }
}
