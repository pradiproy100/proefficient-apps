﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class BreakInfoService
    {
        private readonly IConfiguration _configuration;

        public BreakInfoService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public BreakInfoDAL DataAccessLayer => new BreakInfoDAL(_configuration);
    }
}
