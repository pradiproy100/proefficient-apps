﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class LogService
    {
        private readonly IConfiguration _configuration;

        public LogService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public LogDAL DataAccessLayer => new LogDAL(_configuration);
    }
}
