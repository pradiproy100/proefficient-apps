﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class BreakDownService
    {
        private readonly IConfiguration _configuration;

        public BreakDownService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public BreakDownEntryDAL DataAccessLayer => new BreakDownEntryDAL(_configuration);
    }
}
