﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class ShiftService
    {
        private readonly IConfiguration _configuration;

        public ShiftService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ShiftDAL DataAccessLayer => new ShiftDAL(_configuration);
    }
}
