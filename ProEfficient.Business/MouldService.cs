﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class MouldService
    {
        private readonly IConfiguration _configuration;

        public MouldService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public MouldDAL DataAccessLayer => new MouldDAL(_configuration);
    }
}
