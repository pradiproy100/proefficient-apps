﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Dal.DAL;

namespace ProEfficient.Business
{
    public class StopageSubReasonService
    {
        private readonly IConfiguration _configuration;

        public StopageSubReasonService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public StopageSubReasonDAL DataAccessLayer => new StopageSubReasonDAL(_configuration);
    }
}
