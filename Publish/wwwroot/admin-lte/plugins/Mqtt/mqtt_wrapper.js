var connections = new Connections();

function Connections() {
	this.connections = new Map();
}

Connections.prototype.getConnection = function(hostname, port) {
	if (this.connections.get(hostname) == undefined) {
		var wrapper = new MQTTWrapper(hostname, port);
		this.connections.set(hostname, wrapper);
		console.log("Requested connection " + hostname + " refcount " + wrapper.refcount);
		return wrapper.mqttClient;
	} 
	else
	{
		var wrapper = this.connections.get(hostname);
		wrapper.refcount++;
		console.log("Requested connection " + hostname + " refcount " + wrapper.refcount);
		return wrapper.mqttClient;
	}
}

Connections.prototype.releaseConnection = function(hostname) {
	var wrapper = this.connections.get(hostname);
	if(wrapper != undefined) {
		wrapper.refcount--;
		console.log("Released connection " + hostname + " refcount " + wrapper.refcount);
		if(wrapper.refcount < 1)
		{
            if(wrapper.mqttClient.isConnected() == true)
                wrapper.mqttClient.disconnect();
			this.connections.delete(hostname);
		}
	}
	
}

function MQTTWrapper(hostname, port) {
	this.hostname = hostname;
	this.port = port;
	this.refcount = 1;
	this.clientId = "";
	this.mqttClient = new Paho.MQTT.Client(this.hostname, Number(this.port), this.clientId);
}


// to use...
// Get a connection from the pool
// connections.getConnection(hostname, port);
//
// Release use of and close if last user.
// connections.releaseConnection(hostname);




