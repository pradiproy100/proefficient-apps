﻿/// <reference path="../jquery-ui-1.12.1.js" />
/// <reference path="../jquery-1.12.4.js" />
//$(document).ready(function () {
//    var activeIndexval = $("#ActiveIndexValue").val();
//    $(".accordion").accordion("option", "active", activeIndexval);
//})

$(document).ready(function () {
    $(".Hidden").hide();
   // $("select").chosen();    
    $('.cTimePick').timepicker({
        'timeFormat': 'H:i',
        'step':5,
        'disableTextInput':true});
    $('.TimePick').timepicker({ 'timeFormat': 'H.i' });
    $(".accordion").accordion({
        collapsible: true
    });//ActiveIndexValue
    var activeIndexval = $("#ActiveIndexValue").val();    
    $(".ui_tabs").tabs();    
    $(".ui_tabs").tabs("option", "active", activeIndexval);    
    $(".accordion").accordion("option", "active", activeIndexval);
    $(".Date-Picker").datepicker({
        dateFormat: 'yyyy/mm/dd'
    });

    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy'
    });
    
    
    
    $(".newdatepicker").datepicker({
        dateFormat: 'yy-mm-dd',
        pickTime: false
    });
    //$("[type*=datetime]").datepicker();
    $(".spinner").spinner();
    $(".DeleteAnc").click(function () {      

        var Href = $(".DeleteAnc").attr("href");
        if (confirm("Are you sure?")) {
            window.location.href = Href;
        }
        else {
            return false;
        }
    })


    //$(".Daterangepicker").dateRangePicker();



    $(".timepicker").datetimepicker({
        format: 'yyyy/mm/dd hh:ii', autoclose: true,
        todayBtn: true, minuteStep: 2
    });

    $("#ProductionDate").datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function (d, i) {
            $('.lblProductionDatePE').val(d);
            $('.lblProductionDateBD').val(d);
            $("#hdBreakdownProdDate").val(d);
            if (d !== i.lastVal) {
                
                var prodDate = d;
               
                window.sessionStorage.setItem("ProdDate", prodDate);
                window.location.href = "/Production/Index/?appId="+"proddate="+ prodDate;
            }
        }

    });

    $(".Submit_confirm").click(function () {
        if (confirm("are you sure...?") === true) {
            return true;
        }
        else {
            return false;
        }

    })

    $(".delete-confirm").click(function () {
        if (confirm("are you sure...?") === true) {
            return true;
        }
        else {
            return false;
        }

    })
    try {
        $.validator.setDefaults({ ignore: ":hidden:not(select)" });
    }
    catch (err) {
        console.log(err);
    }
   
   
})
function ShowAlertMessage(message) {
    debugger;
    $(".alert-success").hide();
    $("#divAlert").show();
    $("#alertcontent").html(message);
    document.documentElement.scrollTop = 0;
}
function SubmitCheck() {
    var returnval = true;
    debugger;
    var Message = "";
    if (($("#CurrentMachineId").val() == 0) || ($("#CurrentMachineId").val() == "")) {
        
        returnval = false;
        Message = "Please select a Machine.";
    }
    if (($("#CurrentShiftId").val() == 0) || ($("#CurrentShiftId").val() == "")) {
        if (Message != "") {
            Message = Message+ "\n Please select a Shift.";
        }
        else {
            Message = "Please select a Shift.";
        }
        returnval= false;
    }
    if (returnval == false) {
        ShowAlertMessage(Message);
    }
    return returnval;
}
function FillBreakDownList(caption) {  
    var Url = "\BreakDownReport\getBreakDownReport"
    //$.ajax({
    //    type: 'POST',
    //    url: '/BreakDownReport/getBreakDownReport',        
    //    data: { MachineId: '0', ShiftId: '0', StartDate: '', EndDate: '', ReasonId:'' },
    //    dataType: 'json',
    //    success: function (result) {
    //        alert(result.data);
    //    },
    //    error: function (error) {
    //        alert(error);
    //    }
    //});
    $("#BreakDownBody").html("No Data Found");
    $.post('/BreakDownReport/getBreakDownReport', { MachineId: $("#MachineID").val(), ShiftId: $("#ShiftId").val(), StartDate: $("#StartDate").val(), EndDate: $("#EndDate").val(), ReasonId: caption }, function (data, status) {
        //alert("Data: " + data + "\nStatus: " + status);
        $("#BreakDownBody").html(data);
        });
   
}

$body = $("body");

$(document).on({
    ajaxStart: function () { LoaderStart(); },
    ajaxStop: function () { LoaderStop(); }
});

function LoaderStart() {
    $body.addClass("loading");
}
function LoaderStop() {
    $body.removeClass("loading");
}