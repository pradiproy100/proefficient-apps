﻿$(document).ready(function () {

    //
    console.log($("#hdMachine").val());

    try {
        Connect();
    } catch (e) {

    }
})
function getMachineName(machineId) {
    var AllMachines = JSON.parse($("#hdMachine").val());
    return AllMachines.filter(x => x.MachineID == parseInt(machineId))[0].MachineName;
}
function open_Modal(machineid, reasonId) {
    $("#hdSelectedMachineId").val(machineid);

    if (reasonId > 0) {
        $("#ddlBreakDownReason").val(reasonId);
    }

    $('#breakdownReasonModal').modal('show')
}
function SaveReason() {
    var MachineId = $('#hdSelectedMachineId').val();
    var ReasonId = $("#ddlBreakDownReason").val();

    PublishMessage("{ \"MachineId\": " + MachineId + ", \"ReasonId\": " + ReasonId + " }");
    $('#breakdownReasonModal').modal('hide');



}
function PublishMessage(message) {
    message = new Paho.MQTT.Message(message);
    message.destinationName = GetMqttPublishTopic();
    client.send(message);
}

function PublishMapping(message) {

    message = new Paho.MQTT.Message(JSON.stringify(message));
    message.destinationName = "Machine_Mapping";
    if (client == undefined) {
        Connect();
        client.send(message);
    }
    else {
        client.send(message);
    }

}

function GetMqttSubscribeTopic() {
    return $("#hdSubscribeTopic").val();
}
function GetMqttPublishTopic() {
    return $("#hdPublishTopic").val();
}
function GetMqttIpAddress() {
    return $("#hdMqttIpAddress").val();
}
function GetMqttPortNo() {
    return $("#hdMqttPortNo").val();
}
function GetClientId() {
    return $("#hdClientId").val();
}
function Connect() {
    // 
    // Create a client instance
    /*  client = new Paho.MQTT.Client(location.hostname, Number(location.port), "clientId");*/
    client = new Paho.MQTT.Client(GetMqttIpAddress(), Number(GetMqttPortNo()), GetClientId());
    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    // connect the client
    client.connect({ onSuccess: onConnect });


    // called when the client connects
    function onConnect() {
        // Once a connection has been made, make a subscription and send a message.
        // 
        console.log("onConnect");
        client.subscribe(GetMqttSubscribeTopic());
        //message = new Paho.MQTT.Message("Hello");
        //message.destinationName = "World";
        //client.send(message);
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
        }
    }
    // called when a message arrives
    function onMessageArrived(message) {
        // 
        console.log("onMessageArrived:" + message.payloadString);
        $("#machine_main_status_container").empty();
        if ((message.payloadString !== "") && (message.payloadString !== null)) {
            var full_content = "";
            full_content = full_content + "<div class=\"row\">";
            $.each(JSON.parse(message.payloadString), function (index, it) {

                console.log(JSON.stringify(it));
                full_content = full_content + ShowLiveStatus(it.mahine_status, it.machine_id, it.breakdown_reasonid, it.breakdown_start);
            });
            full_content = full_content + "</div>";
            $("#machine_main_status_container").append(full_content);
        }
    }
}
function GetTimeDifference(breakdown_start) {
    if (breakdown_start === 'time' || breakdown_start === '' || breakdown_start === undefined || breakdown_start === null) {
        return 0;
    }
    else {
        var currentDate = new Date();
        var break_startDate = new Date(breakdown_start);
        var diff = (currentDate - break_startDate);

        var Difference_In_Minute = diff / (1000 * 60);
        return Difference_In_Minute;
    }
}
function ShowLiveStatus(staus, machine_id, reasonId, breakdown_start) {
    console.log("Break Down Start : " + breakdown_start);
    var timeGap = GetTimeDifference(breakdown_start);
    reasonId = parseInt(reasonId);
    reasonId = isNaN(reasonId) ? 0 : reasonId;
    console.log("reason Id :" + reasonId);
    var machine_name = getMachineName(machine_id);
    var statsClass = "";
    var status_label = "";
    var text_colour = "";
    var display_style = " style=\"display:none;\" ";

    switch (staus) {
        case "0":
            statsClass = "green-circle";
            status_label = "Running";
            text_colour = "text-success";
            break;
        case "1":
            statsClass = "yellow-circle";
            status_label = "Idle";
            text_colour = "text-warning";
            break;
        case "2":
            statsClass = "red-circle";
            status_label = "Break down";
            text_colour = "text-danger";
            display_style = "";
            break;
    }
    var running_break = "";
    if (timeGap > 0) {
        running_break = "<div style=\"margin-bottom: 2px\">Stop Time: " + timeGap.toFixed(2) +" min</div>"
    }
    var machine_tile = "<div class=\"col-sm-2\"><div class=\"card\"><div class=\"card-header\"><div class=\"" + statsClass + "\"></div>" + machine_name + "</div><div class=\"card-body text-secondary\"><h5 class=\"card-title " + text_colour + "\">" + status_label + "</h5>"+running_break+"<input type=\"button\" " + display_style + " onclick=\"open_Modal(" + machine_id + "," + reasonId + ");\" class=\"btn btn-info\"value=\"Add Reason\"/></div></div></div>";
    return machine_tile;
}

