﻿$(".checkbox-menu").on("change", "input[type='checkbox']", function () {
    $(this).closest("li").toggleClass("active", this.checked);
});
$(document).on('click', '.allow-focus', function (e) {
    e.stopPropagation();
});
var prodCount = 0;


function ShowProductSelection() {
    $("#product_selection").html("");
    prodCount = 0;
    $("#ProductDropDown input[type=checkbox]").each(function () {
        if ($(this).is(":checked")) {
            prodCount++;
            $("#product_selection").html($("#hd" + $(this).attr('id')).val() + "," + $("#product_selection").html());
        }
    });
    LoadMappingTable();
}

function SelectAllProduct(control) {
    var ischecked = $(control).prop("checked");
    $("#ProductDropDown input[type=checkbox]").each(function () {
        if ($(this).attr("id") != "product0") {
            $(this).prop("checked", !ischecked);
            $(this).trigger('click');
        }
    });
}
function SelectAllDie(control) {
    var ischecked = $(control).prop("checked");
    $("#DieDropDown input[type=checkbox]").each(function () {
        if ($(this).attr("id") != "Die0") {
            $(this).prop("checked", !ischecked);
            $(this).trigger('click');
        }
    });
}
var DieCount = 0;
var DataToSave = [];
function ShowDieSelection() {
    DieCount = 0;
    $("#Die_selection").html("");
    $("#DieDropDown input[type=checkbox]").each(function () {
        if ($(this).is(":checked")) {
            DieCount++;
            $("#Die_selection").html($("#hd" + $(this).attr('id')).val() + "," + $("#Die_selection").html());
        }
    });
    LoadMappingTable();
}
function LoadMappingTable() {
    $("#mapping_body").html("");
    var rowcount = 0;
    var Htmldata = "";
    DataToSave = [];
    $("#ProductDropDown input[type=checkbox]").each(function () {
        if ($(this).is(":checked") && $(this).attr("id") != "product0") {
            var currentproduct = $(this);
            $("#DieDropDown input[type=checkbox]").each(function () {
                if ($(this).is(":checked") && $(this).attr("id") != "Die0") {
                    rowcount++;
                    var currentDie = $(this);
                    Htmldata = Htmldata + '<tr><th scope="row">' + rowcount + '</th><td>' + $("#hdcode" + currentproduct.attr('id')).val() +
                        '</td><td>' + $("#hd" + currentproduct.attr('id')).val() + '</td><td>' + $("#hd" + currentDie.attr('id')).val() + '</td></tr>';

                    DataToSave.push({
                        "productId": currentproduct.attr('id'),
                        "dieId": currentDie.attr('id')
                    });
                }
            });

        }
    });

    if (Htmldata != "") {
        $('#table_container').show();
    }
    else {
        $('#table_container').hide();
    }
    $("#mapping_body").html(Htmldata);

}
function SaveData() {
    $("#SaveMapping").click(function () {       
        if (DataToSave === [] || DataToSave.length===0) {
            alert("Please select product and die...");
            return;
        }
        var strvaldata = JSON.stringify(DataToSave);

        $.get("/ProductDie/SaveJsonData/id='" + strvaldata + "'", function (data, status) {
            // alert(data);
            if (data == true) {
                alert("Data saved successfully");
            }
            else {
                alert("Data is not saved");
            }
            //$("[data-dismiss=modal]").trigger({ type: "click" });
        });
    });


}
$(document).ready(function () {
    $('#table_container').hide();
    SaveData();
});