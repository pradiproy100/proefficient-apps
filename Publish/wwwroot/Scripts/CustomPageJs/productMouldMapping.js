﻿$(".checkbox-menu").on("change", "input[type='checkbox']", function () {
    $(this).closest("li").toggleClass("active", this.checked);
});
$(document).on('click', '.allow-focus', function (e) {
    e.stopPropagation();
});
var prodCount = 0;


function ShowProductSelection() {
    $("#product_selection").html("");
    prodCount = 0;
    $("#ProductDropDown input[type=checkbox]").each(function () {
        if ($(this).is(":checked")) {
            prodCount++;
            $("#product_selection").html($("#hd" + $(this).attr('id')).val() + "," + $("#product_selection").html());
        }
    });
    LoadMappingTable();
}

function SelectAllProduct(control) {
    var ischecked = $(control).prop("checked");
    $("#ProductDropDown input[type=checkbox]").each(function () {
        if ($(this).attr("id") != "product0") {
            $(this).prop("checked", !ischecked);
            $(this).trigger('click');
        }
    });
}
function SelectAllMould(control) {
    var ischecked = $(control).prop("checked");
    $("#MouldDropDown input[type=checkbox]").each(function () {
        if ($(this).attr("id") != "Mould0") {
            $(this).prop("checked", !ischecked);
            $(this).trigger('click');
        }
    });
}
var MouldCount = 0;
var DataToSave = [];
function ShowMouldSelection() {
    MouldCount = 0;
    $("#Mould_selection").html("");
    $("#MouldDropDown input[type=checkbox]").each(function () {
        if ($(this).is(":checked")) {
            MouldCount++;
            $("#Mould_selection").html($("#hd" + $(this).attr('id')).val() + "," + $("#Mould_selection").html());
        }
    });
    LoadMappingTable();
}
function LoadMappingTable() {
    $("#mapping_body").html("");
    var rowcount = 0;
    var Htmldata = "";
    DataToSave = [];
    $("#ProductDropDown input[type=checkbox]").each(function () {
        if ($(this).is(":checked") && $(this).attr("id") != "product0") {
            var currentproduct = $(this);
            $("#MouldDropDown input[type=checkbox]").each(function () {
                if ($(this).is(":checked") && $(this).attr("id") != "Mould0") {
                    rowcount++;
                    var currentMould = $(this);
                    Htmldata = Htmldata + '<tr><th scope="row">' + rowcount + '</th><td>' + $("#hdcode" + currentproduct.attr('id')).val() +
                        '</td><td>' + $("#hd" + currentproduct.attr('id')).val() + '</td><td>' + $("#hd" + currentMould.attr('id')).val() + '</td></tr>';

                    DataToSave.push({
                        "productId": currentproduct.attr('id'),
                        "MouldId": currentMould.attr('id')
                    });
                }
            });

        }
    });

    if (Htmldata != "") {
        $('#table_container').show();
    }
    else {
        $('#table_container').hide();
    }
    $("#mapping_body").html(Htmldata);

}
function SaveData() {
    $("#SaveMapping").click(function () {       
        if (DataToSave === [] || DataToSave.length===0) {
            alert("Please select product and Mould...");
            return;
        }
        var strvaldata = JSON.stringify(DataToSave);

        $.get("/ProductMould/SaveJsonData/id='" + strvaldata + "'", function (data, status) {
            // alert(data);
            if (data == true) {
                alert("Data saved successfully");
            }
            else {
                alert("Data is not saved");
            }
            //$("[data-dismiss=modal]").trigger({ type: "click" });
        });
    });


}
$(document).ready(function () {
    $('#table_container').hide();
    SaveData();
});