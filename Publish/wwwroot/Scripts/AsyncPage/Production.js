﻿

function selectSoppageSubReason() {
    RefreshStoppageSubReason($("#ReasonId").val());
}

$(document).ready(function () {
    $("#ReasonId").change(function () {
        debugger;
        RefreshStoppageSubReason($("#ReasonId").val());
    });

   
    RefreshCreateProduction();
    RefreshCreateBreakDown();
    $("#CurrentShiftId").change(function () {
        //event.preventDefault();
        //if ($("form").valid()) {
        //    debugger;
        //    alert("Valid");

        RefreshCreateProduction();
        RefreshCreateBreakDown();
        // }

    });
    $("#CurrentMachineId").change(function () {
        //event.preventDefault();
        //if ($("form").valid()) {
        //    debugger;
        //    alert("Valid");

        RefreshCreateProduction();
        RefreshCreateBreakDown();
        // }

    });
    LoadDatePicker();
});
function OnLoadMain() {
    $(".Hidden").hide();
    $("select").chosen();
    $('.cTimePick').timepicker({
        'timeFormat': 'H:i',
        'step': 5,
        'disableTextInput': true
    });
    $('.TimePick').timepicker({ 'timeFormat': 'H.i' });
    $(".accordion").accordion({
        collapsible: true
    });//ActiveIndexValue
    var activeIndexval = $("#ActiveIndexValue").val();
    $(".ui_tabs").tabs();
    $(".ui_tabs").tabs("option", "active", activeIndexval);
    $(".accordion").accordion("option", "active", activeIndexval);
    $(".Date-Picker").datepicker({
        dateFormat: 'yyyy/mm/dd'
    });
}

function SubmitWithCheck() {
    
    event.preventDefault();
    var basicvalidation = true;
    if ($("#CurrentMachineId").val() == "") {
        alert("Please select the machine");
        $("#CurrentMachineId").focus();
        basicvalidation = false;
    }
    if ($("#CurrentShiftId").val() == "") {
        alert("Please select the shift");
        $("#CurrentShiftId").focus();
        basicvalidation = false;
    }
    if ($("#SupervisorID").val() == "") {
        alert("Please select the Supervisor");
        $("#SupervisorID").focus();
        basicvalidation = false;
    }
    if ($("#OperatorID").val() == "") {
        alert("Please select the Operator");
        $("#OperatorID").focus();
        basicvalidation = false;
    }
    if (basicvalidation == false) {
        return;
    }

    debugger;
    SaveRejectionReasons();
    isProductionValid($("#sProductionStartTime").val(), $("#sProductionEndTime").val());
    
}




function LoadDatePicker() {
    $("#ProdDate").datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function (d, i) {
            $('.lblProductionDatePE').val(d);
            $('.lblProductionDateBD').val(d);
            $("#hdBreakdownProdDate").val(d);
            if (d !== i.lastVal) {

                var prodDate = d;

                window.sessionStorage.setItem("ProdDate", prodDate);
                RefreshCreateProduction();
            }
        }

    });
}
function RefreshCreateProduction() {
    $.ajax({
        type: 'GET',
        url: "/ProductionPage/Create/?ShiftId=" + getShiftId() + "&MachineId=" + getMachineId() + "&ProdDate=" + getDate()
    })
        .done(function (result) {
            debugger;
            //var r = $(result).find("#MainContentDiv").html();
            //$("#MainContentDiv").html(r);
            $("#CreateProductionPage").html($(result));
            RefreshProductionList();
            OnLoadMain();
        })
        .fail(function () {
            alert("An error occured");
        });
    //event.preventDefault();
}

function RefreshProductionList() {
    $.ajax({
        type: 'GET',
        url: "/ProductionPage/ProductionList/?ShiftId=" + getShiftId() + "&ProdDate=" + getDate() + "&MachineId=" + getMachineId()
    })
        .done(function (result) {
            debugger;
            //var r = $(result).find("#MainContentDiv").html();
            //$("#MainContentDiv").html(r);
            $("#ProductionListPage").html($(result));
        })
        .fail(function () {
            alert("An error occured");
        });
    //event.preventDefault();
}
function RefreshBreakDownList() {
    $.ajax({
        type: 'GET',
        url: "/ProductionPage/BreakDownList/?ShiftId=" + getShiftId() + "&ProdDate=" + getDate() + "&MachineId=" + getMachineId()
    })
        .done(function (result) {
            debugger;
            //var r = $(result).find("#MainContentDiv").html();
            //$("#MainContentDiv").html(r);
            $("#BreakDownListPage").html($(result));
            RefreshStoppageSubReason($("#ReasonId").val());
        })
        .fail(function () {
            alert("An error occured");
        });
    //event.preventDefault();
}






function getShiftId() {
    return $("#CurrentShiftId").val();
}
function getMachineId() {
    return $("#CurrentMachineId").val();
}
function getDate() {
    return $("#ProdDate").val();
}


///Break Down Portion Started Here
function RefreshCreateBreakDown() {
    $.ajax({
        type: 'GET',
        url: "/ProductionPage/CreateBreakDown/?ShiftId=" + getShiftId() + "&MachineId=" + getMachineId() + "&ProdDate=" + getDate()
    })
        .done(function (result) {
            debugger;
            //var r = $(result).find("#MainContentDiv").html();
            //$("#MainContentDiv").html(r);
            $("#CreateBreakDownPage").html($(result));
            RefreshBreakDownList();
            OnLoadMain();
        })
        .fail(function (error) {
            console.log(error);
            alert("An error occured");
        });
    //event.preventDefault();
}

function RefreshStoppageSubReason(reasonid) {
    $("#SubReasonId option").remove();
    $.ajax({
        type: 'GET',
        url: "/ProductionPage/RefreshStoppageSubReason/" + reasonid
    })
        .done(function (result) {
            debugger;

            var selectedSubReasonId = $("#SubReasonId").val();
            var $dropdown = $("#SubReasonId");
            $.each(result, function () {
                $dropdown.append($("<option />").val(this.SubReasonId).text(this.SubReasonName));
            });

            $("#SubReasonId").trigger("chosen:updated");

            if (selectedSubReasonId !== '' && selectedSubReasonId !== null) {
                $("#SubReasonId").val(selectedSubReasonId);
            }
        })
        .fail(function (error) {
            console.log(error);
            alert("An error occured");
        });
    //event.preventDefault();
}


function BreakDownSubmitWithCheck() {
    debugger;
    event.preventDefault();
    var basicvalidation = true;
    if ($("#CurrentMachineId").val() == "") {
        alert("Please select the machine");
        $("#CurrentMachineId").focus();
        basicvalidation = false;
    }
    if ($("#CurrentShiftId").val() == "") {
        alert("Please select the shift");
        $("#CurrentShiftId").focus();
        basicvalidation = false;
    }
    if ($("#SupervisorID").val() == "") {
        alert("Please select the Supervisor");
        $("#SupervisorID").focus();
        basicvalidation = false;
    }
    if ($("#OperatorID").val() == "") {
        alert("Please select the Operator");
        $("#OperatorID").focus();
        basicvalidation = false;
    }
    if (basicvalidation == false) {
        return;
    }
    $(".current_machine").val($("#CurrentMachineId").val());
    $("#ShiftId").val($("#CurrentShiftId").val());
    $("#OrganizationId").val($("#CurrentOrganizationId").val());
    $(".Supervisor").val($("#SupervisorID").val());
    $(".Operator").val($("#OperatorID").val());
    $("#UserID").val($("#OperatorID").val());
    $("#frmcreateBreakDown").validate();
    if ($("#frmcreateBreakDown").valid()) {

        var frmValues = $("#frmcreateBreakDown").serialize();
        $.ajax({
            type: "POST",
            url: "/ProductionPage/CreateBreakDown",
            data: frmValues
        })
            .done(function (result) {

                RefreshCreateBreakDown();

            })
            .fail(function () {
                alert("An error occured");
            });
        event.preventDefault();
    }
}


function EditAction(actionname, idval, targetid,pagename) {

    $.ajax({
        type: 'GET',
        url: "/ProductionPage/" + actionname + "/?id=" + idval
    })
        .done(function (result) {
            
            //var r = $(result).find("#MainContentDiv").html();
            //$("#MainContentDiv").html(r);           
            var MachineId = $(result).find("#MachineID").val();            
            $("#CurrentMachineId").val(MachineId);
            $("#CurrentMachineId").val(MachineId).trigger("chosen:updated");
            $("#" + targetid).html($(result));
            if (pagename == "production") {
                RefreshProductionList();
            }
            else {
                RefreshBreakDownList();
            }

            OnLoadMain();
        })
        .fail(function (error) {
            console.log(error);
            alert("An error occured");
        });

}

function DeleteAction(actionname, idval, targetid, pagename) {
    if (!confirm("Are you Sure..?")) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: "/ProductionPage/" + actionname + "/?id=" + idval
    })
        .done(function (result) {
            if (pagename == "production") {
                RefreshProductionList();
            }
            else {
                RefreshBreakDownList();
            }

            OnLoadMain();
        })
        .fail(function (error) {
            console.log(error);
            alert("An error occured");
        });

}





function CalculateTotalBreakDownTime() {
    calculateStoppageTime("sBreakDownStartTime", "sBreakDownEndTime", "TotalStoppageTime");
}





function calculateStoppageTime(StartTimeControl, EndTimeControl, ResultControl) {


    try {
        var StartTimeVal = $('#' + StartTimeControl).val();
        var EndTimeVal = $('#' + EndTimeControl).val();

        var StartHour = parseInt(StartTimeVal.split(".")[0]);
        var StartMinute = parseInt(StartTimeVal.split(".")[1]);


        var EndHour = parseInt(EndTimeVal.split(".")[0]);
        var EndMinute = parseInt(EndTimeVal.split(".")[1]);

        var TotalStartMinute = (StartHour * 60) + StartMinute;
        var TotalEndMinute = (EndHour * 60) + EndMinute;


        var TotalDifference = TotalEndMinute - TotalStartMinute;
        if (TotalDifference < 0) {
            TotalDifference = parseInt(OverNightValue()) + TotalDifference;
        }
        if (TotalDifference < 0) {
            TotalDifference = 0;
        }
        $('#' + ResultControl).val(TotalDifference);
    }
    catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }

}





function OverNightValue() {
    var value = 0;
    if ($("#isOverNight") != undefined && $("#isOverNight") != null) {
        if (($("#isOverNight").val() == true) || ($("#isOverNight").val() == 'true') || ($("#isOverNight").val() == 'True')) {
            value = 24 * 60;
        }
    }
    return value;
}
function isProductionValid(startTime, endTime) {

    try {
        $.ajax({
            type: 'GET',
            url: "/ProductionPage/isTimeValidProduction/?id=" + $("#ProductionId").val() + "&" +
                "optype=no" + "&" +
                "startTime=" + startTime + "&" +
                "endTime=" + endTime + "&" +
                "shiftid=" + getShiftId() + "&" +
                "machineid=" + getMachineId() + "&" +
                "_Date=" + getDate()
        })
            .done(function (result) {
                if (result == false || result == "False") {
                    alert("You have already entered Full shift data.");
                }
                else {
                    _ProductionDataSubmit();
                }
            })
            .fail(function (error) {
                console.log(error);
                alert("An error occured");
            });
    }
    catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }



   
}

function isBreakDownValid(startTime, endTime) {

    try {
        $.ajax({
            type: 'GET',
            url: "/ProductionPage/isTimeValidBreakDown/?id=" + $("#BreakDownID").val() + "&" +
                "optype=no" + "&" +
                "startTime=" + startTime + "&" +
                "endTime=" + endTime + "&" +
                "shiftid=" + getShiftId() + "&" +
                "machineid=" + getMachineId() + "&" +
                "_Date=" + getDate()
        })
            .done(function (result) {
                if (result == false || result == "False") {
                    alert("You have already entered Full shift data.");
                }
                else {
                    _ProductionDataSubmit();
                }
            })
            .fail(function (error) {
                console.log(error);
                alert("An error occured");
            });
    }
    catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }




}
function _ProductionDataSubmit() {
    $("#MachineID").val($("#CurrentMachineId").val());
    $("#ShiftId").val($("#CurrentShiftId").val());
    $("#OrganizationId").val($("#CurrentOrganizationId").val());
    $(".Supervisor").val($("#SupervisorID").val());
    $(".Operator").val($("#OperatorID").val());
    $("#UserID").val($("#OperatorID").val());
    $("#frmcreateProduction").validate();
    if ($("#frmcreateProduction").valid()) {
        //$("#frmcreateProduction").submit();
        var frmValues = $("#frmcreateProduction").serialize();
        $.ajax({
            type: "POST",
            url: "/ProductionPage/Create",
            data: frmValues
        })
            .done(function (result) {
                //$("#frmcreateProduction").html($(result));
                RefreshCreateProduction();
                //RefreshProductionList();
                //OnLoadMain();
            })
            .fail(function () {
                alert("An error occured");
            });
        event.preventDefault();

    }
}