﻿namespace ProEfficient.Core.Models
{
    public class ProductionRejection
    {
        public int RejectionId { get; set; }
        public int? ProductionEntryId { get; set; }
        public int? RejectionReasonId { get; set; }
        public int? RejectionQuantity { get; set; }
        public string Xml { get; set; }
        public int OrganizationId { get; set; }
    }
}