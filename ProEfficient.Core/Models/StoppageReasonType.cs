﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class StoppageReasonType
    {
        public int StoppageReasonTypeID { get; set; }

        public string StoppageReasonTypeName { get; set; }

        public bool? IsActive { get; set; }

        public int? AddBy { get; set; }

        public DateTime? AddDate { get; set; }

        public int? EditBy { get; set; }

        public DateTime? EditDate { get; set; }
    }

  

}
