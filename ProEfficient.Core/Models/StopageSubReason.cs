﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class StopageSubReason
    {
        [Display(Name = "Sub Reason")]
        public int SubReasonId { get; set; }
        [Display(Name = "Reason")]
        [Required]
        public int? ReasonId { get; set; }
        [Display(Name = "Sub Reason Name")]
        [Required]
        public string SubReasonName { get; set; }
        [Display(Name = "Sub Reason Code")]
        [Required]
        public string SubReasonCode { get; set; }
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
        [Display(Name = "Reason Description")]
        public string ReasonDescription { get; set; }
        [Display(Name = "Add By")]
        public int? AddBy { get; set; }
        [Display(Name = "Edit By")]
        public int? EdittedBy { get; set; }
        [Display(Name = "PLC SubReason Id")]
        public int InfluxSubReasonId { get; set; }
    }
}