﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class StopageReason
    {
        public int ReasonId { get; set; }
        [Required]
        [Display(Name = "Reason Name")]
        public string ReasonName { get; set; }
        [Required]
        [Display(Name = "Reason Code")]
        public string ReasonCode { get; set; }
        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        [Display(Name = "Scheduled Loss")]
        public bool IsSchLoss { get; set; }
        public int OrganizationId { get; set; }
        [Display(Name = "PLC Reason Id")]
        public int InfluxReasonId { get; set; }
        [Required]
        [Display(Name = "Stoppage Reason Type")]
        public int StoppageReasonTypeId { get; set; }
    }


}