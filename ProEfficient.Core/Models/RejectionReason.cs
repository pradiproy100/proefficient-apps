﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class RejectionReason
    {
        [Display(Name = "Reason Id")]
        public int ReasonId { get; set; }
        [Display(Name = "Reason Name")]
        [Required]
        public string ReasonName { get; set; }
        [Display(Name = "Reason Description")]
        [Required]
        public string ReasonDescription { get; set; }
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
        public int OrganizationId { get; set; }
        public string ReasonGroup { get; set; }
        [Display(Name = "PLC Rejection Reason Id")]
        public int InfluxRejReasonId { get; set; }
    }
}