﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.HomeDashBoardModels
{
    public class ReportModel
    {
        public int MachineID { get; set; }
        public int WorkShiftTime { get; set; }
        public int productiontime { get; set; }
        public int shiftId { get; set; }
        public int productID { get; set; }
        public string ShutdownTime { get; set; }
        public string ProductionDate { get; set; }

        public string Utliizationloss {get;set;}
        public string TotalProduction { get; set; }
        public string Qualityloss { set; get; }
        public string DemandType { get; set; }
        public string CustomerName { get; set; }
        public string ShiftBreakTime { get; set; }
        public string MachineName { get; set; }
        public string ShiftName { get; set; }

    }
    public class LossReportModel
    {
        public string ProductionDate { get; set; }
        public string Remarks { get; set; }
        public int MAchineID { get; set; }
        public string MachineName { get; set; }
        public int ReasonId { get; set; }
        public string Reason { get; set; }
        public int SubReasonId { get; set; }
        public string SubReason { get; set; }
        public decimal TotalStoppagetime { get; set; }
        public decimal Duration { get; set; }
        public int Freequency { get; set; }
        
    }
    public class LossReportDashboardModel
    {
        
        public int ReasonId { get; set; }
        public string Reason { get; set; }       
        public decimal Duration { get; set; }
        public int Freequency { get; set; }
        public List<LossReportDashboardChildModel> LossDetails { get; set; }
       // public string rptDate { get; set; }
    }
    public class LossReportDashboardChildModel
    {

        public string ProductionDate { get; set; }
        public string Remarks { get; set; }
        public int MAchineID { get; set; }
        public string MachineName { get; set; }
        public int ReasonId { get; set; }
        public string Reason { get; set; }
        public int SubReasonId { get; set; }
        public string SubReason { get; set; }
        public decimal TotalStoppagetime { get; set; }
    }
    public class LossReportDetailsModel
    {
        public string ProductionDate { get; set; }
       // public string Remarks { get; set; }
        public string MachineName { get; set; }
        public string Reason { get; set; }
       // public string SubReason { get; set; }
       // public decimal TotalStoppagetime { get; set; }
       
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Duration { get; set; }

    }
}
