﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.HomeDashBoardModels
{
    public class FinalResultSetModel
    {
        public List<UserReportConfig> Machinelst { get; set; }
       // public List<ReportModel> Reportlst { get; set; }

        public List<SelectListItem> MachineList { get; set; }
        public List<SelectListItem> DatasetNames { get; set; }
        public List<FinalARPRQROEE> PerformanceReportList { get; set; }
        public List<LossReportDashboardModel> LossReportList { get; set; }
        public List<FinalWeekWiseOEE> PerformanceReportWEEKList { get; set; }
        public List<FinalARPRQROEEHome> PerformanceReportListHome { get; set; }
        public List<RejectionReasonReportModel> RejectionReportListHome { get; set; }
        public List<SelectListItem> ShiftList { get; set; }
        public List<LossReportModel> LossReportRptList { get; set; }
        public List<RejectionReasonDetailsModel> RejectionReasonRpt { get; set; }
        
        public FinalResultSetModel()
        {
            MachineList = new List<SelectListItem>();
        }

    }
    public class RawDataModel
    {
        public List<RawProduction> rawProduction { get; set; }
        public List<RawBreakdown> rawBreakdown { get; set; }
        public List<RawRejection> rawRejection { get; set; }
    }
    public class RawProduction
    {
        public string ProductionDate { get; set; }
        public string MachineName { get; set; }
        public string ShiftName { get; set; }
        public string Product { get; set; }
        public int ProCount { get; set; }
        public int RejCount { get; set; }
    }
    public class RawBreakdown
    {      
       
      
        public string ProductionDate { get; set; }       
        public string MachineName { get; set; }
        public string ShiftName { get; set; }
        public string Reason { get; set; } 
        public int ProCount { get; set; }        
        public string StartTime { get; set; }
        public string EndTime { get; set; }

    }
    public class RawRejection
    {
        public string ProductionDate { get; set; }
        public string MachineName { get; set; }
        public string ShiftName { get; set; }
        public string Reason { get; set; }
        public int ProCount { get; set; }

    }
}
