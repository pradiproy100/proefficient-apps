﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.HomeDashBoardModels
{
    public class TypeofData
    {
        public string typeofdata { get; set; }
        public int monthnumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate {get;set;}
        public string MachineID { get; set; }
        public string OrganizationID { get; set; }
        public string EmailId { get; set; }
    }
    public class TypeofDataLoss
    {
        public string typeofdata { get; set; }
        public int monthnumber { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string MachineID { get; set; }
        public string OrganizationID { get; set; }
        public string Reason { get; set; }
    }
    public class UserDashboard
    {
        public int machineId { get; set; }
        public string dateRange { get; set; }
       
    }
}
