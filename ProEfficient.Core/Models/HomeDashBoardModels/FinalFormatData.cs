﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.HomeDashBoardModels
{
   public class FinalFormatData
    {
        public string StartDate { get; set; }
        public string MachineName { get; set; }
        public string MachineID { get; set; }
        public string AR { get; set; }
        public string PR { get; set; }
        public string QR { get; set; }

        public string OEE { get; set; }
        public string ShiftID { get; set; }
        public string DataType { get; set; }


    }
}
