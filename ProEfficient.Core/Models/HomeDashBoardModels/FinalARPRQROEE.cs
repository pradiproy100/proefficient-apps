﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.HomeDashBoardModels
{
    public class FinalARPRQROEE
    {
        public double AR { get; set; }
        public double PR { get; set; }
        public double QR { get; set; }
        public double OEE { get; set; }

        public string DataLevel { get; set; }
        public string rptDate { get; set; }
        public string MachineName { get; set; }
        public string ShiftName { get; set; }
        public string RecordDate { get; set; }
    }
    public class FinalARPRQROEEHome
    {
        public string DataLevel { get; set; }
        public string rptDate { get; set; }
        public string MachineName { get; set; }
        public string ShiftA { get; set; }
        public string ShiftB { get; set; }
        public string ShiftC { get; set; }
        public string tShiftA { get; set; }
        public string tShiftB { get; set; }
        public string tShiftC { get; set; }
        public string OverAll { get; set; }
        public string tOverAll { get; set; }
        public string RecordDate { get; set; }
    }
    public class ChartsModel
    {
        public List<WaterFallModel> waterFallList { get; set; }
        public List<RejectionReasonReportModel> bubbleChartList { get; set; }
    }
    public class FinalWeekWiseOEE
    {
        public string weekno { get; set; }
        public double OEE { get; set; }

        public string DataLevel { get; set; }
    }
    public class WaterFallModel
    {
        public string Total_Time { get; set; }
        public string Holidays { get; set; }
        public string Break_Time { get; set; }
        public string Shutdown_Time { get; set; }
        public string Utilisation_Loss { get; set; }        
        public string Production_Loss { get; set; }
        public string Quality_Loss { get; set; }
        
    }
    public class RejectionReasonReportModel
    {
        public string Name { get; set; }
        //public int TotalRejection { get; set; }
        public int Count { get; set; }
    }
    public class RejectionReasonDetailsModel
    {
        public string ProductionDate { get; set; }
        public string MachineName { get; set; }
        public string ReasonName { get; set; }
        public int ProdCount { get; set; }
    }
    public class FinalARPRQROEEExcel
    {
      
        public string Date { get; set; }
        public string MachineName { get; set; }
        public string ShiftAAR { get; set; }
        public string ShiftAPR { get; set; }
        public string ShiftAQR { get; set; }
        public string ShiftAOEE { get; set; }
        public string ShiftBAR { get; set; }
        public string ShiftBPR { get; set; }
        public string ShiftBQR { get; set; }
        public string ShiftBOEE { get; set; }
        public string ShiftCAR { get; set; }
        public string ShiftCPR { get; set; }
        public string ShiftCQR { get; set; }
        public string ShiftCOEE { get; set; }
        public string OverAllAR { get; set; }
        public string OverAllPR { get; set; }
        public string OverAllQR { get; set; }
        public string OverAllOEE { get; set; }
    }
}
