﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class Product_Die_Rel
    {
        public int RecId { get; set; }

        public int? DieId { get; set; }

        public int? OrganizationId { get; set; }

        public int? ProductID { get; set; }

        public int? CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

    }
}
