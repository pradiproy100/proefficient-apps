﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class MachineCategory
    {

        public int CategoryId { get; set; }
        [Required]
        [Display(Name = "Category Name")]
        public String CategoryName { get; set; }
        [Display(Name = "Organization Name")]
        public int OrganizationId { get; set; }
    }
}