﻿using ProEfficient.Core.Models.ViewModel;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace ProEfficient.Core.Models
{
    public class ProductionReport
    {

        [Required]
        [Display(Name = "Machine")]
        public int? MachineID { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public string StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public string EndDate { get; set; }
        [Display(Name = "Shift")]
        public int? ShiftId { get; set; }

        [Display(Name = "dt")]
        public DataTable dtBreakdown { get; set; }

        [Display(Name = "dtProd")]
        public DataTable dtProd { get; set; }
        public int? UserId { get; set; }


        public SearchCriteria SearchCriteria { get; set; }
    }
    public class ProductionReportAllMachine
    {


        [Display(Name = "Machine")]
        public int? MachineID { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public string StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public string EndDate { get; set; }
        [Display(Name = "Shift")]
        public int? ShiftId { get; set; }

        [Display(Name = "dt")]
        public DataTable dtBreakdown { get; set; }

        [Display(Name = "dtProd")]
        public DataTable dtProd { get; set; }
        public DataTable dtAverage { get; set; }
        public int? UserId { get; set; }

        public int OrgId { get; set; }
    }
}