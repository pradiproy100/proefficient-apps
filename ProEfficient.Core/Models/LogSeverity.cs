﻿namespace ProEfficient.Core.Models
{
    public enum LogSeverity
    {
        High = 3,
        Mid = 2,
        Low = 1
    }
}