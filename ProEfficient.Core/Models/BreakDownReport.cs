﻿using System.ComponentModel.DataAnnotations;
using System.Data;

namespace ProEfficient.Core.Models
{
    public class BreakDownReport
    {
        [Required]
        [Display(Name = "Machine")]
        public int? MachineID { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public string StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
        public string EndDate { get; set; }
        [Display(Name = "Shift")]
        public int? ShiftId { get; set; }

        [Display(Name = "dt")]
        public DataTable dtBreakdown { get; set; }
        public int? UserID { get; set; }
    }
}