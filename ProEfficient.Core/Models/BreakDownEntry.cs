﻿using ProEfficient.Core.Utility;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class BreakDownEntry
    {
        [Display(Name = "BreakDown Id")]
        public int BreakDownID { get; set; }
        [Display(Name = "Machine")]
        public String MachineName { get; set; }
        public String ProductName { get; set; }
        public int? MachineID { get; set; }
        [Display(Name = "Stoppage Reason")]
        [Required(ErrorMessage = "The Stoppage Reason field is required.")]
        public int? ReasonId { get; set; }
        [Display(Name = "Stoppage Sub Reason")]
        public int? SubReasonId { get; set; }
        [Display(Name = "User")]
        //[Required]
        public int? UserID { get; set; }
        [Display(Name = "Operator")]
        [Required]
        public int? OperatorID { get; set; }




        [Display(Name = "Supervisor")]
        //[Required]
        public int? SupervisorID { get; set; }



        [Display(Name = "Time Of Entry")]
        public DateTime? TimeOfEntry { get; set; }


        [Required]
        [Display(Name = "Shift")]
        public int? ShiftId { get; set; }


        [Required]
        [Display(Name = "Start Time")]
        public Decimal BreakDownStartTime { get; set; }

        [Display(Name = "Start Time")]
        public String sBreakDownStartTime
        {
            get
            {
                try
                {

                    return BreakDownStartTime.TwoDecimalFormat();
                }

                catch (Exception)
                {
                    return "0.00";
                }
            }

            set

            {
                BreakDownStartTime = value.ToStringWithNullEmpty().Replace(":", ".").ToDecimal0();
            }
        }

        [Display(Name = "End Time")]
        public String sBreakDownEndTime
        {
            get
            {
                try
                {

                    return BreakDownEndTime.TwoDecimalFormat();
                }

                catch (Exception)
                {
                    return "0.00";
                }
            }

            set

            {
                BreakDownEndTime = value.ToStringWithNullEmpty().Replace(":", ".").ToDecimal0();
            }
        }
        //[Required]
        [Display(Name = "Start Time")]
        public DateTime? StartTime { get; set; }
        [Required]
        [Display(Name = "Remarks")]
        public String Remarks { get; set; }
        [Required]
        [Display(Name = "end Time")]
        public Decimal BreakDownEndTime { get; set; }
        [Display(Name = "Organization Id")]
        public int? OrganizationId { get; set; }
        [Display(Name = "End Time")]

        public DateTime? EndTime { get; set; }
        [Display(Name = "Total Stoppage Time")]
        [Required]
        public decimal? TotalStoppageTime { get; set; }
        [Display(Name = "Active")]
        public bool? IsActive { get; set; }

        public int? AddBy { get; set; }

        public int? EdittedBy { get; set; }

        public DateTime ProductionDate { get; set; }
        public bool IsSaved { get; set; }
        public string deletedEntries { get; set; }
    }

}