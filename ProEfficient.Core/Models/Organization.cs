﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class Organization
    {
        [Display(Name = "Organization")]
        public int OrganizationId { get; set; }
        [Display(Name = "Organization Name")]
        [Required]
        public string OrganizationName { get; set; }
        [Display(Name = "Phone No")]
        [Required]
        public string PhoneNo { get; set; }
        [Display(Name = "Email Id")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }
        [Display(Name = "Location")]
        public string Location { get; set; }

        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }

        public int? AddBy { get; set; }

        public int? EdittedBy { get; set; }
        public bool IsManualProductionEntry { get; set; }
        public bool HasBreakdown { get; set; }
        public bool HasRejection { get; set; }

    }

}