﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class MasterOperator
    {
        public int OperatorId { get; set; }

        [MaxLength(20)]
        [MinLength(1)]
        [Display(Name = "Employee Id")]
        public string EmployeeId { get; set; }

        public int? OrganizationId { get; set; }
        [MaxLength(100)]
        [MinLength(2)]
        [Display(Name = "Operator Name")]
        public string OperatorName { get; set; }

        public int? AddBy { get; set; }

        public int? EditBy { get; set; }

        public bool? IsActive { get; set; }

    }

}
