﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class MasterMachine
    {
        public int MachineID { get; set; }
        [Required]
        [Display(Name = "Machine")]
        public string MachineName { get; set; }
        [Required]
        [Display(Name = "Location")]
        public string Location { get; set; }
        [Display(Name = "Organization")]
        [Required]
        public int? OrganizationId { get; set; }
        [Required]
        [Display(Name = "Machine Code")]
        public string MachineCode { get; set; }
        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }
        public int? CategoryId { get; set; }
        public int? AddBy { get; set; }
        public bool IsSelected { get; set; }
        public int? EdittedBy { get; set; }
        [Display(Name = "Demand Type")]
        public string DemandType { get; set; }

    }

}