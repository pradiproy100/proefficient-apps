﻿namespace ProEfficient.Core.Models
{
    public class ProductCategory
    {
        public string CategoryName { get; set; }
        public int CategoryID { get; set; }
    }
}