﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class OrganizationUser
    {
        [Display(Name = "User")]
        public int UserId { get; set; }

        [Display(Name = "User Name")]
        [Required]
        public String UserName { get; set; }
        [Display(Name = "Organization")]
        [Required]
        public int? OrganizationId { get; set; }
        public String OrganizationName { get; set; }
        [Display(Name = "User Type")]
        [Required]
        public int? UserTypeId { get; set; }
        public String UserTypeName { get; set; }
        [Display(Name = "Phone No")]
        [Required]
        public string PhoneNo { get; set; }
        [DataType(DataType.EmailAddress)]
        [Required]
        [Display(Name = "Email")]
        public string EmailId { get; set; }
        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }
        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }
        public String ChangeCode { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }

    }
    public class APIUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int OrganizationId { get; set; }
         public String OrganizationName { get; set; }
        public int UserTypeId { get; set; }
         public String UserTypeName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsManualProductionEntry { get; set; }
        public bool HasBreakdown { get; set; }
        public bool HasRejection { get; set; }
    }

}