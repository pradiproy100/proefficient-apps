﻿namespace ProEfficient.Core.Models
{
    public class UserType
    {
        public int UserTypeID { get; set; }

        public string UserTypeName { get; set; }

        public string PermissionModule { get; set; }

    }


}