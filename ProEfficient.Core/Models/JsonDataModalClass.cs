﻿using System;
using System.Collections.Generic;

namespace ProEfficient.Core.Models
{
    public class JsonDataModalClass
    {
        public string shift { set; get; }
        public string Ar { set; get; }
        public string Pr { set; get; }
        public string Qr { set; get; }
        public string Oee { set; get; }
        public double ArValue { set; get; }
        public double PrValue { set; get; }
        public double QrValue { set; get; }
        public double OeeValue { set; get; }
        public string Date { set; get; }
        public string seriesname { set; get; }
        public string label { set; get; }
        public DateTime ShiftDate { set; get; }
        List<string> parametrelst { set; get; }
        public JsonDataModalClass()
        {
            shift = "";
            Ar = "";
            Pr = "";
            Oee = "";
            Qr = "";
            seriesname = "";
            Date = "";
            ShiftDate = new DateTime();
            parametrelst = new List<string>();
        }
    }
}