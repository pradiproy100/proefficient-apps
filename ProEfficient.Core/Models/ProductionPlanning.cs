﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class ProductionPlanning
    {
        public int RelationId { get; set; }
        public int OrganizationId { get; set; }
        
        public int  MachineId { get; set; }
        public String MachineName { get; set; }

        public int  ProductId { get; set; }

        public String ProductName { get; set; }
        public DateTime  SheduleDate { get; set; }

        public DateTime ?StartDate{ get; set; }
        public DateTime ?EndDate { get; set; }

        public int? AddByUser { get; set; }

        public int? EditByUser { get; set; }

        public DateTime? AddDate { get; set; }

        public DateTime? EditDate { get; set; }

    }

   

}
