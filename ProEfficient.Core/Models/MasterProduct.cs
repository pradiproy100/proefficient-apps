﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class MasterProduct
    {
        public int ProductId { get; set; }

        [Display(Name = "Product Code")]
        [Required]
        public string ProductCode { get; set; }


        [Display(Name = "Organization Name")]
        public int OrganizationId { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required]
        [Display(Name = "Cycle Time")]

        public Decimal? CycleTime { get; set; }
        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }

        public int? AddBy { get; set; }

        public int? EdittedBy { get; set; }

        public string CategoryName { get; set; }

        public string NewCategory { get; set; } = "NA";

        public bool IsNewCategory { get; set; } = false;


        public string ProductType { get; set; }

        [Display(Name = "ProductCategory")]
        public int ProductCategoryID { get; set; }
        [Display(Name = "Unit")]
        public int UnitID { get; set; }


        public ProductType? ProductTypeName { get; set; }
        public string CustomerName { get; set; }
        public string NewCustomerName { get; set; } = "NA";
        public bool IsNewCustomer { get; set; } = false;

        [Display(Name = "End Customer")]
        public int CustomerID { get; set; }

    }



    public enum ProductType
    {
        None = 1,
        Runner = 2,
        Repeater = 3,
        Stranger = 4
    }
   



}