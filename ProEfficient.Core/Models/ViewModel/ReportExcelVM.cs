﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.ViewModel
{
    public class ReportExcelVM
    {
    }
    public class ReportExcelGetConfigModel
    {
        public int UserMachineReportID { get; set; }
        public int ReportTypeId { get; set; }
        public string ReportName { get; set; }
        public string DateRange { get; set; }
        public string EmailTo { get; set; }
    }
    public class ReportExcelOEEModel
    {
        public string MachineName { get; set; }

        public decimal ARShiftA { get; set; }
        public decimal PRShiftA { get; set; }
        public decimal QRShiftA { get; set; }
        public decimal OEEShiftA { get; set; }

        public decimal ARShiftB { get; set; }
        public decimal PRShiftB { get; set; }
        public decimal QRShiftB { get; set; }
        public decimal OEEShiftB { get; set; }

        public decimal ARShiftC { get; set; }
        public decimal PRShiftC { get; set; }
        public decimal QRShiftC { get; set; }
        public decimal OEEShiftC { get; set; }

        public decimal AROverAll { get; set; } 
        public decimal PROverAll { get; set; }
        public decimal QROverAll { get; set; }
        public decimal OEE { get; set; }
    }
    public class LossReportExcelModel
    {
      
        public int ReasonId { get; set; }
        public string Reason { get; set; }       
        public decimal Duration { get; set; }
        public int Freequency { get; set; }

    }
}
