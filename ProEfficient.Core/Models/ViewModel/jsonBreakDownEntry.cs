﻿using System;

namespace ProEfficient.Core.Models.ViewModel
{
    public class jsonBreakDownEntry
    {

        public string BreakDownID { get; set; }



        public string MachineID { get; set; }


        public string ReasonId { get; set; }

        public string UserID { get; set; }


        public string OperatorID { get; set; }


        public string SupervisorID { get; set; }

        public string TimeOfEntry { get; set; }


        public string ShiftId { get; set; }


        public string BreakDownStartTime { get; set; }

        public string StartTime { get; set; }


        public string Remarks { get; set; }


        public string BreakDownEndTime { get; set; }

        public string OrganizationId { get; set; }


        public string EndTime { get; set; }


        public string TotalStoppageTime { get; set; }

        public string IsActive { get; set; }

        public string AddBy { get; set; }

        public string EdittedBy { get; set; }

        public string ProductionDate { get; set; }
        public string IsSaved { get; set; }
        public string SubReasonId { get; set; }
    }

}