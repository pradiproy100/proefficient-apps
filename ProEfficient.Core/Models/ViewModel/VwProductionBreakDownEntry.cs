﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace ProEfficient.Core.Models.ViewModel
{
    public class VwProductionBreakDownEntry
    {
        [UIHint("BreakDownList")]
        [Display(Name = "Breakdown List")]
        public List<BreakDownEntry> ListOfBreakDown { get; set; }
        [Display(Name = "Production List")]
        [UIHint("ProductionList")]
        public List<ProductionEntry> ListOfProduction { get; set; }

        [Display(Name = "Operator")]
        //[Required]
        public int? OperatorID { get; set; }
        [Display(Name = "Supervisor")]
        //[Required]
        public int? SupervisorID { get; set; }

        [UIHint("BreakDownEntry")]
        [Display(Name = "Breakdown Entry")]
        public BreakDownEntry AddedBreakDown { get; set; }
        [UIHint("ProductionDownEntry")]
        [Display(Name = "Production Entry")]
        public ProductionEntry AddedProductionEntry { get; set; }
        [Display(Name = "Production Date")]
        public DateTime? ProductionDate { get; set; }

        [Display(Name = "Production Date")]

        public String strProductionDate
        {
            get
            {
                return (ProductionDate == null ? "" : ProductionDate.ToDateTime().ToString("dd/MM/yyyy"));
            }

            set
            {
                ProductionDate = (value == null ? DateTime.Now : DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            }
        }

        public int ActiveIndex { get; set; }
        [Display(Name = "Remarks")]
        public String Remarks { get; set; }
        [Display(Name = "Machine")]
        //[Required]
        public int CurrentMachineId { get; set; }

        [Display(Name = "Product")]
        //[Required]
        public int CurrentProductId { get; set; }

        [Display(Name = "Shift")]
        //[Required]
        public int CurrentShiftId { get; set; }
        public int CurrentOrganizationId { get; set; }
        public Boolean IsSubmitted { get; set; }
        public List<RejectReason> RejectReasonlst { get; set; }
        public List<StopageReason> StoppageReasonlst { get; set; }

        [Display(Name = "Stoppage Reason")]
        public int StoppageReaseonID { get; set; }

    }
    public class ProductionBreakDownEntryViewModel
    {
        public int CurrentOrganizationId { get; set; }
        public int CurrentUserId { get; set; }
        public int ActiveIndex { get; set; }

        [Display(Name = "Supervisor")]
        //[Required]
        public int? SupervisorID { get; set; }

        [Display(Name = "From Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ProductionDate { get; set; }

        [Display(Name = "To Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ProductionToDate { get; set; }

        [Display(Name = "Shift")]
        //[Required]
        public int CurrentShiftId { get; set; }
        public List<SelectListItem> lstAllShift { get; set; }
        public List<ProductionBreakEntryShiftVM> lstAllShiftData { get; set; }
        [Display(Name = "Remarks")]
        public String Remarks { get; set; }
        [Display(Name = "Machine")]
        //[Required]
        public int CurrentMachineId { get; set; }
        public List<SelectListItem> lstAllMachine { get; set; }

        [Display(Name = "Operator")]
        //[Required]
        public int? OperatorID { get; set; }

        public List<SelectListItem> lstAllOperators { get; set; }
        [Display(Name = "Product")]
        //[Required]
        public int CurrentProductId { get; set; }

        public List<SelectListItem> lstAllProduct { get; set; }
        public List<ProductionBreakEntryProductMachineVM> lstvmAllProduct { get; set; }



        [Display(Name = "Stoppage Reason")]
        public int StoppageReaseonID { get; set; }
        public List<SelectListItem> lstAllStopageReason { get; set; }

        public List<ProductionBreakEntryStopageVM> lstAllSubStopageReason { get; set; }

        public List<ProductionBreakEntryRejectionVM> lstAllRejectReason { get; set; }
        public List<RejectionReasonModel> lstAllRejectionReason { get; set; }

        public Boolean IsSubmitted { get; set; }

    }
}