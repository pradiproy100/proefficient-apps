﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class UserReportConfig
    {
      
        public int UserMachineReportID { get; set; }

        [Required]
        [Display(Name = "Machine")]

        public int UserId { get; set; }       
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Report")]
        public string ReportName { get; set; }

        [Required]
        [Display(Name = "Freequency")]
        public string DateRange { get; set; }
       
        public string MailTo { get; set; }

        public string MachineIds { get; set; }
        
        [Required]
        [Display(Name = "Machines")]
        public int[] arrMachineIds { get; set; }

        

        [Required]
        [Display(Name = "Recipients")]
        public int[] arrRecipients { get; set; }

        [Required]
        [Display(Name = "Is Default")]
        public bool IsDefault { get; set; }

    }

}