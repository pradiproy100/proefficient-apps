﻿using System;
using System.Collections.Generic;

namespace ProEfficient.Core.Models.ViewModel
{
    public class GetProductionEntryByShiftAndDateVM
    {
        public int ProductionRowIndex { get; set; }
        public int ProductionId { get; set; }
        public int OrganizationId { get; set; }
        public int ShiftId { get; set; }
        public int MachineID { get; set; }
        public int OperatorID { get; set; }
        public int ProductID { get; set; }
        public string ProductionDate { get; set; }
        public string ShiftName { get; set; }
        public string MachineName { get; set; }
        public string ProductName { get; set; }
        public string OperatorName { get; set; }
       
        public float TotalProduction { get; set; }
        public int ReWork { get; set; }
       
        public bool IsSaved { get; set; }
        public bool IsDeleted { get; set; }
        public string RejectionList { get; set; }
        public int Rejection { get; set; }
        public DateTime ProductionStartTime { get; set; }
        public DateTime ProductionEndTime { get; set; }
        public int UserID { get; set; }
        public List<RejectionEntryByShiftAndDateVM> RejectionReasonList { get; set; }

    }
    public class GetProBreakDataParamsVM
    {
        public DateTime ProductionDate { get; set; }
        public DateTime ProductionToDate { get; set; }
        public int ShiftId { get; set; }
        public int OrganizationID { get; set; }

    }
    public class RejectionEntryByShiftAndDateVM
    {
        public int MachineID { get; set; }
        public string ReasonName { get; set; }
        public int ReasonId { get; set; }
        public int ProdCount { get; set; }
        public int ProdEntryId { get; set; }

    }

}