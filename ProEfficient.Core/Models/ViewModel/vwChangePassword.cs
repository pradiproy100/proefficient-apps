﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models.ViewModel
{
    public class VwChangePassword
    {
        [Display(Name = "Varification Code")]
        public String ChangeCode { get; set; }
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public String EmailAddress { get; set; }
        [Display(Name = "Password")]
        public String Password { get; set; }
        [Display(Name = "Confirm Password")]
        public String ConfirmPassword { get; set; }
        public Boolean IsCodeSend { get; set; }

    }
}