﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProEfficient.Core.Models.ViewModel
{
    public class ProductCategoryViewModel
    {
       
        public int Id { get; set; }

        public int OrganizationId { get; set; }

        [Required]
        [Display(Name = "Category")]
        public string CategoryName { get; set; }

        [Display(Name = "Rejection Reasons")]
        public string RejectionReasonIds { get; set; }

        [Display(Name = "Rejection Reasons")]
        public int[] arrRejectionReasonIds { get; set; }
    }
}
