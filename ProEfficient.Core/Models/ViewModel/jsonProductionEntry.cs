﻿using System;
using System.Collections.Generic;

namespace ProEfficient.Core.Models.ViewModel
{
    public class jsonProductionEntry
    {
        public string ProductionId;
        public string MachineID;
        public string ProductID;
        public string UserID;
        public string OperatorID;
        public string SupervisorID;
        public string OrganizationId;
        public string TimeOfEntry;
        public string ShiftId;
        public string ProductionStartTime;
        public string ProductionEndTime;
        public string StartTime;
        public string EndTime;
        public string TotalProduction;
        public string ReWork;
        public string Rejection;
        public string ProductionDate;
        public string IsSaved;
        public string ProdDate;
        public string MachineName;
        public string ProductName;
        public string RejectionList;
        public List<RejectionEntryByShiftAndDateVM> RejectionReasonList;
        public List<jsonBreakDownEntry> breakdownentrylst;
    }
}