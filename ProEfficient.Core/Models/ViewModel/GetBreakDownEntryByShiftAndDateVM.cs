﻿using System;

namespace ProEfficient.Core.Models.ViewModel
{
    public class GetBreakDownEntryByShiftAndDateVM
    {
        public int BreakdownRowIndex { get; set; }
        public int BreakdownId { get; set; }
        public int OrganizationId { get; set; }
        public int ShiftId { get; set; }
        public int MachineID { get; set; }
        public int OperatorID { get; set; }
        public string ProductionDate { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public String strStartTime
        {
            get
            {
                return (StartTime == null ? "00:00" : StartTime.ToString("HH:mm"));
            }
            set
            {               
            }
        }
        public String strEndTime
        {
            get
            {
                return (EndTime == null ? "00:00" : EndTime.ToString("HH:mm"));
            }
            set
            {
            }
        }
        
        public decimal TotalStoppageTime { get; set; }
        public string Remarks { get; set; }
        public string ShiftName { get; set; }
        public string MachineName { get; set; }
      //  public string ProductName { get; set; }
        public string OperatorName { get; set; }
        public int ReasonId { get; set; }
        public int SubReasonId { get; set; }
        public string ReasonName { get; set; }
        public string SubReasonName { get; set; }
        
       
        public bool IsSaved { get; set; }
        public bool IsDeleted { get; set; }
        public int UserID { get; set; }

    }
   

}