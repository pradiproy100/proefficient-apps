﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.ViewModel
{
  public  class vwMachineProductPlanning
    {
        public MasterMachine machine { get; set; }
        public MasterProduct product { get; set; }
    }
}
