﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models.ViewModel
{
    public class SupervisorCalendarViewModel
    {
        public int id { get; set; }

        public int machineId { get; set; }

        public int shiftId { get; set; }

        public int supervisorId { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }
        public List<SelectListItem> AllShift { get; set; }
        public List<SelectListItem> AllMachine { get; set; }
    }
    public class SupervisorCalendarSaveViewModel
    {
        public string id;
        public string machineId;
        public string shiftId;
        public string supervisorId;
        public string startDate;
        public string SupervisorID;
        public string endDate;
        public bool IsActive;
    }
}
