﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProEfficient.Core.Models.ViewModel
{
    public class ProductMachineViewModel
    {
        //public ProductMachineMapViewModel productMachineMapViewModel = new ProductMachineMapViewModel();
        public ProductMachineVMSelect productMachineVMSelect = new ProductMachineVMSelect();
        public List<ProductMachineVM> lstproductmachineVM = new List<ProductMachineVM>();
        public List<PMProductVM> lstproductmachineProductVM = new List<PMProductVM>();
        public List<PMMachineVM> lstproductmachineMachineVM = new List<PMMachineVM>();
    }
    public class PMProductVM
    {
        public int ProductId { get; set; }
        [Display(Name = "Product Code")]
        [Required]
        public string ProductCode { get; set; }
        [Required]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        public string ProductCustomer { get; set; }
        public string ProductCategory { get; set; }
        public string ProductDemandType { get; set; }
        [Display(Name = "Unit")]
        public string ProductUnit { get; set; }
        [Required]
        [Display(Name = "Cycle Time")]
        public Decimal ProductCycleTime { get; set; }
        public bool isChecked { get; set; }
    }
    public class PMMachineVM
    {
        public int MachineID { get; set; }
        [Required]
        [Display(Name = "Machine")]
        public string MachineName { get; set; }
        [Required]
        [Display(Name = "Machine Code")]
        public string MachineCode { get; set; }
        public string MachineCategory { get; set; }
        [Required]
        [Display(Name = "Location")]
        public string MachineLocation { get; set; }
        public string MachineDemandType { get; set; }
        public bool isChecked { get; set; }
    }
    public class ProductMachineVMSelect
    {
        public int OrganizationId { get; set; }
        public string MachineIds { get; set; }
        public int[] arrMachineIds { get; set; }
        public string ProductIds { get; set; }
        public int[] arrProductIds { get; set; }
        public UtilityMasterMapping Mapping { get; set; }

    }
    [Serializable]
    public class UtilityMasterMapping
    {
        public int? RecId { get; set; }
        public int ?OrganizationId { get; set; }
        public int ?MachineId { get; set; }
        public int ?ProductId { get; set; }
        public int ?OperatorId { get; set; }
        public int ?MouldId { get; set; }
        public int ?OperationId { get; set; }
        public int ?DieId { get; set; }
        public int? TargetProduction { get; set; }
        public int? MouldCavity { get; set; }
        public int? DieCavity { get; set; }

    }

    public class ProductMachineMachine
    {
        public string MachineIds { get; set; }
        public string ProductIds { get; set; }
        public int OrganizationId { get; set; }
    }
    public class ProductMachineVM
    {
        public int MachineID { get; set; }
        [Required]
        [Display(Name = "Machine")]
        public string MachineName { get; set; }
        [Required]
        [Display(Name = "Machine Code")]
        public string MachineCode { get; set; }
        public string MachineLocation { get; set; }
        public string MachineDemandType { get; set; }
        public int ProductId { get; set; }
        [Display(Name = "Product Code")]
        [Required]
        public string ProductCode { get; set; }
        [Required]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        public string ProductDemandType { get; set; }
        [Display(Name = "Unit")]
        public string ProductUnit { get; set; }
        [Required]
        [Display(Name = "Cycle Time")]
        public Decimal ProductCycleTime { get; set; }
        public Boolean IsMapped { get; set; }
        public  UtilityMasterMapping mapping { get; set; }

    }
    public class ProductMachineRequestModel
    {
        [Required]
        public int MachineID { get; set; }
        public string MachineDemandType { get; set; }
        [Required]
        public int ProductId { get; set; }
        public Decimal ProductCycleTime { get; set; }
        public bool IsMapped { get; set; }
    }
}
