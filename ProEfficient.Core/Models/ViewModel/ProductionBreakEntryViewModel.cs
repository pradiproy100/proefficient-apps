﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace ProEfficient.Core.Models.ViewModel
{
   
    public class ProductionBreakEntryViewModel
    {
        public int CurrentOrganizationId { get; set; }
        public int CurrentUserId { get; set; }
        public int ActiveIndex { get; set; }
        
        [Display(Name = "Supervisor")]       
        public int? SupervisorID { get; set; }  

       
        
        [Display(Name = "Production Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ProductionDate { get; set; }
        

        [Display(Name = "Shift")]
        public int CurrentShiftId { get; set; }
        public List<SelectListItem> lstAllShift { get; set; }
        public List<Shift> lstAllShiftData { get; set; }
        [Display(Name = "Remarks")]
        public String Remarks { get; set; }
        [Display(Name = "Machine")]
        //[Required]
        public int CurrentMachineId { get; set; }
        public List<SelectListItem> lstAllMachine { get; set; }

        [Display(Name = "Operator")]
        //[Required]
        public int? OperatorID { get; set; }

        public List<SelectListItem> lstAllOperators { get; set; }
        [Display(Name = "Product")]
        //[Required]
        public int CurrentProductId { get; set; }       
        
        public List<SelectListItem> lstAllProduct { get; set; }
        public List<ProductMachineVM> lstvmAllProduct { get; set; }



        [Display(Name = "Stoppage Reason")]
        public int StoppageReaseonID { get; set; }
        public List<SelectListItem> lstAllStopageReason { get; set; }

        public List<SubStoppageReason> lstAllSubStopageReason { get; set; }

        public List<RejectReason> lstAllRejectReason { get; set; }
      
        public Boolean IsSubmitted { get; set; }

    }
    public class ProductionPageVM
    {
        public List<ProductionBreakEntryShiftVM> lstshiftData { get; set; }
        public List<ProductionBreakEntryRejectionVM> lstrejectionData { get; set; }
        public List<ProductionBreakEntryStopageVM> lststopageData { get; set; }
        public List<ProductionBreakEntryProductMachineVM> lstmachineProductData { get; set; }
        public List<ProductionBreakEntryUserVM> lstuserData { get; set; }
        public List<RejectionReasonModel> lstRejectionReason { get; set; }

    }
    public class ProductionBreakEntryShiftVM
    {
        public int ShiftId { get; set; }
        [Required]
        [Display(Name = "Shift Name")]
        public string ShiftName { get; set; }
        [Required]
        [Display(Name = "Shift Description")]
        public string ShiftDescription { get; set; }
        //[Required]
        [Display(Name = "Start Time")]
        public decimal? StartTime { get; set; }
        //[Required]
        [Display(Name = "End Time")]
        public decimal? EndTime { get; set; }
        [Display(Name = "Overnight")]
        public bool isOverNight { get; set; }

    }
    public class ProductionBreakEntryRejectionVM
    {
        public string Rejectiondiscription { set; get; }
        public int RejectionreasonID { get; set; }
    }
    public class ProductionBreakEntryStopageVM
    {       
        public string ReasonName { set; get; }
        public int ReasonId { get; set; }
        public string SubReasonName { set; get; }
        public int SubReasonId { get; set; }
    }
    public class ProductionBreakEntryProductMachineVM
    {
        public int MachineId { get; set; }        
        public string MachineName { get; set; }
        public string MachineCode { set; get; }
        public string MachineDemandType { set; get; }

        public int ProductId { get; set; }
        public string ProductName { set; get; }
        public string ProductCode { set; get; }        
        public string ProductDemandType { set; get; }

        public string ProductUnit { set; get; }
       
    }
    public class ProductionBreakEntryUserVM
    {
        public int UserId { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
        public string UserTypeName { get; set; }

    }
}