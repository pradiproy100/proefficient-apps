﻿namespace ProEfficient.Core.Models.ViewModel
{
    public class ProductionBreakdownEntryModel
    {
        public int QualityEntryId { get; set; }
        public int ProductionEntryId { get; set; }
        public int ReasonId { get; set; }
        public string ReasonName { get; set; }
        public int ProdCount { get; set; }
    }
}