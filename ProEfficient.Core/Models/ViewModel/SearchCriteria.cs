﻿namespace ProEfficient.Core.Models.ViewModel
{
    public class SearchCriteria
    {

        public string MachineID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ShiftID { get; set; }
    }
}