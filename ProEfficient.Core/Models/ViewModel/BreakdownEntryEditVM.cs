﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProEfficient.Core.Models.ViewModel
{
   public class BreakdownEntryEditVM
    {
        public int BreakdownId { get; set; }
        public int MachineID { get; set; }
        public int ReasonId { get; set; }
        public int SubReasonId { get; set; }
        public string MachineName { get; set; }
        public string ShiftName { get; set; }
        public string TotalStoppageTime { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ProductionDate { get; set; }
        public string Remarks { get; set; }
    }
    public class BreakdownEntryEditPageVM
    {
       
        public List<SelectListItem> lstAllShift { get; set; }
        public List<SelectListItem> lstAllMachine { get; set; }
        public List<SelectListItem> lstAllStopageReason { get; set; }
        public List<ProductionBreakEntryStopageVM> lstAllSubStopageReason { get; set; }
        public int OrganizationId { get; set; }
        public int UserId { get; set; }
        public List<BreakdownEntryEditVM> lstBreakdownEntryEditVM { get; set; }
        public BreakdownSearchVM breakdownSearchVM { get; set; }
    }
    public class BreakdownSearchVM
    {
        [Required]
        [Display(Name = "Production Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public string ProductionDate { get; set; }
        [Required]
        [Display(Name = "Shift")]
        public int ShiftId { get; set; }
        public List<SelectListItem> lstAllShift { get; set; }
    }
}
