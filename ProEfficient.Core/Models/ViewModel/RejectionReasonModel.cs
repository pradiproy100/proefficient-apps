﻿using System;

namespace ProEfficient.Core.Models.ViewModel
{
    public class RejectionReasonModel
    {
        public int ReasonId { get; set; }
        public String ReasonName { get; set; }
        public int ReasonQuantity { get; set; }
        public int TotalCount { get; set; }
    }
}