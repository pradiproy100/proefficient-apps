﻿namespace ProEfficient.Core.Models
{
    public class BreakInfo
    {
        public int BreakId { get; set; }
        public int? ShiftId { get; set; }
        public string BreakName { get; set; }
        public decimal? StartTime { get; set; }
        public decimal? EndTime { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public bool? IsFirst { get; set; }
        public string strBreakStartTime { get; set; }
        public string strBreakEndTime { get; set; }
    }

}