﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class UtilityModule
    {
        public int? RecId { get; set; }
        public string? ModuleName { get; set; }
        public int? OrganizationId { get; set; }
        public int? IsActive { get; set; }
    }

    public class UtilityModuleParam
    {
       public int? MouldId { get; set; }
       public int? DieId { get; set; }
       public int? OperatorId { get; set; }
       public int? OperationId { get; set; }
        public int OrganizationId { get; set; }
    }


}
