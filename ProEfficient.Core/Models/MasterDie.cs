﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class MasterDie
    {
        public int DieId { get; set; }
        [Display(Name = "Die Description")]
        public string DieDescription { get; set; }
        [Display(Name = "Cavity Count")]
        public int? AvailableCavityCount { get; set; }

        public int? AddBy { get; set; }

        public DateTime? AddDate { get; set; }

        public int? EditBy { get; set; }

        public DateTime? EditDate { get; set; }

        public bool? IsActive { get; set; }

        public int? OrganizationId { get; set; }

    }

}
