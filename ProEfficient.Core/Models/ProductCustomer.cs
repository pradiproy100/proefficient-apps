﻿namespace ProEfficient.Core.Models
{
    public class ProductCustomer
    {
        public string CustomerName { get; set; }
        public int CustomerID { get; set; }
    }
}