﻿using System.Collections.Generic;

namespace ProEfficient.Core.Models.ChartModel
{
    public class ChartData
    {
        public string name { get; set; }
        public string type { get; set; }
        public bool showInLegend { get; set; }
        public int lineThickness { get; set; }
        public List<DataPoint> dataPoints { get; set; }
        public ChartData()
        {
            type = "spline";
            showInLegend = true;
            lineThickness = 1;
        }
    }
    public class DataPoint
    {

        public double y { get; set; }
        public string label { get; set; }
    }


}