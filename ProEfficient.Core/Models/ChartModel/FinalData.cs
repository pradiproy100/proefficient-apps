﻿using System.Collections.Generic;
using System.Data;

namespace ProEfficient.Core.Models.ChartModel
{

    public class FinalData
    {
        public List<ChartData> data { set; get; }
        public DataTable DT { get; set; }
        public DataTable DT1 { get; set; }
        public DataTable DT2 { get; set; }
        public DataTable DT3 { get; set; }
        public DataTable DT4 { get; set; }
        public DataTable DT5 { get; set; }
        public DataTable MachinewsiedataPareto { get; set; }
        public List<JsonDataModalClass> jsonslst { set; get; }
        public FinalData()
        {
            jsonslst = new List<JsonDataModalClass>();
            data = new List<ChartData>();
            DT = new DataTable();
            DT1 = new DataTable();
            DT2 = new DataTable();
            DT3 = new DataTable();
            DT4 = new DataTable();
            DT5 = new DataTable();
            MachinewsiedataPareto = new DataTable();
        }
    }


}