﻿using System;

namespace ProEfficient.Core.Models.ChartModel
{
    public class BreakDownLine
    {

        public String Date { get; set; }
        public String ShiftName { get; set; }
        public String CY { get; set; }
        public String AR { get; set; }
        public String PR { get; set; }
        public String QR { get; set; }
        public String TP { get; set; }
        public String PT { get; set; }
        public String SP { get; set; }
        public String OEE { get; set; }

    }

}