﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class LoginModel
    {
        [Display(Name = "Email")]
        [Required]
        public String Email { get; set; }
        [Display(Name = "Password")]
        [Required]
        public String Password { get; set; }
        //[Display(Name = "Organization")]
        //public int OrganizationId { get; set; }
       
    }
}