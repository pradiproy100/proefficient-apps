﻿using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class MasterOperation
    {
        public int OperationId { get; set; }      

        public int? OrganizationId { get; set; }
        [MaxLength(100)]
        [MinLength(2)]
        [Display(Name = "Operation Description")]
        public string OperationDescription { get; set; }

        public int? AddBy { get; set; }

        public int? EditBy { get; set; }

        public bool? IsActive { get; set; }

    }

}
