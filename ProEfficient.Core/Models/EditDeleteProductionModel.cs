﻿namespace ProEfficient.Core.Models
{
    public class EditDeleteProductionModel
    {
        public int ProductionID { set; get; }
        public string productName { set; get; }
        public string ProductionDate { set; get; }
        public string MachineName { set; get; }
        public string OperatorName { set; get; }
        public decimal TotlaProduction { set; get; }
        public string RejectionList { get; set; }
        public decimal Rework { set; get; }

    }
}