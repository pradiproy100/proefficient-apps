﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class User : IdentityUser<int>
    {
        public int OrganizationId { get; set; }
        public int UserTypeId { get; set; }
        public bool IsActive { get; set; }
        public int AddBy { get; set; }
        public int EdittedBy { get; set; }



        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
    public class UserRole : IdentityUserRole<int>
    {
        public virtual User user { get; set; }
        public virtual Role Role { get; set; }
    }
    public class Role : IdentityRole<int>
    {
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
