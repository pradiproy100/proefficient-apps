﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class Shift
    {
        public int ShiftId { get; set; }
        [Required]
        [Display(Name = "Shift Name")]
        public string ShiftName { get; set; }
        [Required]
        [Display(Name = "Shift Description")]
        public string ShiftDescription { get; set; }
        //[Required]
        [Display(Name = "Start Time")]
        public decimal? StartTime { get; set; }
        //[Required]
        [Display(Name = "End Time")]
        public decimal? EndTime { get; set; }
        [Required]
        [Display(Name = "Organization Name")]
        public int? OrganizationId { get; set; }
        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }
        public DateTime ActualStartTime { get; set; }
        public DateTime ActualEndTime { get; set; }
        public int? AddBy { get; set; }

        public int? EdittedBy { get; set; }

        public List<BreakInfo> _BreakInfo { get; set; }

        public string BreakName { get; set; }

        public string BreakStartTime { get; set; }

        public string BreakEndTime { get; set; }

        public String ShiftInfoList { get; set; }

        public string strStartTime { get; set; }

        public string strEndTime { get; set; }

        [Display(Name = "Overnight")]
        public bool isOverNight { get; set; }

        public Shift()
        {
            isOverNight = false;
            _BreakInfo = new List<BreakInfo>();
        }
    }
}