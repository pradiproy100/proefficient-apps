﻿using ProEfficient.Core.Utility;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Models
{
    public class ProductionEntry
    {
        public int? ProductionId { get; set; }
        [Display(Name = "Machine")]
        public int? MachineID { get; set; }
        [Display(Name = "Machine Category")]
        public int? MachineCategoryID { get; set; }
        public string MachineName { get; set; }
        [Display(Name = "Product")]
        [Required]
        public int? ProductID { get; set; }
        public string ProductName { get; set; }
        [Display(Name = "User")]
        [Required]
        public int? UserID { get; set; }
        [Display(Name = "Operator")]
        [Required]
        public int? OperatorID { get; set; }
        [Display(Name = "Supervisor")]
        [Required]
        public int? SupervisorID { get; set; }
        public int? OrganizationId { get; set; }
        [Display(Name = "Time Of Entry")]
        public DateTime? TimeOfEntry { get; set; }
        [Display(Name = "Shift")]
        [Required]
        public int? ShiftId { get; set; }
        [Required]
        [Display(Name = "Start Time")]
        public Decimal ProductionStartTime { get; set; }
        [Display(Name = "Start Time")]
        public String sProductionStartTime
        {
            get
            {
                try
                {
                    return ProductionStartTime.TwoDecimalFormat();
                }
                catch (Exception)
                {
                    return "0.00";
                }

            }

            set

            {
                ProductionStartTime = value.ToStringWithNullEmpty().Replace(":", ".").ToDecimal0();
            }
        }
        //[Required]
        [Display(Name = "Start Time")]
        public DateTime? StartTime { get; set; }
        [Required]
        [Display(Name = "End Time")]
        public Decimal ProductionEndTime { get; set; }
        [Display(Name = "End Time")]
        public String sProductionEndTime
        {
            get
            {
                try
                {

                    return ProductionEndTime.TwoDecimalFormat();
                }

                catch (Exception)
                {
                    return "0.00";
                }
            }

            set

            {
                ProductionEndTime = value.ToStringWithNullEmpty().Replace(":", ".").ToDecimal0();
            }
        }
        [Display(Name = "End Time")]
        //[Required]
        public DateTime? EndTime { get; set; }
        [Required]
        [Display(Name = "Total Production")]
        public decimal? TotalProduction { get; set; }
        [Display(Name = "Rework")]
        public int? ReWork { get; set; }
        [Display(Name = "Rejection")]
        public int? Rejection { get; set; }
        public DateTime ProductionDate { get; set; }
        public String ProdDate
        {
            get
            {
                if (ProductionDate != null)
                {
                    return ProductionDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
        }
        public bool IsSaved { get; set; }
        public string OperatorName { get; set; }
        public string RejectionReasons { get; set; }
        public string rejectionlist { get; set; }
        public string deletedEntries { get; set; }
    }


}