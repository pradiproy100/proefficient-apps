﻿namespace ProEfficient.Core.Models
{
    public class SubStoppageReason
    {
        public int ReasonID { set; get; }
        public int SubReasonID { set; get; }
        public string SubReasonName { set; get; }
        public string SubReasonCode { set; get; }
    }
}