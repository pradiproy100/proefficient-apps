﻿using System;

namespace ProEfficient.Core.Models
{
    public class AuditLog
    {
        public int Logid { get; set; }
        public String Message { get; set; }
        public String ModuleName { get; set; }
        public int Level { get; set; }
        public DateTime LogDate { get; set; }
    }
}