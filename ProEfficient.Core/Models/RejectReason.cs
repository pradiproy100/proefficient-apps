﻿namespace ProEfficient.Core.Models
{
    public class RejectReason
    {
        public string Rejectiondiscription { set; get; }
        public int RejectionreasonID { get; set; }
        public int RejectionValue { get; set; }
    }
}