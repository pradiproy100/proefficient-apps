using System;
using System.Collections.Generic;

namespace ProEfficient.Core.Auth.DTO
{
    public class UserForDetailedDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int OrganizationId { get; set; }
        public String OrganizationName { get; set; }
        public int UserTypeId { get; set; }
        public String UserTypeName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

    }
}