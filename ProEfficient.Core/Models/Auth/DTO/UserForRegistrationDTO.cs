using System;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Auth.DTO
{
    public class UserForRegistrationDTO
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "User Name")]
        public string Username { get; set; }
        
        [Required]
        [StringLength(16, MinimumLength=4,ErrorMessage="Must specify password between 4 and 16 characters.")]
        public string Password { get; set; }
        
        [Required]
        public string Email { get; set; }
       
        [Required]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        
        [Required]
        [Display(Name = "Organization")]
        public int OrganizationId { get; set; }
        
        [Required]
        [Display(Name = "User Type")]
        public int UserTypeId { get; set; }

        [Display(Name = "Organization")]
        public String OrganizationName { get; set; }
        [Display(Name = "User Type")]
        public String UserTypeName { get; set; }

        [Required]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public int AddBy { get; set; }
        public int EdittedBy { get; set; }

        public UserForRegistrationDTO()
        {
            IsActive = true;
            
        }
    }
}