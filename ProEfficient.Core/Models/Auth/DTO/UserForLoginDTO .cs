using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Core.Auth.DTO
{
    public class UserForLoginDTO
    {
        [Required]
        public string Email { get; set; }
        [Required]
        [StringLength(20, MinimumLength=4,ErrorMessage="Must specify password between 4 and 20 characters.")]
        public string Password { get; set; }
    }
}