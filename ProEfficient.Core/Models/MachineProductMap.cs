﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProEfficient.Core.Models
{
    public class MachineProductMap
    {
        public int MachineID { get; set; }
        public String MachineName { get; set; }
        public int ProductID { get; set; }
        public String ProductName { get; set; }
        public int OperatorId { get; set; }
        public int MouldId { get; set; }
        public int OperationId { get; set; }
        public int DieId { get; set; }
        public int TargetProduction { get; set; }
        public int MouldCavity { get; set; }
        public int DieCavity { get; set; }
        public decimal ProductCycleTime { get; set; }

    }
}
