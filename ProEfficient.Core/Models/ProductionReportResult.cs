﻿using System;

namespace ProEfficient.Core.Models
{
    public class ProductionReportResult
    {
        public DateTime TimeOfEntry { get; set; }
        public String DateOfEntry { get; set; }
        public int OperatorID { get; set; }
        public String OperatorName { get; set; }
        public int MachineID { get; set; }
        public String MachineName { get; set; }
        public String ProductCode { get; set; }
        public int ProductID { get; set; }
        public String ProductName { get; set; }
        public String ShiftName { get; set; }
        public int ShiftID { get; set; }
        public String OperationName { get; set; }
        public int OperationID { get; set; }
        public String DieName { get; set; }
        public int DieID { get; set; }
        public String MouldName { get; set; }
        public int MouldID { get; set; }
        public int TotalProduction { get; set; }
        public int TargetProduction { get; set; }
        public int MouldCavity { get; set; }
        public int DieCavity { get; set; }
    }
}
