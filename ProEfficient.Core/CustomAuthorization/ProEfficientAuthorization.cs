﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ProEfficient.Core.Models;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;


namespace ProEfficient.Core.CustomAuthorization
{
    public class ProEfficientAuthorization : AuthorizeAttribute, IAuthorizationFilter
    {

        private readonly string[] allowedroles;

        public ProEfficientAuthorization(params string[] roles)
        {
            this.allowedroles = roles;
        }
       
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var data = context.HttpContext.Session.GetString("CurrentUser");
            if (data == null || data=="null")
            {
                context.Result = new RedirectResult("~/Login/Login");

            }
            else
            {
               
                var user = JsonConvert.DeserializeObject<APIUser>(data);

                if (allowedroles.Length>0 && !allowedroles.Contains(user.UserTypeName)) // Here you can place your logic to check the roles and permissions
                {
                    context.Result = new RedirectResult("~/Login/Login");
                }
            }

        }
    }

    public class ApiKeyAuthOpts : AuthenticationSchemeOptions
    {
    }
    public class ApiKeyAuthHandler : AuthenticationHandler<ApiKeyAuthOpts>
    {
        public ApiKeyAuthHandler(IOptionsMonitor<ApiKeyAuthOpts> options, ILoggerFactory logger, UrlEncoder encoder, Microsoft.AspNetCore.Authentication.ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }


        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (Context.Session.GetString("CurrentUser") == null || Context.Session.GetString("CurrentUser") == "null")
            {
                return AuthenticateResult.Fail("Unauthorize");
                //return AuthenticateResult.NoResult();


            }
            else
            {
                string token = "Authorize";



                var id = new ClaimsIdentity(
                    new Claim[] { new Claim("Key", token) },  // not safe , just as an example , should custom claims on your own
                    Scheme.Name
                );
                ClaimsPrincipal principal = new ClaimsPrincipal(id);
                var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(), Scheme.Name);
                return AuthenticateResult.Success(ticket);
            }

        }
        protected override Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Context.Response.Redirect("/Login/Login");// redirect to your error page
            return Task.CompletedTask;
        }
    }

}