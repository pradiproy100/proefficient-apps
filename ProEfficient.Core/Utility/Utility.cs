﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace ProEfficient.Core.Utility
{
    public static class UtilityAll
    {

        private static IConfiguration Configuration;

        public static void Configure(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public static string SessionErrorMessage
        {
            get
            {
                return "sessionErrorMessage";
            }
        }
        public static string SessionSuccessMessage
        {
            get
            {
                return "sessionSuccessMessage";
            }
        }


        public static String ToStringx(this object val)
        {
            try
            {
                return Convert.ToString(val);
            }
            catch (Exception Ex)
            {
                return "";
            }
        }
        public static String GetUserTypeName(long TypeId)
        {
            switch (TypeId)
            {
                case 1:
                    return "Admin";
                case 2:
                    return "Approver";
                case 3:
                    return "Normal";
                default:
                    return "";
            }
        }
        public static Guid GetUserTypeIdByName(String UserTypeName)
        {
            switch (UserTypeName.ToLower())
            {
                case "admin":
                    return new Guid("b0d8e49d-58b3-441a-b1d8-116de8376b24");
                case "approver":
                    return new Guid("0cc2442e-00ac-4bcc-ad6b-43974f8dd06d");
                default:
                    return new Guid("ffdbfefe-ec9d-436b-a317-612efbd2832f");


            }
        }
        public static Boolean IsUndefinedOrnull(this String val)
        {
            if (String.IsNullOrEmpty(val))
            {
                return true;
            }
            else if (val.ToLower() == "undefined")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static String ToStringWithNullEmpty(this object value, Boolean IsNullAllowed = true)
        {
            if (IsNullAllowed)
            {
                return Convert.ToString(value);
            }
            else
            {
                return (value == null ? String.Empty : Convert.ToString(value));
            }
        }


        public static double Todouble(this object value)
        {
            double Value = 0.0;
            double.TryParse(value.ToString(), out Value);
            return Value;

        }


        public static DateTime ToDateTimeNew(string value)
        {
            DateTime date = new DateTime();
            DateTime.TryParse(value.ToString(), out date);
            return date;

        }




        public static Int32 ToInteger(this object value)
        {
            return Convert.ToInt32(value);
        }
        public static Int32 ToInteger0(this object value)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch (Exception Ex)
            {
                return 0;

            }

        }
        public static string TwoDecimalFormat(this Decimal myNumber)
        {
            var s = string.Format("{0:0.00}", myNumber);

            if (s.EndsWith("00"))
            {
                return ((int)myNumber).ToString();
            }
            else
            {
                return s;
            }
        }
        public static Int32 ToIntegerD0(this Decimal value)
        {
            try
            {
                return Convert.ToInt32(Math.Floor(value));
            }
            catch (Exception Ex)
            {
                return 0;

            }

        }


        //return "";
        // public static String AdminEmailId => Configuration.GetValue<string>("AdminEmailId");


        public static String FromEmailsmtpClient => Configuration.GetValue<string>("FromEmailsmtpClient");

        public static String PublishTopic => Configuration.GetValue<string>("PublishTopic");
        public static String UtilityModulePassword => Configuration.GetValue<string>("UtilityModulePassword");
        public static String SubscribeTopic => Configuration.GetValue<string>("SubscribeTopic");
        public static String MqttIpAddress => Configuration.GetValue<string>("MqttIpAddress");
        public static String MqttPortNo => Configuration.GetValue<string>("MqttPortNo");
        public static String ClientId => Configuration.GetValue<string>("ClientId");

        public static String FromEmailId => Configuration.GetValue<string>("FromEmailId");

        public static String FromEmailPassword => Configuration.GetValue<string>("FromEmailPassword");

        public static int FromEmailsmtpClientPort => Configuration.GetValue<int>("FromEmailsmtpClientPort");


        public static Boolean SendEmail(String ToEmail, String Subject, String EmailMessage, String AttachmentPath)
        {
            try
            {

                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(UtilityAll.FromEmailId);
                // Recipient e-mail address.
                Msg.To.Add(ToEmail);
                Msg.Subject = Subject;
                Msg.IsBodyHtml = true;
                Msg.Body = EmailMessage;
                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Host = UtilityAll.FromEmailsmtpClient;
                smtp.Port = UtilityAll.FromEmailsmtpClientPort;
                smtp.Credentials = new System.Net.NetworkCredential(UtilityAll.FromEmailId, UtilityAll.FromEmailPassword);
                smtp.EnableSsl = true;
                System.Net.Mail.Attachment attachment;
                Thread.Sleep(1000);
                attachment = new System.Net.Mail.Attachment(AttachmentPath);
                Msg.Attachments.Add(attachment);
                smtp.Send(Msg);


                //var client = new SmtpClient("smtp.gmail.com", 587)
                //{
                //    Credentials = new NetworkCredential(UtilityAll.FromEmailId, UtilityAll.FromEmailPassword),
                //    EnableSsl = true
                //};
                //client.Send(UtilityAll.FromEmailId, ToEmail, "test", "testbody");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static Boolean ToBoolean(this object value)
        {
            try
            {
                return Convert.ToBoolean(value);

            }
            catch (Exception Ex)
            {
                return false;

            }

        }
        public static DateTime? ToDateTimeWithNull(this object value)
        {
            try
            {
                if (value == null)
                {
                    return null;
                }
                else
                {
                    return Convert.ToDateTime(value);
                }
            }
            catch (Exception Ex)
            {
                return null;
            }


        }

        public static Decimal ToDecimal(this object value)
        {
            return Convert.ToDecimal(value);
        }
        public static Decimal ToDecimal0(this object value)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch (Exception Ex)
            {
                return 0;

            }

        }
        public static DateTime ToDateTime(this object value)
        {

            return Convert.ToDateTime(value);

        }
        public static DateTime ToDateTime_yyyyMMdd(this string value)
        {

            string format = "yyyy-MM-dd";            
           return DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);

        }

        public static int GetIntPart(this Decimal value)
        {
            int integral = value.ToIntegerD0();
            return integral;
        }
        public static int GetFractionPart(this Decimal value)
        {
            int integral = (int)(((decimal)value % 1) * 100);
            return integral;
        }
        public static Guid ToGuid(this object value)
        {
            try
            {
                return new Guid(value.ToStringWithNullEmpty());
            }
            catch (Exception Ex)
            {
                return Guid.Empty;

            }


        }

        public static Boolean IsNull(this object value)
        {
            if (value == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static T ToEnum<T>(this int id)
        {
            T val = (T)Enum.ToObject(typeof(T), id);
            return val;
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }


        public static TimeSpan getTimeDifference(Decimal StartTime, Decimal EndTime)
        {
            TimeSpan Start = new TimeSpan(StartTime.GetIntPart(), StartTime.GetFractionPart(), 0);
            TimeSpan End = new TimeSpan(EndTime.GetIntPart(), EndTime.GetFractionPart(), 0);
            return (Start - End);
        }





        public static double doubleTP(this string OrigVal)
        {

            double dblVal = 0;
            double.TryParse(OrigVal, out dblVal);
            return dblVal;
        }
        public static int intTP(this string OrigVal)
        {
            int dblVal = 0;
            int.TryParse(OrigVal, out dblVal);
            return dblVal;
        }
        public static long longTP(this string OrigVal)
        {
            long dblVal = 0;
            long.TryParse(OrigVal, out dblVal);
            return dblVal;
        }
        public static DateTime dtTP(this string OrigVal)
        {
            DateTime dblVal = new DateTime();
            DateTime.TryParse(OrigVal, out dblVal);
            return dblVal;
        }
        public static DataTable ListToDataTable<T>(this IList<T> data, string tableName)
        {
            DataTable table = new DataTable(tableName);

            //special handling for value types and string
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {

                DataColumn dc = new DataColumn("Value");
                table.Columns.Add(dc);
                foreach (T item in data)
                {
                    DataRow dr = table.NewRow();
                    dr[0] = item;
                    table.Rows.Add(dr);
                }
            }
            else
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in properties)
                {
                    table.Columns.Add(prop.Name,
                    Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        try
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        catch (Exception ex)
                        {
                            row[prop.Name] = DBNull.Value;
                        }
                    }
                    table.Rows.Add(row);
                }
            }
            return table;
        }
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
        public static DataTable ToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public static DataTable Transpose(DataTable dt)
        {
            DataTable dtNew = new DataTable();

            //adding columns    
            for (int i = 0; i <= dt.Rows.Count; i++)
            {
                dtNew.Columns.Add("Col" + i.ToString());
            }
            //Changing Column Captions: 
            dtNew.Columns[0].ColumnName = "Col0";

            //Adding Row Data
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                DataRow r = dtNew.NewRow();
                r[0] = dt.Columns[k].ToString();
                for (int j = 1; j <= dt.Rows.Count; j++)
                    r[j] = dt.Rows[j - 1][k];
                dtNew.Rows.Add(r);
            }

            return dtNew;
        }




    }
}