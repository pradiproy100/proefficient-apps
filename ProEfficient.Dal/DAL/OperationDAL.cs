﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class OperationDAL : CoreDBAccess
    {

        private DynamicParameters pUpdateMasterOperation(MasterOperation pMasterOperation)
        {
            var parameters = new DynamicParameters(pMasterOperation);
            return parameters;
        }  
        public Boolean UpdateMasterOperation(MasterOperation pMasterOperation)
        {

            String StoredProcedureName = "USP_CreateUpdateOperation";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterOperation(pMasterOperation));
            return result;
        }


        public List<MasterOperation> GetMasterOperationList(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetOperations";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<MasterOperation>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }

        }
        public MasterOperation GetMasterOperationById(int OrganizationId,int OperationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetOperationById";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                param.Add("OperationId", OperationId);
                var result = GetList<MasterOperation>(StoredProcedureName, param);

                return result.FirstOrDefault();
            }
            catch (Exception)
            {

                return null;
            }

        }


        public OperationDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}