﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProEfficient.Dal.DAL
{
    public class ProductDieDAL : CoreDBAccess
    {
        public ProductDieDAL(IConfiguration configuration) : base(configuration)
        {
        }
        public Boolean USP_SaveProductDie(Product_Die_Rel prel)
        {
            String StoredProcedureName = "USP_CreateProduct_Die_Rel";
            var result = ExecuteProcedure(StoredProcedureName,
                pUSP_SaveProductDie(prel));
            return result;
        }
        public Boolean USP_DeleteProductDieRel(int organizationId, int productID)
        {
            String StoredProcedureName = "USP_DeleteProduct_Die_Rel";
            DynamicParameters param = new DynamicParameters();
            param.Add("OrganizationId", organizationId);
            param.Add("ProductID", productID);
            var result = ExecuteProcedure(StoredProcedureName, param);
            return result;
        }
        private DynamicParameters pUSP_SaveProductDie(Product_Die_Rel prel)
        {
            var parameters = new DynamicParameters(prel);
            return parameters;
        }

        public List<Product_Die_Rel> GetProductDieRel(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetProDuctDieRels";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<Product_Die_Rel>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
