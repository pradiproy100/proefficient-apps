﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class MachineProductDAL : CoreDBAccess
    {

       private SqlParameter[] pGetMachineProductMap(ProductMachineVMSelect pMachineProductSelect)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineIds", pMachineProductSelect.MachineIds));
            lstParameter.Add(new SqlParameter("@ProductIds", pMachineProductSelect.ProductIds));
            lstParameter.Add(new SqlParameter("@OrganizationID", pMachineProductSelect.OrganizationId));

            return lstParameter.ToArray();
        }
        private SqlParameter[] pGetMachineProduct(int OrganizationId, string mode)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationID", OrganizationId));
            lstParameter.Add(new SqlParameter("@mode", mode));

            return lstParameter.ToArray();
        }

        public List<ProductMachineVM> GetMachineProductList(ProductMachineVMSelect pMachineProductSelect)
        {
            String StoredProcedureName = "GetMachineProductMap";
            Boolean IsSuccess;
            var result = (List<ProductMachineVM>)ExecuteReaderProcedure(
                StoredProcedureName, MachineProductMapping
               , out IsSuccess, pGetMachineProductMap(pMachineProductSelect));

            return result;

        }
        public Boolean UpdateMasterMapping(UtilityMasterMapping pMastermapp)
        {
            String StoredProcedureName = "SaveUtilityMapping";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterMap(pMastermapp));
            return result;
        }

        public Boolean USP_SaveUtilityModule(UtilityModuleParam pMastermapp)
        {
            String StoredProcedureName = "USP_SaveUtilityModule";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateUtilityModule(pMastermapp));
            return result;
        }

        private DynamicParameters pUpdateUtilityModule(UtilityModuleParam utilityModule)
        {
            var parameters = new DynamicParameters(utilityModule);
            return parameters;
        }

        public List<UtilityModule> GetUtilityModule(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetUtilityModule";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<UtilityModule>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }
        public List<MachineProductMap> GetMachineProductMap(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetMachineProductMap";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<MachineProductMap>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }
        //USP_GetUtilityModule

        private DynamicParameters pUpdateMasterMap(UtilityMasterMapping pMastermap)
        {
            var parameters = new DynamicParameters(pMastermap);
            return parameters;
        }
        public List<UtilityMasterMapping> GetUtilityMasterMapping(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "GetUtilityMasterMapping";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<UtilityMasterMapping>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }

        }
        public List<PMMachineVM> GetMachineList(int OrganizationId, string mode)
        {
            String StoredProcedureName = "GetMachineProductList";
            Boolean IsSuccess;
            var result = (List<PMMachineVM>)ExecuteReaderProcedure(
                StoredProcedureName, MachineMapping
               , out IsSuccess, pGetMachineProduct(OrganizationId, mode));

            return result;

        }
        public List<PMProductVM> GetProductList(int OrganizationId, string mode)
        {
            String StoredProcedureName = "GetMachineProductList";
            Boolean IsSuccess;
            var result = (List<PMProductVM>)ExecuteReaderProcedure(
                StoredProcedureName, ProductMapping
               , out IsSuccess, pGetMachineProduct(OrganizationId, mode));

            return result;

        }
        private List<PMMachineVM> MachineMapping(SqlDataReader Reader)
        {
            List<PMMachineVM> LstProductMachine = new List<PMMachineVM>();
            while (Reader.Read())
            {
                LstProductMachine.Add(new PMMachineVM()
                {                   
                    MachineID = Reader["MachineID"].ToInteger0(),                   
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),                    
                    MachineCode = Reader["MachineCode"].ToStringWithNullEmpty(),                    
                    MachineLocation = Reader["Location"].ToStringWithNullEmpty(),
                    MachineCategory= Reader["MachineCategory"].ToStringWithNullEmpty(),
                    MachineDemandType=Reader["MachineDemandType"].ToStringWithNullEmpty()

                });

            }
            return LstProductMachine;
        }
        private List<PMProductVM> ProductMapping(SqlDataReader Reader)
        {
            List<PMProductVM> LstProduct = new List<PMProductVM>();
            while (Reader.Read())
            {
                LstProduct.Add(new PMProductVM()
                {                    
                    ProductId = Reader["ProductId"].ToInteger0(),
                    ProductName = Reader["ProductName"].ToStringWithNullEmpty(),
                    ProductCode = Reader["ProductCode"].ToStringWithNullEmpty(),
                    ProductCustomer = Reader["CustomerName"].ToStringWithNullEmpty(),
                    ProductCategory = Reader["CategoryName"].ToStringWithNullEmpty(),
                    ProductDemandType = Reader["ProductDemandType"].ToStringWithNullEmpty()

                });

            }
            return LstProduct;
        }
        private SqlParameter[] pProductMachine(int orgid, DateTime proddate)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationId", orgid));
            lstParameter.Add(new SqlParameter("@ProductionDate", proddate));
            return lstParameter.ToArray();
        }
        public List<ProductMachineVM> GetProductMachineList(int orgid,DateTime proddate)
        {
            String StoredProcedureName = "USP_GetMachineMapping";
            Boolean IsSuccess;
            var result = (List<ProductMachineVM>)ExecuteReaderProcedure(
                StoredProcedureName, MachineProductMapping
               , out IsSuccess, pProductMachine(orgid,proddate));

            return result;

        }
        private List<ProductMachineVM> MachineProductMapping(SqlDataReader Reader)
        {
            List<ProductMachineVM> LstProductMachine = new List<ProductMachineVM>();
            while (Reader.Read())
            {
                LstProductMachine.Add(new ProductMachineVM()
                {
                    ProductId = Reader["ProductId"].ToInteger0(),
                    MachineID = Reader["MachineID"].ToInteger0(),
                    ProductName = Reader["ProductName"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),                   
                    ProductCode = Reader["ProductCode"].ToStringWithNullEmpty(),
                    MachineCode = Reader["MachineCode"].ToStringWithNullEmpty(),
                    MachineDemandType = Reader["MachineDemandType"].ToStringWithNullEmpty(),
                    ProductDemandType = Reader["ProductDemandType"].ToStringWithNullEmpty(),
                    ProductCycleTime = Reader["ProductCycletime"].ToDecimal0(),
                    MachineLocation = Reader["Location"].ToStringWithNullEmpty(),
                   // ProductUnit= Reader["ProductUnit"].ToStringWithNullEmpty(),
                    IsMapped =Reader["IsMapped"].ToBoolean()
                });

            }
            return LstProductMachine;
        }
        private SqlParameter[] pCreateMachineProductMapping(DataTable dt,int createdBy,string mode)
        {

            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@UDT", dt));
            lstParameter.Add(new SqlParameter("@CreatedBy", createdBy));
            lstParameter.Add(new SqlParameter("@mode", mode));
            return lstParameter.ToArray();
        }
      
        public Boolean CreateMachineProductMapping(DataTable dt,int createdBy, string mode)
        {

            String StoredProcedureName = "CreateMachineProductMap";
            var result = ExecuteProcedure(StoredProcedureName,
                pCreateMachineProductMapping(dt,createdBy,mode));

            return result;
        }
        
        public MachineProductDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}