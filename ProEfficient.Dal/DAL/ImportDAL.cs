﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class ImportDAL : CoreDBAccess
    {

       
        private SqlParameter[] pValidateImportData(string type,int userid,int orgid)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@type", type));
            lstParameter.Add(new SqlParameter("@AddedBy", userid));
            lstParameter.Add(new SqlParameter("@OrgId", orgid));
            return lstParameter.ToArray();
        }      
      

        public Boolean ValidateImporteData(string type,int userid,int orgid)
        {
            String StoredProcedureName = "ValidateImportData";
            var result = ExecuteProcedure(StoredProcedureName,
                pValidateImportData(type,userid,orgid));

            return result;
        }
       
        public ImportDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}