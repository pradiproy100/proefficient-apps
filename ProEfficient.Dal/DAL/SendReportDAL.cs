﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ProEfficient.Dal.DAL
{
    public class SendReportDAL:CoreDBAccess
    {
        private IConfiguration Configuration;
        private SqlParameter[] pPassOrganizationId(int OrganizationId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationID", OrganizationId));
            return lstParameter.ToArray();
        }
        private SqlParameter[] ParametreReportModel(string startDtae,string Enddate,int ORGnizationID,int machineID=0)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@startdate", startDtae));
            lstParameter.Add(new SqlParameter("@enddate", Enddate));
            lstParameter.Add(new SqlParameter("@organizationId", ORGnizationID));
            lstParameter.Add(new SqlParameter("@machineID", machineID));

            return lstParameter.ToArray();
        }
        

        public List<ReportExcelGetConfigModel> GetReportSentList(int orgid)
        {
            String StoredProcedureName = "GetReportToBeSent";
            Boolean IsSuccess;
            var result = (List<ReportExcelGetConfigModel>)ExecuteReaderProcedure(
                StoredProcedureName, ReportSentModelMapping
               , out IsSuccess,pPassOrganizationId(orgid));

            return result;

        }
        private List<ReportExcelGetConfigModel> ReportSentModelMapping(SqlDataReader Reader)
        {
            List<ReportExcelGetConfigModel> LstUser = new List<ReportExcelGetConfigModel>();
            while (Reader.Read())
            {
                LstUser.Add(new ReportExcelGetConfigModel()
                {
                    UserMachineReportID = Reader["ReportGroupId"].ToInteger0(),
                    DateRange = Reader["DateRange"].ToStringWithNullEmpty(),
                    EmailTo = Reader["MailTo"].ToStringWithNullEmpty(),
                    ReportTypeId = Reader["ReportTypeId"].ToInteger0(),
                    ReportName = Reader["ReportName"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }

        public List<ReportExcelOEEModel> GetReportSentOEE(string stdate, string enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDatesScheduler";
            Boolean IsSuccess;
            var result = (List<ReportExcelOEEModel>)ExecuteReaderProcedure(
                StoredProcedureName, ReportSentOEEMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        private List<ReportExcelOEEModel> ReportSentOEEMapping(SqlDataReader Reader)
        {
            List<ReportExcelOEEModel> LstUser = new List<ReportExcelOEEModel>();
            while (Reader.Read())
            {
                LstUser.Add(new ReportExcelOEEModel()
                {
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ARShiftA = Reader["ARtShiftA"].ToDecimal0(),
                    ARShiftB = Reader["ARtShiftB"].ToDecimal0(),
                    ARShiftC = Reader["ARtShiftC"].ToDecimal0(),
                    PRShiftA = Reader["PRtShiftA"].ToDecimal0(),
                    PRShiftB = Reader["PRtShiftB"].ToDecimal0(),
                    PRShiftC = Reader["PRtShiftC"].ToDecimal0(),
                    QRShiftA = Reader["QRtShiftA"].ToDecimal0(),
                    QRShiftB = Reader["QRtShiftB"].ToDecimal0(),
                    QRShiftC = Reader["QRtShiftC"].ToDecimal0(),
                    OEEShiftA = Reader["ShiftA"].ToDecimal0(),
                    OEEShiftB = Reader["ShiftB"].ToDecimal0(),
                    OEEShiftC = Reader["ShiftC"].ToDecimal0(),
                    AROverAll = Reader["ARtOverAll"].ToDecimal0(),
                    PROverAll = Reader["PRtOverAll"].ToDecimal0(),
                    QROverAll = Reader["QRtOverAll"].ToDecimal0(),
                    OEE = Reader["OverAll"].ToDecimal0()
                });

            }
            return LstUser;
        }


        public List<LossReportExcelModel> GetLossReport(string stdate, string enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetLossReportSend";
            Boolean IsSuccess;
            var result = (List<LossReportExcelModel>)ExecuteReaderProcedure(
                StoredProcedureName, LossReportModelMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        private List<LossReportExcelModel> LossReportModelMapping(SqlDataReader Reader)
        {
            List<LossReportExcelModel> LstUser = new List<LossReportExcelModel>();
            while (Reader.Read())
            {
                LstUser.Add(new LossReportExcelModel()
                {
                  
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    Reason = Reader["Reason"].ToStringWithNullEmpty(),                  
                    Duration = Reader["Duration"].ToDecimal0(),
                    Freequency = Reader["Freequency"].ToInteger0()
                });

            }
            return LstUser;
        }
        public List<WaterFallModel> GetWaterfall(string stdate, string enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDatesWaterfall";
            Boolean IsSuccess;
            var result = (List<WaterFallModel>)ExecuteReaderProcedure(
                StoredProcedureName, WaterfallMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        private List<WaterFallModel> WaterfallMapping(SqlDataReader Reader)
        {
            List<WaterFallModel> LstUser = new List<WaterFallModel>();
            while (Reader.Read())
            {
                LstUser.Add(new WaterFallModel()
                {
                    Total_Time = Reader["Total_Time"].ToStringWithNullEmpty(),
                    Shutdown_Time = Reader["Shutdown_Time"].ToStringWithNullEmpty(),
                    Utilisation_Loss = Reader["Utilisation_Loss"].ToStringWithNullEmpty(),
                    Break_Time = Reader["Break_Time"].ToStringWithNullEmpty(),
                    Production_Loss = Reader["Production_Loss"].ToStringWithNullEmpty(),
                    Quality_Loss = Reader["Quality_Loss"].ToStringWithNullEmpty(),
                    Holidays = Reader["Holidays"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }
        public SendReportDAL(IConfiguration configuration) : base(configuration)
        {
            Configuration = configuration;
        }

    }
}
