﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class ShiftDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateShift(Shift pShift)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ShiftId", pShift.ShiftId));
            lstParameter.Add(new SqlParameter("@ShiftName", pShift.ShiftName));
            lstParameter.Add(new SqlParameter("@StartTime", pShift.StartTime));
            lstParameter.Add(new SqlParameter("@EndTime", pShift.EndTime));
            lstParameter.Add(new SqlParameter("@OrganizationId", pShift.OrganizationId));
            lstParameter.Add(new SqlParameter("@ShiftDescription", pShift.ShiftDescription));
            lstParameter.Add(new SqlParameter("@IsActive", pShift.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pShift.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pShift.EdittedBy));
            lstParameter.Add(new SqlParameter("@isOverNight", pShift.isOverNight));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetShift(Shift pShift)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ShiftID", pShift.ShiftId));

            return lstParameter.ToArray();
        }



        public Boolean UpdateShift(Shift pShift)
        {
            String StoredProcedureName = "USP_CreateUpdateShift";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateShift(pShift));

            return result;
        }

        public List<Shift> GetShiftList()
        {
            String StoredProcedureName = "USP_GetShift";
            Boolean IsSuccess;
            var result = (List<Shift>)ExecuteReaderProcedure(
                StoredProcedureName, ShiftMapping
               , out IsSuccess, null);

            return result;

        }

        public Shift GetShift(Shift pShift)
        {
            String StoredProcedureName = "USP_GetShift";
            Boolean IsSuccess;
            var result = (List<Shift>)ExecuteReaderProcedure(
                StoredProcedureName, ShiftMapping
               , out IsSuccess, pGetShift(pShift));

            return result.FirstOrDefault();
        }

        private List<Shift> ShiftMapping(SqlDataReader Reader)
        {
            List<Shift> LstUser = new List<Shift>();
            while (Reader.Read())
            {
                LstUser.Add(new Shift()
                {
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    ShiftDescription = Reader["ShiftDescription"].ToStringWithNullEmpty(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    StartTime = Reader["StartTime"].ToDecimal(),
                    EndTime = Reader["EndTime"].ToDecimal(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    isOverNight = Reader["isOverNight"].ToBoolean()
                });

            }
            return LstUser;
        }


        public ShiftDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}