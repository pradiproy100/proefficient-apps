﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ProEfficient.Dal.DAL
{
    public class RejectionReason : CoreDBAccess
    {
        private SqlParameter[] pGetrejectReason(int OrganisationID)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationId", OrganisationID));
            lstParameter.Add(new SqlParameter("@ReasonId", 0));

            return lstParameter.ToArray();
        }
        public List<RejectReason> GetRejectionReason(int OrganisationID)
        {
            List<RejectReason> rejectreasonlst = new List<RejectReason>();

            String StoredProcedureName = "USP_GetRejectionReason";
            DataSet result =  ExecuteDataSet(StoredProcedureName,
                pGetrejectReason(OrganisationID));


            if (result.Tables.Count > 0)
            {
                for (int j = 0; j < result.Tables[0].Rows.Count; j++)
                {
                    RejectReason rejectReasonobj = new RejectReason();
                    rejectReasonobj.Rejectiondiscription = result.Tables[0].Rows[j]["ReasonName"].ToString();
                    rejectReasonobj.RejectionreasonID = result.Tables[0].Rows[j]["ReasonID"].ToString().ToInteger();
                    rejectreasonlst.Add(rejectReasonobj);
                }
            }



            return rejectreasonlst;
        }

        public RejectionReason(IConfiguration configuration) : base(configuration)
        {
        }
    }
}