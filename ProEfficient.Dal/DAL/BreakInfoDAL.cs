﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class BreakInfoDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateBreakInfo(BreakInfo pBreakInfo)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@BreakId", pBreakInfo.BreakId));
            lstParameter.Add(new SqlParameter("@BreakName", pBreakInfo.BreakName));
            lstParameter.Add(new SqlParameter("@EndTime", pBreakInfo.EndTime));
            lstParameter.Add(new SqlParameter("@StartTime", pBreakInfo.StartTime));
            lstParameter.Add(new SqlParameter("@ShiftId", pBreakInfo.ShiftId));
            lstParameter.Add(new SqlParameter("@IsActive", pBreakInfo.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pBreakInfo.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pBreakInfo.EdittedBy));
            lstParameter.Add(new SqlParameter("@IsFirst", pBreakInfo.IsFirst));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetBreakInfo(BreakInfo pBreakInfo)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ShiftId", pBreakInfo.ShiftId));

            return lstParameter.ToArray();
        }

        public Boolean UpdateBreakInfo(BreakInfo pBreakInfo)
        {

            String StoredProcedureName = "USP_CreateUpdateBreakInfo";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateBreakInfo(pBreakInfo));

            return result;
        }

        public List<BreakInfo> GetBreakInfoList()
        {
            String StoredProcedureName = "USP_GetBreakInfo";
            Boolean IsSuccess;
            var result = (List<BreakInfo>)ExecuteReaderProcedure(
                StoredProcedureName, BreakInfoMapping
               , out IsSuccess, null);

            return result;

        }
        public BreakInfo GetBreakInfo(BreakInfo pBreakInfo)
        {
            String StoredProcedureName = "USP_GetBreakInfo";
            Boolean IsSuccess;
            var result = (List<BreakInfo>)ExecuteReaderProcedure(
                StoredProcedureName, BreakInfoMapping
               , out IsSuccess, pGetBreakInfo(pBreakInfo));

            return result.FirstOrDefault();

        }

        public List<BreakInfo> USP_GetBreakInfoByShiftId(BreakInfo pBreakInfo)
        {
            String StoredProcedureName = "USP_GetBreakInfoByShiftId";
            Boolean IsSuccess;
            var result = (List<BreakInfo>)ExecuteReaderProcedure(
                StoredProcedureName, BreakInfoMapping
               , out IsSuccess, pGetBreakInfo(pBreakInfo));

            return result;

        }


        private List<BreakInfo> BreakInfoMapping(SqlDataReader Reader)
        {
            List<BreakInfo> LstUser = new List<BreakInfo>();
            while (Reader.Read())
            {
                LstUser.Add(new BreakInfo()
                {
                    BreakId = Reader["BreakId"].ToInteger0(),
                    BreakName = Reader["BreakName"].ToStringWithNullEmpty(),
                    EndTime = Reader["EndTime"].ToDecimal(),
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    StartTime = Reader["StartTime"].ToDecimal()
                });

            }
            return LstUser;
        }


        public BreakInfoDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}