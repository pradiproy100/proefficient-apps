﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class DieDAL : CoreDBAccess
    {

        private DynamicParameters pUpdateMasterDie(MasterDie pMasterDie)
        {
            var parameters = new DynamicParameters(pMasterDie);
            return parameters;
        }  
        public Boolean UpdateMasterDie(MasterDie pMasterDie)
        {

            String StoredProcedureName = "USP_CreateUpdateDie";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterDie(pMasterDie));
            return result;
        }


        public List<MasterDie> GetMasterDieList(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetDies";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<MasterDie>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }

        }
        public MasterDie GetMasterDieById(int OrganizationId,int DieId)
        {
            try
            {
                String StoredProcedureName = "USP_GetDieById";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                param.Add("DieId", DieId);
                var result = GetList<MasterDie>(StoredProcedureName, param);

                return result.FirstOrDefault();
            }
            catch (Exception)
            {

                return null;
            }

        }


        public DieDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}