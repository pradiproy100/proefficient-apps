﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ProEfficient.Dal.DAL
{
    public class ProductReasonMappingDAL : CoreDBAccess
    {
        public ProductReasonMappingDAL
            (IConfiguration configuration) : base(configuration)
        {
        }
        private SqlParameter[] pInsertProductionReasonMapping(ProductionBreakdownEntryModel productionBreakdownEntryModel)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@QualityEntryId", productionBreakdownEntryModel.QualityEntryId));
            lstParameter.Add(new SqlParameter("@ProductionEntryId", productionBreakdownEntryModel.ProductionEntryId));
            lstParameter.Add(new SqlParameter("@ReasonId", productionBreakdownEntryModel.ReasonId));
            lstParameter.Add(new SqlParameter("@ProdCount", productionBreakdownEntryModel.ProdCount));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pGetProductionReasonMapping(int productionEntryId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionEntryId", productionEntryId));

            return lstParameter.ToArray();
        }
        public Boolean InsertProductionReasonMapping(List<ProductionBreakdownEntryModel> listProductionBreakdownEntryModel)
        {
            bool result = false;
            String StoredProcedureName = "USP_CreateUpdateProductionRejectionEntry";
            if (listProductionBreakdownEntryModel != null)
            {
                foreach (var item in listProductionBreakdownEntryModel)
                {
                    if (item.QualityEntryId == 0 && item.ProdCount == 0)
                        continue;
                    result = ExecuteProcedure(StoredProcedureName,
                                pInsertProductionReasonMapping(item));
                }
            }
            return result;
        }
        public List<ProductionBreakdownEntryModel> GetProductionReasonMapping(int productionId)
        {
            string StoredProcedureName = "USP_GetProductionRejectionEntry";
            Boolean IsSuccess;
            var result = (List<ProductionBreakdownEntryModel>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionReasonMapping
               , out IsSuccess, pGetProductionReasonMapping(productionId));
            return result;
        }
        private List<ProductionBreakdownEntryModel> ProductionReasonMapping(SqlDataReader Reader)
        {
            List<ProductionBreakdownEntryModel> list =
                                new List<ProductionBreakdownEntryModel>();
            while (Reader.Read())
            {
                list.Add(new ProductionBreakdownEntryModel()
                {
                    QualityEntryId = Reader["QualityEntryId"].ToInteger0(),
                    ProductionEntryId = Reader["ProductionEntryId"].ToInteger0(),
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    ReasonName = Reader["ReasonName"].ToStringWithNullEmpty(),
                    ProdCount = Reader["ProdCount"].ToInteger0()
                });
            }
            return list;
        }
    }
}