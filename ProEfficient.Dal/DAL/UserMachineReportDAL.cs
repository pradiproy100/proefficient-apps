﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class UserMachineReportDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateReportConfig(UserReportConfig pReportConfig)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@UserMachineReportID", pReportConfig.UserMachineReportID));
            lstParameter.Add(new SqlParameter("@UserId", pReportConfig.UserId));
            lstParameter.Add(new SqlParameter("@ReportName", pReportConfig.ReportName));
            lstParameter.Add(new SqlParameter("@DateRange", pReportConfig.DateRange));
            lstParameter.Add(new SqlParameter("@MachineIds", pReportConfig.MachineIds));
            lstParameter.Add(new SqlParameter("@IsDefault", pReportConfig.IsDefault));
            lstParameter.Add(new SqlParameter("@MailTo", pReportConfig.MailTo));
            return lstParameter.ToArray();
        }
       
        private SqlParameter[] pGetReportConfig(int UserMachineReportID, int UserId,string mode)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            if (mode == "Edit")
            {
                lstParameter.Add(new SqlParameter("@UserMachineReportID", (object)UserMachineReportID));
                lstParameter.Add(new SqlParameter("@UserId", (object)0));
            }
            else if (mode == "List")
            {
                lstParameter.Add(new SqlParameter("@UserMachineReportID", (object)0));
                lstParameter.Add(new SqlParameter("@UserId", (object)UserId));
            }
            else if (mode == "Delete")
            {
                lstParameter.Add(new SqlParameter("@UserMachineReportID", (object)UserMachineReportID));
            }
            return lstParameter.ToArray();
        }
       
        public Boolean UpdateReportConfig(UserReportConfig pReportConfig)
        {

            String StoredProcedureName = "UserMachineReportOP";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateReportConfig(pReportConfig));

            return result;
        }
        public Boolean DeleteReportConfig(int reportConfigId)
        {

            String StoredProcedureName = "UserMachineReportDelete";
            var result = ExecuteProcedure(StoredProcedureName,
                pGetReportConfig(reportConfigId, 0, "Delete"));

            return result;
        }

        public List<UserReportConfig> GetReportConfigList(int userId)
        {
            String StoredProcedureName = "GetUserMachineReport";
            Boolean IsSuccess;
            var result = (List<UserReportConfig>)ExecuteReaderProcedure(
                StoredProcedureName, ReportConfigMapping
               , out IsSuccess, pGetReportConfig(0,userId,"List"));

            return result;

        }
        public UserReportConfig GetReportConfig(int reportConfigId)
        {
            String StoredProcedureName = "GetUserMachineReport";
            Boolean IsSuccess;
            var result = (List<UserReportConfig>)ExecuteReaderProcedure(
                StoredProcedureName, ReportConfigMapping
               , out IsSuccess, pGetReportConfig(reportConfigId,0,"Edit"));

            return result.FirstOrDefault();

        }


        private List<UserReportConfig> ReportConfigMapping(SqlDataReader Reader)
        {
            List<UserReportConfig> LstUser = new List<UserReportConfig>();
            while (Reader.Read())
            {
                LstUser.Add(new UserReportConfig()
                {
                    UserMachineReportID = Reader["UserMachineReportID"].ToInteger0(),
                    MachineIds = Reader["MachineIds"].ToStringWithNullEmpty(),
                    ReportName = Reader["ReportName"].ToStringWithNullEmpty(),
                    UserId = Reader["UserId"].ToInteger0(),
                    DateRange = Reader["DateRange"].ToStringWithNullEmpty(),
                    UserName = Reader["UserName"].ToStringWithNullEmpty(),
                    MailTo = Reader["MailTo"].ToStringWithNullEmpty(),
                    IsDefault = Reader["IsDefault"].ToBoolean()
                });

            }
            return LstUser;
        }


        public UserMachineReportDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}