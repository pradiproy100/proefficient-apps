﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class MachineDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateMasterMachine(MasterMachine pMasterMachine)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineID", pMasterMachine.MachineID));
            lstParameter.Add(new SqlParameter("@MachineName", pMasterMachine.MachineName));
            lstParameter.Add(new SqlParameter("@Location", pMasterMachine.Location));
            lstParameter.Add(new SqlParameter("@OrganizationId", pMasterMachine.OrganizationId));
            lstParameter.Add(new SqlParameter("@MachineCode", pMasterMachine.MachineCode));
            lstParameter.Add(new SqlParameter("@IsActive", pMasterMachine.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pMasterMachine.AddBy));
            lstParameter.Add(new SqlParameter("@CategoryId", pMasterMachine.CategoryId));
            lstParameter.Add(new SqlParameter("@EdittedBy", pMasterMachine.EdittedBy));
            lstParameter.Add(new SqlParameter("@DemandType", pMasterMachine.DemandType));
            return lstParameter.ToArray();
        }
       
        private SqlParameter[] pGetMasterMachine(MasterMachine pMasterMachine)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineID", pMasterMachine.MachineID));

            return lstParameter.ToArray();
        }

        public Boolean UpdateMasterMachine(MasterMachine pMasterMachine)
        {

            String StoredProcedureName = "USP_CreateUpdateMachine";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterMachine(pMasterMachine));

            return result;
        }
        

        public List<MasterMachine> GetMasterMachineList()
        {
            String StoredProcedureName = "USP_GetMachine";
            Boolean IsSuccess;
            var result = (List<MasterMachine>)ExecuteReaderProcedure(
                StoredProcedureName, MasterMachineMapping
               , out IsSuccess, null);

            return result;

        }
        public MasterMachine GetMasterMachine(MasterMachine pMasterMachine)
        {
            String StoredProcedureName = "USP_GetMachine";
            Boolean IsSuccess;
            var result = (List<MasterMachine>)ExecuteReaderProcedure(
                StoredProcedureName, MasterMachineMapping
               , out IsSuccess, pGetMasterMachine(pMasterMachine));

            return result.FirstOrDefault();

        }


        private List<MasterMachine> MasterMachineMapping(SqlDataReader Reader)
        {
            List<MasterMachine> LstUser = new List<MasterMachine>();
            while (Reader.Read())
            {
                LstUser.Add(new MasterMachine()
                {
                    MachineID = Reader["MachineID"].ToInteger0(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    Location = Reader["Location"].ToStringWithNullEmpty(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    MachineCode = Reader["MachineCode"].ToStringWithNullEmpty(),
                    CategoryId = Reader["CategoryId"].ToInteger0(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    DemandType = Reader["DemandType"].ToStringWithNullEmpty(),
                });

            }
            return LstUser;
        }


        public MachineDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}