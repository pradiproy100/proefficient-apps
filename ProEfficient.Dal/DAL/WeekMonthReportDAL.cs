﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ProEfficient.Dal.DAL
{
    public class WeekMonthReportDAL : CoreDBAccess
    {
        private IConfiguration Configuration;

        private SqlParameter[] ParametreReportModel(DateTime startDtae, DateTime Enddate, int ORGnizationID = 5, int machineID = 0)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@startdate", startDtae));
            lstParameter.Add(new SqlParameter("@enddate", Enddate));
            lstParameter.Add(new SqlParameter("@organizationId", ORGnizationID));
            lstParameter.Add(new SqlParameter("@machineID", machineID));

            return lstParameter.ToArray();
        }
        private SqlParameter[] ParametreLossDetailModel(DateTime startDtae, DateTime Enddate, string reason, int ORGnizationID = 5, int machineID = 0)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@startdate", startDtae));
            lstParameter.Add(new SqlParameter("@enddate", Enddate));
            lstParameter.Add(new SqlParameter("@organizationId", ORGnizationID));
            lstParameter.Add(new SqlParameter("@machineID", machineID));
            lstParameter.Add(new SqlParameter("@reason", reason));

            return lstParameter.ToArray();
        }



        public List<ReportModel> GetReportList(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDates";
            Boolean IsSuccess;
            var result = (List<ReportModel>)ExecuteReaderProcedure(
                StoredProcedureName, ReportModelMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<FinalARPRQROEEHome> GetReportHomeList(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDatesHome_Optimized";
            Boolean IsSuccess;
            var result = (List<FinalARPRQROEEHome>)ExecuteReaderProcedure(
                StoredProcedureName, OEEHomeMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<FinalARPRQROEEHome> GetOEEReport(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetOEEDetailsRpt_Optimized";
            Boolean IsSuccess;
            var result = (List<FinalARPRQROEEHome>)ExecuteReaderProcedure(
                StoredProcedureName, OEEHomeMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }

        public List<WaterFallModel> GetWaterfall(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDatesWaterfall";
            Boolean IsSuccess;
            var result = (List<WaterFallModel>)ExecuteReaderProcedure(
                StoredProcedureName, WaterfallMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<RejectionReasonReportModel> GetRejectionReport(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetRejectionReport";
            Boolean IsSuccess;
            var result = (List<RejectionReasonReportModel>)ExecuteReaderProcedure(
                StoredProcedureName, RejectionReportMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<RejectionReasonDetailsModel> GetRejectionReportRpt(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetRejectionReportRpt";
            Boolean IsSuccess;
            var result = (List<RejectionReasonDetailsModel>)ExecuteReaderProcedure(
                   StoredProcedureName, RejectionReasonDetailsModelMapping
                  , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<FinalARPRQROEE> GetReportListYTD(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDatesYTD";
            Boolean IsSuccess;
            var result = (List<FinalARPRQROEE>)ExecuteReaderProcedure(
                StoredProcedureName, ReportModelYTDMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<FinalWeekWiseOEE> GetReportListWEEK(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetProductMachinelevelDetailsByDatesDayMonthWeek";
            Boolean IsSuccess;
            var result = (List<FinalWeekWiseOEE>)ExecuteReaderProcedure(
                StoredProcedureName, ReportModelWEEKMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<LossReportModel> GetLossReport(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetLossReport";
            Boolean IsSuccess;
            var result = (List<LossReportModel>)ExecuteReaderProcedure(
                StoredProcedureName, LossReportModelMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<LossReportDetailsModel> GetLossReportRptDetails(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetLossReportRptDetails";
            Boolean IsSuccess;
            var result = (List<LossReportDetailsModel>)ExecuteReaderProcedure(
                StoredProcedureName, LossReportRptDetailsModelMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<LossReportModel> GetLossReportRpt(DateTime stdate, DateTime enddate, int orgid, int machineid)
        {
            String StoredProcedureName = "USP_GetLossReportRpt";
            Boolean IsSuccess;
            var result = (List<LossReportModel>)ExecuteReaderProcedure(
                StoredProcedureName, LossReportModelMapping
               , out IsSuccess, ParametreReportModel(stdate, enddate, orgid, machineid));

            return result;

        }
        public List<LossReportModel> GetLossReportDetails(DateTime stdate, DateTime enddate, int orgid, int machineid, string reason)
        {
            String StoredProcedureName = "USP_GetLossReportDetails";
            Boolean IsSuccess;
            var result = (List<LossReportModel>)ExecuteReaderProcedure(
                StoredProcedureName, LossReportModelMapping
               , out IsSuccess, ParametreLossDetailModel(stdate, enddate, reason, orgid, machineid));

            return result;

        }
        public List<RejectionReasonDetailsModel> GetRejectionReportDetails(DateTime stdate, DateTime enddate, int orgid, int machineid, string reason)
        {
            String StoredProcedureName = "USP_GetRejectionReportDetails";
            Boolean IsSuccess;
            var result = (List<RejectionReasonDetailsModel>)ExecuteReaderProcedure(
                StoredProcedureName, RejectionReasonDetailsModelMapping
               , out IsSuccess, ParametreLossDetailModel(stdate, enddate, reason, orgid, machineid));

            return result;

        }

        private List<FinalARPRQROEE> ReportModelYTDMapping(SqlDataReader Reader)
        {
            List<FinalARPRQROEE> LstUser = new List<FinalARPRQROEE>();
            while (Reader.Read())
            {
                LstUser.Add(new FinalARPRQROEE()
                {
                    AR = Reader["AR"].Todouble(),
                    PR = Reader["PR"].Todouble(),
                    QR = Reader["QR"].Todouble(),
                    OEE = Reader["OEE"].Todouble()
                });

            }
            return LstUser;
        }
        private List<FinalWeekWiseOEE> ReportModelWEEKMapping(SqlDataReader Reader)
        {
            List<FinalWeekWiseOEE> LstUser = new List<FinalWeekWiseOEE>();
            while (Reader.Read())
            {
                LstUser.Add(new FinalWeekWiseOEE()
                {
                    weekno = Reader["weekno"].ToStringWithNullEmpty(),
                    DataLevel = Reader["DataLevel"].ToStringWithNullEmpty(),
                    OEE = Reader["OEE"].Todouble()
                });

            }
            return LstUser;
        }
        private List<ReportModel> ReportModelMapping(SqlDataReader Reader)
        {
            List<ReportModel> LstUser = new List<ReportModel>();
            while (Reader.Read())
            {
                LstUser.Add(new ReportModel()
                {
                    MachineID = Reader["MachineID"].ToInteger0(),
                    WorkShiftTime = Reader["WorkingShiftTime"].ToInteger0(),
                    ShiftBreakTime = Reader["BreakTime"].ToStringWithNullEmpty(),
                    shiftId = Reader["ShiftId"].ToInteger0(),
                    productID = Reader["productid"].ToInteger0(),
                    ShutdownTime = Reader["ShutdownTime"].ToStringWithNullEmpty(),
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    Utliizationloss = Reader["UtilisationLoss"].ToStringWithNullEmpty(),
                    TotalProduction = Reader["TotalProductointime"].ToStringWithNullEmpty(),
                    Qualityloss = Reader["QualityLoss"].ToStringWithNullEmpty(),
                    DemandType = Reader["DemandType"].ToStringWithNullEmpty(),
                    CustomerName = Reader["CustomerName"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }
        private List<FinalARPRQROEEHome> OEEHomeMapping(SqlDataReader Reader)
        {
            List<FinalARPRQROEEHome> LstUser = new List<FinalARPRQROEEHome>();
            while (Reader.Read())
            {
                LstUser.Add(new FinalARPRQROEEHome()
                {
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ShiftA = Reader["ShiftA"] == DBNull.Value ? "0" : Reader["ShiftA"].ToStringWithNullEmpty(),
                    ShiftB = Reader["ShiftB"] == DBNull.Value ? "0" : Reader["ShiftB"].ToStringWithNullEmpty(),
                    ShiftC = Utility.UserUtility.GetDataForColumn("ShiftC", Reader) == DBNull.Value ? "0" : Utility.UserUtility.GetDataForColumn("ShiftC", Reader).ToStringWithNullEmpty(),
                    //ShiftC = Reader["ShiftC"].ToStringWithNullEmpty(),                   
                    tShiftA = Reader["tShiftA"].ToStringWithNullEmpty(),
                    tShiftB = Reader["tShiftB"].ToStringWithNullEmpty(),
                    //tShiftC = Reader["tShiftC"].ToStringWithNullEmpty(),
                    tShiftC = Utility.UserUtility.GetDataForColumn("tShiftC", Reader).ToStringWithNullEmpty(),
                    OverAll = Reader["OverAll"] == DBNull.Value ? "0" : Reader["OverAll"].ToStringWithNullEmpty(),
                    tOverAll = Reader["tOverAll"].ToStringWithNullEmpty(),
                    DataLevel = Reader["DataLevel"].ToStringWithNullEmpty(),
                    RecordDate = Reader["ProductionDate"].ToStringWithNullEmpty().Substring(0, 10)
                }); ;

            }
            return LstUser;
        }

        private List<LossReportModel> LossReportModelMapping(SqlDataReader Reader)
        {
            List<LossReportModel> LstUser = new List<LossReportModel>();
            while (Reader.Read())
            {
                LstUser.Add(new LossReportModel()
                {
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    Remarks = Reader["Remarks"].ToStringWithNullEmpty(),
                    MAchineID = Reader["MachineID"].ToInteger0(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    Reason = Reader["Reason"].ToStringWithNullEmpty(),
                    SubReasonId = Reader["SubReasonId"].ToInteger0(),
                    SubReason = Reader["SubReason"].ToStringWithNullEmpty(),
                    TotalStoppagetime = Reader["TotalStoppagetime"].ToDecimal0(),
                    Duration = Reader["Duration"].ToDecimal0(),
                    Freequency = Reader["Freequency"].ToInteger0()
                });

            }
            return LstUser;
        }
        private List<LossReportDetailsModel> LossReportRptDetailsModelMapping(SqlDataReader Reader)
        {
            List<LossReportDetailsModel> LstUser = new List<LossReportDetailsModel>();
            while (Reader.Read())
            {
                LstUser.Add(new LossReportDetailsModel()
                {
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    //Remarks = Reader["Remarks"].ToStringWithNullEmpty(),

                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),

                    Reason = Reader["Reason"].ToStringWithNullEmpty(),

                    // SubReason = Reader["SubReason"].ToStringWithNullEmpty(),
                    // TotalStoppagetime = Reader["TotalStoppagetime"].ToDecimal0(),
                    Duration = Reader["Duration"].ToStringWithNullEmpty(),
                    StartTime = Reader["StartTime"].ToStringWithNullEmpty(),
                    EndTime = Reader["EndTime"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }
        private List<RejectionReasonDetailsModel> RejectionReasonDetailsModelMapping(SqlDataReader Reader)
        {
            List<RejectionReasonDetailsModel> LstUser = new List<RejectionReasonDetailsModel>();
            while (Reader.Read())
            {
                LstUser.Add(new RejectionReasonDetailsModel()
                {
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),

                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),

                    ReasonName = Reader["ReasonName"].ToStringWithNullEmpty(),
                    ProdCount = Reader["ProdCount"].ToInteger0()
                });

            }
            return LstUser;
        }

        private List<WaterFallModel> WaterfallMapping(SqlDataReader Reader)
        {
            List<WaterFallModel> LstUser = new List<WaterFallModel>();
            while (Reader.Read())
            {
                LstUser.Add(new WaterFallModel()
                {
                    Total_Time = Reader["Total_Time"].ToStringWithNullEmpty(),
                    Shutdown_Time = Reader["Shutdown_Time"].ToStringWithNullEmpty(),
                    Utilisation_Loss = Reader["Utilisation_Loss"].ToStringWithNullEmpty(),
                    Break_Time = Reader["Break_Time"].ToStringWithNullEmpty(),
                    Production_Loss = Reader["Production_Loss"].ToStringWithNullEmpty(),
                    Quality_Loss = Reader["Quality_Loss"].ToStringWithNullEmpty(),
                    Holidays = Reader["Holidays"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }
        private List<RejectionReasonReportModel> RejectionReportMapping(SqlDataReader Reader)
        {
            List<RejectionReasonReportModel> LstUser = new List<RejectionReasonReportModel>();
            while (Reader.Read())
            {
                LstUser.Add(new RejectionReasonReportModel()
                {
                    Name = Reader["ReasonName"].ToStringWithNullEmpty(),
                    Count = Reader["ProdCount"].ToInteger0()

                });

            }
            return LstUser;
        }
        private SqlParameter[] pGetUserDashboardSearch(int userId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@UserId", userId));

            return lstParameter.ToArray();
        }
        private List<UserDashboard> UserDashboardSearchMapping(SqlDataReader Reader)
        {
            List<UserDashboard> LstUser = new List<UserDashboard>();
            while (Reader.Read())
            {
                LstUser.Add(new UserDashboard()
                {

                    dateRange = Reader["DateRange"].ToStringWithNullEmpty(),
                    machineId = Reader["MachineId"].ToInteger0()

                });

            }
            return LstUser;
        }
        public UserDashboard GetUserDashboardSearch(int userId)
        {
            String StoredProcedureName = "GetUserDashboard";
            Boolean IsSuccess;
            var result = (List<UserDashboard>)ExecuteReaderProcedure(
                StoredProcedureName, UserDashboardSearchMapping
               , out IsSuccess, pGetUserDashboardSearch(userId));

            return result.FirstOrDefault();

        }
        public Boolean SaveUserDashboardSearch(int MachineId, int UserId, string DateRange)
        {

            String StoredProcedureName = "SaveUserDashboard";
            var result = ExecuteProcedure(StoredProcedureName,
                pSaveUserDashboardSearchMapping(MachineId, UserId, DateRange));

            return result;
        }
        private SqlParameter[] pSaveUserDashboardSearchMapping(int MachineId, int UserId, string DateRange)
        {

            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineId", MachineId));
            lstParameter.Add(new SqlParameter("@UserId", UserId));
            lstParameter.Add(new SqlParameter("@DateRange", DateRange));
            return lstParameter.ToArray();
        }

        public List<RawProduction> GetRawProduction()
        {
            String StoredProcedureName = "DownloadRawProductionData";
            Boolean IsSuccess;
            var result = (List<RawProduction>)ExecuteReaderProcedure(StoredProcedureName, RawProductionMapping, out IsSuccess, null);
            return result;
        }
        public List<RawBreakdown> GetRawBreakdown()
        {
            String StoredProcedureName = "DownloadRawBreakdownData";
            Boolean IsSuccess;
            var result = (List<RawBreakdown>)ExecuteReaderProcedure(StoredProcedureName, RawBreakdownMapping, out IsSuccess, null);
            return result;
        }
        public List<RawRejection> GetRawRejection()
        {
            String StoredProcedureName = "DownloadRawRejectionData";
            Boolean IsSuccess;
            var result = (List<RawRejection>)ExecuteReaderProcedure(StoredProcedureName, RawRejectionMapping, out IsSuccess, null);
            return result;
        }
        private List<RawProduction> RawProductionMapping(SqlDataReader Reader)
        {
            List<RawProduction> LstUser = new List<RawProduction>();
            while (Reader.Read())
            {
                LstUser.Add(new RawProduction()
                {
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    Product = Reader["Product"].ToStringWithNullEmpty(),
                    ProCount = Reader["ProCount"].ToInteger0(),
                    RejCount = Reader["RejCount"].ToInteger0()
                });
            }
            return LstUser;
        }
        private List<RawBreakdown> RawBreakdownMapping(SqlDataReader Reader)
        {
            List<RawBreakdown> LstUser = new List<RawBreakdown>();
            while (Reader.Read())
            {
                LstUser.Add(new RawBreakdown()
                {
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    Reason = Reader["Reason"].ToStringWithNullEmpty(),
                    ProCount = Reader["ProCount"].ToInteger0(),
                    StartTime = Reader["StartTime"].ToStringWithNullEmpty(),
                    EndTime = Reader["EndTime"].ToStringWithNullEmpty()
                });
            }
            return LstUser;
        }
        private List<RawRejection> RawRejectionMapping(SqlDataReader Reader)
        {
            List<RawRejection> LstUser = new List<RawRejection>();
            while (Reader.Read())
            {
                LstUser.Add(new RawRejection()
                {
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    Reason = Reader["Reason"].ToStringWithNullEmpty(),
                    ProCount = Reader["ProCount"].ToInteger0()
                });
            }
            return LstUser;
        }
        public bool DropAllData(int OrgId)
        {

            String StoredProcedureName = "DropAllData";
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrgId", OrgId));

            var result = ExecuteProcedure(StoredProcedureName, lstParameter.ToArray());

            return result;
        }
        public WeekMonthReportDAL(IConfiguration configuration) : base(configuration)
        {
            Configuration = configuration;
        }

    }
}
