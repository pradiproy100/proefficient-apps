﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class BreakDownEntryDAL : CoreDBAccess
    {

        public BreakDownEntryDAL(IConfiguration configuration) : base(configuration) { }
        private SqlParameter[] pUpdateBreakDownEntry(BreakDownEntry pBreakDownEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@BreakDownID", pBreakDownEntry.BreakDownID));
            lstParameter.Add(new SqlParameter("@MachineID", pBreakDownEntry.MachineID));
            lstParameter.Add(new SqlParameter("@OperatorID", pBreakDownEntry.OperatorID));
            lstParameter.Add(new SqlParameter("@ReasonId", pBreakDownEntry.ReasonId));
            lstParameter.Add(new SqlParameter("@SubReasonId", pBreakDownEntry.SubReasonId));
            lstParameter.Add(new SqlParameter("@ShiftId", pBreakDownEntry.ShiftId));
            lstParameter.Add(new SqlParameter("@StartTime", pBreakDownEntry.StartTime));
            lstParameter.Add(new SqlParameter("@EndTime", pBreakDownEntry.EndTime));
            lstParameter.Add(new SqlParameter("@TimeOfEntry", pBreakDownEntry.TimeOfEntry));
            lstParameter.Add(new SqlParameter("@TotalStoppageTime", pBreakDownEntry.TotalStoppageTime));
            lstParameter.Add(new SqlParameter("@UserID", pBreakDownEntry.UserID));
            lstParameter.Add(new SqlParameter("@IsActive", pBreakDownEntry.IsActive));
            lstParameter.Add(new SqlParameter("@SupervisorID", pBreakDownEntry.SupervisorID));
            lstParameter.Add(new SqlParameter("@AddBy", pBreakDownEntry.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pBreakDownEntry.EdittedBy));
            lstParameter.Add(new SqlParameter("@ProductionDate", pBreakDownEntry.ProductionDate));
            lstParameter.Add(new SqlParameter("@Remarks", pBreakDownEntry.Remarks));
            lstParameter.Add(new SqlParameter("@deletedEntries", pBreakDownEntry.deletedEntries));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pUpdateBreakDownEntryBydate(DateTime DateVal)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionDate", DateVal));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pUpdateBreakDownEntryFilter(DateTime StartTime, DateTime EndTime, int ShiftId, int MachineId, String Reason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@StartDate", StartTime));
            lstParameter.Add(new SqlParameter("@EndDate", EndTime));
            lstParameter.Add(new SqlParameter("@ShiftId", ShiftId));
            lstParameter.Add(new SqlParameter("@MachineId", MachineId));
            lstParameter.Add(new SqlParameter("@Reason", Reason));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pDeleteProdBreakEntryByshift(DateTime ProdDate, int ShiftId, String type)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionDate", ProdDate));
            lstParameter.Add(new SqlParameter("@ShiftId", ShiftId));
            lstParameter.Add(new SqlParameter("@Typ", type));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetBreakDownEntry(BreakDownEntry pBreakDownEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@BreakDownID", pBreakDownEntry.BreakDownID));

            return lstParameter.ToArray();
        }
        private SqlParameter[] pDeleteBreakDownEntry(int breakDownId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@BreakDownID", breakDownId));

            return lstParameter.ToArray();
        }


        public Boolean UpdateBreakDownEntry(BreakDownEntry pBreakDownEntry)
        {

            String StoredProcedureName = "USP_CreateUpdateBreakDownEntry";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateBreakDownEntry(pBreakDownEntry));

            return result;
        }
        public Boolean DeleteProdBreakEntryByshift(DateTime productionDate, int shiftId, string typ)
        {

            String StoredProcedureName = "DeleteProdBreakEntryByshift";
            var result = ExecuteProcedure(StoredProcedureName,
                pDeleteProdBreakEntryByshift(productionDate, shiftId, typ));

            return result;
        }
        public Boolean DeleteBreakDownEntry(int BreakDownId)
        {

            String StoredProcedureName = "USP_DeleteBreakDown";
            var result = ExecuteProcedure(StoredProcedureName,
                pDeleteBreakDownEntry(BreakDownId));
            return result;
        }



        public List<BreakDownEntry> GetBreakDownEntryList()
        {
            String StoredProcedureName = "USP_GetBreakDownEntry";
            Boolean IsSuccess;
            var result = (List<BreakDownEntry>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryMapping
               , out IsSuccess, null);

            return result;

        }
        public List<BreakDownEntry> GetBreakDownEntryListDateTime(DateTime CurrentDate)
        {
            String StoredProcedureName = "USP_GetBreakDownEntryByDate";
            Boolean IsSuccess;
            var result = (List<BreakDownEntry>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryMapping
               , out IsSuccess, pUpdateBreakDownEntryBydate(CurrentDate));

            return result;

        }
        public List<BreakDownEntry> GetBreakDownEntryFilter(DateTime StartDate, DateTime EndDate, int MachineId,
            int ShiftId, String Reason)
        {
            String StoredProcedureName = "USP_GetBreakDownEntryByFilter";
            Boolean IsSuccess;
            var result = (List<BreakDownEntry>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryMapping
               , out IsSuccess, pUpdateBreakDownEntryFilter(StartDate, EndDate, ShiftId, MachineId, Reason));

            return result;

        }

        public List<BreakDownEntry> GetBreakDownEntryListDateTime(DateTime CurrentDate, int OrganizationId)
        {
            String StoredProcedureName = "USP_GetBreakDownEntryByDate";
            Boolean IsSuccess;
            var result = (List<BreakDownEntry>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryMapping
               , out IsSuccess, pUpdateBreakDownEntryBydate(CurrentDate));
            if (result != null)
            {
                //result = result.FindAll(i => i.OrganizationId == OrganizationId && i.ProductionDate.ToString("dd/MM/yyyy") == CurrentDate.ToString("dd/MM/yyyy"));
            }

            return result;

        }


        public List<BreakDownEntry> GetBreakDownEntryListFormMaxProdToTwoDays()
        {
            String StoredProcedureName = "USP_GetBreakDownEntryByDate";
            Boolean IsSuccess;
            var result = (List<BreakDownEntry>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryMapping
               , out IsSuccess);
            if (result != null)
            {
                //result = result.FindAll(i => i.OrganizationId == OrganizationId && i.ProductionDate.ToString("dd/MM/yyyy") == CurrentDate.ToString("dd/MM/yyyy"));
            }

            return result;

        }


        public BreakDownEntry GetBreakDownEntry(BreakDownEntry pBreakDownEntry)
        {
            String StoredProcedureName = "USP_GetBreakDownEntryById";
            Boolean IsSuccess;
            var result = (List<BreakDownEntry>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryMapping
               , out IsSuccess, pGetBreakDownEntry(pBreakDownEntry));

            return result.FirstOrDefault();

        }


        private List<BreakDownEntry> BreakDownEntryMapping(SqlDataReader Reader)
        {
            List<BreakDownEntry> LstUser = new List<BreakDownEntry>();
            while (Reader.Read())
            {
                LstUser.Add(new BreakDownEntry()
                {
                    MachineID = Reader["MachineID"].ToInteger0(),
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    SubReasonId = Reader["SubReasonId"].ToInteger0(),
                    UserID = Reader["UserID"].ToInteger0(),
                    OperatorID = Reader["OperatorID"].ToInteger0(),
                    SupervisorID = Reader["SupervisorID"].ToInteger0(),
                    TimeOfEntry = Reader["TimeOfEntry"].ToDateTime(),
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    StartTime = Reader["StartTime"].ToDateTime(),
                    EndTime = Reader["EndTime"].ToDateTime(),
                    TotalStoppageTime = Reader["TotalStoppageTime"].ToDecimal0(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    BreakDownID = Reader["BreakDownID"].ToInteger0(),
                    Remarks = Reader["Remarks"].ToStringWithNullEmpty(),
                    ProductionDate = Reader["ProductionDate"].ToDateTime(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0()
                });

            }
            return LstUser;
        }

        // Edit BreakdownEntry from Influx

        public List<BreakdownEntryEditVM> GetBreakDownEntryEdit(string ProdDate, int ShiftId)
        {
            String StoredProcedureName = "GetBreakDownEntryForEdit";
            Boolean IsSuccess;
            var result = (List<BreakdownEntryEditVM>)ExecuteReaderProcedure(
                StoredProcedureName, BreakDownEntryEditMap
               , out IsSuccess, pGetBreakDownEntryEdit(ProdDate, ShiftId));

            return result;
        }
        private SqlParameter[] pGetBreakDownEntryEdit(string ProdDate, int ShiftId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProdDate", ProdDate));
            lstParameter.Add(new SqlParameter("@ShiftID", ShiftId));
            return lstParameter.ToArray();
        }
        private List<BreakdownEntryEditVM> BreakDownEntryEditMap(SqlDataReader Reader)
        {
            List<BreakdownEntryEditVM> LstBeakdownEntryEdit = new List<BreakdownEntryEditVM>();
            while (Reader.Read())
            {
                LstBeakdownEntryEdit.Add(new BreakdownEntryEditVM()
                {
                    BreakdownId = Reader["BreakdownId"].ToInteger0(),
                    TotalStoppageTime = Reader["TotalStoppageTime"].ToStringWithNullEmpty(),
                    MachineID = Reader["MachineID"].ToInteger0(),
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    SubReasonId = Reader["SubReasonId"].ToInteger0(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    StartTime = Reader["StartTime"].ToStringWithNullEmpty(),
                    EndTime = Reader["EndTime"].ToStringWithNullEmpty(),
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    Remarks = Reader["Remarks"].ToStringWithNullEmpty(),
                });

            }
            return LstBeakdownEntryEdit;
        }

        public Boolean EditBreakDownEntry(DataTable UDT)
        {

            String StoredProcedureName = "EditBreakDownEntryInflux";
            var result = ExecuteProcedure(StoredProcedureName,
                pEditBreakDownEntry(UDT));

            return result;
        }
        private SqlParameter[] pEditBreakDownEntry(DataTable UDT)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@BreakDownDT", UDT));

            return lstParameter.ToArray();
        }
        // End of Edit BreakdownEntry from Influx 
    }
}