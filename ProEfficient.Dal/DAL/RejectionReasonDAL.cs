﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class RejectionReasonDAL : CoreDBAccess
    {
        private SqlParameter[] pUpdateRejectionReason(ProEfficient.Core.Models.RejectionReason pRejectionReason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ReasonId", pRejectionReason.ReasonId));
            lstParameter.Add(new SqlParameter("@ReasonName", pRejectionReason.ReasonName));
            lstParameter.Add(new SqlParameter("@ReasonDescription", pRejectionReason.ReasonDescription));
            lstParameter.Add(new SqlParameter("@OrganizationId", pRejectionReason.OrganizationId));
            lstParameter.Add(new SqlParameter("@IsActive", pRejectionReason.IsActive));
            lstParameter.Add(new SqlParameter("@InfluxRejReasonId", pRejectionReason.InfluxRejReasonId));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetRejectionReason(ProEfficient.Core.Models.RejectionReason pRejectionReason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ReasonId", pRejectionReason.ReasonId));
            lstParameter.Add(new SqlParameter("@OrganizationId", pRejectionReason.OrganizationId));

            return lstParameter.ToArray();
        }
        private SqlParameter[] pGetAllRejectionReason()
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();

            lstParameter.Add(new SqlParameter("@OrganizationId", UserUtility.CurrentUser.OrganizationId));

            return lstParameter.ToArray();
        }
        private SqlParameter[] pGetProductionRejectionReason(int ProductionEntryId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionEntryId", ProductionEntryId));
            return lstParameter.ToArray();
        }



        public Boolean UpdateRejectionReason(ProEfficient.Core.Models.RejectionReason pRejectionReason)
        {
            pRejectionReason.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            String StoredProcedureName = "USP_CreateUpdateRejectionReason";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateRejectionReason(pRejectionReason));

            return result;
        }

        public List<ProEfficient.Core.Models.RejectionReason> GetRejectionReasonList()
        {
            String StoredProcedureName = "USP_GetRejectionReason";
            Boolean IsSuccess;
            var result = (List<ProEfficient.Core.Models.RejectionReason>)ExecuteReaderProcedure(
                StoredProcedureName, RejectionReasonMapping
               , out IsSuccess, pGetAllRejectionReason());
            if (result == null)
            {
                result = new List<ProEfficient.Core.Models.RejectionReason>();
            }
            return result;

        }
        public List<ProductionRejection> GetProductionRejectionReasonList(int id)
        {
            String StoredProcedureName = "USP_GetProductionRejection";
            Boolean IsSuccess;

            try
            {
                var result = (List<ProductionRejection>)ExecuteReaderProcedure(
                       StoredProcedureName, ProductionRejectionReasonMapping
                      , out IsSuccess, pGetProductionRejectionReason(id));

                return result;
            }
            catch (Exception)
            {
                return new List<ProductionRejection>();
            }

        }
        public ProEfficient.Core.Models.RejectionReason GetRejectionReason(ProEfficient.Core.Models.RejectionReason pRejectionReason)
        {
            pRejectionReason.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            String StoredProcedureName = "USP_GetRejectionReason";
            Boolean IsSuccess;
            var result = (List<ProEfficient.Core.Models.RejectionReason>)ExecuteReaderProcedure(
                StoredProcedureName, RejectionReasonMapping
               , out IsSuccess, pGetRejectionReason(pRejectionReason));

            return result.FirstOrDefault();

        }


        private List<ProEfficient.Core.Models.RejectionReason> RejectionReasonMapping(SqlDataReader Reader)
        {
            List<ProEfficient.Core.Models.RejectionReason> LstUser = new List<ProEfficient.Core.Models.RejectionReason>();
            while (Reader.Read())
            {
                LstUser.Add(new ProEfficient.Core.Models.RejectionReason()
                {
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    ReasonName = Reader["ReasonName"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    ReasonDescription = Reader["ReasonDescription"].ToString(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    ReasonGroup= Reader["ReasonGroup"].ToString(),
                    InfluxRejReasonId = Reader["InfluxRejReasonId"].ToInteger0()
                });

            }
            return LstUser;
        }

        private List<ProductionRejection> ProductionRejectionReasonMapping(SqlDataReader Reader)
        {
            List<ProductionRejection> LstUser = new List<ProductionRejection>();
            while (Reader.Read())
            {
                LstUser.Add(new ProductionRejection()
                {
                    RejectionReasonId = Reader["RejectionReasonId"].ToInteger0(),
                    RejectionId = Reader["RejectionId"].ToInteger0(),
                    ProductionEntryId = Reader["ProductionEntryId"].ToInteger0(),
                    RejectionQuantity = Reader["RejectionQuantity"].ToInteger0(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0()
                });

            }
            return LstUser;
        }

        public RejectionReasonDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}