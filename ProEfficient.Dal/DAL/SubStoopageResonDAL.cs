﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ProEfficient.Dal.DAL
{
    public class SubStoopageResonDAL : CoreDBAccess
    {
        private SqlParameter[] pGetsubstoopageReason(int subreasonid)
        {
            int OrganizationID = Utility.UserUtility.CurrentUser.OrganizationId.ToInteger0();
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationId", OrganizationID));
            return lstParameter.ToArray();
        }
        public List<SubStoppageReason> GetSUBStoppageReason()
        {

           

            List<SubStoppageReason> rejectreasonlst = new List<SubStoppageReason>();

            String StoredProcedureName = "USP_GetStopageSubReason";
               DataSet result =  ExecuteDataSet(StoredProcedureName,
               pGetsubstoopageReason(0));


            if (result.Tables.Count > 0)
            {
                for (int j = 0; j < result.Tables[0].Rows.Count; j++)
                {
                    SubStoppageReason rejectReasonobj = new SubStoppageReason();
                    rejectReasonobj.ReasonID = result.Tables[0].Rows[j]["ReasonID"].ToString().ToInteger();
                    rejectReasonobj.SubReasonID = result.Tables[0].Rows[j]["SubReasonID"].ToString().ToInteger();
                    rejectReasonobj.SubReasonName = result.Tables[0].Rows[j]["SubReasonName"].ToString();
                    rejectReasonobj.SubReasonCode = result.Tables[0].Rows[j]["SubReasonCode"].ToString();
                    rejectreasonlst.Add(rejectReasonobj);
                }
            }



            return rejectreasonlst;
        }


        public SubStoopageResonDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}