﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class UserManagementDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateUser(OrganizationUser pUser)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@UserId", pUser.UserId));
            lstParameter.Add(new SqlParameter("@UserName", pUser.UserName));
            lstParameter.Add(new SqlParameter("@OrganizationId", pUser.OrganizationId));
            lstParameter.Add(new SqlParameter("@UserTypeId", pUser.UserTypeId));
            lstParameter.Add(new SqlParameter("@PhoneNo", pUser.PhoneNo));
            lstParameter.Add(new SqlParameter("@EmailId", pUser.EmailId));
            lstParameter.Add(new SqlParameter("@Password", pUser.Password));
            lstParameter.Add(new SqlParameter("@IsActive", pUser.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pUser.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pUser.EdittedBy));
            lstParameter.Add(new SqlParameter("@ChangeCode", pUser.ChangeCode));

            return lstParameter.ToArray();
        }


        private DynamicParameters pUpdateUserByDapper(OrganizationUser pUser)
        {
          
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@UserId", pUser.UserId);
            parameters.Add("@UserName", pUser.UserName);
            parameters.Add ("@OrganizationId", pUser.OrganizationId);
            parameters.Add("@UserTypeId", pUser.UserTypeId);
            parameters.Add("@PhoneNo", pUser.PhoneNo);
            parameters.Add("@EmailId", pUser.EmailId);
            parameters.Add("@Password", pUser.Password);
            parameters.Add("@IsActive", pUser.IsActive);
            parameters.Add("@AddBy", pUser.AddBy);
            parameters.Add("@EdittedBy", pUser.EdittedBy);
            parameters.Add("@ChangeCode", pUser.ChangeCode);
            return parameters;
        }




        private SqlParameter[] pGetUser(OrganizationUser pUser)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@UserId", pUser.UserId));
            return lstParameter.ToArray();
        }



        public Boolean UpdateUser(OrganizationUser pUser)
        {

            String StoredProcedureName = "USP_CreateUpdateUser";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateUser(pUser));

            return result;
        }



        public Boolean UpdateUserByDapper(OrganizationUser pUser)
        {

            String StoredProcedureName = "USP_CreateUpdateUser";
            var result = ExecuteDapperProcedure(StoredProcedureName,
                pUpdateUserByDapper(pUser));

            return result;
        }

        public List<OrganizationUser> GetUserList()
        {
            String StoredProcedureName = "USP_GetUser";
            Boolean IsSuccess;
            var result = (List<OrganizationUser>)ExecuteReaderProcedure(
                StoredProcedureName, UserMapping
               , out IsSuccess, null);

           // var result = ExecuteReadDapperProcedure(StoredProcedureName, UserMappingDapper,out IsSuccess, null);


            if (result != null)
            {
                try
                {
                    foreach (var User in result)
                    {
                        User.OrganizationName = new OrganizationDAL(Configuration).GetOrganization(new Organization() { OrganizationId = User.OrganizationId.ToInteger0() }).OrganizationName;
                    }
                }
                catch (Exception)
                {

                }

            }
            return result;

        }

        public OrganizationUser GetUser(OrganizationUser pUser)
        {
            String StoredProcedureName = "USP_GetUser";
            Boolean IsSuccess;
            var result = (List<OrganizationUser>)ExecuteReaderProcedure(
                StoredProcedureName, UserMapping
               , out IsSuccess, pGetUser(pUser));

            return result.FirstOrDefault();

        }



        public List<UserType> GetAllUserType()
        {

            String StoredProcedureName = "USP_GetUserType";
            Boolean IsSuccess;
            var result = (List<UserType>)ExecuteReaderProcedure(
                StoredProcedureName, UserTypeMapping
               , out IsSuccess, null);

            return result;

        }


        private List<OrganizationUser> UserMapping(SqlDataReader Reader)
        {
            List<OrganizationUser> LstUser = new List<OrganizationUser>();
            while (Reader.Read())
            {
                LstUser.Add(new OrganizationUser()
                {
                    UserId = Reader["UserId"].ToInteger0(),
                    UserName = Reader["UserName"].ToStringWithNullEmpty(false),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    UserTypeId = Reader["UserTypeId"].ToInteger0(),
                    PhoneNo = Reader["PhoneNo"].ToStringWithNullEmpty(),
                    EmailId = Reader["EmailId"].ToStringWithNullEmpty(),
                    Password = Reader["Password"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    ChangeCode = Reader["ChangeCode"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }

        private List<OrganizationUser> UserMappingDapper(IDataReader Reader)
        {
            List<OrganizationUser> LstUser = new List<OrganizationUser>();
            while (Reader.Read())
            {
                LstUser.Add(new OrganizationUser()
                {
                    UserId = Reader["UserId"].ToInteger0(),
                    UserName = Reader["UserName"].ToStringWithNullEmpty(false),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    UserTypeId = Reader["UserTypeId"].ToInteger0(),
                    PhoneNo = Reader["PhoneNo"].ToStringWithNullEmpty(),
                    EmailId = Reader["EmailId"].ToStringWithNullEmpty(),
                    Password = Reader["Password"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    ChangeCode = Reader["ChangeCode"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }

        private List<UserType> UserTypeMapping(SqlDataReader Reader)
        {
            List<UserType> LstUserType = new List<UserType>();
            while (Reader.Read())
            {
                LstUserType.Add(new UserType()
                {
                    UserTypeID = Reader["UserTypeID"].ToInteger(),
                    UserTypeName = Reader["UserTypeName"].ToStringWithNullEmpty(),
                    PermissionModule = Reader["PermissionModule"].ToStringWithNullEmpty()

                });

            }
            return LstUserType;
        }

        private IConfiguration Configuration;
        public UserManagementDAL(IConfiguration configuration) : base(configuration)
        {
            Configuration = configuration;
        }
    }
}