﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class MouldDAL : CoreDBAccess
    {

        private DynamicParameters pUpdateMasterMould(MasterMould pMasterMould)
        {
            var parameters = new DynamicParameters(pMasterMould);
            return parameters;
        }  
        public Boolean UpdateMasterMould(MasterMould pMasterMould)
        {

            String StoredProcedureName = "USP_CreateUpdateMould";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterMould(pMasterMould));
            return result;
        }


        public List<MasterMould> GetMasterMouldList(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetMoulds";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<MasterMould>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }

        }
        public MasterMould GetMasterMouldById(int OrganizationId,int MouldId)
        {
            try
            {
                String StoredProcedureName = "USP_GetMouldById";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                param.Add("MouldId", MouldId);
                var result = GetList<MasterMould>(StoredProcedureName, param);

                return result.FirstOrDefault();
            }
            catch (Exception)
            {

                return null;
            }

        }


        public MouldDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}