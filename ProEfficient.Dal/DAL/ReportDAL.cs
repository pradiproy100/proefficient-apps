﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ProEfficient.Dal.DAL
{
    public class ReportDAL : CoreDBAccess
    {
        public async Task<DataSet> GetBreakDownList(BreakDownReport data)
        {
            String StoredProcedureName = "USP_RPT_BREAKDOWNINFO";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            //paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;
        }

        public async Task<DataSet> GetBreakDownListLatest(BreakDownReport data)
        {
            String StoredProcedureName = "USP_RPT_BREAKDOWNINFO_LATEST";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            //paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());
            return ds;
        }

        public async Task<DataSet> GetBreakDownListLatestDesc(BreakDownReport data)
        {
            String StoredProcedureName = "USP_RPT_BREAKDOWNINFO_LATEST_2";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            //paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());
            return ds;
        }
        public async Task<DataSet> GetLossScheduleDuration(BreakDownReport data)
        {
            String StoredProcedureName = "USP_RPT_SCHEDULELOSSDURATION";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            //paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());
            return ds;
        }
        public async Task<DataSet> GetLossScheduleFreq(BreakDownReport data)
        {
            String StoredProcedureName = "USP_RPT_SCHEDULELOSSFREEQUENCY";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            //paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());
            return ds;
        }

        public async Task<DataSet> GetProductionEffList(ProductionReport data)
        {
            // String StoredProcedureName = "USP_RPT_PROD_EFF";
            String StoredProcedureName = "USP_RPT_PROD_EFF_NEW";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            paramList.Add(new SqlParameter("@SHIFT_ID", data.ShiftId));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;
        }
        public async Task<DataSet> GetProductionEffLatestList(ProductionReport data)
        {
            String StoredProcedureName = "USP_RPT_PROD_EFF_Latest";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            paramList.Add(new SqlParameter("@SHIFT_ID", data.ShiftId));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;

        }
        public async Task<DataSet> getProductionRejectionData(ProductionReport data)
        {
            String StoredProcedureName = "getProductionRejectionData";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            paramList.Add(new SqlParameter("@SHIFT_ID", data.ShiftId));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;

        }

        public async Task<DataSet> GetProductionEffListExcel(ProductionReport data)
        {
            String StoredProcedureName = "USP_RPT_PROD_EFF_nw";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@MACHINEID", data.MachineID));
            paramList.Add(new SqlParameter("@SHIFT_ID", data.ShiftId));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;

        }
        public async Task<DataSet> GetProductionEffListAllMachine(ProductionReportAllMachine data)
        {
            String StoredProcedureName = "USP_RPT_PROD_EFF_AllMachine";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@STARTDATE", data.StartDate));
            paramList.Add(new SqlParameter("@ENDDATE", data.EndDate));
            paramList.Add(new SqlParameter("@CategoryId", data.MachineID == null ? 0 : data.MachineID));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;

        }
        public async Task<DataSet> GetProductionEffListWithMachieCategory(ProductionReportAllMachine data)
        {
            String StoredProcedureName = "USP_RPT_PROD_EFF_WithMachineCategory";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@OrgId", data.OrgId));
            paramList.Add(new SqlParameter("@MachineCategoryId", data.MachineID == null ? 0 : data.MachineID));
            paramList.Add(new SqlParameter("@FromDate", data.StartDate));
            paramList.Add(new SqlParameter("@ToDate", data.EndDate));
            paramList.Add(new SqlParameter("@ShiftId", data.ShiftId));
            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;

        }
        public async Task<DataSet> GetMachineEfficiencyReport(ProductionReportAllMachine data)
        {
            String StoredProcedureName = "USP_MachineEfficiencyReport";
            List<SqlParameter> paramList = new List<SqlParameter>();
            paramList.Add(new SqlParameter("@ShiftId", data.ShiftId));
            paramList.Add(new SqlParameter("@OrgId", data.OrgId));
            paramList.Add(new SqlParameter("@FromDate", data.StartDate));
            paramList.Add(new SqlParameter("@ToDate", data.EndDate));
            paramList.Add(new SqlParameter("@MachineId", data.MachineID == null ? 0 : data.MachineID));

            DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());

            return ds;

        }
        public async Task<DataSet> GetProductionEffListAllMachineNew(ProductionReportAllMachine data)
        {
            String StoredProcedureName = "USP_RPT_PROD_EFF_AllMachine_New";


            DataSet Ds = new DataSet();

            var _StartDate = data.StartDate.ToDateTime();
            var _EndDate = data.EndDate.ToDateTime();
            var dates = new List<DateTime>();

            for (var dt = _StartDate; dt <= _EndDate; dt = dt.AddDays(1))
            {
                dates.Add(dt);
            }
            foreach (var SingleDate in dates)
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                paramList.Add(new SqlParameter("@STARTDATE", SingleDate.ToString("yyyy-MM-dd")));
                paramList.Add(new SqlParameter("@ENDDATE", SingleDate.ToString("yyyy-MM-dd")));
                paramList.Add(new SqlParameter("@CategoryId", data.MachineID == null ? 0 : data.MachineID));

                DataSet ds = await ExecuteDataSet(StoredProcedureName, paramList.ToArray());
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (Ds.Tables.Count <= 0)
                    {
                        Ds.Tables.Add(ds.Tables[0].Clone());
                    }
                    else
                    {
                        Ds.Tables[0].Merge(ds.Tables[0]);
                    }

                }
            }


            return Ds;

        }

        public ReportDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}