﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class StopageSubReasonDAL : CoreDBAccess
    {
        private SqlParameter[] pUpdateStopageSubReason(StopageSubReason pStopageSubReason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@SubReasonId", pStopageSubReason.SubReasonId));
            lstParameter.Add(new SqlParameter("@ReasonId", pStopageSubReason.ReasonId));
            lstParameter.Add(new SqlParameter("@SubReasonName", pStopageSubReason.SubReasonName));
            lstParameter.Add(new SqlParameter("@ReasonDescription", pStopageSubReason.ReasonDescription));
            lstParameter.Add(new SqlParameter("@SubReasonCode", pStopageSubReason.SubReasonCode));
            lstParameter.Add(new SqlParameter("@IsActive", pStopageSubReason.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pStopageSubReason.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pStopageSubReason.EdittedBy));
            lstParameter.Add(new SqlParameter("@InfluxSubReasonId", pStopageSubReason.InfluxSubReasonId));

            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetStopageSubReason(StopageSubReason pStopageSubReason)
        {
            var orgID = UserUtility.CurrentUser.OrganizationId;
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("SubReasonId", pStopageSubReason.SubReasonId));
            lstParameter.Add(new SqlParameter("OrganizationId", orgID));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pGetStopageSubReasonByReasonId(StopageSubReason pStopageSubReason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("ReasonId", pStopageSubReason.ReasonId));

            return lstParameter.ToArray();
        }



        public Boolean UpdateStopageSubReason(StopageSubReason pStopageSubReason)
        {

            String StoredProcedureName = "USP_CreateUpdateStopageSubReason";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateStopageSubReason(pStopageSubReason));

            return result;
        }

        public List<StopageSubReason> GetStopageSubReasonList()
        {
            var pStopageSubReason = new StopageSubReason();
            String StoredProcedureName = "USP_GetStopageSubReason";
            Boolean IsSuccess;
            var result = (List<StopageSubReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageSubReasonMapping
               , out IsSuccess, pGetStopageSubReason(pStopageSubReason));

            return result;

        }
        public StopageSubReason GetStopageSubReason(StopageSubReason pStopageSubReason)
        {
            String StoredProcedureName = "USP_GetStopageSubReason";
            Boolean IsSuccess;
            var result = (List<StopageSubReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageSubReasonMapping
               , out IsSuccess, pGetStopageSubReason(pStopageSubReason));

            return result.FirstOrDefault();

        }

        public List<StopageSubReason> GetStopageSubReasonByReasonId(StopageSubReason pStopageSubReason)
        {
            String StoredProcedureName = "USP_GetStopageSubReasonByReasonId";
            Boolean IsSuccess;
            var result = (List<StopageSubReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageSubReasonMapping
               , out IsSuccess, pGetStopageSubReasonByReasonId(pStopageSubReason));

            return result;

        }


        private List<StopageSubReason> StopageSubReasonMapping(SqlDataReader Reader)
        {
            List<StopageSubReason> LstUser = new List<StopageSubReason>();
            while (Reader.Read())
            {
                LstUser.Add(new StopageSubReason()
                {
                    SubReasonCode = Reader["SubReasonCode"].ToStringWithNullEmpty(),
                    SubReasonName = Reader["SubReasonName"].ToStringWithNullEmpty(),
                    SubReasonId = Reader["SubReasonId"].ToInteger0(),
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    ReasonDescription = Reader["ReasonDescription"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    InfluxSubReasonId = Reader["InfluxSubReasonId"].ToInteger0()


                });

            }
            return LstUser;
        }

        public StopageSubReasonDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}