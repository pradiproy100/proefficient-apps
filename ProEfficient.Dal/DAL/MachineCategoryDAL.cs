﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ProEfficient.Dal.DAL
{
    public class MachineCategoryDAL : CoreDBAccess
    {
        private SqlParameter[] pUpdateMachineCategory(MachineCategory pMachineCategory)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@CategoryId", pMachineCategory.CategoryId));
            lstParameter.Add(new SqlParameter("@CategoryName", pMachineCategory.CategoryName));
            lstParameter.Add(new SqlParameter("@OrganizationId", pMachineCategory.OrganizationId));

            return lstParameter.ToArray();
        }

        private List<MachineCategory> MachineCategoryMapping(SqlDataReader Reader)
        {
            List<MachineCategory> LstMachineCategory = new List<MachineCategory>();
            while (Reader.Read())
            {
                LstMachineCategory.Add(new MachineCategory()
                {
                    CategoryId = Reader["CategoryId"].ToInteger0(),
                    CategoryName = Reader["CategoryName"].ToStringWithNullEmpty(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0()
                });

            }
            return LstMachineCategory;
        }

        public Boolean UpdateMachineCategory(MachineCategory pMachineCategory)
        {

            String StoredProcedureName = "USP_CreateUpdateMachineCategory";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMachineCategory(pMachineCategory));

            return result;
        }
        public List<MachineCategory> GetMachineCategoryList()
        {
            String StoredProcedureName = "USP_GetMachineCategory";
            Boolean IsSuccess;
            var result = (List<MachineCategory>)ExecuteReaderProcedure(
                StoredProcedureName, MachineCategoryMapping
               , out IsSuccess, null);

            return result;

        }

        public MachineCategoryDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}