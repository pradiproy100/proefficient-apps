﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class StopageReasonDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateStopageReason(StopageReason pStopageReason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ReasonId", pStopageReason.@ReasonId));
            lstParameter.Add(new SqlParameter("@ReasonName", pStopageReason.ReasonName));
            lstParameter.Add(new SqlParameter("ReasonCode", pStopageReason.ReasonCode));
            lstParameter.Add(new SqlParameter("@IsActive", pStopageReason.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pStopageReason.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pStopageReason.EdittedBy));
            lstParameter.Add(new SqlParameter("@IsSchLoss", pStopageReason.IsSchLoss));
            lstParameter.Add(new SqlParameter("@OrganizationId", pStopageReason.OrganizationId));
            lstParameter.Add(new SqlParameter("@InfluxReasonId", pStopageReason.InfluxReasonId));
            lstParameter.Add(new SqlParameter("@StoppageReasonTypeId", pStopageReason.StoppageReasonTypeId));            
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetStopageReason(StopageReason pStopageReason)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ReasonId", pStopageReason.ReasonId));
            lstParameter.Add(new SqlParameter("@OrganizationId", pStopageReason.OrganizationId));

            return lstParameter.ToArray();
        }


        public List<StoppageReasonType> GetAllStoppageReasonTypes()
        {
            
                try
                {
                    String StoredProcedureName = "GetAllStoppageReasons";
                    DynamicParameters param = new DynamicParameters();                   
                    var result = GetList<StoppageReasonType>(StoredProcedureName, param);

                    return result.ToList();
                }
                catch (Exception)
                {

                    return null;
                }
           
        }

        public Boolean UpdateStopageReason(StopageReason pStopageReason)
        {
            pStopageReason.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            String StoredProcedureName = "USP_CreateUpdateStopageReason";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateStopageReason(pStopageReason));

            return result;
        }

        public List<StopageReason> GetStopageReasonList()
        {
            String StoredProcedureName = "USP_GetStopageReason";
            Boolean IsSuccess;
            var result = (List<StopageReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageReasonMapping
               , out IsSuccess, pGetStopageReason(new StopageReason() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() }));
            return result;
        }
        public List<StopageReason> GetStopageReasonListByOrganizationId(int organizationId)
        {
            String StoredProcedureName = "USP_GetStopageReason";
            Boolean IsSuccess;
            var result = (List<StopageReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageReasonMapping
               , out IsSuccess, pGetStopageReason(new StopageReason() { OrganizationId = organizationId }));
            return result;
        }
        public StopageReason GetStopageReason(StopageReason pStopageReason)
        {
            String StoredProcedureName = "USP_GetStopageReason";
            pStopageReason.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            Boolean IsSuccess;
            var result = (List<StopageReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageReasonMapping
               , out IsSuccess, pGetStopageReason(pStopageReason));

            return result.FirstOrDefault();

        }

        public List<StopageReason> GetStopageReasonListWithOrganisation(int OrganisationIDs)
        {
            String StoredProcedureName = "USP_GetStopageReason";
            Boolean IsSuccess;
            var result = (List<StopageReason>)ExecuteReaderProcedure(
                StoredProcedureName, StopageReasonMapping
               , out IsSuccess, new SqlParameter("@OrganizationId", OrganisationIDs));

            return result;

        }


        private List<StopageReason> StopageReasonMapping(SqlDataReader Reader)
        {
            List<StopageReason> LstUser = new List<StopageReason>();
            while (Reader.Read())
            {
                LstUser.Add(new StopageReason()
                {
                    ReasonCode = Reader["ReasonCode"].ToStringWithNullEmpty(),
                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    ReasonName = Reader["ReasonName"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    IsSchLoss = Reader["IsSchLoss"].ToBoolean(),
                    InfluxReasonId = Reader["InfluxReasonId"].ToInteger0(),
                    StoppageReasonTypeId = Reader["StoppageReasonTypeID"].ToInteger0()
                });

            }
            return LstUser;
        }

        public StopageReasonDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}