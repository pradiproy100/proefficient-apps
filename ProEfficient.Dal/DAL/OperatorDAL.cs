﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class OperatorDAL : CoreDBAccess
    {

        private DynamicParameters pUpdateMasterOperator(MasterOperator pMasterOperator)
        {
            var parameters = new DynamicParameters(pMasterOperator);
            return parameters;
        }  
        public Boolean UpdateMasterOperator(MasterOperator pMasterOperator)
        {

            String StoredProcedureName = "USP_CreateUpdateOperator";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterOperator(pMasterOperator));
            return result;
        }


        public List<MasterOperator> GetMasterOperatorList(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetOperators";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<MasterOperator>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }

        }
        public MasterOperator GetMasterOperatorById(int OrganizationId,int OperatorId)
        {
            try
            {
                String StoredProcedureName = "USP_GetOperatorById";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                param.Add("OperatorId", OperatorId);
                var result = GetList<MasterOperator>(StoredProcedureName, param);

                return result.FirstOrDefault();
            }
            catch (Exception)
            {

                return null;
            }

        }


        public OperatorDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}