﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class OrganizationDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateOrganization(Organization pOrganization)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationId", pOrganization.OrganizationId));
            lstParameter.Add(new SqlParameter("@OrganizationName", pOrganization.OrganizationName));
            lstParameter.Add(new SqlParameter("@PhoneNo", pOrganization.PhoneNo));
            lstParameter.Add(new SqlParameter("@EmailId", pOrganization.EmailId));
            lstParameter.Add(new SqlParameter("@Location", pOrganization.Location));
            lstParameter.Add(new SqlParameter("@IsActive", pOrganization.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pOrganization.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pOrganization.EdittedBy));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetOrganization(Organization pOrganization)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationId", pOrganization.OrganizationId));

            return lstParameter.ToArray();
        }
        private SqlParameter[] orgaProductCategory(int orgid)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationID", orgid));

            return lstParameter.ToArray();
        }
        private SqlParameter[] orgaProductCustomer(int orgid)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationID", orgid));

            return lstParameter.ToArray();
        }
        private SqlParameter[] organisationUnits(int orgid)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationID", orgid));

            return lstParameter.ToArray();
        }


        public Boolean UpdateOrganization(Organization pOrganization)
        {

            String StoredProcedureName = "USP_CreateUpdateOrganization";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateOrganization(pOrganization));

            return result;
        }

        public List<Organization> GetOrganizationList()
        {
            String StoredProcedureName = "USP_GetOrganization";
            Boolean IsSuccess;
            var result = (List<Organization>)ExecuteReaderProcedure(
                StoredProcedureName, OrganizationMapping
               , out IsSuccess, null);

            return result;

        }
        public Organization GetOrganization(Organization pOrganization)
        {
            String StoredProcedureName = "USP_GetOrganization";
            Boolean IsSuccess;
            var result = (List<Organization>)ExecuteReaderProcedure(
                StoredProcedureName, OrganizationMapping
               , out IsSuccess, pGetOrganization(pOrganization));

            return result.FirstOrDefault();

        }


        private List<Organization> OrganizationMapping(SqlDataReader Reader)
        {
            List<Organization> LstUser = new List<Organization>();
            while (Reader.Read())
            {
                LstUser.Add(new Organization()
                {
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    OrganizationName = Reader["OrganizationName"].ToStringWithNullEmpty(),
                    PhoneNo = Reader["PhoneNo"].ToStringWithNullEmpty(),
                    EmailId = Reader["EmailId"].ToStringWithNullEmpty(),
                    Location = Reader["Location"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    IsManualProductionEntry= Reader["IsManualProductionEntry"].ToBoolean(),
                    HasBreakdown = Reader["HasBreakdown"].ToBoolean(),
                    HasRejection = Reader["HasRejection"].ToBoolean()
                });

            }
            return LstUser;
        }

        public List<ProductCategory> OrganizationCategoeylst(int organizationID)
        {
            String StoredProcedureName = "USP_GetOrganizationCategory";
            Boolean IsSuccess;
            var result = (List<ProductCategory>)ExecuteReaderProcedure(
                StoredProcedureName, ProductCategoryMapping
               , out IsSuccess, orgaProductCategory(organizationID));

            return result;
        }
       
        private List<ProductCategory> ProductCategoryMapping(SqlDataReader Reader)
        {
            List<ProductCategory> LstCategory = new List<ProductCategory>();
            while (Reader.Read())
            {
                LstCategory.Add(new ProductCategory()
                {
                    CategoryID = Reader["ID"].ToInteger0(),
                    CategoryName = Reader["CategoryName"].ToStringWithNullEmpty()

                });

            }
            return LstCategory;
        }
        public List<ProductCustomer> OrganizationCustomerlst(int organizationID)
        {
            String StoredProcedureName = "USP_GetOrganizationCustomer";
            Boolean IsSuccess;
            var result = (List<ProductCustomer>)ExecuteReaderProcedure(
                StoredProcedureName, ProductCustomerMapping
               , out IsSuccess, orgaProductCustomer(organizationID));

            return result;
        }
        private List<ProductCustomer> ProductCustomerMapping(SqlDataReader Reader)
        {
            List<ProductCustomer> LstCustomer = new List<ProductCustomer>();
            while (Reader.Read())
            {
                LstCustomer.Add(new ProductCustomer()
                {
                    CustomerID = Reader["ID"].ToInteger0(),
                    CustomerName = Reader["CustomerName"].ToStringWithNullEmpty()

                });

            }
            return LstCustomer;
        }

        public List<ProductUnits> OrganizationUnitlst(int organizationID)
        {
            String StoredProcedureName = "USP_GetOrganizationUnits";
            Boolean IsSuccess;
            var result = (List<ProductUnits>)ExecuteReaderProcedure(
                StoredProcedureName, ProductUnitMapping
               , out IsSuccess, orgaProductCategory(organizationID));

            return result;
        }

        private List<ProductUnits> ProductUnitMapping(SqlDataReader Reader)
        {
            List<ProductUnits> LstCategory = new List<ProductUnits>();
            while (Reader.Read())
            {
                LstCategory.Add(new ProductUnits()
                {
                    id = Reader["ID"].ToInteger0(),
                    unitName = Reader["Unit"].ToStringWithNullEmpty()

                });

            }
            return LstCategory;
        }


        public OrganizationDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}