﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ProEfficient.Dal.DAL
{
    public class ProductionReportDAL : CoreDBAccess
    {
        public ProductionReportDAL(IConfiguration configuration) : base(configuration)
        {
        }

        public List<ProductionReportResult> GetProductionReport(int OrganizationId,DateTime StartDate, DateTime EndDate)
        {
            try
            {
                String StoredProcedureName = "USP_GetProductionReport";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                param.Add("FromDate", StartDate);
                param.Add("Todate", EndDate);
                var result = GetList<ProductionReportResult>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetProductionReportWithBreakDown(int OrganizationId, DateTime StartDate, DateTime EndDate)
        {
            try
            {
                String StoredProcedureName = "GetProductionDataAndBreakDown";
                DynamicParameters param = new DynamicParameters();             
                param.Add("StartDate", StartDate);
                param.Add("Enddate", EndDate);

                List<SqlParameter> lstParameter = new List<SqlParameter>();
                lstParameter.Add(new SqlParameter("@StartDate", StartDate));
                lstParameter.Add(new SqlParameter("@Enddate", EndDate));

                var result = ExecuteDataSet(StoredProcedureName, lstParameter.ToArray());

                return result.Tables[0];
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
