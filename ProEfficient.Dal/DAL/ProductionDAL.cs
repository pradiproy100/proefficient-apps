﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class ProductionDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateProductionEntry(ProductionEntry pProductionEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineID", pProductionEntry.MachineID));
            lstParameter.Add(new SqlParameter("@OperatorID", pProductionEntry.OperatorID));
            lstParameter.Add(new SqlParameter("@ProductID", pProductionEntry.ProductID));
            lstParameter.Add(new SqlParameter("@ProductionId", pProductionEntry.ProductionId));
            lstParameter.Add(new SqlParameter("@Rejection", pProductionEntry.Rejection));
            lstParameter.Add(new SqlParameter("@ReWork", pProductionEntry.ReWork));
            lstParameter.Add(new SqlParameter("@ShiftId", pProductionEntry.ShiftId));
            lstParameter.Add(new SqlParameter("@StartTime", pProductionEntry.StartTime));
            lstParameter.Add(new SqlParameter("@EndTime", pProductionEntry.EndTime));
            lstParameter.Add(new SqlParameter("@SupervisorID", pProductionEntry.SupervisorID));
            lstParameter.Add(new SqlParameter("@TimeOfEntry", pProductionEntry.TimeOfEntry));
            lstParameter.Add(new SqlParameter("@TotalProduction", pProductionEntry.TotalProduction));
            lstParameter.Add(new SqlParameter("@UserID", pProductionEntry.UserID));
            lstParameter.Add(new SqlParameter("@ProductionDate", pProductionEntry.ProductionDate));
            lstParameter.Add(new SqlParameter("@RejectionReasons", pProductionEntry.RejectionReasons));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pUpdateProductionEntryThroughApi(ProductionEntry pProductionEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineID", pProductionEntry.MachineID));
            lstParameter.Add(new SqlParameter("@OperatorID", pProductionEntry.OperatorID));
            lstParameter.Add(new SqlParameter("@ProductID", pProductionEntry.ProductID));
            lstParameter.Add(new SqlParameter("@ProductionId", pProductionEntry.ProductionId));
            lstParameter.Add(new SqlParameter("@Rejection", pProductionEntry.Rejection));
            lstParameter.Add(new SqlParameter("@ReWork", pProductionEntry.ReWork));
            lstParameter.Add(new SqlParameter("@ShiftId", pProductionEntry.ShiftId));
            lstParameter.Add(new SqlParameter("@StartTime", pProductionEntry.StartTime));
            lstParameter.Add(new SqlParameter("@EndTime", pProductionEntry.EndTime));
            lstParameter.Add(new SqlParameter("@SupervisorID", pProductionEntry.SupervisorID));
            lstParameter.Add(new SqlParameter("@TimeOfEntry", pProductionEntry.TimeOfEntry));
            lstParameter.Add(new SqlParameter("@TotalProduction", pProductionEntry.TotalProduction));
            lstParameter.Add(new SqlParameter("@UserID", pProductionEntry.UserID));
            lstParameter.Add(new SqlParameter("@ProductionDate", pProductionEntry.ProductionDate));
            lstParameter.Add(new SqlParameter("@Rejectionlist", pProductionEntry.rejectionlist));
            lstParameter.Add(new SqlParameter("@deletedEntries", pProductionEntry.deletedEntries));
            return lstParameter.ToArray();
        }
        private SqlParameter[] SetOEECalculationParam(DateTime dateTime)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@MachineId", 0));
            lstParameter.Add(new SqlParameter("@ShiftId", 0));
            lstParameter.Add(new SqlParameter("@ProductionDate", dateTime.ToString("yyyy-MM-dd")));
            return lstParameter.ToArray();
        }



        private SqlParameter[] pUpdateProductionRejectionEntry(ProductionRejection pProductionRejection)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionEntryId", pProductionRejection.ProductionEntryId));
            lstParameter.Add(new SqlParameter("@XML", pProductionRejection.Xml));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pGetProductionRejectionEntry(ProductionEntry pProductionRejection)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionEntryId", pProductionRejection.ProductionId));

            return lstParameter.ToArray();
        }

        private SqlParameter[] pProductionDate(DateTime ProductionDate, int OrganizationId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionDate", ProductionDate));
            lstParameter.Add(new SqlParameter("@OrganizationId", OrganizationId));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetProductionEntry(ProductionEntry pProductionEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductId", pProductionEntry.ProductID));
            lstParameter.Add(new SqlParameter("@MachineID", pProductionEntry.MachineID));
            lstParameter.Add(new SqlParameter("@OperatorID", pProductionEntry.OperatorID));
            lstParameter.Add(new SqlParameter("@SupervisorID", pProductionEntry.SupervisorID));
            lstParameter.Add(new SqlParameter("@ShiftId", pProductionEntry.ShiftId));
            lstParameter.Add(new SqlParameter("@ProductionDate", pProductionEntry.ProductionDate));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetProductionEntryNew(ProductionEntry pProductionEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ShiftId", pProductionEntry.ShiftId));
            lstParameter.Add(new SqlParameter("@OrganisationID", pProductionEntry.OrganizationId));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pgetProBreakEntryDataByShiftAndDate(GetProBreakDataParamsVM pProBreakParams)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProdDate", pProBreakParams.ProductionDate));
            lstParameter.Add(new SqlParameter("@ProdToDate", pProBreakParams.ProductionToDate));
            lstParameter.Add(new SqlParameter("@ShiftID", pProBreakParams.ShiftId));
            lstParameter.Add(new SqlParameter("@OrganisationID", pProBreakParams.OrganizationID));
            return lstParameter.ToArray();
        }


        private SqlParameter[] pGetProductionEntryById(ProductionEntry pProductionEntry)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionId", pProductionEntry.ProductionId));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pDeleteProductionEntry(int productionId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductionId", productionId));
            return lstParameter.ToArray();
        }


        public Boolean UpdateProductionEntry(ProductionEntry pProductionEntry)
        {
            //    pProductionEntry.RejectionReasons = "1-2,2-2";

            String StoredProcedureName = "USP_CreateUpdateProductionEntry";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateProductionEntry(pProductionEntry));



            return result;
        }

        public Boolean UpdateProductionRejection(ProductionRejection pProductionRejection)
        {

            String StoredProcedureName = "USP_CreateUpdateProductionRejection";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateProductionRejectionEntry(pProductionRejection));

            return result;
        }
        public Boolean UpdateProductionEntryThroughApi(ProductionEntry pProductionEntry)
        {

            String StoredProcedureName = "USP_CreateUpdateProductionEntry";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateProductionEntryThroughApi(pProductionEntry));

            return result;
        }

        public Boolean UpdateOEECalculation(DateTime dateTime)
        {
            string StoredProcedureName = "SP_CALCULATE_OEE_REPORT";
            var result = ExecuteProcedure(StoredProcedureName, SetOEECalculationParam(dateTime));
            return result;
        }

        public Boolean SubmitProduction(DateTime ProductionDate, int OrganizationId)
        {

            String StoredProcedureName = "USP_SubmitProduction";
            var result = ExecuteProcedure(StoredProcedureName,
                pProductionDate(ProductionDate, OrganizationId));

            return result;
        }
        public Boolean DeleteProductionEntry(int ProductionId)
        {

            String StoredProcedureName = "USP_DeleteProduction";
            var result = ExecuteProcedure(StoredProcedureName,
                pDeleteProductionEntry(ProductionId));

            return result;
        }



        public List<ProductionEntry> GetProductionEntry(ProductionEntry pProductionEntry)
        {
            String StoredProcedureName = "USP_GetProductionEntry";
            Boolean IsSuccess;
            var result = (List<ProductionEntry>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionEntryMapping
               , out IsSuccess, pGetProductionEntry(pProductionEntry));

            return result;
        }


        public List<ProductionEntry> GetProductionEntryNew(ProductionEntry pProductionEntry)
        {
            String StoredProcedureName = "USP_GetProductionEntryNew";
            Boolean IsSuccess;
            var result = (List<ProductionEntry>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionEntryMappingNew
               , out IsSuccess, pGetProductionEntryNew(pProductionEntry));

            return result;
        }
        public List<GetProductionEntryByShiftAndDateVM> GetProductionEntryByShiftAndDate(GetProBreakDataParamsVM pProBreakParams)
        {
            String StoredProcedureName = "GetProductionEntryByShiftAndDate";
            Boolean IsSuccess;
            var result = (List<GetProductionEntryByShiftAndDateVM>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionByShifAndDateMapping
               , out IsSuccess, pgetProBreakEntryDataByShiftAndDate(pProBreakParams));

            return result;
        }
        public List<GetBreakDownEntryByShiftAndDateVM> GetBreakDownEntryByShiftAndDateVM(GetProBreakDataParamsVM pProBreakParams)
        {
            String StoredProcedureName = "GetBreakDownEntryByShiftAndDate";
            Boolean IsSuccess;
            var result = (List<GetBreakDownEntryByShiftAndDateVM>)ExecuteReaderProcedure(
                StoredProcedureName, BreakdownByShifAndDateMapping
               , out IsSuccess, pgetProBreakEntryDataByShiftAndDate(pProBreakParams));

            return result;
        }
        public List<RejectionEntryByShiftAndDateVM> GetRejectionEntryByShiftAndDate(GetProBreakDataParamsVM pProBreakParams)
        {
            String StoredProcedureName = "GetRejectionEntryByShiftAndDate";
            Boolean IsSuccess;
            var result = (List<RejectionEntryByShiftAndDateVM>)ExecuteReaderProcedure(
                StoredProcedureName, RejectionByShifAndDateMapping
               , out IsSuccess, pgetProBreakEntryDataByShiftAndDate(pProBreakParams));

            return result;
        }
        public List<ProductionRejection> GetProductionRejection(ProductionEntry pProductionEntry)
        {
            String StoredProcedureName = "USP_GetProductionRejection";
            Boolean IsSuccess;
            var result = (List<ProductionRejection>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionRejectionMapping
               , out IsSuccess, pGetProductionRejectionEntry(pProductionEntry));

            return result;
        }

        public ProductionEntry GetProductionEntryById(ProductionEntry pProductionEntry)
        {
            String StoredProcedureName = "USP_GetProductionEntryById";
            Boolean IsSuccess;
            var result = (List<ProductionEntry>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionEntryMapping
               , out IsSuccess, pGetProductionEntryById(pProductionEntry));

            return result.FirstOrDefault();
        }
        public List<ProductionEntry> GetProductionEntry(ProductionEntry pProductionEntry, int OrganizationId)
        {
            String StoredProcedureName = "USP_GetProductionEntry";
            Boolean IsSuccess;
            var result = (List<ProductionEntry>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionEntryMapping
               , out IsSuccess, pGetProductionEntry(pProductionEntry));
            if (result != null)
            {
                result = result.FindAll(i => i.OrganizationId == OrganizationId && i.ProductionDate.ToString("dd/MM/yyyy") == pProductionEntry.ProductionDate.ToString("dd/MM/yyyy"));
            }
            return result;
        }

        public Boolean IsProductionSubmitted(DateTime ProductionDate, int OrganizationId)
        {

            try
            {
                String StoredProcedureName = "USP_IsSubmitted";
                Boolean IsSuccess;
                var result = (int)ExecuteReaderProcedure(
                    StoredProcedureName, ProductionSubmitMapping
                   , out IsSuccess, pProductionDate(ProductionDate, OrganizationId));
                if (result <= 0)
                {
                    IsSuccess = false;
                }
                return IsSuccess;
            }
            catch (Exception)
            {

                return false;
            }
        }

        //public ProductionEntry USP_GetProductionEntryByShiftId(ProductionEntry pProductionEntry)
        //{
        //    String StoredProcedureName = "USP_GetProductionEntryByShiftId";
        //    Boolean IsSuccess;
        //    var result = (List<ProductionEntry>)ExecuteReaderProcedure(
        //        StoredProcedureName, ProductionEntryMapping
        //       , out IsSuccess, pGetProductionEntry(pProductionEntry));

        //    return result.FirstOrDefault();

        //}


        private List<ProductionEntry> ProductionEntryMapping(SqlDataReader Reader)
        {
            List<ProductionEntry> LstUser = new List<ProductionEntry>();
            while (Reader.Read())
            {
                LstUser.Add(new ProductionEntry()
                {

                    ProductionId = Reader["ProductionId"].ToInteger0(),
                    MachineID = Reader["MachineID"].ToInteger0(),
                    ProductID = Reader["ProductID"].ToInteger0(),
                    UserID = Reader["UserID"].ToInteger0(),
                    OperatorID = Reader["OperatorID"].ToInteger0(),
                    SupervisorID = Reader["SupervisorID"].ToInteger0(),
                    TimeOfEntry = Reader["TimeOfEntry"].ToDateTimeWithNull(),
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    StartTime = Reader["StartTime"].ToDateTimeWithNull(),
                    EndTime = Reader["EndTime"].ToDateTimeWithNull(),
                    TotalProduction = Reader["TotalProduction"].ToInteger0(),
                    ReWork = Reader["ReWork"].ToInteger0(),
                    Rejection = Reader["Rejection"].ToInteger0(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    ProductionDate = Reader["ProductionDate"].ToDateTime()
                });

            }
            return LstUser;
        }
        private List<ProductionEntry> ProductionEntryMappingNew(SqlDataReader Reader)
        {
            List<ProductionEntry> LstUser = new List<ProductionEntry>();
            while (Reader.Read())
            {
                LstUser.Add(new ProductionEntry()
                {

                    ProductionId = Reader["ProductionId"].ToInteger0(),
                    ProductionDate = Reader["ProductionDate"].ToDateTime(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ProductName = Reader["ProductName"].ToStringWithNullEmpty(),
                    TotalProduction = Reader["TotalProduction"].ToInteger0(),
                    ReWork = Reader["ReWork"].ToInteger0(),
                    Rejection = Reader["Rejection"].ToInteger0(),
                    rejectionlist = Reader["RejectionList"].ToStringWithNullEmpty(),
                    OperatorName = Reader["UserName"].ToStringWithNullEmpty()
                });

            }
            return LstUser;
        }

        private List<GetProductionEntryByShiftAndDateVM> ProductionByShifAndDateMapping(SqlDataReader Reader)
        {
            List<GetProductionEntryByShiftAndDateVM> LstUser = new List<GetProductionEntryByShiftAndDateVM>();
            while (Reader.Read())
            {
                LstUser.Add(new GetProductionEntryByShiftAndDateVM()
                {
                    ProductionRowIndex = Reader["ProductionRowIndex"].ToInteger0(),
                    ProductionId = Reader["ProductionId"].ToInteger0(),
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    ProductName = Reader["ProductName"].ToStringWithNullEmpty(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    MachineID = Reader["MachineID"].ToInteger0(),
                    ProductID = Reader["ProductID"].ToInteger0(),
                    OperatorID = Reader["OperatorID"].ToInteger0(),
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    TotalProduction = Reader["TotalProduction"].ToInteger0(),
                    ReWork = Reader["ReWork"].ToInteger0(),
                    Rejection = Reader["Rejection"].ToInteger0(),
                    RejectionList = Reader["RejectionList"].ToStringWithNullEmpty(),
                    OperatorName = Reader["OperatorName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    ProductionStartTime = Reader["ProductionStartTime"].ToDateTime(),
                    ProductionEndTime = Reader["ProductionEndTime"].ToDateTime(),
                    IsSaved = Reader["IsSaved"].ToBoolean(),
                    IsDeleted = Reader["IsDeleted"].ToBoolean(),
                    UserID = Reader["UserID"].ToInteger0()

                });

            }
            return LstUser;
        }
        private List<GetBreakDownEntryByShiftAndDateVM> BreakdownByShifAndDateMapping(SqlDataReader Reader)
        {
            List<GetBreakDownEntryByShiftAndDateVM> LstUser = new List<GetBreakDownEntryByShiftAndDateVM>();
            while (Reader.Read())
            {
                LstUser.Add(new GetBreakDownEntryByShiftAndDateVM()
                {
                    BreakdownRowIndex = Reader["BreakdownRowIndex"].ToInteger0(),
                    BreakdownId = Reader["BreakdownId"].ToInteger0(),
                    ProductionDate = Reader["ProductionDate"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    MachineID = Reader["MachineID"].ToInteger0(),
                    OperatorID = Reader["OperatorID"].ToInteger0(),
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    OperatorName = Reader["OperatorName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    StartTime = Reader["StartTime"].ToDateTime(),
                    EndTime = Reader["EndTime"].ToDateTime(),
                    TotalStoppageTime = Reader["TotalStoppageTime"].ToDecimal0(),

                    ReasonId = Reader["ReasonId"].ToInteger0(),
                    ReasonName = Reader["ReasonName"].ToStringWithNullEmpty(),
                    SubReasonId = Reader["SubReasonId"].ToInteger0(),
                    SubReasonName = Reader["SubReasonName"].ToStringWithNullEmpty(),
                    Remarks = Reader["Remarks"].ToStringWithNullEmpty(),
                    IsSaved = Reader["IsSaved"].ToBoolean(),
                    IsDeleted = Reader["IsDeleted"].ToBoolean(),
                    UserID = Reader["UserID"].ToInteger0()

                });

            }
            return LstUser;
        }
        private List<RejectionEntryByShiftAndDateVM> RejectionByShifAndDateMapping(SqlDataReader Reader)
        {
            List<RejectionEntryByShiftAndDateVM> LstUser = new List<RejectionEntryByShiftAndDateVM>();
            while (Reader.Read())
            {
                LstUser.Add(new RejectionEntryByShiftAndDateVM()
                {
                    MachineID = Reader["MachineID"].ToInteger0(),
                    ReasonName = Reader["ReasonName"].ToStringWithNullEmpty(),
                    ProdCount = Reader["ProdCount"].ToInteger0(),
                    ProdEntryId = Reader["ProductionEntryId"].ToInteger0(),
                });

            }
            return LstUser;
        }
        private List<ProductionRejection> ProductionRejectionMapping(SqlDataReader Reader)
        {
            List<ProductionRejection> LstUser = new List<ProductionRejection>();
            while (Reader.Read())
            {
                LstUser.Add(new ProductionRejection()
                {

                    ProductionEntryId = Reader["ProductionEntryId"].ToInteger0(),
                    RejectionId = Reader["RejectionId"].ToInteger0(),
                    RejectionQuantity = Reader["RejectionQuantity"].ToInteger0(),
                    RejectionReasonId = Reader["RejectionReasonId"].ToInteger0()
                });

            }
            return LstUser;
        }


        private object ProductionSubmitMapping(SqlDataReader Reader)
        {
            int SubmitCount = 0;
            while (Reader.Read())
            {


                SubmitCount = Reader["SubmitCount"].ToInteger0();


            }
            return SubmitCount;
        }

        public ProductionPageVM GetProductionEntryUI(int orgid, DateTime proddate, DateTime prodTodate)
        {
            String StoredProcedureName = "USP_GetProductionPageData";
            DataSet ds = ExecuteDataSet(StoredProcedureName, pProductionEntryUI(orgid, proddate, prodTodate));
            ProductionPageVM model = new ProductionPageVM();
            List<ProductionBreakEntryShiftVM> lstShift = new List<ProductionBreakEntryShiftVM>();
            lstShift = UserUtility.ConvertDataTable<ProductionBreakEntryShiftVM>(ds.Tables[0]);

            List<ProductionBreakEntryUserVM> lstUsers = new List<ProductionBreakEntryUserVM>();
            lstUsers = UserUtility.ConvertDataTable<ProductionBreakEntryUserVM>(ds.Tables[1]);

            List<ProductionBreakEntryRejectionVM> lstRejections = new List<ProductionBreakEntryRejectionVM>();
            lstRejections = UserUtility.ConvertDataTable<ProductionBreakEntryRejectionVM>(ds.Tables[2]);

            List<ProductionBreakEntryStopageVM> lstStoppage = new List<ProductionBreakEntryStopageVM>();
            lstStoppage = UserUtility.ConvertDataTable<ProductionBreakEntryStopageVM>(ds.Tables[3]);

            List<ProductionBreakEntryProductMachineVM> lstMAchineProduct = new List<ProductionBreakEntryProductMachineVM>();
            lstMAchineProduct = UserUtility.ConvertDataTable<ProductionBreakEntryProductMachineVM>(ds.Tables[4]);

            List<RejectionReasonModel> lstRejectionReasons = new List<RejectionReasonModel>();
            lstRejectionReasons = UserUtility.ConvertDataTable<RejectionReasonModel>(ds.Tables[5]);
            // return result;
            model.lstshiftData = lstShift;
            model.lstuserData = lstUsers;
            model.lstrejectionData = lstRejections;
            model.lststopageData = lstStoppage;
            model.lstmachineProductData = lstMAchineProduct;
            model.lstRejectionReason = lstRejectionReasons;
            return model;

        }
        private SqlParameter[] pProductionEntryUI(int orgid, DateTime proddate, DateTime prodTodate)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@OrganizationId", orgid));
            lstParameter.Add(new SqlParameter("@ProductionDate", proddate));
            lstParameter.Add(new SqlParameter("@ProductionToDate", prodTodate));
            return lstParameter.ToArray();
        }
        public ProductionDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}