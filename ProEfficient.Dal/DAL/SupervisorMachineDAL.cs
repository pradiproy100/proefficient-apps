﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class SupervisorMachineDAL : CoreDBAccess
    {

        private SqlParameter[] pGetSupervisorMachineMap(SupervisorScheduleSearch pSupervisorScheduleSearch)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@machineIds", pSupervisorScheduleSearch.MachineIds.ToStringWithNullEmpty(false)));
            lstParameter.Add(new SqlParameter("@shiftIds", pSupervisorScheduleSearch.ShiftIds.ToStringWithNullEmpty(false)));
            lstParameter.Add(new SqlParameter("@supervisorIds", pSupervisorScheduleSearch.SupervisorIds.ToStringWithNullEmpty(false)));
            lstParameter.Add(new SqlParameter("@OrganizationId", pSupervisorScheduleSearch.OrganizationId.ToInteger()));
          

            return lstParameter.ToArray();
        }       
        public List<SupervisorMachineMapVM> GetSupervisorScheduleList(SupervisorScheduleSearch pSupervisorScheduleSearch)
        {
            String StoredProcedureName = "GetSupervisorSchedule";
            Boolean IsSuccess;
            var result = (List<SupervisorMachineMapVM>)ExecuteReaderProcedure(
                StoredProcedureName, SupervisorMachineMapping
               , out IsSuccess, pGetSupervisorMachineMap(pSupervisorScheduleSearch));

            return result;

        }
        private List<SupervisorMachineMapVM> SupervisorMachineMapping(SqlDataReader Reader)
        {
            List<SupervisorMachineMapVM> LstSupervisorMachineMap = new List<SupervisorMachineMapVM>();
            while (Reader.Read())
            {
                LstSupervisorMachineMap.Add(new SupervisorMachineMapVM()
                {

                    SupervisorMachineId = Reader["SupervisorMachineId"].ToInteger0(),
                    SupervisorId = Reader["SupervisorId"].ToInteger0(),
                    MachineId = Reader["MachineId"].ToInteger0(),
                    ShiftId = Reader["ShiftId"].ToInteger0(),
                    StartDate = Reader["StartDate"].ToDateTime(),
                    EndDate = Reader["EndDate"].ToDateTime(),
                    ThemeColor = Reader["ThemeColor"].ToStringWithNullEmpty(),
                    MachineName = Reader["MachineName"].ToStringWithNullEmpty(),
                    SupervisorName = Reader["SupervisorName"].ToStringWithNullEmpty(),
                    ShiftName = Reader["ShiftName"].ToStringWithNullEmpty(),
                    IsFullDay = Reader["IsFullDay"].ToBoolean(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    MachineIds= Reader["MachineIds"].ToStringWithNullEmpty(),
                    OrganizationId=Reader["OrganizationId"].ToInteger0()
                });

            }
            return LstSupervisorMachineMap;
        }

        public Boolean UpdateSupervisorMachine(SupervisorMachineMapVM pSupervisorMachineMapVM, ref string msg_Error)
        {

            String StoredProcedureName = "UpdateSupervisorSchedule";
            var result = ExecuteProcedureOut(StoredProcedureName, ref msg_Error,
                pUpdateSupervisorMachine(pSupervisorMachineMapVM,ref msg_Error));
           
            return result;
        }
        private SqlParameter[] pDeleteSupervisorMachine(int id)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@SupervisorMachineID", id));

            return lstParameter.ToArray();
        }

        public Boolean DeleteSupervisorMachine(int id)
        {

            String StoredProcedureName = "DeleteSupervisorSchedule";
            var result = ExecuteProcedure(StoredProcedureName,
                pDeleteSupervisorMachine(id));

            return result;
        }

        private SqlParameter[] pUpdateSupervisorMachine(SupervisorMachineMapVM pSupervisorMachineMapVM, ref string msg_Error)
        {
            
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@StartDate", pSupervisorMachineMapVM.StartDate));
            lstParameter.Add(new SqlParameter("@EndDate", pSupervisorMachineMapVM.EndDate));
            lstParameter.Add(new SqlParameter("@machineIds", pSupervisorMachineMapVM.MachineIds));
            lstParameter.Add(new SqlParameter("@shiftId", pSupervisorMachineMapVM.ShiftId));
            lstParameter.Add(new SqlParameter("@supervisorId", pSupervisorMachineMapVM.SupervisorId));
            lstParameter.Add(new SqlParameter("@ThemeColor", pSupervisorMachineMapVM.ThemeColor));
            lstParameter.Add(new SqlParameter("@SupervisorMachineID", pSupervisorMachineMapVM.SupervisorMachineId));
            lstParameter.Add(new SqlParameter("@msg_Error",System.Data.SqlDbType.VarChar,500,ParameterDirection.Output,false,0,0,null,DataRowVersion.Original, msg_Error));
            lstParameter.Add(new SqlParameter("@OrganizationId", pSupervisorMachineMapVM.OrganizationId));
            return lstParameter.ToArray();
        }


        public SupervisorMachineDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}