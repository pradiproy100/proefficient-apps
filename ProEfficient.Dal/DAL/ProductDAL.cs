﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class ProductDAL : CoreDBAccess
    {

        private SqlParameter[] pUpdateMasterProduct(MasterProduct pMasterProduct)
        {

            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductId", pMasterProduct.ProductId));
            lstParameter.Add(new SqlParameter("@ProductName", pMasterProduct.ProductName));
            lstParameter.Add(new SqlParameter("@CycleTime", pMasterProduct.CycleTime));
            lstParameter.Add(new SqlParameter("@OrganizationId", pMasterProduct.OrganizationId));
            lstParameter.Add(new SqlParameter("@ProductCode", pMasterProduct.ProductCode));
            lstParameter.Add(new SqlParameter("@IsActive", pMasterProduct.IsActive));
            lstParameter.Add(new SqlParameter("@AddBy", pMasterProduct.AddBy));
            lstParameter.Add(new SqlParameter("@EdittedBy", pMasterProduct.EdittedBy));
            lstParameter.Add(new SqlParameter("@Type", pMasterProduct.ProductType));
            lstParameter.Add(new SqlParameter("@CatID", pMasterProduct.ProductCategoryID));
            lstParameter.Add(new SqlParameter("@unitID", pMasterProduct.UnitID));
            lstParameter.Add(new SqlParameter("@NewCategoryName", pMasterProduct.NewCategory));
            lstParameter.Add(new SqlParameter("@CustomerID", pMasterProduct.CustomerID));
            lstParameter.Add(new SqlParameter("@NewCustomerName", pMasterProduct.NewCustomerName));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pGetMasterProduct(MasterProduct pMasterProduct)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ProductId", pMasterProduct.ProductId));

            return lstParameter.ToArray();
        }



        public Boolean UpdateMasterProduct(MasterProduct pMasterProduct)
        {

            String StoredProcedureName = "USP_CreateUpdateProduct";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateMasterProduct(pMasterProduct));

            return result;
        }

        public List<MasterProduct> GetMasterProductList(int? OrganizationId = null, bool? IsActive = null,int ?productId=null)
        {
            String StoredProcedureName = "USP_GetProduct";
            Boolean IsSuccess;
            List<SqlParameter> lstParameter = new List<SqlParameter>();
         
            if (OrganizationId != null)
            {
                lstParameter.Add(new SqlParameter("@OrganizationId", OrganizationId));
            }
            if (IsActive != null)
            {
                lstParameter.Add(new SqlParameter("@IsActive", IsActive));
            }
            if (productId != null)
            {
                lstParameter.Add(new SqlParameter("@ProductId", productId));
            }
            var result = (List<MasterProduct>)ExecuteReaderProcedure(
                StoredProcedureName, MasterProductMapping
               , out IsSuccess, lstParameter.ToArray());
            return result;

        }
        public MasterProduct GetMasterProduct(MasterProduct pMasterProduct)
        {
            String StoredProcedureName = "USP_GetProduct";
            Boolean IsSuccess;
            var result = (List<MasterProduct>)ExecuteReaderProcedure(
                StoredProcedureName, MasterProductMapping
               , out IsSuccess, pGetMasterProduct(pMasterProduct));

            return result.FirstOrDefault();

        }


        private List<MasterProduct> MasterProductMapping(SqlDataReader Reader)
        {
            List<MasterProduct> LstUser = new List<MasterProduct>();
            while (Reader.Read())
            {
                LstUser.Add(new MasterProduct()
                {
                    ProductId = Reader["ProductId"].ToInteger0(),
                    ProductName = Reader["ProductName"].ToStringWithNullEmpty(),
                    CycleTime = Reader["CycleTime"].ToDecimal0(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0(),
                    ProductCode = Reader["ProductCode"].ToStringWithNullEmpty(),
                    IsActive = Reader["IsActive"].ToBoolean(),
                    AddBy = Reader["AddBy"].ToInteger0(),
                    EdittedBy = Reader["EdittedBy"].ToInteger0(),
                    ProductType = Reader["ProductType"].ToStringWithNullEmpty(),
                    ProductCategoryID = Reader["PrdouctCategoryID"].ToInteger0(),
                    UnitID = Reader["UnitID"].ToInteger0(),
                    CustomerID = Reader["CustomerID"].ToInteger0()
                });

            }
            return LstUser;
        }


        public ProductDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}