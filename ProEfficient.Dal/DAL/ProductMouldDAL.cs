﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProEfficient.Dal.DAL
{
    public class ProductMouldDAL : CoreDBAccess
    {
        public ProductMouldDAL(IConfiguration configuration) : base(configuration)
        {
        }
        public Boolean USP_SaveProductMould(Product_Mould_Rel prel)
        {
            String StoredProcedureName = "USP_CreateProduct_Mould_Rel";
            var result = ExecuteProcedure(StoredProcedureName,
                pUSP_SaveProductMould(prel));
            return result;
        }

        public Boolean USP_DeleteProductMouldRel(int organizationId,int productID)
        {
            String StoredProcedureName = "USP_DeleteProduct_Mould_Rel";
            DynamicParameters param = new DynamicParameters();
            param.Add("OrganizationId", organizationId);
            param.Add("ProductID", productID);
            var result = ExecuteProcedure(StoredProcedureName, param);
            return result;
        }

        private DynamicParameters pUSP_SaveProductMould(Product_Mould_Rel prel)
        {
            var parameters = new DynamicParameters(prel);
            return parameters;
        }

        public List<Product_Mould_Rel> GetProductMouldRel(int OrganizationId)
        {
            try
            {
                String StoredProcedureName = "USP_GetProDuctMouldRels";
                DynamicParameters param = new DynamicParameters();
                param.Add("OrganizationId", OrganizationId);
                var result = GetList<Product_Mould_Rel>(StoredProcedureName, param);

                return result.ToList();
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
