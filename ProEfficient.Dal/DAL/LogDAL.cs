﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ProEfficient.Dal.DAL
{
    public class LogDAL : CoreDBAccess
    {
        private SqlParameter[] pUpdateLog(AuditLog pLog)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@ModuleName", pLog.ModuleName));
            lstParameter.Add(new SqlParameter("@Message", pLog.Message));
            lstParameter.Add(new SqlParameter("@Level", pLog.Level));
            return lstParameter.ToArray();
        }

        public Boolean UpdateLog(AuditLog pLog)
        {

            String StoredProcedureName = "USP_CREATELog";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateLog(pLog));
            return result;
        }

        public LogDAL(IConfiguration configuration) : base(configuration)
        {
        }
    }
}