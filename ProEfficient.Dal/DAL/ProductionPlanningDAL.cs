﻿using Microsoft.Extensions.Configuration;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProEfficient.Dal.DAL
{
    public class ProductionPlanningDAL : CoreDBAccess
    {
        private readonly IConfiguration _configuration;
        public ProductionPlanningDAL(IConfiguration configuration) : base(configuration)
        {
            _configuration = configuration;
        }
        private SqlParameter[] pUpdateProductionPlanning(ProductionPlanning pProductionPlanning,
            Boolean IsRestOfTheMonth=false)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();

            lstParameter.Add(new SqlParameter("@SheduleStartDate", pProductionPlanning.StartDate));
            lstParameter.Add(new SqlParameter("@SheduleEndDate", pProductionPlanning.EndDate));
            lstParameter.Add(new SqlParameter("@IsRestOfTheMonth", IsRestOfTheMonth));
            lstParameter.Add(new SqlParameter("@OrganizationId", pProductionPlanning.OrganizationId));
            lstParameter.Add(new SqlParameter("@MachineId", pProductionPlanning.MachineId));        
            lstParameter.Add(new SqlParameter("@ProductId", pProductionPlanning.ProductId));
            lstParameter.Add(new SqlParameter("@UserId", pProductionPlanning.AddByUser));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pDeleteProductionPlanning(ProductionPlanning pProductionPlanning)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();

            lstParameter.Add(new SqlParameter("@SheduleStartDate", pProductionPlanning.StartDate));
            lstParameter.Add(new SqlParameter("@SheduleEndDate", pProductionPlanning.EndDate));          
            lstParameter.Add(new SqlParameter("@OrganizationId", pProductionPlanning.OrganizationId));
            return lstParameter.ToArray();
        }
        private SqlParameter[] pListBySheduleDate(DateTime DateVal, int OrganizationId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@SheduleDate", DateVal));
            lstParameter.Add(new SqlParameter("@OrganizationId", OrganizationId));
            return lstParameter.ToArray();
        }

        private SqlParameter[] pListByDateRange(DateTime StartDateVal, DateTime EndDateVal,int OrganizationId)
        {
            List<SqlParameter> lstParameter = new List<SqlParameter>();
            lstParameter.Add(new SqlParameter("@StartDate", StartDateVal));
            lstParameter.Add(new SqlParameter("@EndDate", EndDateVal));
            lstParameter.Add(new SqlParameter("@OrganizationId", OrganizationId));
            return lstParameter.ToArray();
        }


        private List<ProductionPlanning> ProductionPlanningMapping(SqlDataReader Reader)
        {
            List<ProductionPlanning> LstUser = new List<ProductionPlanning>();
            while (Reader.Read())
            {
                LstUser.Add(new ProductionPlanning()
                {
                    RelationId = Reader["RelationId"].ToInteger0(),
                    MachineId = Reader["MachineId"].ToInteger0(),
                    ProductId = Reader["ProductId"].ToInteger0(),
                    MachineName = Reader["MachineName"].ToString(),
                    ProductName = Reader["ProductName"].ToString(),
                    SheduleDate = Reader["SheduleDate"].ToDateTime(),
                    AddByUser = Reader["AddByUser"].ToInteger0(),
                    EditByUser = Reader["EditByUser"].ToInteger0(),
                    AddDate = Reader["AddDate"].ToDateTime(),
                    EditDate = Reader["EditDate"].ToDateTimeWithNull(),
                    OrganizationId = Reader["OrganizationId"].ToInteger0()
                });

            }
            return LstUser;
        }

        // Edit BreakdownEntry from Influx 
        public List<ProductionPlanning> GetListOfProductionPlanningBySheduleDate(DateTime sheduleDate, int organizationId)
        {
            String StoredProcedureName = "GetProductionPlanningByScheduleDate";
            Boolean IsSuccess;
            var result = (List<ProductionPlanning>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionPlanningMapping
               , out IsSuccess, pListBySheduleDate(sheduleDate, organizationId));

            return result;
        }
        public List<ProductionPlanning> GetListOfProductionPlanningBySheduleDateRange(DateTime sheduleStartDate, DateTime sheduleEndDate, int organizationId)
        {
            String StoredProcedureName = "GetProductionPlanningByDateRange";
            Boolean IsSuccess;
            var result = (List<ProductionPlanning>)ExecuteReaderProcedure(
                StoredProcedureName, ProductionPlanningMapping
               , out IsSuccess, pListByDateRange(sheduleStartDate,sheduleEndDate
               , organizationId));

            return result;
        }

        public Boolean UpdateProductionPlanning(ProductionPlanning pProductionPlanning,Boolean IsRestOfTheMonth=false)
        {

            String StoredProcedureName = "SaveProductionPlanning";
            var result = ExecuteProcedure(StoredProcedureName,
                pUpdateProductionPlanning(pProductionPlanning, IsRestOfTheMonth));

            return result;
        }

        public Boolean DeleteProductionPlanning(ProductionPlanning pProductionPlanning)
        {

            String StoredProcedureName = "USP_DeleteProductionPlanning";
            var result = ExecuteProcedure(StoredProcedureName,
                pDeleteProductionPlanning(pProductionPlanning));

            return result;
        }

       
    }
}