﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataSet = System.Data.DataSet;

namespace ProEfficient.Dal
{
    public abstract class CoreDBAccess
    {
       

        private String ConnectionString { get; set; }


        public CoreDBAccess(IConfiguration configuration)
        {
            ConnectionString = configuration.GetValue<string>("ConnectionString");
        }
        protected delegate object Mapping(SqlDataReader reader);

        protected object ExecuteReaderProcedure(String StoredProcedureName, Mapping mapped, out Boolean IsSuccess, params SqlParameter[] Params)
        {
            object mappedobject;
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand Command = new SqlCommand(StoredProcedureName, Connection);
                Command.CommandTimeout = 0;
                Command.CommandType = CommandType.StoredProcedure;
                if (Params != null)
                    Command.Parameters.AddRange(Params);

                Command.Connection = Connection;

                try
                {
                    mappedobject = mapped(Command.ExecuteReader());
                    IsSuccess = true;
                    Connection.Close();
                }
                catch (SqlException ex)
                {
                    IsSuccess = false;
                    mappedobject = null;
                }
                catch (Exception ex)
                {
                    IsSuccess = false;
                    mappedobject = null;
                }
                finally
                {
                    Connection.Close();
                } 
            } 
            return mappedobject;

        }


        protected object ExecuteReadDapperProcedure(string StoredProcedureName, out Boolean IsSuccess,DynamicParameters parameters)
        {
            object resultset;
            
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    //Connection.Open();
                    resultset = Connection.Query<object>(StoredProcedureName, parameters,
                    commandType: CommandType.StoredProcedure).ToList();
                    Connection.Close();

                    IsSuccess = true;
                   
                }
                catch (Exception ex)
                {

                    IsSuccess = false;
                    resultset = null;
                }

            }

            return resultset;
        }



        protected Boolean ExecuteDapperProcedure(string StoredProcedureName,DynamicParameters parameters)
        {
            bool result = false;
            return result;
        }


        protected Boolean ExecuteProcedure(String StoredProcedureName, DynamicParameters Params)
        {
            Boolean IsSuccess = true;
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Execute(StoredProcedureName, Params,
                                           commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception)
            {

                return false;
            }
            return IsSuccess;

        }
        protected IEnumerable<T> GetList <T>(String storedProcedureName, DynamicParameters parameters)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                   
                    var results = connection
                        .Query<T>(storedProcedureName, parameters,
                                                commandType: CommandType.StoredProcedure);
                    return results;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        protected Boolean ExecuteProcedure(String StoredProcedureName, params SqlParameter[] Params)
        {
            Boolean IsSuccess = true;
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();

                SqlCommand Command = new SqlCommand(StoredProcedureName, Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddRange(Params);
                Command.Connection = Connection;
                try
                {
                    using (var scope = new System.Transactions.TransactionScope())
                    {
                        try { 
                        Command.ExecuteNonQuery();
                            IsSuccess = true;
                            scope.Complete();
                        }
                        catch (Exception ex)
                        {
                            string str = ex.Message;
                            IsSuccess = false;

                        }
                       
                    }
                }
                catch (Exception ex)
                {
                    string str = ex.Message;
                    IsSuccess = false;

                }
                finally
                {
                    Connection.Close();
                }

            }

            return IsSuccess;

        }
        protected Boolean ExecuteProcedureOut(String StoredProcedureName, ref string msg_Error, params SqlParameter[] Params)
        {
            Boolean IsSuccess = true;
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();

                SqlCommand Command = new SqlCommand(StoredProcedureName, Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddRange(Params);
                Command.Connection = Connection;
                try
                {
                    using (var scope = new System.Transactions.TransactionScope())
                    {
                        Command.ExecuteNonQuery();
                        msg_Error = Convert.ToString(Params[7].Value);
                        IsSuccess = true;
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {

                    IsSuccess = false;

                }
                finally
                {
                    Connection.Close();
                }

            }

            return IsSuccess;

        }
        protected DataSet ExecuteDataSet(String StoredProcedureName, params SqlParameter[] Params)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da;
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();

                SqlCommand Command = new SqlCommand(StoredProcedureName, Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddRange(Params);
                Command.Connection = Connection;
                try
                {
                    using (var scope = new System.Transactions.TransactionScope())
                    {
                        da = new SqlDataAdapter(Command);
                        da.Fill(ds);
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {


                }
                finally
                {
                    Connection.Close();
                }

            }


            return ds;

        }
    }
}