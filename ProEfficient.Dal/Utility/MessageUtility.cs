﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace ProEfficient.Dal.Utility
{
    public static class MessageUtility
    {

        public static void ShowSuccessMessage(this Controller Controlllerobj, String Message)
        {
            Controlllerobj.TempData["SuccessMessage"] = Message;
        }
        public static void ShowSuccessAlertMessage(this Controller Controlllerobj, String Message)
        {
            Controlllerobj.TempData["SuccessAlertMessage"] = Message;
        }
        public static void ShowErrorAletMessage(this Controller Controlllerobj, String Message)
        {
            Controlllerobj.TempData["ErrorAlertMessage"] = Message;
        }
        public static void ErrorMessage(this Controller Controlllerobj, String Message)
        {
            Controlllerobj.TempData["ErrorMessage"] = Message;
        }

        public static void CreateObjectAlert(this Controller Controlllerobj, String ObjectName)
        {

            Controlllerobj.TempData["SuccessMessage"] = ObjectName + " has been created successfully";
        }
        public static void UpdatedObjectAlert(this Controller Controlllerobj, String ObjectName)
        {
            Controlllerobj.TempData["SuccessMessage"] = ObjectName + " has been updated successfully";
        }
        public static void DeletedObjectAlert(this Controller Controlllerobj, String ObjectName)
        {
            Controlllerobj.TempData["SuccessMessage"] = ObjectName + " has been deleted successfully";
        }
    }
}