﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace ProEfficient.Dal.Utility
{
    public static class UserUtility
    {
        private static IHttpContextAccessor _httpContextAccessor;
        private static IConfiguration _configuration;
        private static string SecretKey = "";
        public static void Configure(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            SecretKey = _configuration["SecretKey"];
        }

        public static HttpContext Current => _httpContextAccessor.HttpContext;
        public static object GetDataForColumn(string columnName, IDataReader reader)
        {
            reader.GetSchemaTable().DefaultView.RowFilter = "ColumnName= '" + columnName + "'";
            return reader.GetSchemaTable().DefaultView.Count > 0 ? reader[columnName] : null;
        }
        public static List<SelectListItem> AllUserTypes
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AlluserType = new UserManagementDAL(_configuration).GetAllUserType();
                foreach (var Type in AlluserType)
                {
                    if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
                    {
                        if (!UserUtility.IsSuperAdmin(Type.UserTypeID.ToInteger0()))
                        {
                            list.Add(new SelectListItem()
                            {
                                Text = Type.UserTypeName,
                                Value = Type.UserTypeID.ToStringWithNullEmpty()
                            });
                        }
                    }
                    else
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.UserTypeName,
                            Value = Type.UserTypeID.ToStringWithNullEmpty()
                        });
                    }
                }
                return list;
            }
        }
        public static List<SelectListItem> AllUserTypesEXcludeSuperAdmin
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AlluserType = new UserManagementDAL(_configuration).GetAllUserType();




                foreach (var Type in AlluserType)
                {
                    if (Type.UserTypeName != "SuperAdmin")
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.UserTypeName,
                            Value = Type.UserTypeID.ToStringWithNullEmpty()
                        });
                    }
                }
                return list;
            }
        }
        public static List<SelectListItem> AllUser
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var Alluser = new List<OrganizationUser>();
                if (IsSuperAdmin(CurrentUser.UserTypeId.ToInteger0()))
                {
                    Alluser = new UserManagementDAL(_configuration).GetUserList();
                }
                else
                {
                    Alluser = new UserManagementDAL(_configuration).GetUserList().FindAll(i => i.OrganizationId == CurrentUser.OrganizationId);
                }

                foreach (var Type in Alluser)
                {
                    if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.UserName,
                            Value = Type.UserId.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }
        public static List<SelectListItem> AllMachine
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var AllMachine = new List<MasterMachine>();

                AllMachine = new MachineDAL(_configuration).GetMasterMachineList().FindAll(i => i.OrganizationId == CurrentUser.OrganizationId);


                foreach (var Type in AllMachine)
                {
                    if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.MachineName,
                            Value = Type.MachineID.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }
        public static List<SelectListItem> MachineFromMappingList
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var AllMachine = new List<ProductMachineVM>();

                AllMachine = new MachineProductDAL(_configuration).GetProductMachineList(CurrentUser.OrganizationId, DateTime.Now);
                var distinct = AllMachine.GroupBy(x => x.MachineID).Select(x => x.FirstOrDefault());


                foreach (var Type in distinct)
                {

                    list.Add(new SelectListItem()
                    {
                        Text = Type.MachineName,
                        Value = Type.MachineID.ToStringWithNullEmpty()
                    });


                }
                return list;
            }
        }

        public static List<ProductMachineVM> ProductFromMappingList
        {
            get
            {

                var AllProduct = new List<ProductMachineVM>();

                AllProduct = new MachineProductDAL(_configuration).GetProductMachineList(CurrentUser.OrganizationId, DateTime.Now);

                return AllProduct;
            }
        }

        public static List<MasterMachine> AllOrgMachine
        {
            get
            {

                var AllMachine = new List<MasterMachine>();

                AllMachine = new MachineDAL(_configuration).GetMasterMachineList().FindAll(i => i.OrganizationId == CurrentUser.OrganizationId && i.IsActive.ToBoolean());
                return AllMachine;
            }
        }

        public static List<SelectListItem> AllShift
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AlluserType = new ShiftDAL(_configuration).GetShiftList().FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId);
                foreach (var Type in AlluserType)
                {

                    if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.ShiftName,
                            Value = Type.ShiftId.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }

        public static List<SelectListItem> AllStopageReason
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AllReason = new StopageReasonDAL(_configuration).GetStopageReasonList();
                foreach (var reason in AllReason)
                {

                    if (reason.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = reason.ReasonName,
                            Value = reason.ReasonId.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }
        public static Shift getShift(int ShiftId)
        {
            return new ShiftDAL(_configuration).GetShift(new Shift() { ShiftId = ShiftId });
        }
        public static OrganizationUser getUser(int UserId)
        {
            return new UserManagementDAL(_configuration).GetUserList().FirstOrDefault(i => i.UserId == UserId);
        }
        public static String getUserName(int UserId)
        {
            var UserObj = new UserManagementDAL(_configuration).GetUserList().FirstOrDefault(i => i.UserId == UserId);
            if (UserObj == null)
            {
                return String.Empty;
            }
            else
            {
                return UserObj.UserName;
            }
        }
        public static MasterMachine getMachine(int MachineId)
        {
            return new MachineDAL(_configuration).GetMasterMachine(new MasterMachine() { MachineID = MachineId });
        }

        public static MasterProduct getProduct(int ProductId)
        {
            return new ProductDAL(_configuration).GetMasterProduct(new MasterProduct() { ProductId = ProductId });
        }
        public static StopageReason getReason(int ReasonId)
        {
            return new StopageReasonDAL(_configuration).GetStopageReason(new StopageReason() { ReasonId = ReasonId });
        }
        public static Boolean IsAuthenticated
        {
            get
            {

                //return false;

                if (Current.Session.GetString("CurrentUser") == null)
                {
                    return false;
                }
                else

                {
                    return true;
                }
            }

        }
        public static APIUser CurrentUser
        {
            get
            {
                if (Current.Session.GetString("CurrentUser") == null)
                {
                    return null;
                }
                else
                {
                    var data = Current.Session.GetString("CurrentUser");
                    return JsonConvert.DeserializeObject<APIUser>(data);
                }
            }
            set => Current.Session.SetString("CurrentUser", JsonConvert.SerializeObject(value));
        }

        public static List<SelectListItem> AllOrganizations
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AllOrganization = new OrganizationDAL(_configuration).GetOrganizationList();
                //if (IsSuperAdmin(CurrentUser.UserTypeId.ToInteger0()))
                //{
                //    foreach (var Organization in AllOrganization)
                //    {
                //        if (Organization.IsActive.ToBoolean())
                //        {
                //            list.Add(new SelectListItem()
                //            {
                //                Text = Organization.OrganizationName,
                //                Value = Organization.OrganizationId.ToStringWithNullEmpty()
                //            });
                //        }

                //    }
                //}
                //else
                //{
                foreach (var Organization in AllOrganization)
                {
                    if (Organization.OrganizationId != CurrentUser.OrganizationId)
                    {
                        continue;
                    }
                    if (Organization.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Organization.OrganizationName,
                            Value = Organization.OrganizationId.ToStringWithNullEmpty()
                        });
                    }

                }
                //}
                return list;
            }
        }
        public static List<RejectReason> GetRejectReasonlst(int organisationid)
        {
            List<RejectReason> lstrejectreason = new List<RejectReason>();
            return lstrejectreason = new DAL.RejectionReason(_configuration).GetRejectionReason(organisationid);
        }


        public static List<SelectListItem> AllOrganizationsList
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AllOrganization = new OrganizationDAL(_configuration).GetOrganizationList();

                foreach (var Organization in AllOrganization)
                {

                    if (Organization.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Organization.OrganizationName,
                            Value = Organization.OrganizationId.ToStringWithNullEmpty()
                        });
                    }

                }

                return list;
            }
        }
        public static List<SelectListItem> AllStoppageSubReasons
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AllReason = new StopageSubReasonDAL(_configuration).GetStopageSubReasonList();
                foreach (var Reason in AllReason)
                {

                    list.Add(new SelectListItem()
                    {
                        Text = Reason.SubReasonName,
                        Value = Reason.ReasonId.ToStringWithNullEmpty()
                    });
                }
                return list;
            }
        }
        public static List<SelectListItem> ActiveReasons
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AllReason = new StopageReasonDAL(_configuration).GetStopageReasonList();
                foreach (var Reason in AllReason)
                {
                    if (Reason.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Reason.ReasonName,
                            Value = Reason.ReasonId.ToStringWithNullEmpty()
                        });
                    }
                }
                return list;
            }
        }
        public static List<SelectListItem> ActiveReasonsByOrganizationId(int organizationId)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var AllReason = new StopageReasonDAL(_configuration).GetStopageReasonListByOrganizationId(organizationId);
            foreach (var Reason in AllReason)
            {
                if (Reason.IsActive.ToBoolean())
                {
                    list.Add(new SelectListItem()
                    {
                        Text = Reason.ReasonName,
                        Value = Reason.ReasonId.ToStringWithNullEmpty()
                    });
                }
            }
            return list;
        }
        public static List<SelectListItem> ActiveSubReasons
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                var AllReason = new StopageSubReasonDAL(_configuration).GetStopageSubReasonList();
                foreach (var Reason in AllReason)
                {
                    if (Reason.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Reason.SubReasonName,
                            Value = Reason.ReasonId.ToStringWithNullEmpty()
                        });
                    }
                }
                return list;
            }
        }
        public static Boolean IsSuperAdmin(int UserTypeId)
        {
            return (new UserManagementDAL(_configuration).GetAllUserType().FirstOrDefault(i => i.UserTypeID == UserTypeId).UserTypeName.ToLower() == "SuperAdmin".ToLower());
        }
        public static Boolean IsAdmin(int UserTypeId)
        {
            return (new UserManagementDAL(_configuration).GetAllUserType().FirstOrDefault(i => i.UserTypeID == UserTypeId).UserTypeName.ToLower() == "Admin".ToLower());
        }
        public static Boolean IsApprover(int UserTypeId)
        {
            return (new UserManagementDAL(_configuration).GetAllUserType().FirstOrDefault(i => i.UserTypeID == UserTypeId).UserTypeName.ToLower() == "Supervisor".ToLower());
        }
        public static Boolean IsNormalUser(int UserTypeId)
        {
            return (new UserManagementDAL(_configuration).GetAllUserType().FirstOrDefault(i => i.UserTypeID == UserTypeId).UserTypeName.ToLower() == "Operator".ToLower());
        }

        public static List<SelectListItem> AllProduct
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var AllProducts = new List<MasterProduct>();

                AllProducts = new ProductDAL(_configuration).GetMasterProductList(CurrentUser.OrganizationId.ToInteger0(),true);

                foreach (var Type in AllProducts)
                {

                   
                        list.Add(new SelectListItem()
                        {
                            Text = Type.ProductName,
                            Value = Type.ProductId.ToStringWithNullEmpty()
                        });
                    

                }
                return list;
            }
        }

        public static List<SelectListItem> AllMachineCategory
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var AllMachine = new List<MachineCategory>();

                AllMachine = new MachineCategoryDAL(_configuration).GetMachineCategoryList().FindAll(i => i.OrganizationId == CurrentUser.OrganizationId);


                foreach (var Type in AllMachine)
                {
                    //if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.CategoryName,
                            Value = Type.CategoryId.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }

        public static List<MasterMachine> GetMachineIdsByCategory(int CategoryId)
        {
            List<MasterMachine> list = new List<MasterMachine>();
            var MachineList = new MachineDAL(_configuration).GetMasterMachineList();
            if (MachineList != null)
            {
                list = MachineList.FindAll(item => item.CategoryId == CategoryId && item.OrganizationId == CurrentUser.OrganizationId);
            }
            return list;
        }



        public static List<SelectListItem> Getstoppagereasonlst(int organisationID)
        {
            List<StopageReason> lststoppagereason = new StopageReasonDAL(_configuration).GetStopageReasonListWithOrganisation(organisationID);

            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem()
            {
                Text = "Select Reason",
                Value = "0"
            });

            for (int j = 0; j < lststoppagereason.Count; j++)
            {
                list.Add(new SelectListItem()
                {
                    Text = lststoppagereason[j].ReasonName,
                    Value = lststoppagereason[j].ReasonId.ToString()
                });
            }



            return list;
        }

        public static List<SubStoppageReason> GetSubstoppagereasonlst()
        {
            List<SubStoppageReason> list = new SubStoopageResonDAL(_configuration).GetSUBStoppageReason();
            return list;
        }
        public static List<SelectListItem> OrganizatioProdutcategory
        {

            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                int orgid = CurrentUser.OrganizationId.ToInteger();
                List<ProductCategory> lst = new OrganizationDAL(_configuration).
                    OrganizationCategoeylst(orgid).ToList();

                list.Add(new SelectListItem()
                {
                    Text = "Select Category",
                    Value = "0"
                });

                foreach (var Type in lst)
                {
                    //if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.CategoryName,
                            Value = Type.CategoryID.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }
        public static List<SelectListItem> OrganizationCustomer
        {

            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                int orgid = CurrentUser.OrganizationId.ToInteger();
                List<ProductCustomer> lst = new OrganizationDAL(_configuration).
                    OrganizationCustomerlst(orgid).ToList();

                list.Add(new SelectListItem()
                {
                    Text = "Select Customer",
                    Value = "0"
                });

                foreach (var Type in lst)
                {
                    //if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.CustomerName,
                            Value = Type.CustomerID.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }

        public static List<SelectListItem> OrganizatioUnits
        {

            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                int orgid = CurrentUser.OrganizationId.ToInteger();
                List<ProductUnits> lst = new OrganizationDAL(_configuration).
                    OrganizationUnitlst(orgid).ToList();

                foreach (var Type in lst)
                {
                    //if (Type.IsActive.ToBoolean())
                    {
                        list.Add(new SelectListItem()
                        {
                            Text = Type.unitName,
                            Value = Type.id.ToStringWithNullEmpty()
                        });
                    }

                }
                return list;
            }
        }


        public static List<SelectListItem> ToSelectList<T>(this List<T> input,
    Func<T, string> valueFunc,
    Func<T, string> nameFunc)
        {

            List<SelectListItem> lst = input.Select(i => new SelectListItem
            {
                Value = valueFunc(i),
                Text = nameFunc(i)
            }).ToList();
            return lst.GroupBy(x => x.Value).Select(x => x.First()).ToList();
        }

        //Encrypt Pass word


        public static string EncryptString(string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(SecretKey);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);

        }
        public static string DecryptString(string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(SecretKey);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        if (dr[column.ColumnName] != DBNull.Value)
                        {
                            pro.SetValue(obj, dr[column.ColumnName], null);
                        }
                        else
                            continue;
                }
            }
            return obj;
        }

        public static bool ChangeOrg(int orgId)
        {
            bool isChanged = false;

            if (Current.Session.GetString("CurrentUser") != null)
            {
                var data = Current.Session.GetString("CurrentUser");
                var value = JsonConvert.DeserializeObject<APIUser>(data);
                value.OrganizationId = orgId;

                var AllOrganization = new OrganizationDAL(_configuration).GetOrganizationList();

                value.OrganizationName = AllOrganization.Where(o => o.OrganizationId == orgId).Select(m => m.OrganizationName).FirstOrDefault();
                value.IsManualProductionEntry = AllOrganization.Where(o => o.OrganizationId == orgId).Select(m => m.IsManualProductionEntry).FirstOrDefault();
                value.HasBreakdown = AllOrganization.Where(o => o.OrganizationId == orgId).Select(m => m.HasBreakdown).FirstOrDefault();
                value.HasRejection = AllOrganization.Where(o => o.OrganizationId == orgId).Select(m => m.HasRejection).FirstOrDefault();
                Current.Session.SetString("CurrentUser", JsonConvert.SerializeObject(value));
                isChanged = true;
            }
            return isChanged;

        }
    }
}