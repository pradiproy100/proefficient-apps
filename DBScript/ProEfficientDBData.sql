USE [ProEfficientJKL]
GO
SET IDENTITY_INSERT [dbo].[Organization] ON 

GO
INSERT [dbo].[Organization] ([OrganizationId], [OrganizationName], [PhoneNo], [EmailId], [Location], [IsActive], [AddBy], [EdittedBy], [IsManualProductionEntry], [HasBreakdown], [HasRejection]) VALUES (5, N'JK Laxmi Cement', N'9810099031', N'manish@felidaesystems.com', NULL, 1, NULL, NULL, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Organization] OFF
GO

SET IDENTITY_INSERT [dbo].[UserType] ON 

GO
INSERT [dbo].[UserType] ([UserTypeID], [UserTypeName], [PermissionModule]) VALUES (1, N'Admin', NULL)
GO
INSERT [dbo].[UserType] ([UserTypeID], [UserTypeName], [PermissionModule]) VALUES (2, N'Supervisor', NULL)
GO
INSERT [dbo].[UserType] ([UserTypeID], [UserTypeName], [PermissionModule]) VALUES (3, N'Operator', NULL)
GO
SET IDENTITY_INSERT [dbo].[UserType] OFF
GO
SET IDENTITY_INSERT [dbo].[OrganizationUser] ON 

GO
INSERT [dbo].[OrganizationUser] ([UserId], [OrganizationId], [UserTypeId], [PhoneNo], [EmailId], [Password], [IsActive], [AddBy], [EdittedBy], [UserName], [ChangeCode]) VALUES (11, 5, 1, N'8889898989', N'manish@pmelectronics.in', N'Success', 1, NULL, 0, N'Manish_test', N'')
GO
SET IDENTITY_INSERT [dbo].[OrganizationUser] OFF
GO
SET IDENTITY_INSERT [dbo].[Shift] ON 

GO
INSERT [dbo].[Shift] ([ShiftId], [ShiftName], [ShiftDescription], [StartTime], [EndTime], [OrganizationId], [IsActive], [AddBy], [EdittedBy], [DurationMin], [IsOverNight]) VALUES (1, N'Shift A', NULL, CAST(6.00 AS Decimal(18, 2)), CAST(14.00 AS Decimal(18, 2)), 5, 1, 11, 11, 480, 0)
GO
INSERT [dbo].[Shift] ([ShiftId], [ShiftName], [ShiftDescription], [StartTime], [EndTime], [OrganizationId], [IsActive], [AddBy], [EdittedBy], [DurationMin], [IsOverNight]) VALUES (2, N'Shift B', NULL, CAST(14.00 AS Decimal(18, 2)), CAST(22.00 AS Decimal(18, 2)), 5, 1, 1, 1, 480, 0)
GO
INSERT [dbo].[Shift] ([ShiftId], [ShiftName], [ShiftDescription], [StartTime], [EndTime], [OrganizationId], [IsActive], [AddBy], [EdittedBy], [DurationMin], [IsOverNight]) VALUES (3, N'Shift C', NULL, CAST(22.00 AS Decimal(18, 2)), CAST(6.00 AS Decimal(18, 2)), 5, 1, 1, 1, 480, 1)
GO
SET IDENTITY_INSERT [dbo].[Shift] OFF
GO
SET IDENTITY_INSERT [dbo].[UnitMaster] ON 

GO
INSERT [dbo].[UnitMaster] ([ID], [Unit], [OrganizationID], [UnitPrefix], [UnitSuffix], [QtyMultiplier]) VALUES (1, N'TPH       ', 5, N'Tonne     ', N'hr        ', CAST(3600.000000 AS Decimal(18, 6)))
GO
SET IDENTITY_INSERT [dbo].[UnitMaster] OFF
GO

SET IDENTITY_INSERT [dbo].[AspNetRoles] ON 

GO
INSERT [dbo].[AspNetRoles] ([Id], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (1, NULL, N'Admin', N'ADMIN')
GO
INSERT [dbo].[AspNetRoles] ([Id], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (2, NULL, N'User', N'USER')
GO
SET IDENTITY_INSERT [dbo].[AspNetRoles] OFF
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (1, 1)
GO
SET IDENTITY_INSERT [dbo].[AspNetUsers] ON 

GO
INSERT [dbo].[AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName], [IsActive], [AddBy], [EdittedBy], [OrganizationId], [UserTypeId]) VALUES (1, 0, N'daeed642-0be3-425a-a914-741a22b11fe3', N'manish@pmelectronics.in', 0, 1, NULL, N'MANISH@PMELECTRONICS.IN', N'MANISH_TEST', N'AQAAAAEAACcQAAAAEOraA5gDNfipXZFzlAdxXFgSdL72iWVFYWZxT7sKzvb5ip//byeGjPOPFSPZ0EMBEQ==', N'9911351412', 0, N'GKEAXXCXRHO6HWYLVDA5BNBMARXQ2326', 0, N'Manish_test', 1, 0, 0, 5, 1)
GO

SET IDENTITY_INSERT [dbo].[AspNetUsers] OFF
GO

SET IDENTITY_INSERT [dbo].[Role] ON 

GO
INSERT [dbo].[Role] ([Id], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (1, NULL, N'Admin', N'ADMIN')
GO
INSERT [dbo].[Role] ([Id], [ConcurrencyStamp], [Name], [NormalizedName]) VALUES (2, NULL, N'User', N'USER')
GO
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (1, 1)
GO
