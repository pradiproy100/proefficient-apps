USE [ProEfficientJKL]
GO
/****** Object:  Table [dbo].[UtilityModule]    Script Date: 16-10-2022 11:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UtilityModule](
	[RecId] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](200) NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UtilityModule] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetUtilityModule]    Script Date: 16-10-2022 11:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetUtilityModule]
@OrganizationID int
AS
BEGIN
SELECT * FROM [dbo].[UtilityModule] WHERE OrganizationId=@OrganizationID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_SaveUtilityModule]    Script Date: 16-10-2022 11:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_SaveUtilityModule]
	@RecId int = NULL,
	@ModuleName nvarchar(200) =NULL,
	@OrganizationId int =NULL,
	@IsActive bit =NULL
AS
BEGIN
IF NOT EXISTS(SELECT TOP 1* FROM UtilityModule WHERE RecId=@RecId)
BEGIN
INSERT INTO UtilityModule(	
	ModuleName ,
	OrganizationId ,
	IsActive 
)
VALUES(	
	@ModuleName ,
	@OrganizationId ,
	@IsActive 
)
END
ELSE
BEGIN
UPDATE UtilityModule SET 
								ModuleName=@ModuleName ,
									OrganizationId=@OrganizationId ,
									IsActive =@IsActive
																	WHERE RecId=@RecId
END
END
GO
