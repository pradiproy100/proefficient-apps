USE [master]
GO
/****** Object:  Database [proefficient_mod]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE DATABASE [proefficient_mod]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'proefficient_mod', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER3\MSSQL\DATA\proefficient_mod.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'proefficient_mod_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER3\MSSQL\DATA\proefficient_mod_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [proefficient_mod] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [proefficient_mod].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [proefficient_mod] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [proefficient_mod] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [proefficient_mod] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [proefficient_mod] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [proefficient_mod] SET ARITHABORT OFF 
GO
ALTER DATABASE [proefficient_mod] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [proefficient_mod] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [proefficient_mod] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [proefficient_mod] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [proefficient_mod] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [proefficient_mod] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [proefficient_mod] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [proefficient_mod] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [proefficient_mod] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [proefficient_mod] SET  DISABLE_BROKER 
GO
ALTER DATABASE [proefficient_mod] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [proefficient_mod] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [proefficient_mod] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [proefficient_mod] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [proefficient_mod] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [proefficient_mod] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [proefficient_mod] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [proefficient_mod] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [proefficient_mod] SET  MULTI_USER 
GO
ALTER DATABASE [proefficient_mod] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [proefficient_mod] SET DB_CHAINING OFF 
GO
ALTER DATABASE [proefficient_mod] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [proefficient_mod] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [proefficient_mod] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [proefficient_mod] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [proefficient_mod] SET QUERY_STORE = OFF
GO
USE [proefficient_mod]
GO
/****** Object:  UserDefinedTableType [dbo].[BreakdownEditTableType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[BreakdownEditTableType] AS TABLE(
	[BreakdownId] [int] NULL,
	[ReasonId] [int] NULL,
	[SubReasonId] [int] NULL,
	[Remarks] [varchar](500) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[BreakdownTableType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[BreakdownTableType] AS TABLE(
	[EntryTime] [datetime] NULL,
	[MachineName] [varchar](50) NULL,
	[ProdName] [varchar](50) NULL,
	[ShiftName] [varchar](50) NULL,
	[ReasonId] [int] NULL,
	[TotalStoppageTime] [int] NULL,
	[StartTime] [bigint] NULL,
	[EndTime] [bigint] NULL,
	[IsIdle] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MachineMasterType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[MachineMasterType] AS TABLE(
	[Organization] [int] NULL,
	[MachineCode] [varchar](50) NULL,
	[MachineName] [varchar](50) NULL,
	[Category] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[NonStop] [varchar](50) NULL,
	[DemandTypeText] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MachineProductMap]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[MachineProductMap] AS TABLE(
	[MachineId] [int] NULL,
	[ProductId] [int] NULL,
	[CycleTime] [decimal](18, 2) NULL,
	[DemandTypeText] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[MachineProductMapType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[MachineProductMapType] AS TABLE(
	[MachineCode] [varchar](50) NULL,
	[ProductCode] [varchar](50) NULL,
	[ProductDemandType] [varchar](50) NULL,
	[MachineDemandType] [varchar](50) NULL,
	[CycleTime] [decimal](18, 2) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[ProductionTableType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[ProductionTableType] AS TABLE(
	[EntryTime] [datetime] NULL,
	[MachineName] [varchar](50) NULL,
	[ProdName] [varchar](50) NULL,
	[ShiftName] [varchar](50) NULL,
	[Rejection] [int] NULL,
	[TotalProduction] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[ProductMasterType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[ProductMasterType] AS TABLE(
	[Organization] [int] NULL,
	[ProductCode] [varchar](50) NULL,
	[ProductName] [varchar](50) NULL,
	[ProductCategory] [varchar](50) NULL,
	[Customer] [varchar](50) NULL,
	[Unit] [varchar](50) NULL,
	[CycleTime] [decimal](18, 2) NULL,
	[DemandTypeText] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[QualityTableType]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[QualityTableType] AS TABLE(
	[EntryTime] [datetime] NULL,
	[MachineName] [varchar](50) NULL,
	[ProdName] [varchar](50) NULL,
	[ShiftName] [varchar](50) NULL,
	[ReasonIds] [varchar](50) NULL,
	[ProdCount] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[tblBreakdownENtry]    Script Date: 12/29/2022 11:47:19 PM ******/
CREATE TYPE [dbo].[tblBreakdownENtry] AS TABLE(
	[BreakDownID] [int] IDENTITY(1,1) NOT NULL,
	[MachineCategoryId] [int] NULL,
	[MachineID] [int] NULL,
	[ReasonId] [int] NULL,
	[SubReasonId] [int] NULL,
	[UserID] [int] NULL,
	[OperatorID] [int] NULL,
	[SupervisorID] [int] NULL,
	[TimeOfEntry] [datetime] NULL,
	[ShiftId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalStoppageTime] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[ProductionDate] [datetime] NULL,
	[IsSaved] [bit] NULL,
	[Remarks] [nvarchar](500) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[CSVtoTable]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Arindam Ghosh>
-- Create date: <02-Apr-2020>
-- Description:    <Convert CSV to Table>
-- =============================================
CREATE FUNCTION [dbo].[CSVtoTable]
(
    @LIST varchar(7000),
    @Delimeter varchar(10)
)
RETURNS @RET1 TABLE (RESULT VARCHAR(50))
AS
BEGIN
    DECLARE @RET TABLE(RESULT VARCHAR(50))
    
    IF LTRIM(RTRIM(@LIST))='' RETURN  

    DECLARE @START BIGINT
    DECLARE @LASTSTART BIGINT
    SET @LASTSTART=0
    SET @START=CHARINDEX(@Delimeter,@LIST,0)

    IF @START=0
    INSERT INTO @RET VALUES(SUBSTRING(@LIST,0,LEN(@LIST)+1))

    WHILE(@START >0)
    BEGIN
        INSERT INTO @RET VALUES(SUBSTRING(@LIST,@LASTSTART,@START-@LASTSTART))
        SET @LASTSTART=@START+1
        SET @START=CHARINDEX(@Delimeter,@LIST,@START+1)
        IF(@START=0)
        INSERT INTO @RET VALUES(SUBSTRING(@LIST,@LASTSTART,LEN(@LIST)+1))
    END
    
    INSERT INTO @RET1 SELECT * FROM @RET
    RETURN 
END





GO
/****** Object:  UserDefinedFunction [dbo].[fn_ConvertEpochToDateTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_ConvertEpochToDateTime] (@Datetime BIGINT)
RETURNS DATETIME
AS
BEGIN
    DECLARE @LocalTimeOffset BIGINT
           ,@AdjustedLocalDatetime BIGINT;
    SET @LocalTimeOffset = DATEDIFF(second,GETDATE(),GETUTCDATE())
    SET @AdjustedLocalDatetime = @Datetime - @LocalTimeOffset
    RETURN (SELECT DATEADD(second,@AdjustedLocalDatetime, CAST('1970-01-01 00:00:00' AS datetime)))
END;




GO
/****** Object:  UserDefinedFunction [dbo].[fn_Split]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  FUNCTION [dbo].[fn_Split](@text nvarchar(max), @delimiter nvarchar(20) = ' ')
RETURNS @Strings TABLE
(    
  position int IDENTITY PRIMARY KEY,
  value nvarchar(max)   
)

AS

BEGIN
Set @text = dbo.fn_Trim(@text,' ')
DECLARE @index int 
SET @index = -1 
WHILE (LEN(@text) > 0) 
  BEGIN  
    SET @index = CHARINDEX(@delimiter , @text)  
    IF (@index = 0) AND (LEN(@text) > 0)  
      BEGIN   
        INSERT INTO @Strings VALUES (@text)
          BREAK  
      END  
    IF (@index > 0)  
      BEGIN   
        INSERT INTO @Strings VALUES (LEFT(@text, @index - 1))   
        SET @text = RIGHT(@text, (LEN(@text) - @index))  
      END  
    ELSE 
      SET @text = RIGHT(@text, (LEN(@text) - @index)) 
    END
  RETURN
END






GO
/****** Object:  UserDefinedFunction [dbo].[fn_TRIM]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[fn_TRIM](@string nVARCHAR(Max), @trimCharacter nvarchar(50) = ' ')
RETURNS nVARCHAR(Max)
BEGIN
Declare @Ret nVARCHAR(Max)
Select @Ret = RTRIM(LTRIM(REPLACE(REPLACE(RTRIM(LTRIM(REPLACE(@string, @trimCharacter, '   '))), '   ', @trimCharacter),' ','')))
--Select @Ret = REPLACE(LTRIM(REPLACE(@string, @trimCharacter, ' ')), ' ', @trimCharacter)
RETURN @Ret
END








GO
/****** Object:  UserDefinedFunction [dbo].[fnValidateSupervisorSchedule]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnValidateSupervisorSchedule]
(
@StartDate datetime,
@EndDate datetime,
@machineId int,
@shiftId int,
@SupervisorMachineID int)
RETURNS varchar(500)
AS
BEGIN
-- select [dbo].[fnValidateSupervisorSchedule]('2020-12-09','2020-12-11',9,2,4)
	declare @IsExistsErr varchar(500)='';
	if exists(select 1  FROM [dbo].[SupervisorMachineMap]
	 where  (@StartDate <= EndDate) and (@EndDate >= StartDate) and shiftId=@shiftId and machineId=@machineId and SupervisorMachineID!=@SupervisorMachineID)
	 begin
		select @IsExistsErr= MachineName from MasterMachine where MachineId=@machineId;
	 end
	 
	

RETURN @IsExistsErr

END




GO
/****** Object:  UserDefinedFunction [dbo].[Get1If0]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Get1If0](
@val  DECIMAL(18,2)
)
Returns  DECIMAL(18,2)
AS
BEGIN
	
	IF @val IS NULL OR @val=0
	BEGIN
	RETURN 1
	END
	
	RETURN @val
	

END




GO
/****** Object:  UserDefinedFunction [dbo].[GetActualRatio]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetActualRatio]
(

@PRODUCTIONDATE DATETIME,      
@MACHINEID VARCHAR(10),      
@SHIFT_ID VARCHAR(10)  )
RETURNS DECIMAL(18,2)
AS
BEGIN

DECLARE @TotBreakTime INT, @TotShiftTime INT ,@ProductionTime DECIMAL(18,2),  
@TotBrkDownTime DECIMAL(18,2),@Cy DECIMAL(18,2)     
DECLARE @TotalProduction DECIMAL(18,2),@sp DECIMAL(18,2),@tr DECIMAL(18,2),  
@AR DECIMAL(18,2),@PR DECIMAL(18,2),@QR DECIMAL(18,2)    
DECLARE @dateCnt INT,@cnt INT,@date DATE,@dateCntShft INT,@cntShft INT,@OEE DECIMAL(18,2)
DECLARE @TotBrkDownTimeMngd decimal(18,2),@TotalULoss decimal(18,2),@weightedCY decimal(18,2) 

SELECT @TotBreakTime=dbo.GetToTalBreakTime(@SHIFT_ID)   
SELECT @TotShiftTime=dbo.GetToTalShiftTime(@SHIFT_ID)
SELECT @date=  @PRODUCTIONDATE
SET @ProductionTime=isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)  
SELECT @TotBrkDownTime=dbo.GetToTalBreakDownTime(@SHIFT_ID,@MACHINEID,@date)	
SELECT @TotBrkDownTimeMngd=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,1)		
SELECT @TotalULoss=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,0)
SELECT @Cy= dbo.GetCyCleRatio(@SHIFT_ID,@MachineId,@date)
SELECT  @weightedCY=dbo.GetWeightedCyCle(@SHIFT_ID,@MachineId,@date)


SET @AR=(isnull(@ProductionTime,0)-isnull(@TotBrkDownTime,0))/(isnull(@ProductionTime,0)-isnull(@TotBrkDownTimeMngd,0))    
--set @PR=isnull(@sp,0)/@TotalProduction    
SET @PR=@weightedCY/(isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)-isnull(@TotBrkDownTimeMngd,0)-isnull(@TotalULoss,0))    
SET @PR=@PR/60    
SET @QR=(isnull(@sp,0)-isnull(@tr,0))/@sp    
SET @OEE= isnull(@AR,0)*isnull(@PR,0)*isnull(@QR,0)  

RETURN @AR

END




GO
/****** Object:  UserDefinedFunction [dbo].[GetBreakDownTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetBreakDownTime] (@productionDate Date,@ShiftId int,@MachineId int,@StoppageReasonId int)
RETURNS Decimal(18,2) AS
BEGIN
  RETURN  (select (Sum( isnull(TotalStoppageTime,0))/60) from  BreakDownEntry 
  where cast( ProductionDate as date)=@productionDate and 
	MachineID=@MachineId and ShiftId=@ShiftId	
	and ReasonId=@StoppageReasonId
	)     
END


GO
/****** Object:  UserDefinedFunction [dbo].[GetCyCleRatio]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetCyCleRatio](
@ShiftId Int,
@MachineId int,
@ProductionDate DateTime

)
Returns  DECIMAL(18,2)
AS
begin
DECLARE @Result DECIMAL(18,2)=0
DECLARE @weightedCY DECIMAL(18,2)=0

SELECT @Result=sum(CycleTime)/count(CycleTime) FROM MasterProduct 
WHERE ProductId in   (SELECT DISTINCT(ProductID)  from ProductionEntry WHERE    
CAST(STARTTIME AS DATE) =@ProductionDate AND MachineID=@MACHINEID  AND shiftid=@ShiftId ) and IsActive=1    
SELECT @weightedCY= sum(TP*P.cycletime) FROM  
(SELECT ProductID,sum(TotalProduction) TP FROM ProductionEntry WHERE CAST(STARTTIME AS DATE) =@ProductionDate AND MachineID=@MACHINEID    
AND shiftid=@ShiftId group by ProductID ) A     
join [MasterProduct] P on A.ProductID=P.ProductID

return @Result
end




GO
/****** Object:  UserDefinedFunction [dbo].[GetCyCleTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetCyCleTime](
@Productid Int
)
Returns  DECIMAL(18,2)
AS
BEGIN
		DECLARE @Result DECIMAL(18,2)=0
		SELECT  @Result=CycleTime FROM  MasterProduct WHERE ProductId=@Productid AND ISNULL(IsActive,0)=1
		return @Result
END




GO
/****** Object:  UserDefinedFunction [dbo].[getDie_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getDie_Name] (  @organizationId int ,@DieId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @DieName VARCHAR(200)  =''
  select top 1 @DieName=DieDescription from  MasterDie where DieId=@DieId and OrganizationId=@organizationId
    RETURN @DieName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetDynamicColumnString]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetDynamicColumnString](  
@str varchar(500),  
@prefix varchar(10),  
@aggregate varchar(10),  
@orgId int  
  
  
)  
Returns  varchar(500)  
AS  
begin  
 declare @shiftnames varchar(100)='[ShiftA],[ShiftB],[ShiftC],[OverAll]'  
 DECLARE  @shiftcnt int=0;  
SELECT @shiftcnt =count(ShiftName) FROM [Shift] where organizationid=@orgId AND IsActive=1;  
if(@shiftcnt=2)  
begin   
set @shiftnames ='[ShiftA],[ShiftB],[OverAll]'  
end  
  
--SET @shiftnames = @shiftnames+'[OverAll]';  
  
 declare @returnstr varchar(500)=''  
 if(@prefix!='')  
 begin   
 SELECT @returnstr += replace(value,'[','['+@prefix)  + ',' from dbo.fn_split(@str,',');   
 end  
 if(@aggregate!='')  
 begin   
  if(@prefix!='')  
 begin   
 set @returnstr=''  
 SELECT @returnstr += replace(replace(a.value,'[',@aggregate+'(['+@prefix),']',']) as '+replace(b.value,'[','['+@prefix))  + ',' from dbo.fn_split(@str,',') a  
 inner join dbo.fn_split(@shiftnames,',') b on a.position=b.position;   
 end  
 else  
 begin  
 SELECT @returnstr += replace(replace(a.value,'[',@aggregate+'(['),']',']) as '+b.value)  + ',' from dbo.fn_split(@str,',') a  
 inner join dbo.fn_split(@shiftnames,',') b on a.position=b.position;   
 end  
  end  
  select  @returnstr = Left(@returnstr,len(@returnstr)-1)  
    
   
return @returnstr  
end  
  
  
GO
/****** Object:  UserDefinedFunction [dbo].[getMachine_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getMachine_Name] (  @organizationId int ,@MachineId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @MachineCode VARCHAR(200)  =''
  select top 1 @MachineCode=MachineName from  MasterMachine where MachineId=@MachineId and OrganizationId=@organizationId
    RETURN @MachineCode
END
GO
/****** Object:  UserDefinedFunction [dbo].[getMould_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getMould_Name] (  @organizationId int ,@MouldId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @MouldName VARCHAR(200)  =''
  select top 1 @MouldName=MouldDescription from  MasterMould where MouldId=@MouldId and OrganizationId=@organizationId
    RETURN @MouldName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetOEE]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetOEE]
(

@PRODUCTIONDATE DATETIME,      
@MACHINEID VARCHAR(10),      
@SHIFT_ID VARCHAR(10)  )
RETURNS DECIMAL(18,2)
AS
BEGIN

DECLARE @TotBreakTime INT, @TotShiftTime INT ,@ProductionTime DECIMAL(18,2),  
@TotBrkDownTime DECIMAL(18,2),@Cy DECIMAL(18,2)     
DECLARE @TotalProduction DECIMAL(18,2),@sp DECIMAL(18,2),@tr DECIMAL(18,2),  
@AR DECIMAL(18,2),@PR DECIMAL(18,2),@QR DECIMAL(18,2)    
DECLARE @dateCnt INT,@cnt INT,@date DATE,@dateCntShft INT,@cntShft INT,@OEE DECIMAL(18,2)
DECLARE @TotBrkDownTimeMngd decimal(18,2),@TotalULoss decimal(18,2),@weightedCY decimal(18,2) 

SELECT @TotBreakTime=dbo.GetToTalBreakTime(@SHIFT_ID)   
SELECT @TotShiftTime=dbo.GetToTalShiftTime(@SHIFT_ID)
SELECT @date=  @PRODUCTIONDATE
SET @ProductionTime=isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)  
SELECT @TotBrkDownTime=dbo.GetToTalBreakDownTime(@SHIFT_ID,@MACHINEID,@date)	
SELECT @TotBrkDownTimeMngd=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,1)		
SELECT @TotalULoss=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,0)
SELECT @Cy= dbo.GetCyCleRatio(@SHIFT_ID,@MachineId,@date)
SELECT  @weightedCY=dbo.GetWeightedCyCle(@SHIFT_ID,@MachineId,@date)


SET @AR=(isnull(@ProductionTime,0)-isnull(@TotBrkDownTime,0))/(isnull(@ProductionTime,0)-isnull(@TotBrkDownTimeMngd,0))    
--set @PR=isnull(@sp,0)/@TotalProduction    
SET @PR=@weightedCY/(isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)-isnull(@TotBrkDownTimeMngd,0)-isnull(@TotalULoss,0))    
SET @PR=@PR/60    
SET @QR=(isnull(@sp,0)-isnull(@tr,0))/@sp    
SET @OEE= isnull(@AR,0)*isnull(@PR,0)*isnull(@QR,0)  

RETURN @OEE

END




GO
/****** Object:  UserDefinedFunction [dbo].[getOperation_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getOperation_Name] (  @organizationId int ,@OperationId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @OperationName VARCHAR(200)  =''
  select top 1 @OperationName=OperationDescription from  MasterOperation where OperationId=@OperationId and OrganizationId=@organizationId
    RETURN @OperationName
END
GO
/****** Object:  UserDefinedFunction [dbo].[getOperator_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getOperator_Name] (  @organizationId int ,@OperatorId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @OperatorCode VARCHAR(200)  =''
  select top 1 @OperatorCode=OperatorName from  MasterOperator where OperatorId=@OperatorId and OrganizationId=@organizationId
    RETURN @OperatorCode
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetOrganizationIdByMachineId]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,> 
-- Description:	<Description,,>  SELECT dbo.GetTimeInMinutes(12.0,24)  select * from Shift
-- =============================================
CREATE FUNCTION [dbo].[GetOrganizationIdByMachineId]
(	
	@MachineId int
)
RETURNS int 
AS
BEGIN
DECLARE @Result int=0
SELECT TOP 1 @Result=OrganizationId FROM  [dbo].[MasterMachine] WHERE MachineId=@MachineId
RETURN @Result
END





GO
/****** Object:  UserDefinedFunction [dbo].[GetProducionQualityLossProduct]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetProducionQualityLossProduct]
(@ShiftId Int,
@MachineId int,
@ProductionDate DateTime
)
Returns  DECIMAL(18,2)
AS
begin
DECLARE @Result DECIMAL(18,2)=0
DECLARE @weightedCY DECIMAL(18,2)=0

SELECT  @Result=isnull(sum((PE.Rejection*MP.CycleTime)/60),0) FROM product_machine_rel MP INNER JOIN  ProductionEntry PE 
ON PE.ProductID =MP.ProductId --and PE.ProductID=@ProductId
 AND CAST(PE.ProductionDate AS DATE)  = CAST(@ProductionDate AS date)  AND PE.MachineID=@MACHINEID    
 AND shiftID=@ShiftId group by PE.ProductionDate

return @Result
end






GO
/****** Object:  UserDefinedFunction [dbo].[GetProducionTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetProducionTime](    
@ShiftId Int,    
@MachineId int,    
@ProductionDate DateTime    
)    
Returns  DECIMAL(18,2)    
AS    
BEGIN    
DECLARE @Result DECIMAL(18,2)=0    
 
 Declare @ShiftDuration Decimal(18,2)=0
 Declare @BreakDownTime Decimal(18,2)=0

SELECT @ShiftDuration=DurationMin from  Shift where ShiftId=@ShiftId
SELECT @BreakDownTime=TotalStoppageTime from  BreakDownEntry where ShiftId=@ShiftId and MachineID=@MachineId and
CAST(ProductionDate as date)=CAST(@ProductionDate as date)
   IF @BreakDownTime>0
   BEGIN
    SET @BreakDownTime=(@BreakDownTime/60)
   END
   set @Result=ROUND(@ShiftDuration-@BreakDownTime,2)
return @Result    
end        
    
    
	
	
	
	
	
GO
/****** Object:  UserDefinedFunction [dbo].[GetProducionTimebyProduct]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetProducionTimebyProduct](    
@ShiftId Int,    
@MachineId int,    
@ProductionDate DateTime    
)    
Returns  DECIMAL(18,2)    
AS    
BEGIN    
DECLARE @Result DECIMAL(18,2)=0    
 
;WITH CTE
AS
(
	SELECT r.MachineID,R.ProductID,(CASE WHEN u.QtyMultiplier>1 THEN u.QtyMultiplier / r.CycleTime ELSE r.CycleTime END) AS WeightedCY FROM MasterProduct p INNER JOIN unitmaster u ON p.Unitid=u.id      
 INNER JOIN product_Machine_Rel r ON r.ProductId=p.ProductId AND r.MachineID=@MachineId)
    
SELECT  @Result=isnull(sum(((PE.TotalProduction-PE.Rejection) * CTE.WeightedCY)/60),0) FROM CTE INNER JOIN product_machine_rel MP ON CTE.MachineID=MP.MachineID AND CTE.ProductID=MP.ProductID
INNER JOIN  ProductionEntry PE     
ON PE.ProductID =MP.ProductId and PE.MachineID=MP.MachineID    
 AND CAST(PE.ProductionDate AS DATE)  = CAST(@ProductionDate AS date)  AND PE.MachineID =@MachineId        
 AND PE.ShiftId=@ShiftId group by CONVERT(DATE,PE.ProductionDate)
   
return @Result    
end        
    
    
	
	
	
	
	
GO
/****** Object:  UserDefinedFunction [dbo].[getProduct_Code]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getProduct_Code] (  @organizationId int ,@ProductId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @ProductCode VARCHAR(200)  =''
  select top 1 @ProductCode=ProductCode from  MasterProduct where ProductId=@ProductId and OrganizationId=@organizationId
    RETURN @ProductCode
END
GO
/****** Object:  UserDefinedFunction [dbo].[getProduct_CycleTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[getProduct_CycleTime] (  @organizationId int ,@ProductId int)  
RETURNS Decimal(18,2)  AS  
BEGIN  
     
  declare @ProductCycle Decimal(18,2)  =0  
  select top 1 @ProductCycle=CycleTime from  MasterProduct where ProductId=@ProductId and OrganizationId=@organizationId  
    RETURN @ProductCycle  
END  
  
GO
/****** Object:  UserDefinedFunction [dbo].[getProduct_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE FUNCTION [dbo].[getProduct_Name] (  @organizationId int ,@ProductId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @ProductName VARCHAR(200)  =''
  select top 1 @ProductName=ProductName from  MasterProduct where ProductId=@ProductId and OrganizationId=@organizationId
    RETURN @ProductName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetProductionInEfficiency]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetProductionInEfficiency](
@ShiftId Int,
@MachineId int ,
@ProductionDate DateTime
)
Returns  DECIMAL(18,2)
AS
BEGIN
		DECLARE @Result DECIMAL(18,2)=0
		
		
select  Top 1 @Result=(720-Isnull(ShutdownTime,0)-isnull(BreakTime,0)-isnull(UtilisationLoss,0)-isnull(TotalProductointime,0)) from
[dbo].[OEECalculation]
where 
cast(ProductionDate as date)=cast(@ProductionDate as date)
and ShiftId=@ShiftId and  MachineId=@MachineId
		return @Result
END





GO
/****** Object:  UserDefinedFunction [dbo].[GetProductionRatio]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetProductionRatio]
(

@PRODUCTIONDATE DATETIME,      
@MACHINEID VARCHAR(10),      
@SHIFT_ID VARCHAR(10)  )
RETURNS DECIMAL(18,2)
AS
BEGIN

DECLARE @TotBreakTime INT, @TotShiftTime INT ,@ProductionTime DECIMAL(18,2),  
@TotBrkDownTime DECIMAL(18,2),@Cy DECIMAL(18,2)     
DECLARE @TotalProduction DECIMAL(18,2),@sp DECIMAL(18,2),@tr DECIMAL(18,2),  
@AR DECIMAL(18,2),@PR DECIMAL(18,2),@QR DECIMAL(18,2)    
DECLARE @dateCnt INT,@cnt INT,@date DATE,@dateCntShft INT,@cntShft INT,@OEE DECIMAL(18,2)
DECLARE @TotBrkDownTimeMngd decimal(18,2),@TotalULoss decimal(18,2),@weightedCY decimal(18,2) 

SELECT @TotBreakTime=dbo.GetToTalBreakTime(@SHIFT_ID)   
SELECT @TotShiftTime=dbo.GetToTalShiftTime(@SHIFT_ID)
SELECT @date=  @PRODUCTIONDATE
SET @ProductionTime=isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)  
SELECT @TotBrkDownTime=dbo.GetToTalBreakDownTime(@SHIFT_ID,@MACHINEID,@date)	
SELECT @TotBrkDownTimeMngd=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,1)		
SELECT @TotalULoss=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,0)
SELECT @Cy= dbo.GetCyCleRatio(@SHIFT_ID,@MachineId,@date)
SELECT  @weightedCY=dbo.GetWeightedCyCle(@SHIFT_ID,@MachineId,@date)


SET @AR=(isnull(@ProductionTime,0)-isnull(@TotBrkDownTime,0))/(isnull(@ProductionTime,0)-isnull(@TotBrkDownTimeMngd,0))    
--set @PR=isnull(@sp,0)/@TotalProduction    
SET @PR=@weightedCY/(isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)-isnull(@TotBrkDownTimeMngd,0)-isnull(@TotalULoss,0))    
SET @PR=@PR/60    
SET @QR=(isnull(@sp,0)-isnull(@tr,0))/@sp    
SET @OEE= isnull(@AR,0)*isnull(@PR,0)*isnull(@QR,0)  

RETURN @PR

END




GO
/****** Object:  UserDefinedFunction [dbo].[getProductName]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getProductName] (  @organizationId int ,@MachineId int,@ProductionDate Date)
RETURNS VARCHAR(200) AS
BEGIN
    Declare @ProductId int =0
    DECLARE @ProductName VARCHAR(200)=''

	select top 1 @ProductId=ProductId From ProductionPlanning
	where OrganizationId=@organizationId and MachineId=@MachineId
	and Cast(SheduleDate as Date)=Cast(@ProductionDate as Date)

	select top 1 @ProductName=ISNULL(ProductName,'')
	from  MasterProduct where  ProductID=@ProductId and OrganizationId=@organizationId

    RETURN @ProductName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetQR]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetQR]
(

@PRODUCTIONDATE DATETIME,      
@MACHINEID VARCHAR(10),      
@SHIFT_ID VARCHAR(10)  )
RETURNS DECIMAL(18,2)
AS
BEGIN

DECLARE @TotBreakTime INT, @TotShiftTime INT ,@ProductionTime DECIMAL(18,2),  
@TotBrkDownTime DECIMAL(18,2),@Cy DECIMAL(18,2)     
DECLARE @TotalProduction DECIMAL(18,2),@sp DECIMAL(18,2),@tr DECIMAL(18,2),  
@AR DECIMAL(18,2),@PR DECIMAL(18,2),@QR DECIMAL(18,2)    
DECLARE @dateCnt INT,@cnt INT,@date DATE,@dateCntShft INT,@cntShft INT,@OEE DECIMAL(18,2)
DECLARE @TotBrkDownTimeMngd decimal(18,2),@TotalULoss decimal(18,2),@weightedCY decimal(18,2) 

SELECT @TotBreakTime=dbo.GetToTalBreakTime(@SHIFT_ID)   
SELECT @TotShiftTime=dbo.GetToTalShiftTime(@SHIFT_ID)
SELECT @date=  @PRODUCTIONDATE
SET @ProductionTime=isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)  
SELECT @TotBrkDownTime=dbo.GetToTalBreakDownTime(@SHIFT_ID,@MACHINEID,@date)	
SELECT @TotBrkDownTimeMngd=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,1)		
SELECT @TotalULoss=dbo.GetToTalBreakDownTimeBySchLoss(@SHIFT_ID,@MACHINEID,@date,0)
SELECT @Cy= dbo.GetCyCleRatio(@SHIFT_ID,@MachineId,@date)
SELECT  @weightedCY=dbo.GetWeightedCyCle(@SHIFT_ID,@MachineId,@date)


SET @AR=(isnull(@ProductionTime,0)-isnull(@TotBrkDownTime,0))/(isnull(@ProductionTime,0)-isnull(@TotBrkDownTimeMngd,0))    
--set @PR=isnull(@sp,0)/@TotalProduction    
SET @PR=@weightedCY/(isnull(@TotShiftTime,0)-isnull(@TotBreakTime,0)-isnull(@TotBrkDownTimeMngd,0)-isnull(@TotalULoss,0))    
SET @PR=@PR/60    
SET @QR=(isnull(@sp,0)-isnull(@tr,0))/@sp    
SET @OEE= isnull(@AR,0)*isnull(@PR,0)*isnull(@QR,0)  

RETURN @QR

END




GO
/****** Object:  UserDefinedFunction [dbo].[GetReasonName]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetReasonName]
(	
	@ReasonCode VARCHAR(50)
	
)
RETURNS VARCHAR(500)
AS
BEGIN
DECLARE @Result VARCHAR(500)
SELECT TOP 1 @Result=ReasonName from
[dbo].[StopageReason] where ReasonCode=@ReasonCode

return @Result
END








GO
/****** Object:  UserDefinedFunction [dbo].[GetReasonNameById]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetReasonNameById]
(	
	@ReasonId int
	
)
RETURNS VARCHAR(500)
AS
BEGIN
DECLARE @Result VARCHAR(500)
SELECT TOP 1 @Result=ReasonName from
[dbo].[StopageReason] where ReasonId=@ReasonId

return @Result
END




GO
/****** Object:  UserDefinedFunction [dbo].[getShift_Name]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getShift_Name] (  @organizationId int ,@ShiftId int)
RETURNS VARCHAR(200) AS
BEGIN
   
  declare @ShiftName VARCHAR(200)  =''
  select top 1 @ShiftName=ShiftName from  Shift where ShiftId=@ShiftId and OrganizationId=@organizationId
    RETURN @ShiftName
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeInMinutes]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[GetTimeInMinutes]
(	
	-- Add the parameters for the function here
	@StartTime Decimal(18,2),
	@EndTime Decimal(18,2)
)
RETURNS Decimal(18,2) 
AS
BEGIN
DECLARE @Result DECIMAL(18,2)=0
IF @StartTime>@EndTime
BEGIN
 set @EndTime=@EndTime+24
END

DECLARE @STHR decimal(18,2),@STMIN decimal(18,2), @ENDHR decimal(18,2), @ENDMIN decimal(18,2)
select @STMIN= PARSENAME(@StartTime,1)
select @STHR=floor(@StartTime)
set @StartTime=(@STHR*60)+@STMIN

select @ENDMIN= PARSENAME(@EndTime,1)
select @ENDHR=floor(@EndTime)
set @EndTime=(@ENDHR*60)+@ENDMIN 
 
SET @Result=@EndTime-@StartTime

RETURN @Result
END





GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeInMinutes_bak]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,> 
-- Description:	<Description,,>  SELECT dbo.GetTimeInMinutes(12.0,24)  select * from Shift
-- =============================================
CREATE FUNCTION [dbo].[GetTimeInMinutes_bak]
(	
	-- Add the parameters for the function here
	@StartTime Decimal(18,2),
	@EndTime Decimal(18,2)
)
RETURNS Decimal(18,2) 
AS
BEGIN
DECLARE @Result DECIMAL(18,2)=0
IF @StartTime<@EndTime
BEGIN
SET @Result=(@EndTime*60)-(@StartTime*60)
END
ELSE
BEGIN
SET @Result=((@EndTime+24)*60)-(@StartTime*60)
END
RETURN @Result
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeInMinutes_SM]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetTimeInMinutes_SM]
(	
	-- Add the parameters for the function here
	@StartTime Decimal(18,2),
	@EndTime Decimal(18,2)
)
RETURNS Decimal(18,2) 
AS
BEGIN
DECLARE @Result DECIMAL(18,2)=0
IF @StartTime>@EndTime
BEGIN
 set @EndTime=@EndTime+24
END

DECLARE @STHR decimal(18,2),@STMIN decimal(18,2), @ENDHR decimal(18,2), @ENDMIN decimal(18,2)
select @STMIN= PARSENAME(@StartTime,1)
select @STHR=floor(@StartTime)
set @StartTime=(@STHR*60)+@STMIN

select @ENDMIN= PARSENAME(@EndTime,1)
select @ENDHR=floor(@EndTime)
set @EndTime=(@ENDHR*60)+@ENDMIN 
 
SET @Result=@EndTime-@StartTime

RETURN @Result
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeSpanByTwoDate]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Pradip Roy>
-- Create date: <Create Date, 08-Sep-2019>
-- Description:	<Description, Get Timespan Between 2 Dates>
-- =============================================
CREATE FUNCTION [dbo].[GetTimeSpanByTwoDate] 
(
	-- Add the parameters for the function here
	@StartTime DATETIME=NULL,
	@EndTime DATETIME=NULL
)
RETURNS VARCHAR(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(500);

	-- Add the T-SQL statements to compute the return value here
	 DECLARE @TimeinSecond INT = DATEDIFF(second, @StartTime, @EndTime);
	  
SELECT @Result = (RIGHT('0' + CAST(@TimeinSecond / 3600 AS VARCHAR),2) + ':' +
					RIGHT('0' + CAST((@TimeinSecond / 60) % 60 AS VARCHAR),2) + ':' +
					RIGHT('0' + CAST(@TimeinSecond % 60 AS VARCHAR),2));

	-- Return the result of the function
	RETURN @Result
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalBreakDownTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalBreakDownTime](
@ShiftId Int,
@MachineId int,
@ProductionDate DateTime
)
Returns  int
AS
begin
declare @Result int=0
SELECT @Result=sum(TOTALSTOPPAGETIME) from BreakDownEntry WHERE    
     CAST(STARTTIME AS DATE) =@ProductionDate and MachineID=@MachineId    
          AND IsActive=1 AND shiftid=@ShiftId   
return @Result
end




GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalBreakDownTimeByHolidays]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalBreakDownTimeByHolidays](
@ShiftId Int,
@MachineId int,
@ProductionDate DateTime

)
Returns  decimal(8,2)
AS
begin
	-- select  dbo.[GetToTalBreakDownTimeByHolidays](1,1,'2021-01-17')
DECLARE @Result decimal(8,2)=0.00
declare @holidayId int=0
select @holidayId=HolidayID from holidays where cast(@ProductionDate as date)=cast(Start as date)

if(@holidayId>0)
 begin
 select @Result=DurationMin from Shift where ShiftId=@ShiftId
 end
return @Result
end







GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalBreakDownTimeBySchLoss]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetToTalBreakDownTimeBySchLoss](    
@ShiftId Int,    
@MachineId int,    
@ProductionDate DateTime,    
@isSchLoss BIT    
    
)    
Returns  decimal(8,2)    
AS    
begin    
 -- select  dbo.[GetToTalBreakDownTimeBySchLoss](2,3,'2022-06-13',1)    
    
    
declare @shiftIdIsNight int=0    
select @shiftIdIsNight=ShiftId from [Shift] where ShiftId=@ShiftId and IsOverNight=1    
    
DECLARE @Result decimal(8,2)=0.00    
SELECT @Result=isnull(sum(TOTALSTOPPAGETIME),0)/60 from BreakDownEntry BE left outer join StopageReason S on BE.ReasonID=S.ReasonID where        
cast(@ProductionDate as date)= CAST(ProductionDate AS DATE)     
 and MachineID=@MachineId AND BE.IsActive=1 AND shiftid=@ShiftId and isnull(isSchLoss,0)=@isSchLoss ;      
    
return @Result    
end 


GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalBreakDownTimeBySchLossEntry]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalBreakDownTimeBySchLossEntry](
@ShiftId Int,
@MachineId int,
@ProductionDate DateTime,
@isSchLoss BIT

)
Returns  decimal(8,2)
AS
begin
	-- select  dbo.[GetToTalBreakDownTimeBySchLossEntry](1,1,'2021-01-17',1)
DECLARE @Result decimal(8,2)=0.00
declare @holidayId int=0
select @holidayId=HolidayID from holidays where cast(@ProductionDate as date)=cast(Start as date)

if(@holidayId<1)
begin
	SELECT @Result=isnull(sum(TOTALSTOPPAGETIME),0)/60 from BreakDownEntry BE inner join StopageReason S on BE.ReasonID=S.ReasonID where    
	cast(@ProductionDate as date)=case when @ShiftId=3 then dateadd(day,-1,CAST(ProductionDate AS DATE)) else CAST(ProductionDate AS DATE) end
	and MachineID=@MachineId AND BE.IsActive=1 AND shiftid=@ShiftId and isnull(isSchLoss,1)=@isSchLoss ;  
end
 --else
 --begin
 --select @Result=DurationMin from Shift where ShiftId=@ShiftId
 --end
return @Result
end







GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalBreakDownTimeBySchLossManualEntry]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalBreakDownTimeBySchLossManualEntry](
@ShiftId Int,
@MachineId int,
@ProductionDate DateTime,
@isSchLoss BIT,
@isManualEntry int

)
Returns  decimal(8,2)
AS
begin
	-- select  dbo.[GetToTalBreakDownTimeBySchLoss](1,1,'2021-01-10',1)


declare @shiftIdIsNight int=0
select @shiftIdIsNight=ShiftId from [Shift] where ShiftId=@ShiftId and IsOverNight=1

DECLARE @Result decimal(8,2)=0.00
SELECT @Result=isnull(sum(TOTALSTOPPAGETIME),0)/60 from BreakDownEntry BE left outer join StopageReason S on BE.ReasonID=S.ReasonID where    
cast(@ProductionDate as date)=case when @ShiftId=@shiftIdIsNight and @isManualEntry=0 then dateadd(day,-1,CAST(ProductionDate AS DATE)) else CAST(ProductionDate AS DATE) end
 and MachineID=@MachineId AND BE.IsActive=1 AND shiftid=@ShiftId and isnull(isSchLoss,1)=@isSchLoss ;  

return @Result
end





GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalBreakTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalBreakTime](
@ShiftId Int
)
Returns  int
AS
begin
declare @Result int=0
SELECT @Result=sum(DurationMin) FROM BreakInfo WHERE shiftid=@ShiftId and IsActive=1   
return @Result
end




GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalShiftTime]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalShiftTime](
@ShiftId Int
)
Returns  int
AS
begin
declare @Result int=0
SELECT @Result=sum(DurationMin) FROM Shift WHERE shiftid=@ShiftId and IsActive=1   
return @Result
end




GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalSLoss]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalSLoss](
@ShiftId Int,
@STARTDATE Date,
@ENDDATE Date,
@Machineid INT
)
Returns  DECIMAL(18,2)
AS
begin
declare @Result DECIMAL(18,2)=0
SELECT  @Result=sum(TOTALSTOPPAGETIME) from BreakDownEntry BE join StopageReason S on BE.ReasonID=S.ReasonID where  
     cast(STARTTIME as date) >= @STARTDATE AND cast(STARTTIME as date)<= @ENDDATE AND     
                    cast( ENDTIME as date) >= @STARTDATE  AND cast( ENDTIME as date)<= @ENDDATE AND BE.IsActive=1  and isnull(isSchLoss,0)=1  
      and [MachineID] =@Machineid and ShiftId=@ShiftId
RETURN @Result
end




GO
/****** Object:  UserDefinedFunction [dbo].[GetToTalULoss]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetToTalULoss](
@ShiftId Int,
@STARTDATE Date,
@ENDDATE Date,
@Machineid INT
)
Returns  DECIMAL(18,2)
AS
begin
declare @Result DECIMAL(18,2)=0

SELECT  @Result=sum(TOTALSTOPPAGETIME) from BreakDownEntry BE join StopageReason S on BE.ReasonID=S.ReasonID where  
     cast(STARTTIME as date) >= @STARTDATE AND cast(STARTTIME as date)<= @ENDDATE AND     
                     cast(ENDTIME as date) >= @STARTDATE  AND cast(ENDTIME as date)<= @ENDDATE AND BE.IsActive=1  and isnull(isSchLoss,0)=0  
      and [MachineID] =@Machineid and ShiftId=@ShiftId 


RETURN @Result
end






GO
/****** Object:  UserDefinedFunction [dbo].[GetWeightedCyCle]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetWeightedCyCle](
@ShiftId Int,
@MachineId int,
@ProductionDate DateTime

)
Returns  DECIMAL(18,2)
AS
begin
DECLARE @Result DECIMAL(18,2)=0
DECLARE @weightedCY DECIMAL(18,2)=0

SELECT @Result=sum(CycleTime)/count(CycleTime) FROM MasterProduct 
WHERE ProductId in   (SELECT DISTINCT(ProductID)  from ProductionEntry WHERE    
CAST(STARTTIME AS DATE) =@ProductionDate AND MachineID=@MACHINEID  AND shiftid=@ShiftId ) and IsActive=1    
SELECT @weightedCY= sum(TP*P.cycletime) FROM  
(SELECT ProductID,sum(TotalProduction) TP FROM ProductionEntry WHERE CAST(STARTTIME AS DATE) =@ProductionDate AND MachineID=@MACHINEID    
AND shiftid=@ShiftId group by ProductID ) A     
join [MasterProduct] P on A.ProductID=P.ProductID

return @weightedCY
end




GO
/****** Object:  UserDefinedFunction [dbo].[parseJSON]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[parseJSON]( @JSON NVARCHAR(MAX))
/**
Summary: >
  The code for the JSON Parser/Shredder will run in SQL Server 2005, 
  and even in SQL Server 2000 (with some modifications required).
 
  First the function replaces all strings with tokens of the form @Stringxx,
  where xx is the foreign key of the table variable where the strings are held.
  This takes them, and their potentially difficult embedded brackets, out of 
  the way. Names are  always strings in JSON as well as  string values.
 
  Then, the routine iteratively finds the next structure that has no structure 
  Contained within it, (and is, by definition the leaf structure), and parses it,
  replacing it with an object token of the form ‘@Objectxxx‘, or ‘@arrayxxx‘, 
  where xxx is the object id assigned to it. The values, or name/value pairs 
  are retrieved from the string table and stored in the hierarchy table. G
  radually, the JSON document is eaten until there is just a single root
  object left.
Author: PhilFactor
Date: 01/07/2010
Version: 
  Number: 4.6.2
  Date: 01/07/2019
  Why: case-insensitive version
Example: >
  Select * from parseJSON('{    "Person": 
      {
       "firstName": "John",
       "lastName": "Smith",
       "age": 25,
       "Address": 
           {
          "streetAddress":"21 2nd Street",
          "city":"New York",
          "state":"NY",
          "postalCode":"10021"
           },
       "PhoneNumbers": 
           {
           "home":"212 555-1234",
          "fax":"646 555-4567"
           }
        }
     }
  ')
Returns: >
  nothing
**/
	RETURNS @hierarchy TABLE
	  (
	   Element_ID INT IDENTITY(1, 1) NOT NULL, /* internal surrogate primary key gives the order of parsing and the list order */
	   SequenceNo [int] NULL, /* the place in the sequence for the element */
	   Parent_ID INT null, /* if the element has a parent then it is in this column. The document is the ultimate parent, so you can get the structure from recursing from the document */
	   Object_ID INT null, /* each list or object has an object id. This ties all elements to a parent. Lists are treated as objects here */
	   Name NVARCHAR(2000) NULL, /* the Name of the object */
	   StringValue NVARCHAR(MAX) NOT NULL,/*the string representation of the value of the element. */
	   ValueType VARCHAR(10) NOT null /* the declared type of the value represented as a string in StringValue*/
	  )
	  /*
 
	   */
	AS
	BEGIN
	  DECLARE
	    @FirstObject INT, --the index of the first open bracket found in the JSON string
	    @OpenDelimiter INT,--the index of the next open bracket found in the JSON string
	    @NextOpenDelimiter INT,--the index of subsequent open bracket found in the JSON string
	    @NextCloseDelimiter INT,--the index of subsequent close bracket found in the JSON string
	    @Type NVARCHAR(10),--whether it denotes an object or an array
	    @NextCloseDelimiterChar CHAR(1),--either a '}' or a ']'
	    @Contents NVARCHAR(MAX), --the unparsed contents of the bracketed expression
	    @Start INT, --index of the start of the token that you are parsing
	    @end INT,--index of the end of the token that you are parsing
	    @param INT,--the parameter at the end of the next Object/Array token
	    @EndOfName INT,--the index of the start of the parameter at end of Object/Array token
	    @token NVARCHAR(200),--either a string or object
	    @value NVARCHAR(MAX), -- the value as a string
	    @SequenceNo int, -- the sequence number within a list
	    @Name NVARCHAR(200), --the Name as a string
	    @Parent_ID INT,--the next parent ID to allocate
	    @lenJSON INT,--the current length of the JSON String
	    @characters NCHAR(36),--used to convert hex to decimal
	    @result BIGINT,--the value of the hex symbol being parsed
	    @index SMALLINT,--used for parsing the hex value
	    @Escape INT --the index of the next escape character
	    
	  DECLARE @Strings TABLE /* in this temporary table we keep all strings, even the Names of the elements, since they are 'escaped' in a different way, and may contain, unescaped, brackets denoting objects or lists. These are replaced in the JSON string by tokens representing the string */
	    (
	     String_ID INT IDENTITY(1, 1),
	     StringValue NVARCHAR(MAX)
	    )
	  SELECT--initialise the characters to convert hex to ascii
	    @characters='0123456789abcdefghijklmnopqrstuvwxyz',
	    @SequenceNo=0, --set the sequence no. to something sensible.
	  /* firstly we process all strings. This is done because [{} and ] aren't escaped in strings, which complicates an iterative parse. */
	    @Parent_ID=0;
	  WHILE 1=1 --forever until there is nothing more to do
	    BEGIN
	      SELECT
	        @start=PATINDEX('%[^a-zA-Z]["]%', @json collate SQL_Latin1_General_CP850_Bin);--next delimited string
	      IF @start=0 BREAK --no more so drop through the WHILE loop
	      IF SUBSTRING(@json, @start+1, 1)='"' 
	        BEGIN --Delimited Name
	          SET @start=@Start+1;
	          SET @end=PATINDEX('%[^\]["]%', RIGHT(@json, LEN(@json+'|')-@start) collate SQL_Latin1_General_CP850_Bin);
	        END
	      IF @end=0 --either the end or no end delimiter to last string
	        BEGIN-- check if ending with a double slash...
             SET @end=PATINDEX('%[\][\]["]%', RIGHT(@json, LEN(@json+'|')-@start) collate SQL_Latin1_General_CP850_Bin);
 		     IF @end=0 --we really have reached the end 
				BEGIN
				BREAK --assume all tokens found
				END
			END 
	      SELECT @token=SUBSTRING(@json, @start+1, @end-1)
	      --now put in the escaped control characters
	      SELECT @token=REPLACE(@token, FromString, ToString)
	      FROM
	        (SELECT           '\b', CHAR(08)
	         UNION ALL SELECT '\f', CHAR(12)
	         UNION ALL SELECT '\n', CHAR(10)
	         UNION ALL SELECT '\r', CHAR(13)
	         UNION ALL SELECT '\t', CHAR(09)
			 UNION ALL SELECT '\"', '"'
	         UNION ALL SELECT '\/', '/'
	        ) substitutions(FromString, ToString)
		SELECT @token=Replace(@token, '\\', '\')
	      SELECT @result=0, @escape=1
	  --Begin to take out any hex escape codes
	      WHILE @escape>0
	        BEGIN
	          SELECT @index=0,
	          --find the next hex escape sequence
	          @escape=PATINDEX('%\x[0-9a-f][0-9a-f][0-9a-f][0-9a-f]%', @token collate SQL_Latin1_General_CP850_Bin)
	          IF @escape>0 --if there is one
	            BEGIN
	              WHILE @index<4 --there are always four digits to a \x sequence   
	                BEGIN
	                  SELECT --determine its value
	                    @result=@result+POWER(16, @index)
	                    *(CHARINDEX(SUBSTRING(@token, @escape+2+3-@index, 1),
	                                @characters)-1), @index=@index+1 ;
	         
	                END
	                -- and replace the hex sequence by its unicode value
	              SELECT @token=STUFF(@token, @escape, 6, NCHAR(@result))
	            END
	        END
	      --now store the string away 
	      INSERT INTO @Strings (StringValue) SELECT @token
	      -- and replace the string with a token
	      SELECT @JSON=STUFF(@json, @start, @end+1,
	                    '@string'+CONVERT(NCHAR(5), @@identity))
	    END
	  -- all strings are now removed. Now we find the first leaf.  
	  WHILE 1=1  --forever until there is nothing more to do
	  BEGIN
	 
	  SELECT @Parent_ID=@Parent_ID+1
	  --find the first object or list by looking for the open bracket
	  SELECT @FirstObject=PATINDEX('%[{[[]%', @json collate SQL_Latin1_General_CP850_Bin)--object or array
	  IF @FirstObject = 0 BREAK
	  IF (SUBSTRING(@json, @FirstObject, 1)='{') 
	    SELECT @NextCloseDelimiterChar='}', @type='object'
	  ELSE 
	    SELECT @NextCloseDelimiterChar=']', @type='array'
	  SELECT @OpenDelimiter=@firstObject
	  WHILE 1=1 --find the innermost object or list...
	    BEGIN
	      SELECT
	        @lenJSON=LEN(@JSON+'|')-1
	  --find the matching close-delimiter proceeding after the open-delimiter
	      SELECT
	        @NextCloseDelimiter=CHARINDEX(@NextCloseDelimiterChar, @json,
	                                      @OpenDelimiter+1)
	  --is there an intervening open-delimiter of either type
	      SELECT @NextOpenDelimiter=PATINDEX('%[{[[]%',
	             RIGHT(@json, @lenJSON-@OpenDelimiter)collate SQL_Latin1_General_CP850_Bin)--object
	      IF @NextOpenDelimiter=0 
	        BREAK
	      SELECT @NextOpenDelimiter=@NextOpenDelimiter+@OpenDelimiter
	      IF @NextCloseDelimiter<@NextOpenDelimiter 
	        BREAK
	      IF SUBSTRING(@json, @NextOpenDelimiter, 1)='{' 
	        SELECT @NextCloseDelimiterChar='}', @type='object'
	      ELSE 
	        SELECT @NextCloseDelimiterChar=']', @type='array'
	      SELECT @OpenDelimiter=@NextOpenDelimiter
	    END
	  ---and parse out the list or Name/value pairs
	  SELECT
	    @contents=SUBSTRING(@json, @OpenDelimiter+1,
	                        @NextCloseDelimiter-@OpenDelimiter-1)
	  SELECT
	    @JSON=STUFF(@json, @OpenDelimiter,
	                @NextCloseDelimiter-@OpenDelimiter+1,
	                '@'+@type+CONVERT(NCHAR(5), @Parent_ID))
	  WHILE (PATINDEX('%[A-Za-z0-9@+.e]%', @contents collate SQL_Latin1_General_CP850_Bin))<>0 
	    BEGIN
	      IF @Type='object' --it will be a 0-n list containing a string followed by a string, number,boolean, or null
	        BEGIN
	          SELECT
	            @SequenceNo=0,@end=CHARINDEX(':', ' '+@contents)--if there is anything, it will be a string-based Name.
	          SELECT  @start=PATINDEX('%[^A-Za-z@][@]%', ' '+@contents collate SQL_Latin1_General_CP850_Bin)--AAAAAAAA
              SELECT @token=RTrim(Substring(' '+@contents, @start+1, @End-@Start-1)),
	            @endofName=PATINDEX('%[0-9]%', @token collate SQL_Latin1_General_CP850_Bin),
	            @param=RIGHT(@token, LEN(@token)-@endofName+1)
	          SELECT
	            @token=LEFT(@token, @endofName-1),
	            @Contents=RIGHT(' '+@contents, LEN(' '+@contents+'|')-@end-1)
	          SELECT  @Name=StringValue FROM @strings
	            WHERE string_id=@param --fetch the Name
	        END
	      ELSE 
	        SELECT @Name=null,@SequenceNo=@SequenceNo+1 
	      SELECT
	        @end=CHARINDEX(',', @contents)-- a string-token, object-token, list-token, number,boolean, or null
                IF @end=0
	        --HR Engineering notation bugfix start
	          IF ISNUMERIC(@contents) = 1
		    SELECT @end = LEN(@contents) + 1
	          Else
	        --HR Engineering notation bugfix end 
		  SELECT  @end=PATINDEX('%[A-Za-z0-9@+.e][^A-Za-z0-9@+.e]%', @contents+' ' collate SQL_Latin1_General_CP850_Bin) + 1
	       SELECT
	        @start=PATINDEX('%[^A-Za-z0-9@+.e][A-Za-z0-9@+.e]%', ' '+@contents collate SQL_Latin1_General_CP850_Bin)
	      --select @start,@end, LEN(@contents+'|'), @contents  
	      SELECT
	        @Value=RTRIM(SUBSTRING(@contents, @start, @End-@Start)),
	        @Contents=RIGHT(@contents+' ', LEN(@contents+'|')-@end)
	      IF SUBSTRING(@value, 1, 7)='@object' 
	        INSERT INTO @hierarchy
	          (Name, SequenceNo, Parent_ID, StringValue, Object_ID, ValueType)
	          SELECT @Name, @SequenceNo, @Parent_ID, SUBSTRING(@value, 8, 5),
	            SUBSTRING(@value, 8, 5), 'object' 
	      ELSE 
	        IF SUBSTRING(@value, 1, 6)='@array' 
	          INSERT INTO @hierarchy
	            (Name, SequenceNo, Parent_ID, StringValue, Object_ID, ValueType)
	            SELECT @Name, @SequenceNo, @Parent_ID, SUBSTRING(@value, 7, 5),
	              SUBSTRING(@value, 7, 5), 'array' 
	        ELSE 
	          IF SUBSTRING(@value, 1, 7)='@string' 
	            INSERT INTO @hierarchy
	              (Name, SequenceNo, Parent_ID, StringValue, ValueType)
	              SELECT @Name, @SequenceNo, @Parent_ID, StringValue, 'string'
	              FROM @strings
	              WHERE string_id=SUBSTRING(@value, 8, 5)
	          ELSE 
	            IF @value IN ('true', 'false') 
	              INSERT INTO @hierarchy
	                (Name, SequenceNo, Parent_ID, StringValue, ValueType)
	                SELECT @Name, @SequenceNo, @Parent_ID, @value, 'boolean'
	            ELSE
	              IF @value='null' 
	                INSERT INTO @hierarchy
	                  (Name, SequenceNo, Parent_ID, StringValue, ValueType)
	                  SELECT @Name, @SequenceNo, @Parent_ID, @value, 'null'
	              ELSE
	                IF PATINDEX('%[^0-9]%', @value collate SQL_Latin1_General_CP850_Bin)>0 
	                  INSERT INTO @hierarchy
	                    (Name, SequenceNo, Parent_ID, StringValue, ValueType)
	                    SELECT @Name, @SequenceNo, @Parent_ID, @value, 'real'
	                ELSE
	                  INSERT INTO @hierarchy
	                    (Name, SequenceNo, Parent_ID, StringValue, ValueType)
	                    SELECT @Name, @SequenceNo, @Parent_ID, @value, 'int'
	      if @Contents=' ' Select @SequenceNo=0
	    END
	  END
	INSERT INTO @hierarchy (Name, SequenceNo, Parent_ID, StringValue, Object_ID, ValueType)
	  SELECT '-',1, NULL, '', @Parent_ID-1, @type
	--
	   RETURN
	END



GO
/****** Object:  Table [dbo].[ProductionEntry]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionEntry](
	[ProductionId] [int] IDENTITY(1,1) NOT NULL,
	[MachineID] [int] NULL,
	[MachineCategoryId] [int] NULL,
	[ProductID] [int] NULL,
	[UserID] [int] NULL,
	[OperatorID] [int] NULL,
	[SupervisorID] [int] NULL,
	[TimeOfEntry] [datetime] NULL,
	[ShiftId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalProduction] [decimal](18, 2) NULL,
	[ReWork] [int] NULL,
	[Rejection] [int] NULL,
	[ProductionDate] [datetime] NULL,
	[IsSaved] [bit] NULL,
	[RejectionList] [text] NULL,
	[IsDeleted] [bit] NOT NULL,
	[ProductionDateOnly]  AS (CONVERT([date],[ProductionDate],(0))),
	[MachineDemandType] [varchar](50) NULL,
	[PRoductDemandType] [varchar](50) NULL,
	[ConvertedProduction] [decimal](18, 2) NULL,
	[ConvertedRejection] [decimal](18, 2) NULL,
	[ConvertedRework] [decimal](18, 2) NULL,
	[MouldId] [int] NULL,
	[OperationId] [int] NULL,
	[DieId] [int] NULL,
	[TargetProduction] [int] NULL,
	[MouldCavity] [int] NULL,
	[DieCavity] [int] NULL,
	[ActualDate] [datetime] NULL,
 CONSTRAINT [PK_ProductionEntry] PRIMARY KEY CLUSTERED 
(
	[ProductionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[USF_GetShiftCount]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[USF_GetShiftCount] (
    @STARTDATE DATETIME,
	@ENDDATE DATETIME,
	@MachineId INT
)
RETURNS TABLE
AS
RETURN SELECT distinct ShiftID from ProductionEntry WHERE    
CAST(STARTTIME AS DATE) BETWEEN @STARTDATE AND @ENDDATE AND       
CAST(ENDTIME AS DATE) BETWEEN @STARTDATE AND @ENDDATE AND MachineID=@MACHINEID




GO
/****** Object:  UserDefinedFunction [dbo].[UDF_ProductionReport]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[UDF_ProductionReport](
 @Organizationid int ,
 @FromDate DateTime,
 @Todate DateTime)
 RETURNS TABLE
 AS
RETURN

SELECT  TimeOfEntry,FORMAT (TimeOfEntry, 'dd/MM/yyyy ') DateOfEntry,cast(TimeOfEntry as Date) DD_DateOfEntry,OperatorID ,dbo.getOperator_Name(@Organizationid,OperatorID) OperatorName,MachineID, DBO.getMachine_Name(@Organizationid,MachineID) MachineName,
dbo.getProduct_Code(@Organizationid,ProductID) ProductCode,ProductID,dbo.getProduct_Name(@Organizationid,ProductID) ProductName,
dbo.getShift_Name(@Organizationid,ShiftID) ShiftName,ShiftID,dbo.GetProductionInEfficiency(ShiftID,MachineID,TimeOfEntry) InEfficiency,
dbo.getOperation_Name(@Organizationid,OperationID) OperationName,OperationID, dbo.getDie_Name(@Organizationid,DieID) DieName,DieID, dbo.getMould_Name(@Organizationid,MouldID) MouldName,MouldID, TotalProduction,TargetProduction,Target_Duration,ACTUAL_DURATION_OF_PRODUCTION,MouldCavity,DieCavity,
CAST((isnull(Target_Duration-ACTUAL_DURATION_OF_PRODUCTION,0)) AS DECIMAL(18,2)) TOTAL_TIME_LOSS ,CAST(ROUND(TARGET_SPM,2) AS DECIMAL(18,2)) TARGET_SPM,CAST(ROUND((ACTUAL_DURATION_OF_PRODUCTION/dbo.Get1If0(TotalProduction)),0) AS DECIMAL(18,2)) ACTUAL_SPM,CAST(ROUND((TARGET_SPM-(ACTUAL_DURATION_OF_PRODUCTION/dbo.Get1If0(TotalProduction))),2) AS DECIMAL(18,2)) SPM_SHORT_FALL 


FROM            (SELECT        TimeOfEntry, MachineID, ProductID, ShiftId,OperatorID, OperationId, DieId, MouldId,TargetProduction,MouldCavity,DieCavity, SUM(TotalProduction) AS TotalProduction, isnull(SUM(TotalProduction)*dbo.GetCyCleTime(ProductID),0) Target_Duration ,
dbo.GetProducionTime(ShiftId,MachineID,TimeOfEntry) ACTUAL_DURATION_OF_PRODUCTION,(60/dbo.Get1If0(dbo.GetCyCleTime(ProductID))) TARGET_SPM



                          FROM            (SELECT        ProductionId, MachineID, MachineCategoryId, ProductID, UserID, OperatorID,
						  SupervisorID, CAST(TimeOfEntry AS date) AS TimeOfEntry, 
						  ShiftId, StartTime, EndTime, TotalProduction, ReWork, Rejection, 
                           ProductionDate, IsSaved, RejectionList, IsDeleted,
						 ProductionDateOnly, MachineDemandType, PRoductDemandType, ConvertedProduction, ConvertedRejection, ConvertedRework, MouldId, OperationId, 
                             DieId,TargetProduction,MouldCavity,DieCavity
                                                    FROM            ProductionEntry) AS pd
                          GROUP BY TimeOfEntry, MachineID, ProductID, ShiftId, OperationId,OperatorID, DieId, MouldId,TargetProduction,MouldCavity,DieCavity) AS FinalTable 
						  where  TimeOfEntry between cast(@FromDate as date) and cast(@Todate  as date)
						 

GO
/****** Object:  Table [dbo].[BreakDownEntry]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakDownEntry](
	[BreakDownID] [int] IDENTITY(1,1) NOT NULL,
	[MachineCategoryId] [int] NULL,
	[MachineID] [int] NULL,
	[ReasonId] [int] NULL,
	[SubReasonId] [int] NULL,
	[UserID] [int] NULL,
	[OperatorID] [int] NULL,
	[SupervisorID] [int] NULL,
	[TimeOfEntry] [datetime] NULL,
	[ShiftId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalStoppageTime] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[ProductionDate] [datetime] NULL,
	[IsSaved] [bit] NULL,
	[Remarks] [nvarchar](500) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsIdle] [bit] NOT NULL,
 CONSTRAINT [PK_BreakDownEntry] PRIMARY KEY CLUSTERED 
(
	[BreakDownID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[UDF_BreakDownReport]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  FUNCTION [dbo].[UDF_BreakDownReport]( 
 @FromDate DateTime,
 @Todate DateTime)
 RETURNS TABLE
 AS
RETURN

SELECT  * From BreakDownEntry where cast(ProductionDate as date)
between cast(@FromDate as date) and cast(@Todate as date) 
						 

GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[OrganizationId] [int] NULL,
	[UserTypeId] [int] NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuditLog]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLog](
	[Logid] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](500) NULL,
	[Message] [varchar](2000) NULL,
	[Level] [int] NULL,
	[LogDate] [datetime] NOT NULL,
	[OperationName] [varchar](500) NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED 
(
	[Logid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BreakDownEntryArchive]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakDownEntryArchive](
	[BreakDownID] [int] IDENTITY(1,1) NOT NULL,
	[MachineCategoryId] [int] NULL,
	[MachineID] [int] NULL,
	[ReasonId] [int] NULL,
	[SubReasonId] [int] NULL,
	[UserID] [int] NULL,
	[OperatorID] [int] NULL,
	[SupervisorID] [int] NULL,
	[TimeOfEntry] [datetime] NULL,
	[ShiftId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalStoppageTime] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[ProductionDate] [datetime] NULL,
	[IsSaved] [bit] NULL,
	[Remarks] [nvarchar](500) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_BreakDownEntryArchive] PRIMARY KEY CLUSTERED 
(
	[BreakDownID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BreakdownReason]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakdownReason](
	[ReasonId] [int] IDENTITY(1,1) NOT NULL,
	[ReasonName] [varchar](50) NULL,
	[IsScheduled] [bit] NULL,
 CONSTRAINT [PK_BreakdownReason] PRIMARY KEY CLUSTERED 
(
	[ReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BreakInfo]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakInfo](
	[BreakId] [int] IDENTITY(1,1) NOT NULL,
	[ShiftId] [int] NULL,
	[BreakName] [nvarchar](200) NULL,
	[StartTime] [decimal](18, 2) NULL,
	[EndTime] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[DurationMin] [int] NULL,
 CONSTRAINT [PK_BreakInfo] PRIMARY KEY CLUSTERED 
(
	[BreakId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calendar]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendar](
	[CalendarDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ConfigTbl]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConfigTbl](
	[Production] [nvarchar](max) NOT NULL,
	[ConfigID] [int] NOT NULL,
	[Client] [nvarchar](30) NOT NULL,
	[Line] [nvarchar](30) NOT NULL,
	[Machine] [nvarchar](30) NOT NULL,
	[Location] [nvarchar](30) NOT NULL,
	[Breaks] [nvarchar](max) NULL,
	[Quality] [nvarchar](max) NULL,
	[CycleTime] [int] NULL,
	[BreakdownInterval] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Holidays]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holidays](
	[HolidayID] [int] IDENTITY(1,1) NOT NULL,
	[HolidayName] [varchar](100) NOT NULL,
	[HolidayDescription] [varchar](500) NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NULL,
	[ThemeColor] [varchar](10) NULL,
	[IsFullDay] [bit] NOT NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_Holidays] PRIMARY KEY CLUSTERED 
(
	[HolidayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MachineCategory]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MachineCategory](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](500) NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_MachineCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Master_UnitMaster]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Master_UnitMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Unit] [nchar](10) NULL,
	[TenantId] [int] NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[AddDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[UnitPrefix] [nchar](10) NULL,
	[UnitSuffix] [nchar](10) NULL,
	[QtyMultiplier] [decimal](18, 6) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterDie]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterDie](
	[DieId] [int] IDENTITY(1,1) NOT NULL,
	[DieDescription] [nvarchar](200) NULL,
	[AvailableCavityCount] [int] NULL,
	[AddBy] [int] NULL,
	[AddDate] [datetime] NULL,
	[EditBy] [int] NULL,
	[EditDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[OrganizationId] [int] NULL,
 CONSTRAINT [PK_MasterDie] PRIMARY KEY CLUSTERED 
(
	[DieId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterMachine]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterMachine](
	[MachineID] [int] IDENTITY(1,1) NOT NULL,
	[MachineName] [nvarchar](200) NULL,
	[Location] [nvarchar](500) NULL,
	[OrganizationId] [int] NULL,
	[MachineCode] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[CategoryId] [int] NULL,
	[NonStop] [bit] NULL,
	[DemandType] [varchar](50) NULL,
 CONSTRAINT [PK_MasterMachine] PRIMARY KEY CLUSTERED 
(
	[MachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterMould]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterMould](
	[MouldId] [int] IDENTITY(1,1) NOT NULL,
	[MouldDescription] [nvarchar](200) NULL,
	[OrganizationId] [int] NULL,
	[AvailableCavityCount] [int] NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[AddDate] [datetime] NULL,
	[EditBy] [int] NULL,
	[EditDate] [datetime] NULL,
 CONSTRAINT [PK_MasterMould] PRIMARY KEY CLUSTERED 
(
	[MouldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterOperation]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterOperation](
	[OperationId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NULL,
	[OperationDescription] [nvarchar](200) NULL,
	[AddBy] [int] NULL,
	[EditBy] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Master_Operation] PRIMARY KEY CLUSTERED 
(
	[OperationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterOperator]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterOperator](
	[OperatorId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [nvarchar](50) NOT NULL,
	[OrganizationId] [int] NULL,
	[OperatorName] [nvarchar](200) NULL,
	[AddBy] [int] NULL,
	[EditBy] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Master_Operator] PRIMARY KEY CLUSTERED 
(
	[OperatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterProduct]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterProduct](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [nvarchar](100) NULL,
	[OrganizationId] [int] NULL,
	[ProductName] [nvarchar](100) NULL,
	[CycleTime] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[ProductType] [varchar](200) NULL,
	[PrdouctCategoryID] [int] NULL,
	[UnitID] [int] NULL,
	[CustomerID] [int] NULL,
 CONSTRAINT [PK_MasterProduct_1] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MasterProduct_15112022]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MasterProduct_15112022](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCode] [nvarchar](100) NULL,
	[OrganizationId] [int] NULL,
	[ProductName] [nvarchar](100) NULL,
	[CycleTime] [decimal](18, 2) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[ProductType] [varchar](200) NULL,
	[PrdouctCategoryID] [int] NULL,
	[UnitID] [int] NULL,
	[CustomerID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OEECalculation]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OEECalculation](
	[RowId] [int] IDENTITY(1,1) NOT NULL,
	[MachineId] [int] NOT NULL,
	[ShiftId] [int] NOT NULL,
	[ProductionDate] [datetime] NULL,
	[ShutdownTime] [decimal](10, 2) NULL,
	[BreakTime] [decimal](10, 2) NULL,
	[UtilisationLoss] [decimal](10, 2) NULL,
	[TotalProductointime] [decimal](10, 2) NULL,
	[QualityLoss] [decimal](10, 2) NULL,
 CONSTRAINT [PK_OEECalculation] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organization]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization](
	[OrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationName] [nvarchar](150) NULL,
	[PhoneNo] [nvarchar](20) NULL,
	[EmailId] [nvarchar](200) NULL,
	[Location] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[IsManualProductionEntry] [bit] NULL,
	[HasBreakdown] [bit] NOT NULL,
	[HasRejection] [int] NOT NULL,
 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organization_Customer]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization_Customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](200) NULL,
	[OrganizationID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organization_ProductCategory]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organization_ProductCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](200) NULL,
	[OrganizationID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganizationUser]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganizationUser](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NULL,
	[UserTypeId] [int] NULL,
	[PhoneNo] [nvarchar](20) NULL,
	[EmailId] [nvarchar](200) NULL,
	[Password] [text] NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[UserName] [varchar](200) NULL,
	[ChangeCode] [varchar](200) NULL,
 CONSTRAINT [PK_OrganizationUser] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Die_Rel]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Die_Rel](
	[RecId] [int] IDENTITY(1,1) NOT NULL,
	[DieId] [int] NULL,
	[OrganizationId] [int] NULL,
	[ProductID] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_Product_Die_Rel] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Machine_Rel]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Machine_Rel](
	[MachineID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[CycleTime] [decimal](18, 2) NOT NULL,
	[ProductDemandType] [varchar](50) NOT NULL,
	[MachineDemandType] [varchar](50) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductMachineRel] PRIMARY KEY CLUSTERED 
(
	[MachineID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Machine_Tran]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Machine_Tran](
	[MPID] [int] IDENTITY(1,1) NOT NULL,
	[MachineID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[CycleTime] [decimal](18, 2) NULL,
	[MachineDemandType] [varchar](50) NOT NULL,
	[ProductDemandType] [varchar](50) NOT NULL,
	[MPStartDate] [datetime] NOT NULL,
	[MPEndDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Product_Machine] PRIMARY KEY CLUSTERED 
(
	[MPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Mould_Rel]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Mould_Rel](
	[RecId] [int] IDENTITY(1,1) NOT NULL,
	[MouldId] [int] NULL,
	[OrganizationId] [int] NULL,
	[ProductID] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_Product_Mould_Rel] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductionEntryArchive]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionEntryArchive](
	[ProductionId] [int] IDENTITY(1,1) NOT NULL,
	[MachineID] [int] NULL,
	[MachineCategoryId] [int] NULL,
	[ProductID] [int] NULL,
	[UserID] [int] NULL,
	[OperatorID] [int] NULL,
	[SupervisorID] [int] NULL,
	[TimeOfEntry] [datetime] NULL,
	[ShiftId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[TotalProduction] [decimal](18, 2) NULL,
	[ReWork] [int] NULL,
	[Rejection] [int] NULL,
	[ProductionDate] [datetime] NULL,
	[IsSaved] [bit] NULL,
	[RejectionList] [text] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ProductionEntryArchive] PRIMARY KEY CLUSTERED 
(
	[ProductionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductionPlanning]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionPlanning](
	[RelationId] [int] IDENTITY(1,1) NOT NULL,
	[MachineId] [int] NULL,
	[ProductId] [int] NULL,
	[SheduleDate] [datetime] NULL,
	[AddByUser] [int] NULL,
	[EditByUser] [int] NULL,
	[AddDate] [datetime] NULL,
	[EditDate] [datetime] NULL,
	[OrganizationId] [int] NULL,
 CONSTRAINT [PK_ProductionPlanning] PRIMARY KEY CLUSTERED 
(
	[RelationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductionRejection]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionRejection](
	[RejectionId] [int] IDENTITY(1,1) NOT NULL,
	[ProductionEntryId] [int] NULL,
	[RejectionReasonId] [int] NULL,
	[RejectionQuantity] [int] NULL,
 CONSTRAINT [PK_ProductionRejection] PRIMARY KEY CLUSTERED 
(
	[RejectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductionSubmit]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductionSubmit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductionDate] [datetime] NULL,
	[OrganizationId] [int] NULL,
 CONSTRAINT [PK_ProductionSubmit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductScheduler]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductScheduler](
	[productScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[productId] [int] NOT NULL,
	[start_Date] [date] NOT NULL,
	[endDate] [date] NOT NULL,
	[startTime] [time](0) NULL,
	[endTime] [time](0) NULL,
	[isFullDay] [bit] NOT NULL,
	[pastScheduleId] [int] NOT NULL,
 CONSTRAINT [PK_ProductScheduler] PRIMARY KEY CLUSTERED 
(
	[productScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductSchedulerHistory]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSchedulerHistory](
	[productScheduleHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[productId] [int] NULL,
	[start_Date] [date] NULL,
	[endDate] [date] NULL,
	[startTime] [time](0) NULL,
	[endTime] [time](0) NULL,
	[isFullDay] [bit] NULL,
	[isCancelled] [bit] NULL,
	[isRescheduled] [bit] NULL,
	[pastScheduleId] [int] NULL,
 CONSTRAINT [PK_ProductSchedulerHistory] PRIMARY KEY CLUSTERED 
(
	[productScheduleHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityEntry]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityEntry](
	[QualityEntryId] [int] IDENTITY(1,1) NOT NULL,
	[MachineId] [int] NOT NULL,
	[RejectionReasonId] [int] NOT NULL,
	[ShiftId] [int] NOT NULL,
	[ProductionDate] [datetime] NOT NULL,
	[ProdCount] [int] NOT NULL,
	[TimeOfEntry] [datetime] NULL,
	[ProductionEntryId] [int] NULL,
 CONSTRAINT [PK_QualityEntry] PRIMARY KEY CLUSTERED 
(
	[QualityEntryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RejectionReason]    Script Date: 12/29/2022 11:47:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RejectionReason](
	[ReasonId] [int] IDENTITY(1,1) NOT NULL,
	[ReasonName] [varchar](200) NULL,
	[ReasonDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[OrganizationId] [int] NULL,
	[ReasonGroup] [varchar](50) NULL,
	[InfluxRejReasonId] [int] NOT NULL,
 CONSTRAINT [PK_RejectionReason] PRIMARY KEY CLUSTERED 
(
	[ReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReportDeliveryLog]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportDeliveryLog](
	[ReportDeliveryLogID] [int] IDENTITY(1,1) NOT NULL,
	[UserSendReportConfigId] [int] NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[IsSent] [bit] NOT NULL,
	[ErrMsg] [varchar](500) NULL,
 CONSTRAINT [PK_ReportDeliveryLog] PRIMARY KEY CLUSTERED 
(
	[ReportDeliveryLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReportType]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportType](
	[ReportId] [int] NOT NULL,
	[ReportName] [varchar](50) NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ReportType] PRIMARY KEY CLUSTERED 
(
	[ReportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shift]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shift](
	[ShiftId] [int] IDENTITY(1,1) NOT NULL,
	[ShiftName] [nvarchar](200) NULL,
	[ShiftDescription] [nvarchar](500) NULL,
	[StartTime] [decimal](18, 2) NULL,
	[EndTime] [decimal](18, 2) NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[DurationMin] [int] NULL,
	[IsOverNight] [bit] NULL,
	[ActualStartTime] [varchar](5) NULL,
	[ActualEndTime] [varchar](5) NULL,
 CONSTRAINT [PK_Shift] PRIMARY KEY CLUSTERED 
(
	[ShiftId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StopageReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StopageReason](
	[ReasonId] [int] IDENTITY(1,1) NOT NULL,
	[ReasonName] [nvarchar](500) NULL,
	[ReasonCode] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[IsManaged] [bit] NULL,
	[IsSchLoss] [bit] NULL,
	[OrganizationId] [int] NULL,
	[InfluxReasonId] [int] NOT NULL,
	[StoppageReasonTypeID] [int] NULL,
 CONSTRAINT [PK_StopageReason] PRIMARY KEY CLUSTERED 
(
	[ReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StopageSubReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StopageSubReason](
	[SubReasonId] [int] IDENTITY(1,1) NOT NULL,
	[ReasonId] [int] NULL,
	[SubReasonName] [varchar](200) NULL,
	[SubReasonCode] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ReasonDescription] [varchar](500) NULL,
	[AddBy] [int] NULL,
	[EdittedBy] [int] NULL,
	[InfluxSubReasonId] [int] NOT NULL,
 CONSTRAINT [PK_StopageSubReason] PRIMARY KEY CLUSTERED 
(
	[SubReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StoppageReasonType]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoppageReasonType](
	[StoppageReasonTypeID] [int] IDENTITY(1,1) NOT NULL,
	[StoppageReasonTypeName] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddBy] [int] NULL,
	[AddDate] [datetime] NULL,
	[EditBy] [int] NULL,
	[EditDate] [datetime] NULL,
 CONSTRAINT [PK_StoppageReasonType] PRIMARY KEY CLUSTERED 
(
	[StoppageReasonTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupervisorCalendar]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupervisorCalendar](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[machineId] [int] NOT NULL,
	[shiftId] [int] NOT NULL,
	[supervisorId] [int] NOT NULL,
	[startDate] [datetime] NOT NULL,
	[endDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_SupervisorCalendar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupervisorMachineMap]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupervisorMachineMap](
	[SupervisorMachineID] [int] IDENTITY(1,1) NOT NULL,
	[SupervisorId] [int] NULL,
	[MachineId] [int] NULL,
	[ShiftId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[IsFullDay] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ThemeColor] [varchar](10) NULL,
	[MachineIds] [varchar](500) NULL,
	[MachineNames] [varchar](500) NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_SupervisorMachineMap] PRIMARY KEY CLUSTERED 
(
	[SupervisorMachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnitMaster]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnitMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Unit] [nchar](10) NULL,
	[OrganizationID] [int] NULL,
	[UnitPrefix] [nchar](10) NULL,
	[UnitSuffix] [nchar](10) NULL,
	[QtyMultiplier] [decimal](18, 6) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserDashboard]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserDashboard](
	[DashboardUserID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[MachineId] [int] NOT NULL,
	[DateRange] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserDashboard] PRIMARY KEY CLUSTERED 
(
	[DashboardUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserReportsConfig]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserReportsConfig](
	[UserMachineReportID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ReportName] [varchar](100) NOT NULL,
	[DateRange] [varchar](50) NOT NULL,
	[MachineIds] [varchar](500) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[MailTo] [varchar](500) NULL,
 CONSTRAINT [PK_UserReportsConfig] PRIMARY KEY CLUSTERED 
(
	[UserMachineReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSendReportConfig]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSendReportConfig](
	[UserSendReportConfigId] [int] IDENTITY(1,1) NOT NULL,
	[ReportGroupId] [int] NOT NULL,
	[ReportTypeId] [int] NOT NULL,
	[ReportName] [varchar](100) NOT NULL,
	[DateRange] [varchar](100) NOT NULL,
	[ValidDays] [varchar](200) NULL,
	[WeekStartDay] [varchar](10) NULL,
	[MonthStartDate] [int] NULL,
	[ReportSentTime] [varchar](50) NOT NULL,
	[MailTo] [varchar](500) NOT NULL,
	[OrganizationId] [int] NOT NULL,
 CONSTRAINT [PK_UserSendReportConfig] PRIMARY KEY CLUSTERED 
(
	[UserSendReportConfigId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeName] [nvarchar](100) NULL,
	[PermissionModule] [nvarchar](1000) NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UtilityMasterMapping]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UtilityMasterMapping](
	[RecId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationId] [int] NULL,
	[MachineId] [int] NULL,
	[ProductId] [int] NULL,
	[OperatorId] [int] NULL,
	[MouldId] [int] NULL,
	[OperationId] [int] NULL,
	[DieId] [int] NULL,
	[TargetProduction] [int] NULL,
	[MouldCavity] [int] NULL,
	[DieCavity] [int] NULL,
 CONSTRAINT [PK_UtilityMasterMapping] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UtilityModule]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UtilityModule](
	[RecId] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](200) NULL,
	[OrganizationId] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_UtilityModule] PRIMARY KEY CLUSTERED 
(
	[RecId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [UIN_BREAKDOWN_LOSS]    Script Date: 12/29/2022 11:47:20 PM ******/
CREATE NONCLUSTERED INDEX [UIN_BREAKDOWN_LOSS] ON [dbo].[BreakDownEntry]
(
	[TotalStoppageTime] ASC,
	[ProductionDate] ASC
)
INCLUDE([MachineID],[ReasonId],[SubReasonId],[Remarks]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [UIN_PRODUCTION_ENTRY]    Script Date: 12/29/2022 11:47:20 PM ******/
CREATE NONCLUSTERED INDEX [UIN_PRODUCTION_ENTRY] ON [dbo].[ProductionEntry]
(
	[ProductionDateOnly] ASC
)
INCLUDE([MachineID],[ShiftId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BreakDownEntry] ADD  CONSTRAINT [DF_BreakDownEntry_TimeOfEntry]  DEFAULT (getdate()) FOR [TimeOfEntry]
GO
ALTER TABLE [dbo].[BreakDownEntry] ADD  CONSTRAINT [DF_BreakDownEntry_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[BreakDownEntry] ADD  CONSTRAINT [DF_BreakDownEntry_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BreakDownEntry] ADD  CONSTRAINT [DF_BreakDownEntry_IsIdle]  DEFAULT ((0)) FOR [IsIdle]
GO
ALTER TABLE [dbo].[BreakDownEntryArchive] ADD  CONSTRAINT [DF_BreakDownEntryArchive_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BreakInfo] ADD  DEFAULT ((0)) FOR [DurationMin]
GO
ALTER TABLE [dbo].[Holidays] ADD  CONSTRAINT [DF_Holidays_IsFullDay]  DEFAULT ((0)) FOR [IsFullDay]
GO
ALTER TABLE [dbo].[MasterMachine] ADD  CONSTRAINT [DF_MasterMachine_DemandType]  DEFAULT ('None') FOR [DemandType]
GO
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_HasBreakdown]  DEFAULT ((1)) FOR [HasBreakdown]
GO
ALTER TABLE [dbo].[Organization] ADD  CONSTRAINT [DF_Organization_HasRejection]  DEFAULT ((1)) FOR [HasRejection]
GO
ALTER TABLE [dbo].[Product_Machine_Rel] ADD  CONSTRAINT [DF_Product_Machine_Rel_ProductDemandTypeId]  DEFAULT ('None') FOR [ProductDemandType]
GO
ALTER TABLE [dbo].[Product_Machine_Rel] ADD  CONSTRAINT [DF_Product_Machine_Rel_MachineDemandTypeId]  DEFAULT ('None') FOR [MachineDemandType]
GO
ALTER TABLE [dbo].[Product_Machine_Rel] ADD  CONSTRAINT [DF_Product_Machine_Rel_StartDate]  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[Product_Machine_Rel] ADD  CONSTRAINT [DF_Product_Machine_Rel_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[Product_Machine_Rel] ADD  CONSTRAINT [DF_Product_Machine_Rel_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[Product_Machine_Tran] ADD  CONSTRAINT [DF_Product_Machine_Tran_MachineDemandType]  DEFAULT ('None') FOR [MachineDemandType]
GO
ALTER TABLE [dbo].[Product_Machine_Tran] ADD  CONSTRAINT [DF_Product_Machine_Tran_ProductDemandType]  DEFAULT ('None') FOR [ProductDemandType]
GO
ALTER TABLE [dbo].[Product_Machine_Tran] ADD  CONSTRAINT [DF_Product_Machine_MPStartDate]  DEFAULT (getdate()) FOR [MPStartDate]
GO
ALTER TABLE [dbo].[Product_Machine_Tran] ADD  CONSTRAINT [DF_Product_Machine_Tran_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[ProductionEntry] ADD  CONSTRAINT [DF_ProductionEntry_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ProductionEntry] ADD  CONSTRAINT [DF_ProductionEntry_ActualDate]  DEFAULT (getdate()) FOR [ActualDate]
GO
ALTER TABLE [dbo].[ProductionEntryArchive] ADD  CONSTRAINT [DF_ProductionEntryArchive_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ProductScheduler] ADD  CONSTRAINT [DF_ProductScheduler_isFullDay]  DEFAULT ((1)) FOR [isFullDay]
GO
ALTER TABLE [dbo].[ProductScheduler] ADD  CONSTRAINT [DF_ProductScheduler_pastScheduleId]  DEFAULT ((0)) FOR [pastScheduleId]
GO
ALTER TABLE [dbo].[RejectionReason] ADD  CONSTRAINT [DF_RejectionReason_IsAcive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[RejectionReason] ADD  CONSTRAINT [DF_RejectionReason_InfluxRejReasonId]  DEFAULT ((0)) FOR [InfluxRejReasonId]
GO
ALTER TABLE [dbo].[ReportType] ADD  CONSTRAINT [DF_ReportType_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Shift] ADD  DEFAULT ((0)) FOR [DurationMin]
GO
ALTER TABLE [dbo].[StopageReason] ADD  DEFAULT ((0)) FOR [IsSchLoss]
GO
ALTER TABLE [dbo].[StopageReason] ADD  CONSTRAINT [DF_StopageReason_InfluxReasonId]  DEFAULT ((0)) FOR [InfluxReasonId]
GO
ALTER TABLE [dbo].[StopageSubReason] ADD  CONSTRAINT [DF_StopageSubReason_InfluxSubReasonId]  DEFAULT ((0)) FOR [InfluxSubReasonId]
GO
ALTER TABLE [dbo].[SupervisorMachineMap] ADD  CONSTRAINT [DF_SupervisorMachineMap_IsFullDay]  DEFAULT ((1)) FOR [IsFullDay]
GO
ALTER TABLE [dbo].[SupervisorMachineMap] ADD  CONSTRAINT [DF_SupervisorMachineMap_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SupervisorMachineMap] ADD  DEFAULT ((5)) FOR [OrganizationId]
GO
ALTER TABLE [dbo].[BreakDownEntryArchive]  WITH CHECK ADD  CONSTRAINT [FK_BreakDownEntryArchive_OrganizationUser] FOREIGN KEY([UserID])
REFERENCES [dbo].[OrganizationUser] ([UserId])
GO
ALTER TABLE [dbo].[BreakDownEntryArchive] CHECK CONSTRAINT [FK_BreakDownEntryArchive_OrganizationUser]
GO
ALTER TABLE [dbo].[BreakDownEntryArchive]  WITH CHECK ADD  CONSTRAINT [FK_BreakDownEntryArchive_StopageReason] FOREIGN KEY([ReasonId])
REFERENCES [dbo].[StopageReason] ([ReasonId])
GO
ALTER TABLE [dbo].[BreakDownEntryArchive] CHECK CONSTRAINT [FK_BreakDownEntryArchive_StopageReason]
GO
ALTER TABLE [dbo].[BreakInfo]  WITH CHECK ADD  CONSTRAINT [FK_BreakInfo_Shift] FOREIGN KEY([ShiftId])
REFERENCES [dbo].[Shift] ([ShiftId])
GO
ALTER TABLE [dbo].[BreakInfo] CHECK CONSTRAINT [FK_BreakInfo_Shift]
GO
ALTER TABLE [dbo].[MasterProduct]  WITH CHECK ADD  CONSTRAINT [FK_MasterProduct_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[MasterProduct] CHECK CONSTRAINT [FK_MasterProduct_Organization]
GO
ALTER TABLE [dbo].[OrganizationUser]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationUser_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[OrganizationUser] CHECK CONSTRAINT [FK_OrganizationUser_Organization]
GO
ALTER TABLE [dbo].[OrganizationUser]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationUser_UserType] FOREIGN KEY([UserTypeId])
REFERENCES [dbo].[UserType] ([UserTypeID])
GO
ALTER TABLE [dbo].[OrganizationUser] CHECK CONSTRAINT [FK_OrganizationUser_UserType]
GO
ALTER TABLE [dbo].[Shift]  WITH CHECK ADD  CONSTRAINT [FK_Shift_Organization] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organization] ([OrganizationId])
GO
ALTER TABLE [dbo].[Shift] CHECK CONSTRAINT [FK_Shift_Organization]
GO
ALTER TABLE [dbo].[UnitMaster]  WITH CHECK ADD  CONSTRAINT [FK_UnitMaster_Organization] FOREIGN KEY([OrganizationID])
REFERENCES [dbo].[Organization] ([OrganizationId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UnitMaster] CHECK CONSTRAINT [FK_UnitMaster_Organization]
GO
ALTER TABLE [dbo].[UserSendReportConfig]  WITH CHECK ADD  CONSTRAINT [FK_UserSendReportConfig_ReportType] FOREIGN KEY([ReportTypeId])
REFERENCES [dbo].[ReportType] ([ReportId])
GO
ALTER TABLE [dbo].[UserSendReportConfig] CHECK CONSTRAINT [FK_UserSendReportConfig_ReportType]
GO
ALTER TABLE [dbo].[UserSendReportConfig]  WITH CHECK ADD  CONSTRAINT [FK_UserSendReportConfig_UserReportsConfig] FOREIGN KEY([ReportGroupId])
REFERENCES [dbo].[UserReportsConfig] ([UserMachineReportID])
GO
ALTER TABLE [dbo].[UserSendReportConfig] CHECK CONSTRAINT [FK_UserSendReportConfig_UserReportsConfig]
GO
/****** Object:  StoredProcedure [dbo].[CreateMachineProductMap]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateMachineProductMap]	
	@UDT MachineProductMap READONLY,
	@CreatedBy int,
	@mode varchar(10) 
AS
BEGIN
	-- implement logic of Demand Type
	declare @currDate datetime =GETDATE();
	Declare @T_variable table(MPID int,[MachineID] int,[ProductID] int,[CycleTime] int,[MachineDemandType] varchar(50),[ProductDemandType] varchar(50))
	if(@mode='mapping')
	begin
	-- get list of non-existing Machin-Product entries
		Declare @T_variableNonExisting table(MPID int,[MachineID] int,[ProductID] int,[CycleTime] int)
		insert into @T_variableNonExisting([MachineID],[ProductID],[CycleTime]) 		
		select t1.[MachineID],t1.[ProductID],t1.[CycleTime]
		from @UDT t1
		WHERE NOT EXISTS (SELECT t2.MachineID,t2.ProductID FROM [Product_Machine_Rel] t2 WHERE t1.MachineId = t2.MachineID and t1.ProductId=t2.ProductID)
		-- insert non-existing entries in _Rel and _Tran
		
		insert into [Product_Machine_Tran]([MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType],[MPStartDate],[CreatedBy],[CreatedOn])	
		select t.[MachineID],t.[ProductID],t.[CycleTime],m.[DemandType],p.[ProductType],@currDate,@CreatedBy,@currDate
		from @T_variableNonExisting t 
		inner join [dbo].[MasterMachine] m on t.MachineID=m.MachineID
		inner join [dbo].[MasterProduct] p on t.ProductID=p.ProductId

		insert into [Product_Machine_Rel]([MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType],[StartDate],[CreatedBy],[CreatedOn])	
		select t.[MachineID],t.[ProductID],t.[CycleTime],m.[DemandType],p.[ProductType],@currDate,@CreatedBy,@currDate
		from @T_variableNonExisting t 
		inner join [dbo].[MasterMachine] m on t.MachineID=m.MachineID
		inner join [dbo].[MasterProduct] p on t.ProductID=p.ProductId
	  --get list from _Tran for all entries with product id and EndDate is NULL and DemandType changed	

	insert into @T_variable(MPID,[MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType]) 
	select MPID,t.[MachineID],t.[ProductID],t.[CycleTime],[MachineDemandType],[ProductDemandType]
	from [Product_Machine_Tran] t
	inner join @UDT u on t.MachineID=u.MachineId and t.ProductID=u.ProductId and t.CycleTime != u.CycleTime and MPEndDate is null

	--update _Tran End Date for above listed entries
	update t set t.MPEndDate=@currDate,ModifiedBy=@CreatedBy,ModifiedOn=@currDate from [Product_Machine_Tran] t inner join  @T_variable u on u.MPID=t.MPID

	--insert listed entries into _Tran again with updated startDate, null end date and updated demand type
	insert into [Product_Machine_Tran]([MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType],[MPStartDate],[CreatedBy],[CreatedOn])	
	select t.[MachineID],t.[ProductID],u.[CycleTime],t.[MachineDemandType],t.[ProductDemandType],@currDate,@CreatedBy,@currDate
	from @T_variable t inner join @UDT u on t.MachineID=u.MachineId and t.ProductID=u.ProductId

	--update _Rel table with start date and Demand type
	update r set r.StartDate=@currDate,r.CycleTime=u.CycleTime,CreatedBy=@CreatedBy,CreatedOn=@currDate from [Product_Machine_Rel] r 
	inner join  @UDT u on r.MachineID=u.MachineId and r.ProductID=u.ProductId and r.CycleTime != u.CycleTime
	end
	else if(@mode='product')
	begin	
	
	--get list from _Tran for all entries with product id and EndDate is NULL and DemandType changed
	--Declare @T_variable table(MPID int,[MachineID] int,[ProductID] int,[CycleTime] int,[MachineDemandType] varchar(50),[ProductDemandType] varchar(50))

	insert into @T_variable(MPID,[MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType]) 
	select MPID,t.[MachineID],t.[ProductID],t.[CycleTime],[MachineDemandType],[ProductDemandType]
	from [Product_Machine_Tran] t
	inner join @UDT u on t.ProductID=u.ProductId and t.ProductDemandType != u.DemandTypeText and MPEndDate is null

	--update _Tran End Date for above listed entries
	update t set t.MPEndDate=@currDate,ModifiedBy=@CreatedBy,ModifiedOn=@currDate from [Product_Machine_Tran] t inner join  @T_variable u on u.MPID=t.MPID

	--insert listed entries into _Tran again with updated startDate, null end date and updated demand type
	insert into [Product_Machine_Tran]([MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType],[MPStartDate],[CreatedBy],[CreatedOn])	
	select t.[MachineID],t.[ProductID],t.[CycleTime],t.[MachineDemandType],u.[DemandTypeText],@currDate,@CreatedBy,@currDate
	from @T_variable t inner join @UDT u on t.ProductID=u.ProductId

	--update _Rel table with start date and Demand type
	update r set r.StartDate=@currDate,r.ProductDemandType=u.DemandTypeText,CreatedBy=@CreatedBy,CreatedOn=@currDate from [Product_Machine_Rel] r 
	inner join  @UDT u on r.ProductID=u.ProductId and r.ProductDemandType != u.DemandTypeText

	end
	else if(@mode='machine')
	begin
	 --get list from _Tran for all entries with product id and EndDate is NULL and DemandType changed	

	insert into @T_variable(MPID,[MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType]) 
	select MPID,t.[MachineID],t.[ProductID],t.[CycleTime],[MachineDemandType],[ProductDemandType]
	from [Product_Machine_Tran] t
	inner join @UDT u on t.MachineID=u.MachineId and t.MachineDemandType != u.DemandTypeText and MPEndDate is null

	--update _Tran End Date for above listed entries
	update t set t.MPEndDate=@currDate,ModifiedBy=@CreatedBy,ModifiedOn=@currDate from [Product_Machine_Tran] t inner join  @T_variable u on u.MPID=t.MPID

	--insert listed entries into _Tran again with updated startDate, null end date and updated demand type
	insert into [Product_Machine_Tran]([MachineID],[ProductID],[CycleTime],[MachineDemandType],[ProductDemandType],[MPStartDate],[CreatedBy],[CreatedOn])	
	select t.[MachineID],t.[ProductID],t.[CycleTime],u.[DemandTypeText],t.[ProductDemandType],@currDate,@CreatedBy,@currDate
	from @T_variable t inner join @UDT u on t.MachineID=u.MachineId

	--update _Rel table with start date and Demand type
	update r set r.StartDate=@currDate,r.MachineDemandType=u.DemandTypeText,CreatedBy=@CreatedBy,CreatedOn=@currDate from [Product_Machine_Rel] r 
	inner join  @UDT u on r.MachineID=u.MachineId and r.MachineDemandType != u.DemandTypeText
	end
end



GO
/****** Object:  StoredProcedure [dbo].[DeleteDuplicateDataFromBreakDownEntry]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteDuplicateDataFromBreakDownEntry]  
AS  
BEGIN  
IF OBJECT_ID('tempdb.dbo.#DuplicateBreakDownData', 'U') IS NOT NULL  
DROP TABLE #DuplicateBreakDownData;   
  
CREATE TABLE   #DuplicateBreakDownData (BreakDownId int,ProductionDate datetime,  
StartTime datetime,EndTime datetime,  
ShiftId int,TotalStoppageTime decimal(18,2),MachineID int)  
  
insert into #DuplicateBreakDownData(BreakDownId,ProductionDate,StartTime,EndTime,ShiftId,TotalStoppageTime,MachineID)  
select   MAX(BreakDownID), MAX(ProductionDate), MAX(StartTime), MAX(EndTime), MAX(ShiftId),  
MAX(TotalStoppageTime),max(MachineID)  from  BreakDownEntry where IsActive=1  
group by MachineID,ProductionDate,TotalStoppageTime,StartTime,EndTime,ShiftId  
having count(*)>1  
  
  
DELETE B from  BreakDownEntry   B  
INNER JOIN #DuplicateBreakDownData d  
  ON   
  d.ProductionDate=b.ProductionDate and   
  d.StartTime=b.StartTime and  
   d.EndTime=b.EndTime and   
  d.ShiftId=b.ShiftId and  
   d.TotalStoppageTime=b.TotalStoppageTime and   
  d.MachineID=b.MachineID   
  and B.BreakDownID<>d.BreakDownID  
  
  drop table #DuplicateBreakDownData  
  
END  
GO
/****** Object:  StoredProcedure [dbo].[DeleteProdBreakEntryByshift]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[DeleteProdBreakEntryByshift]    
    
 @ShiftId int,    
 @ProductionDate datetime ,  
 @Typ nvarchar(1000)
 AS    
BEGIN    
	if(@Typ='Production')
	begin

		delete from [ProductionEntry] WHERE ProductionDate=@ProductionDate and ShiftId=@ShiftId 
	end
	else if(@Typ='Breakdown')
	begin

		delete from BreakDownEntry WHERE ProductionDate=@ProductionDate and ShiftId=@ShiftId 
	end
	else
	begin
		delete from [ProductionEntry] WHERE ProductionDate=@ProductionDate and ShiftId=@ShiftId ;
		delete from BreakDownEntry WHERE ProductionDate=@ProductionDate and ShiftId=@ShiftId ;
	end
END    
  
  
  




GO
/****** Object:  StoredProcedure [dbo].[DeleteSupervisorSchedule]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DeleteSupervisorSchedule]
@SupervisorMachineID int
as
begin
	DELETE FROM [dbo].[SupervisorMachineMap]
     WHERE SupervisorMachineID=@SupervisorMachineID
end






GO
/****** Object:  StoredProcedure [dbo].[DownloadRawBreakdownData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DownloadRawBreakdownData]
as
begin
select top 10 convert(varchar(23),TimeOfEntry,106)ProductionDate
,(select MAchineName from MasterMachine where MachineID=b.MachineID) MachineName	
	,case when (Select ReasonName from stopageReason where ReasonId=b.ReasonId) is null then 'No Data Entry' else (Select ReasonName from stopageReason where ReasonId=b.ReasonId) end Reason
	,(Select ShiftName from [Shift] where ShiftId=b.ShiftId) ShiftName	
	,cast(TotalStoppageTime as int) ProCount
	,convert(char(5), StartTime, 108) StartTime
	,convert(char(5), EndTime, 108) EndTime from BreakDownEntry b

end

GO
/****** Object:  StoredProcedure [dbo].[DownloadRawProductionData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DownloadRawProductionData]
as
begin
select top 10 convert(varchar(23),ProductionDateOnly,106) ProductionDate
,(select MAchineName from MasterMachine where MachineID=b.MachineID) MachineName	
	,(Select ProductName from MasterProduct where ProductID=b.productid) Product
	,(Select ShiftName from [Shift] where ShiftId=b.ShiftId) ShiftName,
	cast(TotalProduction as int) ProCount,
	cast(Rejection as int) RejCount from ProductionEntry b

end

GO
/****** Object:  StoredProcedure [dbo].[DropAllData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DropAllData]
@OrgId int
as
begin
	truncate table tblDropTest;
	--truncate table productionentry;
	--truncate table breakdownentry;
	--truncate table qualityentry;
end

GO
/****** Object:  StoredProcedure [dbo].[EditBreakDownEntryInflux]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[EditBreakDownEntryInflux]	
	@BreakDownDT BreakdownEditTableType READONLY
	 
AS
BEGIN
update t set t.ReasonId=u.ReasonId,t.SubReasonId=u.SubReasonId,t.Remarks=u.Remarks
 from [BreakDownEntry] t inner join  @BreakDownDT u on u.BreakdownId=t.BreakdownId
end



GO
/****** Object:  StoredProcedure [dbo].[GetAllStoppageReasons]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllStoppageReasons]
AS 
BEGIN
SELECT        StoppageReasonTypeID, StoppageReasonTypeName, IsActive, AddBy, AddDate, EditBy, EditDate
FROM            StoppageReasonType
END
GO
/****** Object:  StoredProcedure [dbo].[GetBreakDownEntryByShiftAndDate]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBreakDownEntryByShiftAndDate]  --'4-05-2018','18-042019',27,10,'Inspection Delay'       
@ProdDate datetime  ,    
@ProdToDate datetime =null ,    
@ShiftID int,    
@OrganisationID INT    
AS        
BEGIN        
-- exec GetBreakDownEntryByShiftAndDate '2021-01-28',1,5    
--declare @isManualEntry int=0;    
--select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@OrganisationID    
select BreakdownRowIndex = ROW_NUMBER() OVER (ORDER BY m.MachineName),b.BreakdownId, convert(varchar, ProductionDate, 105) ProductionDate,    
b.ShiftId,s.ShiftName,    
b.MachineID,m.MachineName,    
OperatorID,UserName OperatorName,    
 b.StartTime, b.EndTime,    
  b.TotalStoppageTime/60 as TotalStoppageTime ,    
b.ReasonId,sr.ReasonName,    
b.SubReasonId,ssr.SubReasonName,Remarks,m.OrganizationId,TimeOfEntry    
,[IsSaved],b.[IsDeleted],b.UserID    
 from BreakDownEntry b inner join [Shift] s on b.ShiftId=s.ShiftId     
 inner join MasterMachine m on b.MachineID=m.MachineID     
 left outer join StopageReason sr on b.ReasonId=sr.ReasonId    
 left outer join AspNetUsers u on b.UserID=u.Id    
 left outer join StopageSubReason ssr on b.SubReasonId= ssr.SubReasonId    
WHERE  --CAST(FLOOR(CAST(ProductionDate as float)) as datetime)=@ProdDate    
Convert(date,ProductionDate) Between Convert(date,@ProdDate) And Convert(date,@ProdToDate) AND    
b.MachineId in (select MachineID from MasterMachine where OrganizationId=@OrganisationID and IsActive=1)      
 AND B.ShiftId=@ShiftID    
    
END     
    
    
    
    
    
    
GO
/****** Object:  StoredProcedure [dbo].[GetBreakDownEntryForEdit]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBreakDownEntryForEdit]     
@ProdDate varchar(50)  ,  
@ShiftID int  
AS      
BEGIN   
 -- exec GetBreakDownEntryForEdit '2022-11-07',1  
  
 select BreakdownId,MachineName,b.MachineID,ReasonId,SubReasonId,ShiftName,  
 RIGHT('0' + CAST(cast(TotalStoppageTime as int) / 3600 AS VARCHAR),2) + ':' +  
 RIGHT('0' + CAST((cast(TotalStoppageTime as int) / 60) % 60 AS VARCHAR),2) + ':' +  
 RIGHT('0' + CAST(cast(TotalStoppageTime as int) % 60 AS VARCHAR),2) TotalStoppageTime,   
 CONVERT(VARCHAR(5),b.starttime,108) StartTime ,CONVERT(VARCHAR(5),b.endtime,108) EndTime,ProductionDate,Remarks from breakdownentry b  
 inner join MasterMachine m on m.MachineID=b.MachineID  
 inner join [shift] s on s.ShiftId=b.ShiftId  
 where CAST(ProductionDate as date) = cast(@ProdDate as date)  AND  
  b.ShiftId=@ShiftID  
  ORDER BY b.starttime
 End  
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[GetMachineProductList]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetMachineProductList]    
@OrganizationID int,    
@mode varchar(10)    
as    
begin    
 if(@mode='machine')    
 begin    
     
  select distinct  d.[MachineID],d.[MachineName],d.[MachineCode],d.[Location] ,r.MachineDemandType,    
  (select CategoryName from [dbo].[MachineCategory] where CategoryId=d.CategoryId) MachineCategory    
  from [dbo].[MasterMachine] d left join Product_Machine_Rel r on d.MachineID=r.MachineID and d.OrganizationId=@OrganizationID  and d.IsActive=1  
 end    
 else if(@mode='product')    
 begin     
  select distinct a.[ProductId],[ProductCode],[ProductName], ProductDemandType,    
  (select categoryname from [Organization_ProductCategory] where ID=a.PrdouctCategoryID) CategoryName,    
  (select CustomerName from Organization_Customer where ID=a.CustomerID) CustomerName      
  from [dbo].[MasterProduct] a       
  left join Product_Machine_Rel r on a.ProductId=r.ProductID and a.OrganizationId=@OrganizationID   and a.IsActive=1  
 end    
     
    
end
GO
/****** Object:  StoredProcedure [dbo].[GetMachineProductMap]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetMachineProductMap]
@MachineIds varchar(5000),
@ProductIds varchar(5000),
@OrganizationID int
as
begin
	
		-- exec GetMachineProductMap '10,11','9,143',5
		select  d.[MachineID],d.[MachineName],d.[MachineCode],d.[Location],r.MachineDemandType,a.[ProductId],[ProductCode],[ProductName],
		case when coalesce(r.CycleTime,0)>0 then r.CycleTime else a.[CycleTime] end ProductCycletime,
		case when coalesce(r.CycleTime,0)>0 then 'True' else 'False' end IsMapped,
		[UnitID],[ProductType] ProductDemandType,	d.[MachineID],d.[MachineName],d.[Location],d.[MachineCode]
		--,b.CategoryName,c.CustomerName,e.CategoryName 
		from [MasterProduct] a
		
		--inner join [dbo].[Organization_ProductCategory] b on b.ID=a.PrdouctCategoryID and b.OrganizationID=a.OrganizationId
		--inner join [dbo].[Organization_Customer] c on c.ID=a.CustomerID and c.OrganizationID=a.OrganizationId
		cross join [dbo].[MasterMachine] d 
		left outer join Product_Machine_Rel r on r.MachineID=d.MachineID and r.ProductID=a.ProductId
		--inner join [dbo].[MachineCategory] e on e.CategoryId=d.CategoryId and d.OrganizationId=e.OrganizationId
		where a.OrganizationId=@OrganizationID and d.OrganizationId=@OrganizationID and a.ProductId in (select value from dbo.fn_Split(@ProductIds,','))
		and  d.MachineId in (select value from dbo.fn_Split(@MachineIds,','))
	

end



GO
/****** Object:  StoredProcedure [dbo].[GetProductionDataAndBreakDown]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProductionDataAndBreakDown]  --GetProductionDataAndBreakDown '2022-01-01' ,'2023-01-01' 
@StartDate Date,  
@Enddate Date  
AS  
BEGIN  
  
  
  
--select Cast(@StartDate as varchar)  
declare @reasonCount int   
Declare @ReasonTabl TABLE(Sl int,ReasonId int,ReasonName nvarchar(200))  
  
  
insert into @ReasonTabl(sl,ReasonId,ReasonName)  
select   ROW_NUMBER() OVER (order by ReasonId) SL,ReasonId,ReasonName from StopageReason where IsActive=1   
  
  
select @reasonCount=count(*) from  @ReasonTabl  
declare @queryString nvarchar(max)=''  
declare @index int =1  
while @index<=@reasonCount  
begin  
  
Declare @ReasonId int  
Declare  @ReasonName nvarchar(200)  
declare @ReasonType nvarchar(200)  
  
select Top 1 @ReasonId=reasonId, @ReasonName=Replace(ReasonName,'/','') from @ReasonTabl where  Sl=@index  
  
set @ReasonName=Replace(@ReasonName,'&','')  
set @ReasonName=Replace(@ReasonName,' ','')  
select top 1 @ReasonType=isnull( StoppageReasonTypeName,'') from StoppageReasonType T inner join StopageReason R on T.StoppageReasonTypeID=R.StoppageReasonTypeID  
and R.ReasonId=@ReasonId  
  
set @queryString=@queryString+',isnull(dbo.GetBreakDownTime(P.DD_DateOfEntry,P.ShiftId,P.MachineId,'+cast( @ReasonId as varchar)+'),0) '+ ' '+ ISNULL(@ReasonType,'')+'_'+ISNULL(@ReasonName,'')  
set @index=@index+1  
end  
  
--select @queryString  
  
  
  
declare @sqlstr varchar(max)='select P.* '+@queryString+' from dbo.UDF_ProductionReport (5,'''+cast(@StartDate as varchar)+''','''+cast(@EndDate as varchar)+''') P '  
EXEC  (@sqlstr)  
END  
GO
/****** Object:  StoredProcedure [dbo].[GetProductionEntryByShiftAndDate]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetProductionEntryByShiftAndDate] -- NULL,NULL,NULL,NULL,NULL,'2019-02-10'          
@ProdDate datetime,  
@ProdToDate datetime=null,  
@ShiftID INT,          
@OrganisationID INT          
        
 AS         
BEGIN          
     -- exec GetProductionEntryByShiftAndDate '2021/01/28',1,5     
SELECT  ProductionRowIndex = ROW_NUMBER() OVER (ORDER BY msmch.MachineName),[ProductionId],@OrganisationID  OrganizationId    
    ,convert(varchar, [ProductionDate], 105) as ProductionDate    
    ,prod.ShiftId,ShiftName    
    ,prod.MachineID,msmch.MachineName    
    ,OperatorID,orguser.UserName OperatorName    
    ,prod.ProductID,pmas.ProductName    
      ,[TotalProduction]    
      ,[ReWork]    
      ,[Rejection]    
   ,[RejectionList]    
   ,ProductionDate ProductionStartTime    
   ,ProductionDate ProductionEndTime    
      ,0 [IsSaved],[IsDeleted],prod.UserID    
          
  FROM [dbo].[ProductionEntry] as prod    
  left join dbo.MasterMachine as msmch on prod.MachineID=msmch.MachineID    
  left join dbo.MasterProduct as pmas on prod.ProductID=pmas.ProductID    
  left join AspNetUsers as orguser on prod.OperatorID = orguser.Id    
  left join [Shift] s on prod.ShiftId=s.ShiftId     
  where prod.ShiftId=@ShiftID and  --CAST(FLOOR(CAST(ProductionDate as float)) as datetime)=@ProdDate    
  Convert(date,ProductionDate) Between Convert(date,@ProdDate) And Convert(date,@ProdToDate)   
  and msmch.OrganizationId=@OrganisationID --and RejectionList is not null        
END    
    
    
    
    
    
GO
/****** Object:  StoredProcedure [dbo].[GetProductionPlanningByDateRange]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE  [dbo].[GetProductionPlanningByDateRange]
@StartDate DateTime,
@EndDate DateTime,
@OrganizationId	INT
AS	
BEGIN
		SELECT        
				RelationId, MachineId, 
				ProductId, SheduleDate, 
				AddByUser, EditByUser,
				AddDate, EditDate, 
				OrganizationId,
				(select top 1 MachineName from  MasterMachine M where  M.MachineID=ProductionPlanning.MachineId) MachineName,
				(select top 1 ProductName from  MasterProduct P where  P.ProductId=ProductionPlanning.ProductId) ProductName

FROM           
        ProductionPlanning 
				WHERE
				CAST(SheduleDate AS DATE) BETWEEN CAST(@StartDate AS DATE) 
				AND CAST(@EndDate AS DATE) 
				AND OrganizationId=@OrganizationId order by SheduleDate 
END
GO
/****** Object:  StoredProcedure [dbo].[GetReportToBeSent]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetReportToBeSent]
(@OrganizationId int)
as
begin
	-- exec GetReportToBeSent
	declare @date datetime=getdate();
	declare @datenum int;
	declare @dayname varchar(10);
	select @datenum=DATEPART(d, @date),@dayname=DATENAME(dw,@date); 
	select @dayname= Datename(weekday, @date) 

	select ReportGroupId,DateRange,ReportTypeId,ReportName,MailTo=STUFF((SELECT ',' + d.Email 
		FROM AspNetUsers AS d
		INNER JOIN (select value from dbo.fn_Split(MailTo,',')) f
		ON f.value=d.Id   
		FOR XML PATH, TYPE).value('.[1]', 'nvarchar(max)'), 1, 1, '')	
		  from userSendReportconfig where DateRange='Daily' and @dayname in (select value from fn_split(ValidDays,',')) and OrganizationId=@OrganizationId
	union
	select ReportGroupId,DateRange,ReportTypeId,ReportName,MailTo=STUFF((SELECT ',' + d.Email 
		FROM AspNetUsers AS d
		INNER JOIN (select value from dbo.fn_Split(MailTo,',')) f
		ON f.value=d.Id   
		FOR XML PATH, TYPE).value('.[1]', 'nvarchar(max)'), 1, 1, '')	
		  from userSendReportconfig where DateRange='Monthly' and @datenum=MonthStartDate and OrganizationId=@OrganizationId
	union
	select ReportGroupId,DateRange,ReportTypeId,ReportName,MailTo=STUFF((SELECT ',' + d.Email 
		FROM AspNetUsers AS d
		INNER JOIN (select value from dbo.fn_Split(MailTo,',')) f
		ON f.value=d.Id   
		FOR XML PATH, TYPE).value('.[1]', 'nvarchar(max)'), 1, 1, '')	
		  from userSendReportconfig where DateRange='Weekly' and @dayname=WeekStartDay and OrganizationId=@OrganizationId
		--select 0 UserMachineReportID,'Last Month' DateRange,'shailendra.chahar@gmail.com' EmailTo
	
end





GO
/****** Object:  StoredProcedure [dbo].[GetSupervisorSchedule]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetSupervisorSchedule]
--@dateFrom datetime,
--@dateTo datetime,
@machineIds varchar(500),
@shiftIds varchar(50),
@supervisorIds varchar(100),
@OrganizationId int
as
begin
	-- exec GetSupervisorSchedule '','',''
 if(@shiftIds is null or @shiftIds ='') begin set @shiftIds=''; SELECT @shiftIds = COALESCE(@shiftIds + ',', '') + CAST(shiftId AS VARCHAR) from [Shift]  end
 if(@machineIds is null or @machineIds ='') begin set @machineIds=''; SELECT @machineIds = COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from [MasterMachine]  end
 if(@supervisorIds is null or @supervisorIds ='') begin set @supervisorIds=''; SELECT @supervisorIds = COALESCE(@supervisorIds + ',', '') + CAST(Id AS VARCHAR) from AspNetUsers  end

SELECT [SupervisorMachineId]
      ,[SupervisorId]
      ,sm.[MachineId]
      ,sm.[ShiftId]
      ,[StartDate]
      ,[EndDate]
      ,[IsFullDay]
      ,sm.[IsActive]
      ,[ThemeColor]
     -- ,[MachineIds]
	  ,m.[MachineName]
	  ,s.ShiftName
	  ,u.UserName SupervisorName,
	  cast(sm.machineId as varchar) MachineIds,
	  sm.OrganizationId
  FROM [dbo].[SupervisorMachineMap] sm inner join AspNetUsers u on sm.[SupervisorId]=u.Id
  inner join [Shift] s on s.ShiftId=sm.ShiftId
  inner join [MasterMachine] m on sm.MachineId=m.MachineID
  where --StartDate>=@dateFrom and [EndDate]<=@dateTo and 
  sm.ShiftId in (select value from dbo.fn_split(@shiftIds,','))
  and [SupervisorId] in (select value from dbo.fn_split(@supervisorIds,','))
  and  sm.[MachineId] in (select value from dbo.fn_split(@machineIds,','))
end






GO
/****** Object:  StoredProcedure [dbo].[GetUserDashboard]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[GetUserDashboard]
@UserId int
AS
Begin
select MachineId,DateRange from userDashboard
end



GO
/****** Object:  StoredProcedure [dbo].[GetUserMAchineReport]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserMAchineReport]
@UserMachineReportID INT=null,
@UserID int
AS
BEGIN
IF (@UserMachineReportID =0)
	BEGIN
	SELECT  [UserMachineReportID],[UserId],[ReportName],[DateRange],[MachineIds],[IsDefault],[MailTo],UserName FROM [dbo].[UserReportsConfig] r
	inner join AspNetUsers u on r.UserId=u.Id where UserID=@UserID
		/*
		if((select UserTypeId from AspNetUsers where Id=@UserID)=1)
		begin
			SELECT  * FROM [dbo].[UserReportsConfig] 
		end
		else
		begin
			SELECT  * FROM [dbo].[UserReportsConfig] where UserID=@UserID
		end
		*/
	END
ELSE
    BEGIN
	SELECT  [UserMachineReportID],[UserId],[ReportName],[DateRange],[MachineIds],[IsDefault],[MailTo],UserName FROM [dbo].[UserReportsConfig] r
	inner join AspNetUsers u on r.UserId=u.Id WHERE UserMachineReportID=@UserMachineReportID
	
	END
END







GO
/****** Object:  StoredProcedure [dbo].[GetUtilityMasterMapping]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUtilityMasterMapping]   
	@OrganizationId int =NULL	
AS 
BEGIN

SELECT        RecId, OrganizationId, MachineId, ProductId, OperatorId, MouldId, OperationId, DieId,TargetProduction
FROM            UtilityMasterMapping WHERE  OrganizationId=@OrganizationId
 
END
GO
/****** Object:  StoredProcedure [dbo].[SaveDataFromInflux]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SaveDataFromInflux]     
 @UDTBreak BreakdownTableType READONLY,    
 @UDTProd ProductionTableType READONLY,    
 @UDTRej QualityTableType READONLY,    
 @ProductionDate datetime,    
 @OrganizationId int    
AS    
BEGIN    
    
    
-- This is for Production Entry    
 --Start     
 INSERT INTO [dbo].[ProductionEntry]    
           ([TimeOfEntry],[MachineID],[ShiftId],[ProductID],[SupervisorID],[TotalProduction],[Rejection],[ProductionDate],OperatorID,MouldId,OperationId,DieId,TargetProduction,MouldCavity,DieCavity)    
 SELECT A.EntryTime,A.MachineID,A.ShiftId,A.ProductId,A.SupervisorId,A.TotalProduction,A.Rejection,@ProductionDate,map.OperatorId,map.MouldId,map.OperationId,map.DieId,map.TargetProduction,map.MouldCavity,map.DieCavity
FROM (  
 select  DATEADD(MINUTE,-330,EntryTime) AS EntryTime,m.MachineId,s.ShiftId, p.ProductId,0 SupervisorId,TotalProduction,Rejection    
 from @UDTProd u    
 inner join Mastermachine m on case when @OrganizationId=5 and m.machineName=u.MachineName then 1     
 when @OrganizationId=6 and m.machineName=u.MachineName then 1 else 0 end=1    
 inner join [Shift] s on s.ShiftName=u.ShiftName and s.OrganizationId=@OrganizationId    
 inner join MasterProduct P on p.ProductName=dbo.getProductName(@OrganizationId,m.MachineID,@ProductionDate) ) AS A  
 Left Join UtilityMasterMapping as map on A.MachineID=map.MachineID and A.ProductId=map.ProductId  
 --End    
    
    --This is for BreakDown Entry    
    --Start    
 INSERT INTO [dbo].[BreakDownEntry]    
           ([TimeOfEntry],[MachineID],[ShiftId],[ReasonId],[SupervisorID],[TotalStoppageTime],[StartTime],[EndTime],[IsIdle],[ProductionDate],OperatorID)    
  
     select A.EntryTime,A.MachineId,A.ShiftId,A.ReasonId,A.SupervisorId,A.TotalStoppageTime,A.StartTime,A.EndTime,A.IsIdle, @ProductionDate ,  
     (select top 1 OperatorId from UtilityMasterMapping as map where A.MachineId=map.MachineId and map.OrganizationId=@OrganizationId) as OperatorId  
     from (  
 select  DATEADD(MINUTE,-330,EntryTime) as EntryTime,m.MachineId,s.ShiftId, case when u.ReasonId>0 then r.ReasonId else u.ReasonId end ReasonId,    
 sm.SupervisorId,TotalStoppageTime,    
 dbo.fn_ConvertEpochToDateTime(u.StartTime) as StartTime,dbo.fn_ConvertEpochToDateTime(u.EndTime) as EndTime,    
 CAST(IIF ( IsIdle = 1, 1, 0 ) AS BIT) as IsIdle   
 from @UDTBreak u    
 inner join Mastermachine m on case when @OrganizationId=5 and     
 m.machineName=u.MachineName then 1     
 when @OrganizationId=6 and m.machineName=u.MachineName then 1 else 0 end=1    
 inner join [Shift] s on s.shiftname=u.ShiftName and s.OrganizationId=@OrganizationId    
 left outer join StopageReason r on r.InfluxReasonId=u.ReasonId    
 left outer join SupervisorMachineMap sm on StartDate>=DATEADD(MINUTE,-330,EntryTime)    
 and [EndDate]<=DATEADD(MINUTE,-330,EntryTime) and s.ShiftId=sm.ShiftId and     
 sm.MachineId=m.MachineID  ) as A  
 --End    
    
 --REMOVE DUPLICATE DATA FROM BREAK DOWN    
 EXEC DeleteDuplicateDataFromBreakDownEntry    
     
 --This is for Quality Entry    
 --Start    
    
 INSERT INTO [dbo].[QualityEntry]([TimeOfEntry],[MachineID],[ShiftId],[RejectionReasonId],[ProdCount],[ProductionDate])    
   select  DATEADD(MINUTE,-330,EntryTime),m.MachineId,s.ShiftId,ReasonIds,ProdCount,@ProductionDate    
 from @UDTRej u    
 inner join Mastermachine m on case when @OrganizationId=5 and    
 m.machineName=u.MachineName then 1     
 when @OrganizationId=6 and m.machineName=u.MachineName then 1 else 0 end=1    
 inner join [Shift] s on s.shiftname=u.ShiftName    
 left outer join RejectionReason r on cast(u.ReasonIds as int)=r.ReasonId    
 --End    
    
end    
  


GO
/****** Object:  StoredProcedure [dbo].[SaveProductionPlanning]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[SaveProductionPlanning]  
@SheduleStartDate DateTime,  
@SheduleEndDate DateTime,  
@IsRestOfTheMonth Bit=0,  
@OrganizationId INT,  
@MachineId INT,  
@ProductId INT,  
@UserId INT  
AS   
BEGIN  
   
  
   
  
 IF @IsRestOfTheMonth=0  
 BEGIN  
  INSERT INTO [dbo].[ProductionPlanning]  
  (  
      
     MachineId  ,  
     ProductId  ,  
     SheduleDate  ,  
     AddByUser  ,  
     EditByUser  ,  
     AddDate   ,  
     EditDate  ,  
     OrganizationId  
  )  
  VALUES(  
     @MachineId  ,  
     @ProductId  ,  
     @SheduleStartDate  ,  
     @UserId,  
     @UserId,  
     GETDATE(),  
     GETDATE(),  
     @OrganizationId  
  
  )  
 END  
 ELSE  
 BEGIN  
  
   
 DECLARE @StartDay INT=DATEPART(DAY,@SheduleStartDate)  
 DECLARE @StartMonth INT=DATEPART(MONTH,@SheduleStartDate)  
 DECLARE @EndDay INT=DATEPART(DAY,@SheduleEndDate)  
 DECLARE @EndMonth INT=DATEPART(MONTH,@SheduleStartDate)  
  
  
   
 WHILE @StartDay<=@EndDay  
     BEGIN  
  IF @StartMonth=@EndMonth  
     BEGIN  
         INSERT INTO [dbo].[ProductionPlanning]  
        (  
      
           MachineId  ,  
           ProductId  ,  
           SheduleDate  ,  
           AddByUser  ,  
           EditByUser  ,  
           AddDate   ,  
           EditDate  ,  
           OrganizationId  
        )  
        VALUES(  
           @MachineId  ,  
           @ProductId  ,  
           @SheduleStartDate  ,  
           @UserId,  
           @UserId,  
           GETDATE(),  
           GETDATE(),  
           @OrganizationId  
  
        )  
  
        SET @SheduleStartDate=DATEADD(DAY,1,@SheduleStartDate)  
        SET @StartDay=DATEPART(DAY,@SheduleStartDate)  
        SET @StartMonth =DATEPART(MONTH,@SheduleStartDate)  
     END  
  ELSE  
  BREAK  
   
  END  
  
  
  
  
 END  
END  
GO
/****** Object:  StoredProcedure [dbo].[SaveUserDashboard]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SaveUserDashboard]
@MachineId int,
@UserId int,
@DateRange varchar(50)
as
begin
	delete from [UserDashboard] where UserId=@UserId;
	INSERT INTO [dbo].[UserDashboard]
           ([UserId]
           ,[MachineId]
           ,[DateRange])
     VALUES
           (@UserId,@MAchineId,@DateRange)
end



GO
/****** Object:  StoredProcedure [dbo].[SaveUtilityMapping]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[SaveUtilityMapping]   
    @RecId int  = NULL,
	@OrganizationId int =NULL,
	@MachineId int =NULL,
	@ProductId int =NULL,
	@OperatorId int =NULL,
	@MouldId int =NULL,
	@OperationId int =NULL,
	@DieId int =NULL,
	@TargetProduction int= NULL,
	@MouldCavity int= NULL,
	@DieCavity int =NULL  
AS   
BEGIN  

  
BEGIN TRAN T1;    
DELETE FROM UtilityMasterMapping WHERE  MachineId=@MachineId AND OrganizationId=@OrganizationId  
INSERT INTO UtilityMasterMapping(   OrganizationId, MachineId, ProductId, OperatorId, MouldId, OperationId, DieId, TargetProduction, MouldCavity, DieCavity)  
VALUES(
@OrganizationId, 
@MachineId, 
@ProductId, 
@OperatorId, 
@MouldId, 
@OperationId, 
@DieId, 
@TargetProduction,
@MouldCavity,
@DieCavity
)  
COMMIT TRAN T1;    
END  
GO
/****** Object:  StoredProcedure [dbo].[SP_CALCULATE_OEE_REPORT]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[SP_CALCULATE_OEE_REPORT]  
@MachineId INT=0,  
@ShiftId INT=0,  
@ProductionDate DATETIME  
AS  
  
--SP_CALCULATE_OEE_REPORT 0,0,'2022-11-08'  
  
--INSERT SHUTDOWN TIME  

-- DELETE PREVIOUS RECORD

DELETE OEECalculation WHERE CONVERT(DATE,ProductionDate)=CONVERT(DATE,@ProductionDate) AND (@MachineId=0 OR MachineId=@MachineId) AND (@ShiftId=0 OR ShiftId=@ShiftId)
  
;WITH CTE AS(  
  
SELECT DISTINCT BE.MachineID,BE.ShiftId,CONVERT(DATE,BE.ProductionDate) AS ProductionDate,
DBO.GetToTalBreakDownTimeBySchLoss(BE.ShiftId,BE.MachineID,CONVERT(DATE,BE.ProductionDate),1) AS ShutdownTime,  
DBO.GetToTalBreakDownTimeBySchLoss(BE.ShiftId,BE.MachineID,CONVERT(DATE,BE.ProductionDate),0) AS UtilisationLoss  
,(SELECT ISNULL(SUM(DurationMin),0) FROM BreakInfo AS BI WHERE BI.ShiftId=BE.ShiftId AND BI.IsActive=1) AS BreakTime,  
DBO.GetProducionTimebyProduct(BE.ShiftId,BE.MachineID,CONVERT(DATE,BE.ProductionDate)) AS TotalProductointime,  
DBO.GetProducionQualityLossProduct(BE.ShiftId,BE.MachineID,CONVERT(DATE,BE.ProductionDate)) AS QualityLoss  
FROM ProductionEntry BE WHERE          
CONVERT(DATE,@ProductionDate)= CONVERT(DATE,ProductionDate)         
 AND (@MachineId=0 OR BE.MachineID=@MachineId) --AND BE.IsActive=1   
 AND (@ShiftId=0 OR BE.ShiftId=@ShiftId)   
 GROUP BY BE.MachineID,BE.ShiftId,BE.ProductID,CONVERT(DATE,BE.ProductionDate)  
 --ORDER BY BE.MachineID  
)  

--SELECT * FROM CTE  
  
MERGE OEECalculation AS TARGET  
USING CTE AS SOURCE   
ON (TARGET.MachineId = SOURCE.MachineId AND TARGET.ShiftId = SOURCE.ShiftId AND CONVERT(DATE,TARGET.ProductionDate) = CONVERT(DATE,SOURCE.ProductionDate))   
--When records are matched, update the records if there is any change  
WHEN MATCHED   
THEN UPDATE SET TARGET.ShutdownTime = SOURCE.ShutdownTime, TARGET.BreakTime = SOURCE.BreakTime, TARGET.UtilisationLoss = SOURCE.UtilisationLoss  
, TARGET.TotalProductointime = SOURCE.TotalProductointime, TARGET.QualityLoss = SOURCE.QualityLoss  
  
WHEN NOT MATCHED  
THEN INSERT (MachineId, ShiftId, ProductionDate,ShutdownTime,BreakTime,UtilisationLoss,TotalProductointime,QualityLoss) VALUES   
(SOURCE.MachineId, ISNULL(SOURCE.ShiftId,0), SOURCE.ProductionDate,SOURCE.ShutdownTime, SOURCE.BreakTime, SOURCE.UtilisationLoss  
, SOURCE.TotalProductointime, SOURCE.QualityLoss);  
  
  
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateShiftForBreakdownData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- sp_UpdateShiftForBreakdownData '2022-11-21','2022-11-21'
CREATE Proc [dbo].[sp_UpdateShiftForBreakdownData](@FromDate datetime,@ToDate datetime)
As

DECLARE @AddTimeInMin INT=0; -- CONVERT GMT TO IST

CREATE TABLE #TempShift  
(  
	ShiftId int,  
	StartTime datetime,
	EndTime  datetime,
	IsOverNight bit
)

INSERT INTO #TempShift(ShiftId,StartTime,EndTime,IsOverNight) 
SELECT S.ShiftId,
	(Select CAST('2022-01-01 '+REPLACE(SI.StartTime,'.',':') AS datetime) From [Shift] AS SI Where SI.ShiftId=S.ShiftId) AS MShiftStartTime,
	(Select Case When SI.IsOverNight=1 Then DATEADD(DAY,1,CAST('2022-01-01 '+REPLACE((SI.EndTime-24),'.',':') AS DATETIME)) ELSE 
	CAST('2022-01-01 '+REPLACE(SI.EndTime,'.',':') AS datetime) End From [Shift] AS SI Where SI.ShiftId=S.ShiftId) AS MShiftEndTime,S.IsOverNight
    FROM [Shift] AS S
    WHERE S.IsActive=1
 

CREATE TABLE #UpdatedShift  
(  
	BreakDownID int,  
	UpdatedShiftId int
)

Insert Into #UpdatedShift(BreakDownID,UpdatedShiftId)
  SELECT BE.BreakDownID, (Select Top 1 CTE.ShiftId FROM #TempShift As CTE Where 
  Case When (CTE.IsOverNight=1 AND Convert(time, DATEADD(MINUTE,@AddTimeInMin,BE.EndTime)) BETWEEN Convert(time,'00:01') AND Convert(time,CTE.EndTime)) Then Convert(Datetime,'2022-01-02 '+ Convert(varchar(5),Convert(Time, DATEADD(MINUTE,@AddTimeInMin,BE.EndTime))))
  else Convert(Datetime,'2022-01-01 '+ Convert(varchar(5),Convert(Time, DATEADD(MINUTE,@AddTimeInMin,BE.EndTime)))) End Between 
  Convert(datetime,CTE.StartTime) And Convert(datetime,CTE.EndTime)) As UpdatedShiftId
  From [dbo].[BreakDownEntry] As BE Where
  CONVERT(DATE,BE.ProductionDate) Between CONVERT(DATE,@FromDate) And CONVERT(DATE,@ToDate)


 Update [BreakDownEntry] SET ShiftId=t2.UpdatedShiftId
 FROM        [BreakDownEntry] t1
INNER JOIN  #UpdatedShift t2
ON       t1.BreakDownID = t2.BreakDownID

Drop Table #TempShift
Drop Table #UpdatedShift

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateShiftForProductionData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- sp_UpdateShiftForProductionData '2022-06-11','2022-06-11'
create Proc [dbo].[sp_UpdateShiftForProductionData](@FromDate datetime,@ToDate datetime)
As

DECLARE @AddTimeInMin INT=330;

CREATE TABLE #TempShift  
(  
	ShiftId int,  
	StartTime datetime,
	EndTime  datetime,
	IsOverNight bit
)

INSERT INTO #TempShift(ShiftId,StartTime,EndTime,IsOverNight) 
SELECT S.ShiftId,
	(Select CAST('2022-01-01 '+REPLACE(SI.StartTime,'.',':') AS datetime) From [Shift] AS SI Where SI.ShiftId=S.ShiftId) AS MShiftStartTime,
	(Select Case When SI.IsOverNight=1 Then DATEADD(DAY,1,CAST('2022-01-01 '+REPLACE((SI.EndTime-24),'.',':') AS DATETIME)) ELSE 
	CAST('2022-01-01 '+REPLACE(SI.EndTime,'.',':') AS DATETIME) End From [Shift] AS SI Where SI.ShiftId=S.ShiftId) AS MShiftEndTime,S.IsOverNight
    FROM [Shift] AS S
    WHERE S.IsActive=1
 
 
CREATE TABLE #UpdatedShift  
(  
	ProductionId int,  
	UpdatedShiftId int
)

Insert Into #UpdatedShift(ProductionId,UpdatedShiftId)
  SELECT PE.ProductionId, (Select Top 1 CTE.ShiftId FROM #TempShift As CTE Where 
  Case When (CTE.IsOverNight=1 AND Convert(time, DATEADD(MINUTE,@AddTimeInMin,PE.ProductionDate)) 
  BETWEEN Convert(time,'00:01') AND Convert(time,CTE.EndTime)) 
  Then Convert(Datetime,'2022-01-02 '+ Convert(varchar(5),Convert(Time,DATEADD(MINUTE,@AddTimeInMin,PE.ProductionDate))))
  else Convert(Datetime,'2022-01-01 '+ Convert(varchar(5),
  Convert(Time,DATEADD(MINUTE,@AddTimeInMin,PE.ProductionDate)))) End Between 
  Convert(datetime,CTE.StartTime) And Convert(datetime,CTE.EndTime)) As UpdatedShiftId
  From [dbo].[ProductionEntry] As PE Where CONVERT(DATE,PE.ProductionDate) Between CONVERT(DATE,@FromDate) And CONVERT(DATE,@ToDate)


  Update [ProductionEntry] SET ShiftId=t2.UpdatedShiftId
 FROM        [ProductionEntry] t1
INNER JOIN  #UpdatedShift t2
ON       t1.ProductionId = t2.ProductionId

  

  --  SELECT PE.ProductionId,PE.ProductionDate, (Select Top 1 CTE.ShiftId FROM #TempShift As CTE Where 
  --Case When (CTE.IsOverNight=1 AND Convert(time, DATEADD(MINUTE,@AddTimeInMin,PE.ProductionDate))
  --BETWEEN Convert(time,'00:01') AND Convert(time,CTE.EndTime)) Then 
  --Convert(Datetime,'2022-01-02 '+ Convert(varchar(5),Convert(Time, DATEADD(MINUTE,@AddTimeInMin,PE.ProductionDate))))
  --else Convert(Datetime,'2022-01-01 '+ Convert(varchar(5),Convert(Time,DATEADD(MINUTE,@AddTimeInMin,PE.ProductionDate)))) End Between 
  --Convert(datetime,CTE.StartTime) And Convert(datetime,CTE.EndTime)) As UpdatedShiftId
  --From [dbo].[ProductionEntry] As PE Where CONVERT(DATE,PE.ProductionDate) Between CONVERT(DATE,@FromDate) And CONVERT(DATE,@ToDate)


Drop Table #TempShift
Drop Table #UpdatedShift
GO
/****** Object:  StoredProcedure [dbo].[UpdateSupervisorSchedule]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[UpdateSupervisorSchedule]
@StartDate datetime,
@EndDate datetime,
@machineIds varchar(500),
@shiftId int,
@supervisorId int,
@ThemeColor varchar(20),
@SupervisorMachineID int,
@msg_Error varchar(max) output ,
@OrganizationId int
as
begin
/*
declare @a varchar(500);
exec UpdateSupervisorSchedule '2020-12-09','2020-12-11','9,12',3,5,'Blue',0,@a
print @a
-- select [dbo].[fnValidateSupervisorSchedule]('2020-12-09','2020-12-11',9,2,4)
*/
	
declare @strMsg varchar(max)
set @strMsg='';
set @msg_Error='';
	DECLARE @pos INT
	DECLARE @len INT
	DECLARE @value int
	set @pos = 0
	set @len = 0
	
	----------------------------    Validate Section --------------------------------
	
		if(Len(@machineIds)>0)
		begin
			
			set @machineIds=@machineIds+',';
			
			WHILE CHARINDEX(',', @machineIds, @pos+1)>0
			BEGIN
				set @len = CHARINDEX(',', @machineIds, @pos+1) - @pos
				set @value = SUBSTRING(@machineIds, @pos, @len)
           
				select @strMsg= [dbo].[fnValidateSupervisorSchedule](@StartDate,@EndDate,Cast(@value as int),@shiftId,@SupervisorMachineID);
				if(Len(@strMsg)>0) begin set @msg_Error= @msg_Error+','+ @strMsg end
				
				set @pos = CHARINDEX(',', @machineIds, @pos+@len) +1
			END
		end
	
	if(Len(@msg_Error)>0)
	begin
		set @msg_Error='Schedule alredy exists for given dates of Shift: '+(Select ShiftName from [Shift] where shiftId=@shiftId) +' and Machines: '+@msg_Error;
	
	end
	else
	begin 
		if(@SupervisorMachineID=0)
		
	begin
	
		if(Len(@machineIds)>0)
		begin
		set @pos = 0
	set @len = 0
			--set @machineIds=@machineIds+',';
			WHILE CHARINDEX(',', @machineIds, @pos+1)>0
			BEGIN
			
				set @len = CHARINDEX(',', @machineIds, @pos+1) - @pos
				set @value = SUBSTRING(@machineIds, @pos, @len)
           
			   INSERT INTO [dbo].[SupervisorMachineMap]
					   ([SupervisorId],[MachineId],[ShiftId],[StartDate],[EndDate],[IsFullDay] ,[IsActive],[ThemeColor],[OrganizationId])
				 VALUES
					   (@supervisorId,cast(@value as int) ,@shiftId,@StartDate ,@EndDate,1,1,@ThemeColor,@OrganizationId)

				set @pos = CHARINDEX(',', @machineIds, @pos+@len) +1
			END
		end
	end
	else
	begin
	
		set @machineIds=LEFT(@machineIds, LEN(@machineIds) - 1)
		UPDATE [dbo].[SupervisorMachineMap]
		   SET [SupervisorId] =@supervisorId
			  ,[MachineId] = CAST(@machineIds AS int)
			  ,[ShiftId] = @shiftId
			  ,[StartDate] = @StartDate
			  ,[EndDate] = @EndDate
			  ,[IsFullDay] = 1
			  ,[IsActive] =1
			  ,[ThemeColor] = @ThemeColor
			  ,[OrganizationId]=@OrganizationId
		 WHERE SupervisorMachineID=@SupervisorMachineID
	end
	end
	
End




GO
/****** Object:  StoredProcedure [dbo].[UserMachineReportDelete]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserMachineReportDelete]
@UserMachineReportID int
AS
BEGIN
if (@UserMachineReportID>0)
begin
	DELETE FROM [dbo].[UserReportsConfig]
				WHERE UserMachineReportID=@UserMachineReportID
				end
	
END





GO
/****** Object:  StoredProcedure [dbo].[UserMachineReportOP]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserMachineReportOP]
@UserMachineReportID int,
     @UserId int,
      @ReportName varchar(100),
      @DateRange varchar(50),
      @MachineIds varchar(500),
	  @MailTo varchar(500)=null,
      @IsDefault bit	
AS
BEGIN

	if(@IsDefault=1)
	begin
				UPDATE [dbo].[UserReportsConfig] SET  [IsDefault] = 0
					WHERE [UserId] = @UserId
	end
	IF (@UserMachineReportID=0)    
		BEGIN
			INSERT INTO [dbo].[UserReportsConfig]
				([UserId],[ReportName],[DateRange],[MachineIds],[IsDefault],[MailTo])
			VALUES
				(@UserId,@ReportName, @DateRange, @MachineIds,@IsDefault,@MailTo)
		END
	ELSE
		BEGIN
			UPDATE [dbo].[UserReportsConfig]
			   SET [UserId] = @UserId
				  ,[ReportName] = @ReportName
				  ,[DateRange] = @DateRange
				  ,[MachineIds] = @MachineIds
				  ,[IsDefault] = @IsDefault
				  ,[MailTo]=@MailTo
				WHERE UserMachineReportID=@UserMachineReportID
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateProduct_Die_Rel]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateProduct_Die_Rel]
    @RecId int =  NULL,
	@DieId int =NULL,
	@OrganizationId int=NULL,
	@ProductID int =NULL,
	@CreatedBy int =NULL,
	@CreatedOn datetime = NULL
	AS
	BEGIN
	BEGIN TRAN TRAN1	

	INSERT INTO Product_Die_Rel(
	DieId ,
	OrganizationId ,
	ProductID ,
	CreatedBy,
	CreatedOn )
	VALUES
	(	
	@DieId ,
	@OrganizationId ,
	@ProductID ,
	@CreatedBy,
	@CreatedOn 
	)

	COMMIT TRAN TRAN1
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_CreateProduct_Mould_Rel]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_CreateProduct_Mould_Rel]
    @RecId int =  NULL,
	@MouldId int =NULL,
	@OrganizationId int=NULL,
	@ProductID int =NULL,
	@CreatedBy int =NULL,
	@CreatedOn datetime = NULL
	AS
	BEGIN
	BEGIN TRAN TRAN1

	INSERT INTO Product_Mould_Rel(
	MouldId ,
	OrganizationId ,
	ProductID ,
	CreatedBy,
	CreatedOn )
	VALUES
	(	
	@MouldId ,
	@OrganizationId ,
	@ProductID ,
	@CreatedBy,
	@CreatedOn 
	)

	COMMIT TRAN TRAN1
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateBreakDownEntry]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateBreakDownEntry]  
    @BreakDownID int =NULL,  
    @MachineID int =NULL,  
    @OperatorID int= NULL,  
    @ReasonId int= NULL, 
	@SubReasonId int=NULL,
    @ShiftId int =NULL,  
    @StartTime datetime =NULL,  
    @EndTime datetime= NULL,  
    @TimeOfEntry datetime =NULL,  
    @TotalStoppageTime decimal(18, 2)= NULL,  
    @UserID int= NULL,  
    @IsActive bit= NULL,  
    @SupervisorID int =NULL,  
    @AddBy int =NULL,  
    @EdittedBy int= NULL,  
    @ProductionDate DATE = null ,
	@Remarks NVARCHAR(500) =NULL,
 @deletedEntries nvarchar(3000)=null
AS  
BEGIN  
if(Len(@deletedEntries)>0)
begin
declare @maxPID int=0;
select @maxPID=max(cast(value as int)) from dbo.fn_Split(@deletedEntries,',');
	--INSERT INTO [ProductionEntryArchive] 
	--SELECT * FROM [ProductionEntry] WHERE ProductionDate=@ProductionDate and ShiftId=@ShiftId and ProductionId<=@maxPID and
	-- ProductionId not in (select value from dbo.fn_Split(@deletedEntries,','));
	delete from BreakDownEntry WHERE ProductionDate=@ProductionDate and ShiftId=@ShiftId and BreakDownID<=@maxPID and
	 BreakDownID not in (select value from dbo.fn_Split(@deletedEntries,','));
end
IF NOT Exists (SELECT BreakDownID FROM BreakDownEntry WHERE BreakDownID=@BreakDownID)      
  BEGIN  
DECLARE @MachineCategoryid INT=0
SELECT @MachineCategoryid =ISNULL( CategoryId,0) from  [dbo].[MasterMachine] WHERE MachineId=@MachineID

    INSERT INTO [dbo].BreakDownEntry(              
          MachineID,  
          ReasonId,  
          UserID,                
          OperatorID,  
          SupervisorID,  
          TimeOfEntry,  
          ShiftId,  
          StartTime,   
          EndTime,   
          TotalStoppageTime,  
          IsActive,   
          AddBy,   
          EdittedBy,  
          ProductionDate,  
          IsSaved  ,
		  Remarks,
		  SubReasonId,
		  MachineCategoryId
    )  
    VALUES  
                                       (         
          @MachineID,  
          @ReasonId,  
          @UserID,                
          @OperatorID,  
          @SupervisorID,  
          @TimeOfEntry,  
          @ShiftId,  
          @StartTime,   
          @EndTime,   
          @TotalStoppageTime,  
          @IsActive,   
          @AddBy,   
          @EdittedBy,  
          @ProductionDate,  
          0  ,
		  @Remarks,
		  @SubReasonId,
		  @MachineCategoryid
              )  

							--THIS IS FOR LOG ENTRY ----
	DECLARE @MESSAGE1 VARCHAR(500)
								SELECT @MESSAGE1='Breakdown entry is being saved with Start Time '+
								cast ((@StartTime) as varchar)   + ' End Time '+
								cast ( @EndTime as varchar) + ' Total Stoppage Time '+
								cast ( @TotalStoppageTime as varchar)

	EXEC		USP_CREATELog 'BreakDownEntry',
								@MESSAGE1							 ,
								1,
								'ADD',
								@UserID
								--***--
												
											  

  END  
ELSE  
  BEGIN  
         UPDATE BreakDownEntry  
         SET            
         MachineID=@MachineID,  
         ReasonId=@ReasonId,  
         UserID=@UserID,                
         OperatorID=@OperatorID,  
         SupervisorID=@SupervisorID,  
         --TimeOfEntry=@TimeOfEntry,  
         ShiftId=@ShiftId,  
         StartTime=@StartTime,   
         EndTime=@EndTime,   
         TotalStoppageTime=@TotalStoppageTime,  
         IsActive=@IsActive,                 
         EdittedBy=@EdittedBy,  
         ProductionDate = @ProductionDate,  
         IsSaved = 0  ,
		 Remarks=@Remarks,
		 SubReasonId=@SubReasonId
         WHERE   
         BreakDownID=@BreakDownID  

		 	--THIS IS FOR LOG ENTRY ----
	DECLARE @MESSAGE2 VARCHAR(500)
								SELECT @MESSAGE2='Breakdown entry is being saved with Start Time '+
								cast ((@StartTime) as varchar)   + ' End Time '+
								cast ( @EndTime as varchar) + ' Total Stoppage Time '+
								cast ( @TotalStoppageTime as varchar)

	EXEC		USP_CREATELog 'BreakDownEntry',
								@MESSAGE2							 ,
								1,
								'UPDATE',
								@UserID
								--***--
  END  
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateBreakInfo]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateBreakInfo]
    @BreakId int= null,
    @ShiftId int = null,
	@BreakName nvarchar(200)= null,
	@StartTime decimal(18,2)=null,
	@EndTime decimal(18,2)=null,
	@IsActive bit = null,
	@AddBy int=null,
	@EdittedBy int=null,
    @IsFirst bit = null 
AS
BEGIN
IF @IsFirst=1 -- At the Time of First Row Insert in Edit Mode  
BEGIN  
DELETE BreakInfo WHERE ShiftId=@ShiftId  
END 
print @BreakName
IF (@IsFirst is null or @BreakName is not null)
BEGIN  
print 1
				INSERT INTO [dbo].[BreakInfo](												
												ShiftId,
												BreakName,
												StartTime,
												EndTime,
												IsActive,
												AddBy,
												EdittedBy,
												DurationMin
				)
				VALUES
				(																	
												@ShiftId,
												@BreakName,
												@StartTime,
												@EndTime,
												@IsActive,
												@AddBy,
												@EdittedBy,
												dbo.GetTimeInMinutes(@StartTime,@EndTime)
				)
END  
		
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateBulkBreakDownEntry]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_CreateUpdateBulkBreakDownEntry] 
   @tblBreakdownEntryTableType [dbo].tblBreakdownENtry REadonly
AS  
BEGIN  
    

        INSERT into BreakDownEntry( 
		  MachineID,  
          ReasonId,  
          UserID,                
          OperatorID,  
          SupervisorID,  
          TimeOfEntry,  
          ShiftId,  
          StartTime,   
          EndTime,   
          TotalStoppageTime,  
          IsActive,   
          AddBy,   
          EdittedBy,  
          ProductionDate,  
          IsSaved  ,
		  Remarks,
		  SubReasonId)  
        select 
		
		 MachineID,  
         ReasonId,  
         UserID,                
          OperatorID,  
          SupervisorID,  
          TimeOfEntry,  
          ShiftId,  
          StartTime,   
         EndTime,   
         TotalStoppageTime,  
         IsActive,   
          AddBy,   
          EdittedBy,  
          ProductionDate,  
          0  ,
		 Remarks,
		  SubReasonId
		 from @tblBreakdownEntryTableType 
END 




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateDie]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateDie]
	@DieId int  = NULL,
	@DieDescription nvarchar(200) =NULL,
	@AvailableCavityCount int =NULL,
	@OrganizationId int=NULL,
	@AddBy int= NULL,
	@AddDate datetime =NULL,
	@EditBy int =NULL,
	@EditDate datetime =NULL,
	@IsActive bit =NULL
AS
BEGIN


IF NOT Exists (SELECT  * FROM  MasterDie WHERE DieId=@DieId) 
   
		BEGIN
				INSERT INTO [dbo].MasterDie(	
												 DieDescription,
												 AvailableCavityCount,
												 OrganizationId,
												 AddBy, 
												 AddDate,																								
												 IsActive
				)
				VALUES
				(
					
												
												
												 @DieDescription,
												 @AvailableCavityCount,
												 @OrganizationId,
												 @AddBy, 
												 GETDATE(),																								
												 @IsActive
				)
		END
ELSE
		BEGIN
		UPDATE MasterDie
		SET 
		
								DieDescription=@DieDescription,
								AvailableCavityCount=@AvailableCavityCount,
								OrganizationId=@OrganizationId,
								EditBy=@EditBy, 
								EditDate=GETDATE(),																								
								IsActive=@IsActive
									WHERE 
									DieId=@DieId
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateMachine]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateMachine]
	@MachineID int = NULL,
	@MachineName nvarchar(200) =NULL,
	@Location nvarchar(500) =NULL,
	@OrganizationId int= NULL,
	@MachineCode nvarchar(200)= NULL,
	@IsActive bit= NULL,
	@AddBy int =NULL,
	@EdittedBy int =NULL,
	@CategoryId int= NULL,
	@DemandType varchar(50)
AS
BEGIN


IF NOT Exists (SELECT  * FROM  MasterMachine WHERE MachineID=@MachineID) 
   
		BEGIN
				INSERT INTO [dbo].[MasterMachine](
					
												
												MachineName,
												Location ,
												OrganizationId,
												MachineCode ,
												IsActive ,
												AddBy ,
												EdittedBy,
												CategoryId,
												DemandType
				)
				VALUES
				(
					
												
												@MachineName,
												@Location ,
												@OrganizationId,
												@MachineCode ,
												@IsActive ,
												@AddBy ,
												@EdittedBy,
												@CategoryId,
												@DemandType
				)
		END
ELSE
		BEGIN
		UPDATE [MasterMachine]
		SET 
		
											MachineName=@MachineName,
											Location=@Location ,
											OrganizationId=@OrganizationId,
											MachineCode=@MachineCode ,
											IsActive=@IsActive ,											
											EdittedBy=@EdittedBy,
											CategoryId=@CategoryId,
											DemandType=@DemandType
									WHERE 
									MachineID=@MachineID
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateMachineCategory]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateMachineCategory]
    @CategoryId int= null,
	@CategoryName varchar(500),
	@OrganizationId int
AS
BEGIN
IF NOT EXISTS(SELECT * FROM MachineCategory WHERE CategoryId=@CategoryId)
		
		BEGIN
		INSERT INTO MachineCategory(CategoryName,OrganizationId)
		VALUES(@CategoryName,@OrganizationId)
		END

ELSE
        BEGIN
		
		UPDATE MachineCategory
		SET 
		CategoryName=@CategoryName
		,OrganizationId=@OrganizationId
		WHERE 
		CategoryId=@CategoryId

		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateMould]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateMould]
	@MouldId int  = NULL,
	@MouldDescription nvarchar(200) =NULL,
	@AvailableCavityCount int =NULL,
	@OrganizationId int=NULL,
	@AddBy int= NULL,
	@AddDate datetime =NULL,
	@EditBy int =NULL,
	@EditDate datetime =NULL,
	@IsActive bit =NULL
AS
BEGIN


IF NOT Exists (SELECT  * FROM  MasterMould WHERE MouldId=@MouldId) 
   
		BEGIN
				INSERT INTO [dbo].MasterMould(	
												 MouldDescription,
												 AvailableCavityCount,
												 OrganizationId,
												 AddBy, 
												 AddDate,																								
												 IsActive
				)
				VALUES
				(
					
												
												
												 @MouldDescription,
												 @AvailableCavityCount,
												 @OrganizationId,
												 @AddBy, 
												 GETDATE(),																								
												 @IsActive
				)
		END
ELSE
		BEGIN
		UPDATE MasterMould
		SET 
		
								MouldDescription=@MouldDescription,
								AvailableCavityCount=@AvailableCavityCount,
								OrganizationId=@OrganizationId,
								EditBy=@EditBy, 
								EditDate=GETDATE(),																								
								IsActive=@IsActive
									WHERE 
									MouldId=@MouldId
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateOperation]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateOperation]
	@OperationId int= NULL,	
	@OrganizationId int= NULL,
	@OperationDescription nvarchar(200)= NULL,
	@AddBy int =NULL,
	@EditBy int =NULL,
	@IsActive bit =NULL
AS
BEGIN


IF NOT Exists (SELECT  * FROM  MasterOperation WHERE OperationId=@OperationId) 
   
		BEGIN
				INSERT INTO [dbo].MasterOperation(													
												OrganizationId, 
												OperationDescription,
												AddBy,
												EditBy,
												IsActive
				)
				VALUES
				(
					
												
												
												
												@OrganizationId, 
												@OperationDescription,
												@AddBy,
												@EditBy,
												@IsActive
				)
		END
ELSE
		BEGIN
		UPDATE MasterOperation
		SET 		
			
			OrganizationId=@OrganizationId, 
			OperationDescription=@OperationDescription,
			EditBy=@EditBy,
			IsActive=@IsActive
									WHERE 
									OperationId=@OperationId
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateOperator]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateOperator]
	@OperatorId int= NULL,
	@EmployeeId nvarchar(50) = NULL,
	@OrganizationId int= NULL,
	@OperatorName nvarchar(200)= NULL,
	@AddBy int =NULL,
	@EditBy int =NULL,
	@IsActive bit =NULL
AS
BEGIN


IF NOT Exists (SELECT  * FROM  MasterOperator WHERE OperatorId=@OperatorId) 
   
		BEGIN
				INSERT INTO [dbo].MasterOperator(	
												EmployeeId,
												OrganizationId, 
												OperatorName,
												AddBy,
												EditBy,
												IsActive
				)
				VALUES
				(
					
												
												
												@EmployeeId,
												@OrganizationId, 
												@OperatorName,
												@AddBy,
												@EditBy,
												@IsActive
				)
		END
ELSE
		BEGIN
		UPDATE MasterOperator
		SET 
		
			EmployeeId=@EmployeeId,
			OrganizationId=@OrganizationId, 
			OperatorName=@OperatorName,
			EditBy=@EditBy,
			IsActive=@IsActive
									WHERE 
									OperatorId=@OperatorId
		END
END




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateOrganization]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateOrganization]
	@OrganizationId int = NULL,
	@OrganizationName nvarchar(150)= NULL,
	@PhoneNo nvarchar(20)= NULL,
	@EmailId nvarchar(200) =NULL,
	@Location nvarchar(500) =NULL,
	@IsActive bit =NULL,
	@AddBy int =NULL,
	@EdittedBy int= NULL
AS
BEGIN

      

	  
	  
IF    NOT Exists (SELECT  * FROM  Organization WHERE OrganizationId=@OrganizationId) 
		BEGIN
				INSERT INTO [dbo].Organization(		
									OrganizationName,
									PhoneNo ,
									EmailId ,
									Location,
									IsActive,
									AddBy,
									EdittedBy
				)
				VALUES
				(
					
												
									@OrganizationName,
									@PhoneNo ,
									@EmailId ,
									@Location,
									@IsActive,
									@AddBy,
									@EdittedBy
				)


				set @OrganizationId= SCOPE_IDENTITY() 

				Insert into Organization_ProductCategory (CategoryName,OrganizationID)
				values ('None',@OrganizationId)
		END
ELSE
		BEGIN
		UPDATE Organization
		SET 
		
									OrganizationName=@OrganizationName,
									PhoneNo=@PhoneNo ,
									EmailId=@EmailId ,
									Location=@Location,
									IsActive=@IsActive,									
									EdittedBy=@EdittedBy
									WHERE 
									OrganizationId=@OrganizationId
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateProduct]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateProduct]
	@ProductId int = NULL,
	@ProductCode nvarchar(100)= NULL,
	@OrganizationId int= NULL,
	@ProductName nvarchar(100)= NULL,
	@CycleTime DECIMAL(18,2)= NULL,
	@IsActive bit= NULL,
	@AddBy int =NULL,
	@EdittedBy int= NULL,
	@Type varchar(200)=NULL,
	@CatID int=null,
	@unitID int=null,
	@NewCategoryName varchar(100),
	@CustomerID int=null,
	@NewCustomerName varchar(100)
AS
BEGIN

	if (@NewCategoryName!='NA' and @NewCategoryName!='')
		BEGIN
			if Exists (SELECT  * FROM  Organization_ProductCategory WHERE CategoryName=@NewCategoryName and OrganizationID=@OrganizationId) 
				 BEGIN
					 -- Delete from [dbo].[Organization_ProductCategory] where CategoryName='None' and OrganizationID=@OrganizationId
					 SELECT  @CatID=CategoryName FROM  Organization_ProductCategory WHERE CategoryName=@NewCategoryName and OrganizationID=@OrganizationId
				  end
			else
				   begin
						INSERT INTO [dbo].[Organization_ProductCategory] (CategoryName,OrganizationID) Values
						(@NewCategoryName,@OrganizationId)

						set @CatID= SCOPE_IDENTITY() 
					end
		End

	if (@NewCustomerName!='NA' and @NewCustomerName!='')
		BEGIN
		if Exists (SELECT  * FROM  Organization_Customer WHERE CustomerName=@NewCustomerName and OrganizationID=@OrganizationId) 
			 BEGIN         
				 SELECT  @CustomerID=CustomerName FROM  Organization_Customer WHERE CustomerName=@NewCustomerName and OrganizationID=@OrganizationId
			  end
			else
			   begin
					INSERT INTO [dbo].[Organization_Customer] (CustomerName,OrganizationID) 
														Values(@NewCustomerName,@OrganizationId)

					set @CustomerID= SCOPE_IDENTITY() 
				end
		End

	IF NOT Exists (SELECT  * FROM  MasterProduct WHERE ProductId=@ProductId)  
		BEGIN
				INSERT INTO [dbo].[MasterProduct](ProductCode,OrganizationId,ProductName ,CycleTime,IsActive,AddBy ,EdittedBy,ProductType,PrdouctCategoryID,UnitID,CustomerID)
				VALUES( @ProductCode,@OrganizationId,@ProductName ,@CycleTime,@IsActive,@AddBy ,@EdittedBy,@Type,@CatID,@unitID,@CustomerID)
		END
ELSE
		BEGIN
			UPDATE [MasterProduct]SET		
								ProductCode=@ProductCode,
								OrganizationId=@OrganizationId,
								ProductName=@ProductName ,
								CycleTime=@CycleTime,
								IsActive=@IsActive,											
								EdittedBy=@EdittedBy,
								ProductType=@Type,
								PrdouctCategoryID=@CatID,
								UnitID=@unitID,
								CustomerID=@CustomerID
							WHERE ProductId=@ProductId
		END

END




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateProductionRejection]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateProductionRejection]   
    @ProductionEntryId int = null,
	@XML  nvarchar(Max)=''
	--@RejectionReasonId int = null,
	--@RejectionQuantity int=0
AS
BEGIN
DECLARE @handle INT  
DECLARE @PrepareXmlStatus INT  

EXEC @PrepareXmlStatus= sp_xml_preparedocument @handle OUTPUT, @XML  


--DECLARE @XML XML
--SET @XML = '<rows><row>
--    <IdInvernadero>8</IdInvernadero>
--    <IdProducto>3</IdProducto>
--    <IdCaracteristica1>8</IdCaracteristica1>
--    <IdCaracteristica2>8</IdCaracteristica2>
--    <Cantidad>25</Cantidad>
--    <Folio>4568457</Folio>
--</row>
--<row>
--    <IdInvernadero>3</IdInvernadero>
--    <IdProducto>3</IdProducto>
--    <IdCaracteristica1>1</IdCaracteristica1>
--    <IdCaracteristica2>2</IdCaracteristica2>
--    <Cantidad>72</Cantidad>
--    <Folio>4568457</Folio>
--</row></rows>'



			DECLARE @ProductionRejection TABLE( ProductionEntryId int,RejectionReasonId int ,RejectionQuantity int)
			SELECT @ProductionEntryId,RejectionReasonId,RejectionQuantity
				 FROM    OPENXML(@handle, '/rows/row', 2)  
				  WITH (
				RejectionReasonId INT,
				RejectionQuantity INT
				)  

			IF EXISTS(SELECT * FROM  @ProductionRejection)
			BEGIN

			        DELETE FROM [dbo].[ProductionRejection] WHERE ProductionEntryId=@ProductionEntryId

					INSERT INTO ProductionRejection(ProductionEntryId ,RejectionReasonId  ,RejectionQuantity )
					SELECT  ProductionEntryId ,RejectionReasonId  ,RejectionQuantity 
					FROM  @ProductionRejection
			END
END




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateRejectionReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateRejectionReason]  
    @ReasonId int= null,    
 @ReasonName nvarchar(200)= null,  
 @ReasonDescription nvarchar(500)=null,  
 @IsActive BIT=1  ,
 @OrganizationId INT,
 @InfluxRejReasonId int
AS  
BEGIN  
  
IF NOT EXISTS(SELECT  * FROM  RejectionReason WHERE ReasonId=@ReasonId)  
  BEGIN  
       INSERT INTO RejectionReason  
       (    
       ReasonName,   
       ReasonDescription,  
       IsActive  ,
	   OrganizationId,InfluxRejReasonId
       )  
     VALUES(   
     @ReasonName,   
     @ReasonDescription,  
     @IsActive  ,
	 @OrganizationId,@InfluxRejReasonId
     )  
  END  
ELSE  
BEGIN  
UPDATE RejectionReason  
     SET ReasonName=@ReasonName,  
     ReasonDescription=@ReasonDescription,  
     IsActive=@IsActive ,
	 InfluxRejReasonId=@InfluxRejReasonId 
     WHERE ReasonId=@ReasonId  
  
END  
    
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateShift]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[USP_CreateUpdateShift]    
 @ShiftId int=null,    
 @ShiftName nvarchar(200) =NULL,    
 @ShiftDescription nvarchar(500) =NULL,    
 @StartTime decimal(18,2)= NULL,    
 @EndTime decimal(18,2)= NULL,    
 @OrganizationId int= NULL,    
 @IsActive bit= NULL,    
 @AddBy int =NULL,    
 @EdittedBy int =NULL ,  
 @isOverNight bit = NULL,
 @ActualStartTime nvarchar(5),
 @ActualEndTime nvarchar(5)
 AS    
BEGIN    
IF NOT Exists (SELECT ShiftId FROM  Shift WHERE ShiftId=@ShiftId)        
  BEGIN    
    INSERT INTO [dbo].[Shift](      
            ShiftName,    
            ShiftDescription,    
            StartTime,    
            EndTime,    
            OrganizationId,    
            IsActive,    
            AddBy,    
            EdittedBy,    
            DurationMin,  
			isOverNight,
			ActualStartTime,
			ActualEndTime
    )    
    VALUES    
    (         
            @ShiftName,    
            @ShiftDescription,    
            @StartTime,    
            @EndTime,    
            @OrganizationId,    
            @IsActive,    
            @AddBy,    
            @EdittedBy,    
            dbo.GetTimeInMinutes(@StartTime,@EndTime),  
			@isOverNight,
			@ActualStartTime,
			@ActualEndTime
    )    
  END    
ELSE    
  BEGIN    
  UPDATE [Shift]    
  SET     
                  
            ShiftName=@ShiftName,    
            ShiftDescription=@ShiftDescription,    
            StartTime=@StartTime,    
            EndTime=@EndTime,    
            OrganizationId=@OrganizationId,    
            IsActive=@IsActive,    
            AddBy=@AddBy,    
            EdittedBy=@EdittedBy,    
            DurationMin=dbo.GetTimeInMinutes(@StartTime,@EndTime),  
			isOverNight=@isOverNight,
			ActualStartTime = @ActualStartTime,
			ActualEndTime=@ActualEndTime
         WHERE     
         ShiftId=@ShiftId    
  END    
END    
  
GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateStopageReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
CREATE PROCEDURE [dbo].[USP_CreateUpdateStopageReason]      
   @ReasonId int = null,      
   @ReasonName nvarchar(500)=null,      
   @ReasonCode nvarchar(50)=null,      
   @IsActive bit= NULL,      
   @AddBy int =NULL,      
   @EdittedBy int =NULL,      
   @IsSchLoss bit= NULL  ,  
   @OrganizationId INT,  
   @InfluxReasonId int ,
   @StoppageReasonTypeID int =null
AS      
BEGIN      
IF NOT Exists (SELECT ReasonId FROM StopageReason WHERE ReasonId=@ReasonId)          
  BEGIN      
    INSERT INTO [dbo].[StopageReason](      
            ReasonName,      
            ReasonCode,      
            IsActive,      
            AddBy,      
            EdittedBy,  
			StoppageReasonTypeID,
   IsSchLoss  ,  
   OrganizationId,  
   InfluxReasonId  
    )      
    VALUES      
    (           
            @ReasonName,      
            @ReasonCode,      
            @IsActive,      
            @AddBy,      
            @EdittedBy,
			@StoppageReasonTypeID,
   @IsSchLoss    ,  
   @OrganizationId,  
   @InfluxReasonId  
    )      
  END      
ELSE      
  BEGIN      
  UPDATE [StopageReason]      
  SET                
            ReasonName=@ReasonName,      
            ReasonCode=@ReasonCode,      
            IsActive=@IsActive,      
            AddBy=@AddBy,      
            EdittedBy=@EdittedBy,    
			StoppageReasonTypeID=@StoppageReasonTypeID,
   IsSchLoss=@IsSchLoss  ,InfluxReasonId=@InfluxReasonId    
         WHERE       
         ReasonId=@ReasonId      
  END      
END      
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateStopageSubReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateStopageSubReason]  
    @SubReasonId int= null,    
 @ReasonId int=null,  
 @SubReasonName nvarchar(200)= null,  
 @SubReasonCode nvarchar(200)= null,  
 @IsActive bit,  
 @ReasonDescription nvarchar(500)=null,  
 @AddBy int=null,   
 @EdittedBy int  ,
   @InfluxSubReasonId int
AS  
BEGIN  
  
IF NOT EXISTS(SELECT  * FROM  StopageSubReason WHERE SubReasonId=@SubReasonId)  
  BEGIN  
       INSERT INTO StopageSubReason  
       (    
        ReasonId,  
     SubReasonName,  
     SubReasonCode,  
     IsActive,   
     AddBy,   
     EdittedBy ,InfluxSubReasonId 
       )  
     VALUES(   
     @ReasonId,  
     @SubReasonName,  
     @SubReasonCode,  
     @IsActive,   
     @AddBy,   
     @EdittedBy ,@InfluxSubReasonId 
     )  
  END  
ELSE  
BEGIN  
  UPDATE StopageSubReason  
       SET        
       ReasonId=@ReasonId,  
       SubReasonName=@SubReasonName,  
       SubReasonCode=@SubReasonCode,  
	   ReasonDescription=@ReasonDescription,
       IsActive=@IsActive,   
       AddBy=@AddBy,  
       EdittedBy=@EdittedBy ,
	   InfluxSubReasonId=@InfluxSubReasonId 
  
       WHERE SubReasonId=@SubReasonId  
  
END  


    
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateUser]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CreateUpdateUser]  
 @UserId int=NULL,  
 @UserName VARCHAR(200)=NULL,  
 @OrganizationId int= NULL,  
 @UserTypeId int =NULL,  
 @PhoneNo nvarchar(20)= NULL,  
 @EmailId nvarchar(200) =NULL,  
 @Password nvarchar(50)= NULL,  
 @IsActive bit =NULL,  
 @AddBy int= NULL,  
 @EdittedBy int =NULL  ,
 @ChangeCode VARCHAR(200)=NULL
AS  
BEGIN  
  
  
IF   NOT Exists (SELECT  * FROM  OrganizationUser WHERE UserId=@UserId)  
  BEGIN  
    INSERT INTO OrganizationUser(  
       
         OrganizationId ,
		 UserName,  
         UserTypeId ,             
         PhoneNo ,  
         EmailId ,  
         [Password],  
         IsActive ,  
         AddBy,  
         EdittedBy  
    )  
    VALUES  
    (  
       
         @OrganizationId ,  
         @UserName,  
         @UserTypeId ,  
         @PhoneNo ,  
         @EmailId ,  
         @Password,  
         @IsActive ,  
         @AddBy,  
         @EdittedBy  
    )  
  END  
ELSE  
  BEGIN  
  UPDATE OrganizationUser  
  SET   
    
         OrganizationId=@OrganizationId ,  
         UserTypeId=@UserTypeId ,  
         PhoneNo=@PhoneNo ,  
         EmailId=@EmailId ,  
         [Password]=@Password,  
         IsActive=@IsActive ,           
         EdittedBy=@EdittedBy,  
         UserName=@UserName,
		 ChangeCode=@ChangeCode
         WHERE   
         UserId=@UserId  
  END  
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateUserType]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_CreateUpdateUserType]
    @UserTypeID int = null,
	@UserTypeName nvarchar(100)=null,
	@PermissionModule nvarchar(1000)=null
AS
BEGIN
IF NOT Exists (SELECT UserTypeID FROM [UserType] WHERE UserTypeID=@UserTypeID)    
		BEGIN
				INSERT INTO [dbo].[UserType](					
												UserTypeName,
												PermissionModule
				)
				VALUES
				(								
												@UserTypeName,
												@PermissionModule
				)
		END
ELSE
		BEGIN
		UPDATE [UserType]
		SET 																					
												UserTypeName=@UserTypeName,
												PermissionModule=@PermissionModule
									WHERE 
									UserTypeID=@UserTypeID
		END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteBreakDown]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_DeleteBreakDown]
@BreakDownID INT
AS
BEGIN
DELETE FROM [dbo].[BreakDownEntry]  WHERE BreakDownID=@BreakDownID
END 





GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteProduct_Die_Rel]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[USP_DeleteProduct_Die_Rel]   	
	@OrganizationId int,
	@ProductID int 	
	AS
	BEGIN
	BEGIN TRAN TRAN1

	DELETE FROM  Product_Die_Rel WHERE  OrganizationId=@OrganizationId AND ProductID=@ProductID

	COMMIT TRAN TRAN1
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteProduct_Mould_Rel]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_DeleteProduct_Mould_Rel]   	
	@OrganizationId int,
	@ProductID int 	
	AS
	BEGIN
	BEGIN TRAN TRAN1

	DELETE FROM  Product_Mould_Rel WHERE  OrganizationId=@OrganizationId AND ProductID=@ProductID

	COMMIT TRAN TRAN1
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteProduction]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_DeleteProduction]
@ProductionId INT
AS
BEGIN
DELETE FROM  [dbo].[ProductionEntry] WHERE ProductionId=@ProductionId
END 





GO
/****** Object:  StoredProcedure [dbo].[USP_GET_CONFIGURATIONS]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GET_CONFIGURATIONS]
AS
SELECT CFG.ConfigID,CFG.Production,CFG.Client,CFG.Line,CFG.Machine,CFG.Location
		,CFG.Breaks,CFG.Quality,CFG.CycleTime,CFG.BreakdownInterval FROM ConfigTbl AS CFG

GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakDownEntry]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBreakDownEntry] -- NULL,NULL,NULL,NULL,NULL,'2019-02-10'  
@ProductId INT=NULL,  
@MachineID INT=NULL,  
@OperatorID INT=NULL,  
@SupervisorID INT=NULL,  
@ShiftId INT=NULL,  
@ProductionDate datetime = NULL  
AS  
BEGIN  
  
DECLARE @SQL NVARCHAR(2000)='SELECT *,dbo.GetOrganizationIdByMachineId(MachineId) OrganizationId FROM BreakDownEntry'  
DECLARE @ENTRY INT=0  
  
   IF @ProductId IS NOT NULL  
   BEGIN  
    SET @SQL=' WHERE ProductId='+@ProductId  
    SET @ENTRY=1  
   END  
  
  
   IF @MachineID IS NOT NULL  
   BEGIN  
   IF  @ENTRY=1  
   BEGIN  
    SET @SQL=@SQL + ' AND MachineID='+@MachineID  
   END  
   ELSE  
   BEGIN  
    SET @SQL=@SQL + ' WHERE MachineID='+@MachineID  
    SET @ENTRY=1  
   END  
   END  
  
  
  
   IF @OperatorID IS NOT NULL  
   BEGIN  
    IF  @ENTRY=1  
    BEGIN  
     SET @SQL=@SQL + ' AND OperatorID='+@OperatorID  
    END  
   ELSE  
    BEGIN  
     SET @SQL=@SQL + ' WHERE OperatorID='+@OperatorID  
     SET @ENTRY=1  
    END  
   END  
  
     
   IF @SupervisorID IS NOT NULL  
   BEGIN  
    IF  @ENTRY=1  
    BEGIN  
     SET @SQL=@SQL + ' AND SupervisorID='+@SupervisorID  
    END  
   ELSE  
    BEGIN  
     SET @SQL=@SQL + ' WHERE SupervisorID='+@SupervisorID  
     SET @ENTRY=1  
    END  
   END  
  
     IF @ShiftId IS NOT NULL  
   BEGIN  
    IF  @ENTRY=1  
    BEGIN  
     SET @SQL=@SQL + ' AND ShiftId='+@ShiftId  
    END  
   ELSE  
    BEGIN  
     SET @SQL=@SQL + ' WHERE ShiftId='+@ShiftId  
     SET @ENTRY=1  
    END  
   END  
  
    IF @ProductionDate IS NOT NULL  
   BEGIN  
    IF  @ENTRY=1  
    BEGIN  
     SET @SQL=@SQL + ' AND ProductionDate BETWEEN '''+ CONVERT(VARCHAR(10),@ProductionDate, 101) + ''' and '''+ CONVERT(VARCHAR(10),DATEADD(DD,1,@ProductionDate), 101) +''''  
    END   
   ELSE  
    BEGIN  
     SET @SQL=@SQL + ' WHERE ProductionDate BETWEEN '''+ CONVERT(VARCHAR(10),@ProductionDate, 101) + ''' and '''+ CONVERT(VARCHAR(10),DATEADD(DD,1,@ProductionDate), 101) +''''  
     SET @ENTRY=1  
    END  
   END  
-- print @SQL;  
EXEC (@SQL)  
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakDownEntryByDate]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBreakDownEntryByDate] -- NULL,NULL,NULL,NULL,NULL,'2019-02-10'  
@ProductionDate datetime = NULL 

AS  
BEGIN  
  
SELECT B.*,dbo.GetOrganizationIdByMachineId(B.MachineID) OrganizationId FROM 

BreakDownEntry B WHERE 
B.ProductionDate <=Cast((Select Max(ProductionDate) from BreakDownEntry) as Date)
and B.ProductionDate >=DATEADD(DD,-2,Cast((Select Max(ProductionDate) from BreakDownEntry)
as Date)) 

END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakDownEntryByFilter]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBreakDownEntryByFilter]  --'4-05-2018','18-042019',27,10,'Inspection Delay'   
@StartDate datetime  ,
@EndDate datetime ,
@ShiftId int,
@MachineId int,
@Reason VARCHAR(500)
AS    
BEGIN    
DECLARE @ReasonId INT 
SELECT top 1 @ReasonId=ReasonId from  
[dbo].[StopageReason] where ReasonName=@Reason


SELECT B.*,dbo.GetOrganizationIdByMachineId(B.MachineID) OrganizationId FROM BreakDownEntry B 
WHERE B.ProductionDate between  CONVERT(VARCHAR(10),@StartDate, 101) AND
CONVERT(VARCHAR(10),DATEADD(DD,1,@EndDate), 101)   AND
B.MachineId=@MachineId  
 AND ReasonId=@ReasonId

 --AND B.ShiftId=@ShiftId

END 




GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakDownEntryById]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBreakDownEntryById] -- NULL,NULL,NULL,NULL,NULL,'2019-02-10'    
@BreakDownID INT  
AS    
BEGIN    
   SELECT *,dbo.GetOrganizationIdByMachineId(MachineId) OrganizationId FROM BreakDownEntry where BreakDownID=@BreakDownID
END 




GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakdownReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetBreakdownReason] 
AS  
BEGIN  
	SELECT * FROM [dbo].[BreakdownReason]
END  
  
  
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakInfo]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBreakInfo]
@BreakId INT=NULL
AS
BEGIN
IF @BreakId IS NULL
	BEGIN
	SELECT  * FROM [dbo].[BreakInfo]
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[BreakInfo] WHERE BreakId=@BreakId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetBreakInfoByShiftId]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetBreakInfoByShiftId]
@ShiftId INT
AS
BEGIN

	SELECT  * FROM [dbo].[BreakInfo] WHERE ShiftId=@ShiftId
	
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetDieById]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetDieById]  
@DieId INT,
@OrganizationId INT
AS  
BEGIN    
SELECT        DieId, DieDescription, AvailableCavityCount, AddBy, AddDate, EditBy, EditDate, IsActive, OrganizationId
FROM            MasterDie WHERE DieId=@DieId and OrganizationId=@OrganizationId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetDies]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetDies] 
@OrganizationId INT
AS  
BEGIN    
		SELECT        DieId, DieDescription, AvailableCavityCount, AddBy, AddDate, EditBy, EditDate, IsActive, OrganizationId
FROM            MasterDie WHERE  OrganizationId=@OrganizationId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetLossReport]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
  
CREATE PROCEDURE [dbo].[USP_GetLossReport]   
 @startdate datetime,  
 @enddate datetime,  
 @organizationId int,  
 @machineId int  
AS  
BEGIN  
 -- exec [dbo].[USP_GetLossReport] '2022-04-11','2022-04-11',5,0  
 declare @machineIds varchar(500)=''  
if(@machineID=0)   
begin  
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId  
 end   
else   
begin  
	select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID  
 end   

 ;with cte as (   
 select CAST(ProductionDate AS DATE) as ProductionDate,Remarks, b.MachineID,M.MachineName,b.ReasonId,sr.ReasonName as Reason,b.SubReasonId
 ,ssr.SubReasonName as SubReason,TotalStoppagetime  
  
 from BreakDownEntry b  JOIN MasterMachine AS M ON b.MachineID=M.MachineID LEFT JOIN stopageReason as sr on b.ReasonId=sr.ReasonId
 left join stopagesubReason as ssr on ssr.SubReasonId=b.SubReasonId 
 where m.OrganizationId=@organizationId and Convert(date,b.ProductionDate) between convert(date,@startdate) and Convert(date,@enddate) and 
 TotalStoppagetime>599 and b.MachineId in (select value from fn_split(@machineIds,','))   
 ),  
 TotalCount AS  
 (  
  select ReasonId,sum(c.TotalStoppagetime) as Duration,count(c.ReasonId) as Freequency,Reason from cte as c group by ReasonId,Reason  
 )  

  select convert(varchar,getdate(),23) ProductionDate,'' Remarks,0 MachineID, '' MachineName,t.ReasonId,  
   case when Reason is null then 'No Data Entry' else Reason end as Reason,0 SubReasonId,'' SubReason,  
  0.0 TotalStoppagetime,    
   min(convert(numeric(10,2),Duration/60)) Duration,min(Freequency) Freequency --from cte c inner join  
   from TotalCount t --on c.ReasonId=t.ReasonId  
  group by t.ReasonId,Reason  
end  
  
  
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLossReportDetails]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

CREATE PROCEDURE [dbo].[USP_GetLossReportDetails]	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineId int,
	@reason varchar(50)
AS
BEGIN
	-- exec [dbo].[USP_GetLossReportDetails] '2021-02-01','2021-02-28',6,0,'No Man Power'
	declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
 declare @reasonId int=0
 select @reasonId=ReasonId from StopageReason where ReasonName=@reason and organizationId=@organizationId
--  declare @shiftIdIsNight int=0,@isManualEntry int=0;
--select @shiftIdIsNight=ShiftId from [Shift] where IsOverNight=1
-- select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId

	;with cte as (	
	select CAST(ProductionDate AS DATE)  as ProductionDate,Remarks,
	MachineID
	,(select MAchineName from MasterMachine where MachineID=b.MachineID) MachineName
	,ReasonId
	,(Select ReasonName from stopageReason where ReasonId=b.ReasonId) Reason
	,SubReasonId 
	,(Select SubReasonName from stopagesubReason where SubReasonId=b.SubReasonId) SubReason
	,TotalStoppagetime

	from BreakDownEntry b
	where dbo.GetOrganizationIdByMachineId(b.MachineId) =@organizationId and 
	cast(@startdate as date)<= CAST(ProductionDate AS DATE)  and
	cast(@enddate as date)>= CAST(ProductionDate AS DATE) and
	ReasonId=@reasonId and --TotalStoppagetime>300 and
	--MachineId=Coalesce(16,MachineId)
	TotalStoppagetime>599 and
	MachineId in (select value from fn_split(@machineIds,',')) 
	)
	--select * from cte
	 select convert(varchar,ProductionDate,23) ProductionDate,Remarks,MachineID, MachineName,c.ReasonId,
	 case when Reason is null then 'No Data Entry' else Reason end as Reason,SubReasonId,SubReason,
	 convert(numeric(10,2),TotalStoppagetime/60) TotalStoppagetime,0 Duration,0 Freequency from cte c
	 --inner join TotalCount t on c.ReasonId=t.ReasonId
	 -- select convert(varchar,getdate(),23) ProductionDate,'' Remarks,0 MachineID, '' MachineName,0 SubReasonId,'' SubReason,
	 --0.0 TotalStoppagetime,
	 --c.ReasonId,
	 -- case when Reason is null then 'No Data Entry' else Reason end as Reason,
	 -- min(convert(numeric(10,2),Duration/60)) Duration,min(Freequency) Freequency from cte c
	 --inner join TotalCount t on c.ReasonId=t.ReasonId
	 --group by c.ReasonId,t.ReasonId,Reason
	
end




GO
/****** Object:  StoredProcedure [dbo].[USP_GetLossReportRpt]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

CREATE PROCEDURE [dbo].[USP_GetLossReportRpt]	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineId int
AS
BEGIN
	-- exec [dbo].[USP_GetLossReportRpt] '2021-03-01','2021-03-31',5,0
	declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
-- declare @shiftIdIsNight int=0,@isManualEntry int=0;
--select @shiftIdIsNight=ShiftId from [Shift] where IsOverNight=1
-- select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId

	;with cte as (	
	select CAST(ProductionDate AS DATE) as ProductionDate,Remarks,
	MachineID
	,(select MAchineName from MasterMachine where MachineID=b.MachineID) MachineName
	,ReasonId
	,(Select ReasonName from stopageReason where ReasonId=b.ReasonId) Reason
	,SubReasonId 
	,(Select SubReasonName from stopagesubReason where SubReasonId=b.SubReasonId) SubReason
	,TotalStoppagetime

	from BreakDownEntry b
	where dbo.GetOrganizationIdByMachineId(b.MachineId) =@organizationId and 
	cast(@startdate as date)<= CAST(ProductionDate AS DATE)  and
	cast(@enddate as date)>= CAST(ProductionDate AS DATE)  and
	TotalStoppagetime>599 and
	MachineId in (select value from fn_split(@machineIds,',')) 
	),
	TotalMAchinWise AS
	(
	 select ReasonId,sum(c.TotalStoppagetime) as Duration,count(c.ReasonId) as Freequency,Reason,MachineID, MachineName	from cte as c 
	 group by ReasonId,Reason,MachineID, MachineName
	),
	TotalCount AS
	(
	 select ReasonId,sum(c.TotalStoppagetime) as Duration,count(c.ReasonId) as Freequency,Reason	from cte as c group by ReasonId,Reason
	)
	--select * from cte
	/* select convert(varchar,ProductionDate,23) ProductionDate,Remarks,MachineID, MachineName,c.ReasonId,
	 case when c.Reason is null then 'No Data Entry' else c.Reason end as Reason,SubReasonId,SubReason,
	 convert(numeric(10,2),TotalStoppagetime/60) TotalStoppagetime,0 Duration,0 Freequency from cte c
	 --inner join TotalCount t on c.ReasonId=t.ReasonId
	 union
	  select convert(varchar,getdate(),23) ProductionDate,'' Remarks,0 MachineID, '' MachineName,t.ReasonId,
	  case when Reason is null then 'No Data Entry' else Reason end as Reason,0 SubReasonId,'' SubReason,
	 0.0 TotalStoppagetime,
	 
	  min(convert(numeric(10,2),Duration/60)) Duration,min(Freequency) Freequency --from cte c inner join
	  from TotalCount t --on c.ReasonId=t.ReasonId
	 group by t.ReasonId,t.ReasonId,Reason
	 GROUPING SETS (
		(ShiftId,shiftname,MachineName,tShiftName,ProductionDate),
        (ShiftId,shiftname,MachineName,tShiftName),
        (ShiftId,shiftname,tShiftName),
        (ProductionDate,MachineName),
        (MachineName),	
		())
	*/
	
	  select convert(varchar,getdate(),23) ProductionDate,'' Remarks,t.ReasonId,
	  case when Reason is null then 'No Data Entered' else Reason end as Reason,0 SubReasonId,'' SubReason,
	 0.0 TotalStoppagetime,	 
	  (convert(numeric(10,2),Duration/60)) Duration,(Freequency) Freequency --from cte c inner join
	  , '' MachineName,0 MachineID
	  from TotalCount t --on c.ReasonId=t.ReasonId
	 --group by t.ReasonId,Reason
	 union
	 select convert(varchar,getdate(),23) ProductionDate,'' Remarks,ReasonId,
	  case when Min(Reason) is null then 'No Data Entered' else min(Reason) end as Reason,0 SubReasonId,'' SubReason,
	 0.0 TotalStoppagetime,	 
	  min(convert(numeric(10,2),Duration/60)) Duration,min(Freequency) Freequency --from cte c inner join
	  ,case when GROUPING(MachineID) = 1 THEN '''' else max(MachineName) end as MachineName	
	  ,case when GROUPING(MachineID) = 1 THEN 0 else max(MachineID) end as MachineID	
	  from TotalMAchinWise  --on c.ReasonId=t.ReasonId
	 --group by t.ReasonId,t.ReasonId,Reason
	  GROUP BY GROUPING SETS (
	  (ReasonId,MachineID))
end




GO
/****** Object:  StoredProcedure [dbo].[USP_GetLossReportRptDetails]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

CREATE PROCEDURE [dbo].[USP_GetLossReportRptDetails]	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineId int
AS
BEGIN
	-- exec [dbo].[USP_GetLossReportRptDetails] '2021-03-01','2021-03-31',5,0
	declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
-- declare @shiftIdIsNight int=0,@isManualEntry int=0;
--select @shiftIdIsNight=ShiftId from [Shift] where IsOverNight=1
-- select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId

	;with cte as (	
	select 	BreakDownID,convert(varchar(11),ProductionDate,106) as ProductionDate,Remarks	
	,(select MAchineName from MasterMachine where MachineID=b.MachineID) MachineName
	,case when b.ReasonId =0 then 'No Data Entry' else (Select ReasonName from stopageReason where ReasonId=b.ReasonId) end Reason
	,(Select SubReasonName from stopagesubReason where SubReasonId=b.SubReasonId) SubReason
	,TotalStoppagetime,convert(char(5), StartTime, 108) StartTime
	,convert(char(5), EndTime, 108) EndTime
	,RIGHT('0' + CAST(cast(TotalStoppagetime as int) / 3600 AS VARCHAR),2) + ':' +
RIGHT('0' + CAST((cast(TotalStoppagetime as int) / 60) % 60 AS VARCHAR),2) + ':' +
RIGHT('0' + CAST(cast(TotalStoppagetime as int) % 60 AS VARCHAR),2) as Duration

	from BreakDownEntry b
	where dbo.GetOrganizationIdByMachineId(b.MachineId) =@organizationId and 
	cast(@startdate as date)<= CAST(ProductionDate AS DATE)  and
	cast(@enddate as date)>= CAST(ProductionDate AS DATE)  and
	TotalStoppagetime>599 and
	MachineId in (select value from fn_split(@machineIds,',')) and IsIdle!=1
	and cast(TotalStoppagetime as int)>599

	)
	select * from cte 	order by BreakDownID
	
end




GO
/****** Object:  StoredProcedure [dbo].[USP_GetLossReportSend]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

CREATE PROCEDURE [dbo].[USP_GetLossReportSend]	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineId int
AS
BEGIN
	-- exec [dbo].[USP_GetLossReportSend] '2021-01-01','2021-01-31',5,0
--	set @startdate='2021-01-06'
--set @enddate='2021-01-12'
	declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
  declare @shiftIdIsNight int=0,@isManualEntry int=0;
select @shiftIdIsNight=ShiftId from [Shift] where IsOverNight=1
 select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId

	;with cte as (	
	select case when ShiftId=@shiftIdIsNight and @isManualEntry=0 then dateadd(day,-1,CAST(ProductionDate AS DATE)) else CAST(ProductionDate AS DATE) end as ProductionDate,Remarks,
	MachineID
	,(select MAchineName from MasterMachine where MachineID=b.MachineID) MachineName
	,ReasonId
	,(Select ReasonName from stopageReason where ReasonId=b.ReasonId) Reason
	,SubReasonId 
	,(Select SubReasonName from stopagesubReason where SubReasonId=b.SubReasonId) SubReason
	,TotalStoppagetime

	from BreakDownEntry b
	where dbo.GetOrganizationIdByMachineId(b.MachineId) =@organizationId and 
	cast(@startdate as date)<=case when ShiftId=@shiftIdIsNight and @isManualEntry=0 then dateadd(day,-1,CAST(ProductionDate AS DATE)) else CAST(ProductionDate AS DATE) end and
	cast(@enddate as date)>=case when ShiftId=@shiftIdIsNight and @isManualEntry=0 then dateadd(day,-1,CAST(ProductionDate AS DATE)) else CAST(ProductionDate AS DATE) end and
	--ProductionDate BETWEEN  @startdate+' 00:00:00' AND @enddate+' 23:59:59' and 
	--MachineId=Coalesce(16,MachineId)
	MachineId in (select value from fn_split(@machineIds,',')) 
	),
	TotalCount AS
	(
	 select ReasonId,Reason,sum(c.TotalStoppagetime) as Duration,count(c.ReasonId) as Freequency	from cte as c group by ReasonId,Reason
	)
	
	  select ReasonId,
	  case when Reason is null then 'No Data Entry' else Reason end as Reason,
	  convert(numeric(10,2),Duration) Duration,Freequency Freequency from TotalCount t 
	 --group by t.ReasonId,Reason
	 order by ReasonId
	
end




GO
/****** Object:  StoredProcedure [dbo].[USP_GetMachine]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetMachine]
@MachineID INT=NULL
AS
BEGIN
IF @MachineID IS NULL 
	BEGIN
	SELECT  * FROM [dbo].[MasterMachine]
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[MasterMachine] WHERE MachineID=@MachineID
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetMachine_ByOrganization]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetMachine_ByOrganization]
@OrganizationId INT
AS
BEGIN
	SELECT  * FROM [dbo].[MasterMachine] where OrganizationId=@OrganizationId
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMachineCategory]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetMachineCategory]
@CategoryId INT=NULL
AS
BEGIN
IF @CategoryId IS NULL 
	BEGIN
	SELECT  * FROM [dbo].[MachineCategory]
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[MachineCategory] WHERE CategoryId=@CategoryId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetMachineMapping]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetMachineMapping]
@OrganizationId INT,
@ProductionDate datetime=null
AS
BEGIN
	-- exec USP_GetMachineMapping 5

	
	SELECT distinct(r.MachineId),m.MachineName,m.MachineCode,r.MachineDemandType,r.ProductDemandType,r.ProductId,
	p.ProductCode,p.ProductName,r.CycleTime ProductCycletime,m.Location,CAST(1 AS BIT) IsMapped ,
	u.UnitPrefix ProductUnit
	FROM product_machine_rel r
	inner join [dbo].[MasterMachine] m on r.MachineID=m.machineId and m.OrganizationId=@OrganizationId	
	inner join MasterProduct p on p.ProductId=r.ProductId
	inner join unitmaster u on u.ID=p.UnitId
	
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetMachineProductMap]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
CREATE  PROCEDURE [dbo].[USP_GetMachineProductMap]-- USP_GetMachineProductMap 5        
@OrganizationId INT        
AS        
BEGIN     
SELECT Distinct * FROM (  
SELECT               
MM.MachineID, MM.MachineName, ISNULL(UMP.ProductId, 0) AS ProductID,         
ISNULL(dbo.getProduct_Name(MM.OrganizationId, UMP.ProductId), '') AS ProductName,         
ISNULL(dbo.getProduct_CycleTime(MM.OrganizationId, UMP.ProductId), 0) AS ProductCycleTime,         
ISNULL(UMP.OperatorId,0) OperatorId, ISNULL(UMP.MouldId,0) MouldId,ISNULL( UMP.OperationId,0) OperationId,         
ISNULL(UMP.DieId,0) DieId, ISNULL(UMP.TargetProduction,0) TargetProduction, ISNULL(UMP.MouldCavity,0) MouldCavity,        
ISNULL(UMP.DieCavity,0) DieCavity         
FROM            MasterMachine AS MM LEFT OUTER JOIN        
                         UtilityMasterMapping AS UMP ON MM.MachineID = UMP.MachineId AND mm.OrganizationId=@OrganizationId      )  
  
       FINAL_RESULT ORDER BY MachineID ASC  
          
END 
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMouldById]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetMouldById]  
@MouldId INT,
@OrganizationId INT
AS  
BEGIN    
SELECT        MouldId, MouldDescription, AvailableCavityCount, AddBy, AddDate, EditBy, EditDate, IsActive, OrganizationId
FROM            MasterMould WHERE MouldId=@MouldId and OrganizationId=@OrganizationId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetMoulds]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetMoulds] 
@OrganizationId INT
AS  
BEGIN    
		SELECT        MouldId, MouldDescription, AvailableCavityCount, AddBy, AddDate, EditBy, EditDate, IsActive, OrganizationId
FROM            MasterMould WHERE  OrganizationId=@OrganizationId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetOEEDetailsRpt]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		shailendra
-- Create date: 05-12-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetOEEDetailsRpt]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineID int

	--- Exec [USP_GetOEEDetailsRpt] '2021-03-01','2021-03-31',5,0
AS
BEGIN	

declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 

 DECLARE  @columns NVARCHAR(MAX) = '', @sql     NVARCHAR(MAX) = '';
SELECT @columns += QUOTENAME(ShiftName) + ',' FROM [Shift] where organizationid=@organizationId ORDER BY ShiftName;
SET @columns = @columns+'[OverAll]';
PRINT @columns;



declare @caseqry varchar(max)='cast(convert(numeric(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))) as varchar) +'',''+
		cast(convert(numeric(10,2),((sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))))as varchar) +'',''+
		cast(convert(numeric(10,2),(((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end)))) as varchar) Tooltip,
		CONVERT(DECIMAL(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))*
		(sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))*
		((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end))) OEE
		'
SET @sql ='
 ;with cte as (
		SELECT 
		d.MachineID ,d.ProductionDateOnly as ProductionDate,min(t2.DurationMin) as WorkingShiftTime,d.ShiftId,t2.ShiftName,m.MachineName,''t''+t2.shiftName as tShiftName,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDateOnly,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDateOnly,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDateOnly)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDateOnly)),0) as QualityLoss,
			'' ''DemandType,'''' CustomerName		
			FROM [dbo].[ProductionEntry] d
		inner join Shift t2	on d.ShiftId = t2.ShiftId
		inner join MasterMachine m on d.MachineID=m.MachineID
		where ProductionDateOnly BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+'''
		and d.MachineID in (select value from fn_split('''+@machineIds+''','',''))
		
		group by d.ProductionDateOnly,d.ShiftId,d.MAchineID,ShiftName,machinename
		)

		select ProductionDate,MachineName,DataLevel, '+dbo.[GetDynamicColumnString](@columns,'','sum',@organizationId)+',
		'+dbo.[GetDynamicColumnString](@columns,'t','min',@organizationId)+' from
		(
		select case when GROUPING(ProductionDate) = 1 THEN ''1900-01-01'' else max(ProductionDate) end as ProductionDate,		
		case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,
		case when GROUPING(MachineName) = 1 THEN '''' else max(MachineName) end as MachineName,		
		case when GROUPING(tShiftName) = 1 THEN ''tOverAll'' else max(tShiftName) end as tShiftName,
		case when GROUPING(MachineName) = 1 THEN ''TotalLevel'' when GROUPING(ProductionDate) = 1 THEN ''ShiftLevel'' else ''DateLevel'' end as DataLevel,
		'+@caseqry+'
		from cte where ProductionDate BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+''' GROUP BY
		GROUPING SETS (
		(ShiftId,shiftname,MachineName,tShiftName,ProductionDate),
        (ShiftId,shiftname,MachineName,tShiftName),
        (ShiftId,shiftname,tShiftName),
        (ProductionDate,MachineName),
        (MachineName),	
		())
		
		) t
		PIVOT(
		sum (OEE )
    FOR ShiftName IN ('+ @columns +')
) AS pivot_table
pivot
( min(Tooltip)
for tShiftName in ( '+ dbo.[GetDynamicColumnString](@columns,'t','',@organizationId) +')
) as p2 group by  ProductionDate,MachineName,DataLevel
		--Order by ProductionDate,MachineName'
		--select @sql
		EXECUTE sp_executesql @sql;

END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetOEEDetailsRpt_Optimized]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOEEDetailsRpt_Optimized]            
             
 @startdate datetime,            
 @enddate datetime,            
 @organizationId int,            
 @machineID int            
            
 --- Exec [USP_GetOEEDetailsRpt_Optimized] '2022-06-01','2022-06-30',5,1            
AS            
BEGIN             
            
declare @machineIds varchar(500)=''            
if(@machineID=0)             
begin            
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId            
 end             
else             
begin            
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID            
 end             
            
 DECLARE  @columns NVARCHAR(MAX) = '', @sql     NVARCHAR(MAX) = '';            
SELECT @columns += QUOTENAME(ShiftName) + ',' FROM [Shift] where organizationid=@organizationId AND IsActive=1 ORDER BY ShiftName;            
SET @columns = @columns+'[OverAll]';            
--PRINT @columns;            
            
            
            
declare @caseqry varchar(max)='cast(convert(numeric(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))) as varchar) +'',''+            
  cast(convert(numeric(10,2),((sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(UtilisationLoss)- sum(ShutdownTime) - sum(Breaktime)))))as varchar) +'',''+            
  cast(convert(numeric(10,2),(((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end)))) as varchar) Tooltip,            
  CONVERT(DECIMAL(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))*            
  (sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))*            
  ((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end))) OEE            
  '           
  --print @caseqry;      
SET @sql ='            
 ;with cte as (            
   SELECT                 
  d.MachineID ,d.ProductionDateOnly as ProductionDate,min(t2.DurationMin) as WorkingShiftTime,d.ShiftId,t2.ShiftName,m.MachineName,''t''+t2.shiftName as tShiftName,                
   ISNULL(CAL.ShutdownTime,0) AS ShutdownTime,ISNULL(CAL.BreakTime,0) AS BreakTime,ISNULL(CAL.UtilisationLoss,0) AS UtilisationLoss,        
   ISNULL(CAL.TotalProductointime,0) AS TotalProductointime,ISNULL(CAL.QualityLoss,0) AS QualityLoss,               
   '' ''DemandType,'''' CustomerName                  
   FROM [dbo].[ProductionEntry] d  LEFT JOIN OEECalculation AS CAL ON d.ShiftId=CAL.ShiftId AND d.MachineID=CAL.MachineId AND Convert(date,d.ProductionDate)=Convert(date,CAL.ProductionDate)              
  inner join Shift t2 on d.ShiftId = t2.ShiftId                
  inner join MasterMachine m on d.MachineID=m.MachineID                
  where ProductionDateOnly BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+'''                
  and d.MachineID in (select value from fn_split('''+@machineIds+''','',''))                
                  
  group by d.ProductionDateOnly,d.ShiftId,d.MAchineID,ShiftName,machinename,CAL.ShutdownTime,CAL.BreakTime,CAL.UtilisationLoss,CAL.TotalProductointime,CAL.QualityLoss              
  )            
            
  select ProductionDate,MachineName,DataLevel, '+dbo.[GetDynamicColumnString](@columns,'','sum',@organizationId)+',            
  '+dbo.[GetDynamicColumnString](@columns,'t','min',@organizationId)+' from            
  (            
  select case when GROUPING(ProductionDate) = 1 THEN ''1900-01-01'' else max(ProductionDate) end as ProductionDate,              
  case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,            
  case when GROUPING(MachineName) = 1 THEN '''' else max(MachineName) end as MachineName,              
  case when GROUPING(tShiftName) = 1 THEN ''tOverAll'' else max(tShiftName) end as tShiftName,            
  case when GROUPING(MachineName) = 1 THEN ''TotalLevel'' when GROUPING(ProductionDate) = 1 THEN ''ShiftLevel'' else ''DateLevel'' end as DataLevel,            
  '+@caseqry+'           
  from cte where ProductionDate BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+''' GROUP BY            
  GROUPING SETS (            
  (ShiftId,shiftname,MachineName,tShiftName,ProductionDate),            
        (ShiftId,shiftname,MachineName,tShiftName),            
        (ShiftId,shiftname,tShiftName),            
        (ProductionDate,MachineName),            
        (MachineName),             
  ())            
              
  ) t            
  PIVOT(            
  sum (OEE )            
    FOR ShiftName IN ('+ @columns +')            
) AS pivot_table            
pivot            
( min(Tooltip)            
for tShiftName in ( '+ dbo.[GetDynamicColumnString](@columns,'t','',@organizationId) +')            
) as p2 group by  ProductionDate,MachineName,DataLevel            
  --Order by ProductionDate,MachineName'            
 -- print @sql            
             
 EXECUTE sp_executesql @sql;   
 PRINT @sql
            
END 

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOperationById]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOperationById]  
@OperationId INT,
@OrganizationId INT
AS  
BEGIN    
SELECT        OperationId,  OrganizationId, OperationDescription, AddBy, EditBy, IsActive
FROM            MasterOperation WHERE OperationId=@OperationId and OrganizationId=@OrganizationId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetOperations]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOperations]   
@OrganizationId INT  
AS    
BEGIN      
  SELECT        OperationId,  OrganizationId, OperationDescription, AddBy, EditBy, IsActive  
  FROM            MasterOperation WHERE  OrganizationId=@OrganizationId  
END    
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOperatorById]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOperatorById]  
@OperatorId INT,
@OrganizationId INT
AS  
BEGIN    
SELECT        OperatorId, EmployeeId, OrganizationId, OperatorName, AddBy, EditBy, IsActive
FROM            MasterOperator WHERE OperatorId=@OperatorId and OrganizationId=@OrganizationId
END  


GO
/****** Object:  StoredProcedure [dbo].[USP_GetOperators]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOperators] 
@OrganizationId INT
AS  
BEGIN    
		SELECT        OperatorId, EmployeeId, OrganizationId, OperatorName, AddBy, EditBy, IsActive
		FROM            MasterOperator WHERE  OrganizationId=@OrganizationId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetOrganization]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOrganization]
@OrganizationId INT=NULL
AS
BEGIN
IF @OrganizationId IS NULL
	BEGIN
	SELECT  * FROM [dbo].[Organization]
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[Organization] WHERE OrganizationId=@OrganizationId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetOrganizationCategory]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_GetOrganizationCategory]
@OrganizationID INT
AS
BEGIN

	SELECT * FROM [Organization_ProductCategory] WHERE OrganizationID=@OrganizationID
	
END






GO
/****** Object:  StoredProcedure [dbo].[USP_GetOrganizationCustomer]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_GetOrganizationCustomer]
@OrganizationID INT
AS
BEGIN

	SELECT * FROM [Organization_Customer] WHERE OrganizationID=@OrganizationID
	
END






GO
/****** Object:  StoredProcedure [dbo].[USP_GetOrganizationUnits]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationUnits]
@OrganizationID INT
AS
BEGIN

	SELECT * FROM [dbo].[UnitMaster] WHERE OrganizationID=@OrganizationID
	
END




GO
/****** Object:  StoredProcedure [dbo].[USP_GetProduct]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetProduct]    
@ProductId INT=NULL  ,  
@OrganizationId INT=NULL,  
@IsActive BIT=NULL  
AS    
BEGIN    
SELECT  * FROM [dbo].[MasterProduct] WHERE ProductId=isnull(@ProductId,ProductId) AND    
OrganizationId=ISNULL(@OrganizationId,OrganizationId)  AND IsActive=ISNULL(@IsActive,IsActive)   
  
END    
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductDetailsByDates]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krishna
-- Create date: 29-04-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetProductDetailsByDates]
	
	@startdate datetime,
	@enddate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT ProductionId, Max(TotalProduction) AS TotalProduction, Max(ReWork) AS ReWork,  Max(Rejection) AS Rejection
	FROM [dbo].[ProductionEntry]
    WHERE ProductionDate BETWEEN  @startdate AND @enddate
	GROUP BY ProductionId

/*------- Unit Test Case ------

EXEC	@return_value = [dbo].[USP_GetProductDetailsByDates]
		@startdate = N'2019-02-10',
		@enddate = N'2019-03-10'
*/

END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetProDuctDieRels]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetProDuctDieRels]   
	@OrganizationId int
AS
	BEGIN
	SELECT        RecId, DieId, OrganizationId, ProductID, CreatedBy, CreatedOn
	FROM            Product_Die_Rel WHERE  OrganizationId=@OrganizationId
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductionEntry]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetProductionEntry] -- NULL,NULL,NULL,NULL,NULL,'2019-02-10'      
@ProductId INT=NULL,      
@MachineID INT=NULL,      
@OperatorID INT=NULL,      
@SupervisorID INT=NULL,      
@ShiftId INT=NULL,      
@ProductionDate datetime = NULL      
AS      
BEGIN      
      
DECLARE @SQL NVARCHAR(2000)='SELECT *,dbo.GetOrganizationIdByMachineId(MachineID) OrganizationId FROM ProductionEntry'      
DECLARE @ENTRY INT=0      
      
   IF @ProductId IS NOT NULL      
   BEGIN      
    SET @SQL=' WHERE ProductId='+ CAST( @ProductId  AS NVARCHAR)    
    SET @ENTRY=1      
   END      
      
      
   IF @MachineID IS NOT NULL      
   BEGIN      
   IF  @ENTRY=1      
   BEGIN      
    SET @SQL=@SQL + ' AND MachineID='+  CAST(@MachineID  AS NVARCHAR)         
   END      
   ELSE      
   BEGIN      
    SET @SQL=@SQL + ' WHERE MachineID='+CAST(@MachineID  AS NVARCHAR)          
    SET @ENTRY=1      
   END      
   END      
      
      
      
   IF @OperatorID IS NOT NULL      
   BEGIN      
    IF  @ENTRY=1      
    BEGIN      
     SET @SQL=@SQL + ' AND OperatorID='+       CAST(@OperatorID  AS NVARCHAR)    
    END      
   ELSE      
    BEGIN      
     SET @SQL=@SQL + ' WHERE OperatorID='+CAST(@OperatorID  AS NVARCHAR)
     SET @ENTRY=1      
    END      
   END      
      
         
   IF @SupervisorID IS NOT NULL      
   BEGIN      
    IF  @ENTRY=1      
    BEGIN      
     SET @SQL=@SQL + ' AND SupervisorID='+      CAST(@SupervisorID  AS NVARCHAR) 
    END      
   ELSE      
    BEGIN      
     SET @SQL=@SQL + ' WHERE SupervisorID='+CAST(@SupervisorID  AS NVARCHAR)
     SET @ENTRY=1      
    END      
   END      
      
     IF @ShiftId IS NOT NULL      
   BEGIN      
    IF  @ENTRY=1      
    BEGIN      
     SET @SQL=@SQL + ' AND ShiftId='+       CAST(@ShiftId  AS NVARCHAR)
    END      
   ELSE      
    BEGIN      
     SET @SQL=@SQL + ' WHERE ShiftId='+CAST(@ShiftId  AS NVARCHAR)    
     SET @ENTRY=1      
    END      
   END      
      
    IF @ProductionDate IS NOT NULL      
   BEGIN      
    IF  @ENTRY=1      
    BEGIN      
     SET @SQL=@SQL + ' AND ProductionDate BETWEEN '''+ CONVERT(VARCHAR(10),@ProductionDate, 101) + ''' and '''+ CONVERT(VARCHAR(10),DATEADD(DD,1,@ProductionDate), 101) +''''      
    END       
   ELSE      
    BEGIN      
     SET @SQL=@SQL + ' WHERE ProductionDate BETWEEN '''+ CONVERT(VARCHAR(10),@ProductionDate, 101) + ''' and '''+ CONVERT(VARCHAR(10),DATEADD(DD,1,@ProductionDate), 101) +''''      
     SET @ENTRY=1      
    END      
   END      
-- print @SQL;      
EXEC (@SQL)      
END




GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductionEntryById]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetProductionEntryById]  
@ProductionId INT
AS  
BEGIN    
SELECT *,dbo.GetOrganizationIdByMachineId(MachineId) OrganizationId FROM ProductionEntry WHERE ProductionId=@ProductionId
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductionPageData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetProductionPageData]  
@OrganizationId INT,   
@ProductionDate datetime=null ,
@ProductionToDate datetime=null 
AS  
BEGIN  
 -- exec USP_GetProductionPageData 6  
  
 SELECT  ShiftId,ShiftName,ShiftDescription,StartTime,EndTime,isOverNight FROM [dbo].[Shift] where OrganizationId=@OrganizationId  
   
 --SELECT  UserId,u.UserTypeId,UserName,UserTypeName FROM [dbo].[OrganizationUser] u inner join Usertype ut on u.usertypeid=ut.UserTypeID where OrganizationId=@OrganizationId  
 select Id as UserId,u.UserTypeId,UserName,UserTypeName  from [dbo].[AspNetUsers] u inner join Usertype ut on u.usertypeid=ut.UserTypeID  WHERE OrganizationId=@OrganizationId and IsActive=1  
 SELECT  ReasonId RejectionreasonID,ReasonDescription Rejectiondiscription FROM [dbo].[RejectionReason]  WHERE OrganizationId=@OrganizationId and IsActive=1  
   
 -- SELECT  ReasonId,ResonName FROM [dbo].[StopageReason] WHERE OrganizationId=@OrganizationId and IsActive=1  
   
 SELECT  SubReasonId,SubReasonName,stras.ReasonId,ReasonName FROM [dbo].[StopageSubReason] as substp right outer join [dbo].[StopageReason] as  
 stras on substp.ReasonId=stras.ReasonId where OrganizationId=@OrganizationId  
   
 SELECT distinct(r.MachineId),m.MachineName,m.MachineCode,r.MachineDemandType,r.ProductDemandType,r.ProductId,  
 p.ProductCode,p.ProductName,u.UnitPrefix ProductUnit  
 FROM product_machine_rel r  
 inner join [dbo].[MasterMachine] m on r.MachineID=m.machineId and m.OrganizationId=@OrganizationId   
 inner join MasterProduct p on p.ProductId=r.ProductId  
 inner join unitmaster u on u.ID=p.UnitId  
  
 SELECT  ReasonId,ReasonName,0 ReasonQuantity,0 TotalCount FROM [dbo].[RejectionReason]  WHERE OrganizationId=@OrganizationId and IsActive=1  
   
END  
  
  
  
  
  
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductionRejection]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetProductionRejection]
@ProductionEntryId INT
AS
BEGIN

	SELECT  * FROM [dbo].[ProductionRejection] WHERE ProductionEntryId=@ProductionEntryId
	
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductionReport]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[USP_GetProductionReport]

 @Organizationid int ,
 @FromDate DateTime,
 @Todate DateTime
 AS
 BEGIN

SELECT  TimeOfEntry,FORMAT (TimeOfEntry, 'dd/MM/yyyy ') DateOfEntry,OperatorID ,dbo.getOperator_Name(@Organizationid,OperatorID) OperatorName,MachineID, DBO.getMachine_Name(@Organizationid,MachineID) MachineName,
dbo.getProduct_Code(@Organizationid,ProductID) ProductCode,ProductID,dbo.getProduct_Name(@Organizationid,ProductID) ProductName,
dbo.getShift_Name(@Organizationid,ShiftID) ShiftName,ShiftID,
dbo.getOperation_Name(@Organizationid,OperationID) OperationName,OperationID, dbo.getDie_Name(@Organizationid,DieID) DieName,DieID, dbo.getMould_Name(@Organizationid,MouldID) MouldName,MouldID, TotalProduction,TargetProduction,MouldCavity,DieCavity
FROM            (SELECT        TimeOfEntry, MachineID, ProductID, ShiftId,OperatorID, OperationId, DieId, MouldId,TargetProduction,MouldCavity,DieCavity, SUM(TotalProduction) AS TotalProduction
                          FROM            (SELECT        ProductionId, MachineID, MachineCategoryId, ProductID, UserID, OperatorID,
						  SupervisorID, CAST(TimeOfEntry AS date) AS TimeOfEntry, 
						  ShiftId, StartTime, EndTime, TotalProduction, ReWork, Rejection, 
                           ProductionDate, IsSaved, RejectionList, IsDeleted,
						 ProductionDateOnly, MachineDemandType, PRoductDemandType, ConvertedProduction, ConvertedRejection, ConvertedRework, MouldId, OperationId, 
                             DieId,TargetProduction,MouldCavity,DieCavity
                                                    FROM            ProductionEntry) AS pd
                          GROUP BY TimeOfEntry, MachineID, ProductID, ShiftId, OperationId,OperatorID, DieId, MouldId,TargetProduction,MouldCavity,DieCavity) AS FinalTable 
						  where  TimeOfEntry between cast(@FromDate as date) and cast(@Todate  as date)
						  order by TimeOfEntry DESC 

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductMachinelevelDetailsByDates]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		shailendra
-- Create date: 05-12-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetProductMachinelevelDetailsByDates]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineID int

	--- Exec [USP_GetProductMachinelevelDetailsByDates] '2021-01-06','2021-01-06',5,0
AS
BEGIN	
declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
 

		SELECT 
		d.MachineID ,d.ProductionDate,min(t2.DurationMin) as WorkingShiftTime,
			d.ShiftId,coalesce(Min(mp.MachineDemandType),'None') DemandType,
			o.CustomerName, t1.productid,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as QualityLoss
		FROM 
		(select machineId, Productiondate,shiftId from [dbo].[ProductionEntry] where ProductionDateOnly BETWEEN  @startdate AND @enddate
union 
select machineId, ProductionDate,shiftid from [dbo].[BreakDownEntry] where ProductionDate BETWEEN  @startdate AND @enddate ) d
inner join Shift t2	on d.ShiftId = t2.ShiftId
inner join product_machine_rel mp on d.MachineID=mp.MachineID
outer apply(select top 1 * from [ProductionEntry] t where t.machineId=d.machineId and t.ProductionDate=d.ProductionDate and t.ShiftId=d.ShiftId) t1		
outer apply(select top 1 * from Organization_Customer where ID=(select CustomerID from masterProduct where ProductId=t1.ProductID)) o
		
		WHERE  d.MachineID in (select value from fn_split(@machineIds,','))
		--d.MachineID=coalesce(12,d.MachineID)
		group by d.ProductionDate,d.ShiftId,d.MAchineID
				,t1.ProductID,CustomerName
		

END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductMachinelevelDetailsByDatesDayMonthWeek]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetProductMachinelevelDetailsByDatesDayMonthWeek]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineID int

	--- Exec [USP_GetProductMachinelevelDetailsByDatesDayMonthWeek] '2020-01-01','2020-12-31',5,0
AS
begin
declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
declare @weekLast int,@dayWeekMonth varchar(20)
if(datediff(DD,@startdate,@enddate)>31)
begin 
set @dayWeekMonth='Month' 
end
else if(Month(@enddate)-month(@startdate)=0 and (datediff(DD,@startdate,@enddate))>6)
begin 
set @dayWeekMonth='Week' 
end
else begin 
set @dayWeekMonth='Day' 
end
 print @dayWeekMonth
 select @weekLast=DATEPART(WEEK, DATEADD(MM, DATEDIFF(MM,0,@startdate), 0))
--select @startdate=datefromparts(datepart(year,@enddate), 1, 1)
if(@dayWeekMonth='Week') begin
;with cte as (
			SELECT 
		d.MachineID ,d.ProductionDate,min(t2.DurationMin) as WorkingShiftTime,
			d.ShiftId,coalesce(Min(mp.MachineDemandType),'None') DemandType,
			--o.CustomerName, t1.productid,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as QualityLoss
		FROM 
		(select machineId, Productiondate,shiftId from [dbo].[ProductionEntry] where ProductionDateOnly BETWEEN  @startdate AND @enddate
union 
select machineId, ProductionDate,shiftid from [dbo].[BreakDownEntry] where ProductionDate BETWEEN  @startdate AND @enddate ) d
inner join Shift t2	on d.ShiftId = t2.ShiftId
inner join product_machine_rel mp on d.MachineID=mp.MachineID
--outer apply(select top 1 * from [ProductionEntry] t where t.machineId=d.machineId and t.ProductionDate=d.ProductionDate and t.ShiftId=d.ShiftId) t1		
--outer apply(select top 1 * from Organization_Customer where ID=(select CustomerID from masterProduct where ProductId=t1.ProductID)) o
		
		WHERE   d.MachineID in (select value from fn_split(@machineIds,','))--d.MachineID=coalesce(@machineID,d.MachineID)
		group by d.ProductionDate,d.ShiftId,d.MAchineID
				--,t1.ProductID,CustomerName
		)		
		select DATEPART(wk, min(ProductionDate)) -@weekLast+1 weekno,
		(sum(TotalProductointime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)))*
		((sum(TotalProductointime) - sum(QualityLoss)) / sum(TotalProductointime))  OEE,@dayWeekMonth DataLevel
		FROM cte GROUP BY   DATEPART(wk, ProductionDate)
		end
		else if(@dayWeekMonth='Month') begin
;with cte as (
		SELECT 
		d.MachineID ,d.ProductionDate,min(t2.DurationMin) as WorkingShiftTime,
			d.ShiftId,coalesce(Min(mp.MachineDemandType),'None') DemandType,
			--o.CustomerName, t1.productid,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as QualityLoss
		FROM 
		(select machineId, Productiondate,shiftId from [dbo].[ProductionEntry] where ProductionDateOnly BETWEEN  @startdate AND @enddate
union 
select machineId, ProductionDate,shiftid from [dbo].[BreakDownEntry] where ProductionDate BETWEEN  @startdate AND @enddate ) d
inner join Shift t2	on d.ShiftId = t2.ShiftId
inner join product_machine_rel mp on d.MachineID=mp.MachineID
--outer apply(select top 1 * from [ProductionEntry] t where t.machineId=d.machineId and t.ProductionDate=d.ProductionDate and t.ShiftId=d.ShiftId) t1		
--outer apply(select top 1 * from Organization_Customer where ID=(select CustomerID from masterProduct where ProductId=t1.ProductID)) o
		
		WHERE   d.MachineID in (select value from fn_split(@machineIds,','))--d.MachineID=coalesce(@machineID,d.MachineID)
		group by d.ProductionDate,d.ShiftId,d.MAchineID
				--,t1.ProductID,CustomerName
		)		
		select DATEPART(MM, min(ProductionDate)) weekno,
		(sum(TotalProductointime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)))*
		((sum(TotalProductointime) - sum(QualityLoss)) / sum(TotalProductointime))  OEE,@dayWeekMonth DataLevel
		FROM cte GROUP BY   DATEPART(MM, ProductionDate) 
		end
		else 
		begin
	;with cte as (
		SELECT 
		d.MachineID ,d.ProductionDate,min(t2.DurationMin) as WorkingShiftTime,
			d.ShiftId,coalesce(Min(mp.MachineDemandType),'None') DemandType,
			--o.CustomerName, t1.productid,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDate,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as QualityLoss
		FROM 
		(select machineId, Productiondate,shiftId from [dbo].[ProductionEntry] where ProductionDateOnly BETWEEN  @startdate AND @enddate
union 
select machineId, ProductionDate,shiftid from [dbo].[BreakDownEntry] where ProductionDate BETWEEN  @startdate AND @enddate ) d
inner join Shift t2	on d.ShiftId = t2.ShiftId
inner join product_machine_rel mp on d.MachineID=mp.MachineID
--outer apply(select top 1 * from [ProductionEntry] t where t.machineId=d.machineId and t.ProductionDate=d.ProductionDate and t.ShiftId=d.ShiftId) t1		
--outer apply(select top 1 * from Organization_Customer where ID=(select CustomerID from masterProduct where ProductId=t1.ProductID)) o
		
		WHERE   d.MachineID in (select value from fn_split(@machineIds,','))--d.MachineID=coalesce(@machineID,d.MachineID)
		group by d.ProductionDate,d.ShiftId,d.MAchineID
				--,t1.ProductID,CustomerName
		)		
		select convert(varchar, ProductionDate,23) weekno,
		(sum(TotalProductointime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)))*
		((sum(TotalProductointime) - sum(QualityLoss)) / sum(TotalProductointime))  OEE,@dayWeekMonth DataLevel
		FROM cte GROUP BY    ProductionDate
		end
		
		-- select cast((sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime) - sum(UtilisationLoss)) /(sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)) as decimal(18,2)) AR,
		--cast(sum(TotalProductointime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)) as decimal(18,2)) PR,
		--cast((sum(TotalProductointime) - sum(QualityLoss)) / sum(TotalProductointime) as decimal(18,2))  QR,
		--cast(((sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime) - sum(UtilisationLoss)) /(sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)))*
		--(sum(TotalProductointime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(BreakTime)))*
		--((sum(TotalProductointime) - sum(QualityLoss)) / sum(TotalProductointime))as decimal(18,2))  OEE,
		--'PlantLevel' DataLevel
		--from cte
		

END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductMachinelevelDetailsByDatesHome]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		shailendra
-- Create date: 05-12-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetProductMachinelevelDetailsByDatesHome]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineID int

	--- Exec [USP_GetProductMachinelevelDetailsByDatesHome] '2021-02-01','2021-02-12',7,0
AS
BEGIN	
declare @dayDiff int=datediff(day,@startdate,@enddate)
declare @startdateprev date
declare @enddateprev date
if(@dayDiff<1)
begin
SELECT @startdateprev= DATEADD(day, -1,  @startdate)
SELECT @enddateprev= DATEADD(day, -1,  @startdate)
end
else
if(@dayDiff<8)
begin
SELECT @startdateprev= DATEADD(wk,DATEDIFF(wk,7,@startdate),0)
SELECT @enddateprev=DATEADD(wk,DATEDIFF(wk,7,@startdate),6)
end
else if(@dayDiff>8)
begin
select @startdateprev=CONVERT(varchar,dateadd(d,-(day(dateadd(m,-1,@startdate-2))),dateadd(m,-1,@startdate-1)),106)
select @enddateprev=CONVERT(varchar,dateadd(d,-(day(@startdate)),@startdate),106)
end
declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
 --declare @isManualEntry int=0;
 --select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId
 DECLARE  @columns NVARCHAR(MAX) = '', @sql     NVARCHAR(MAX) = '';
SELECT @columns += QUOTENAME(ShiftName) + ',' FROM [Shift] where organizationid=@organizationId ORDER BY ShiftName;
SET @columns = @columns+'[OverAll]';
PRINT @columns;



declare @caseqry varchar(max)='cast(convert(numeric(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))) as varchar) +'',''+
		cast(convert(numeric(10,2),((sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))))as varchar) +'',''+
		cast(convert(numeric(10,2),(((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end)))) as varchar) Tooltip,
		CONVERT(DECIMAL(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))*
		(sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))*
		((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end))) OEE
		'
SET @sql ='
 ;with cte as (
		SELECT 
		d.MachineID ,d.ProductionDateOnly as ProductionDate,min(t2.DurationMin) as WorkingShiftTime,d.ShiftId,t2.ShiftName,m.MachineName,''t''+t2.shiftName as tShiftName,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDateOnly,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDateOnly,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDateOnly)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDateOnly)),0) as QualityLoss,
			'' ''DemandType,'''' CustomerName		
			FROM [dbo].[ProductionEntry] d
		inner join Shift t2	on d.ShiftId = t2.ShiftId
		inner join MasterMachine m on d.MachineID=m.MachineID
		where ProductionDateOnly BETWEEN  '''+cast(@startdateprev as varchar)+''' AND '''+cast(@enddate as varchar)+'''
		and d.MachineID in (select value from fn_split('''+@machineIds+''','',''))
		
		group by d.ProductionDateOnly,d.ShiftId,d.MAchineID,ShiftName,machinename
		)

		select ProductionDate,MachineName,DataLevel, '+dbo.[GetDynamicColumnString](@columns,'','sum',@organizationId)+',
		'+dbo.[GetDynamicColumnString](@columns,'t','min',@organizationId)+' from
		(
		select case when GROUPING(ProductionDate) = 1 THEN ''1900-01-01'' else max(ProductionDate) end as ProductionDate,		
		case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,
		case when GROUPING(MachineName) = 1 THEN '''' else max(MachineName) end as MachineName,		
		case when GROUPING(tShiftName) = 1 THEN ''tOverAll'' else max(tShiftName) end as tShiftName,
		case when GROUPING(MachineName) = 1 THEN ''TotalLevel'' when GROUPING(ProductionDate) = 1 THEN ''ShiftLevel'' else ''DateLevel'' end as DataLevel,
		'+@caseqry+'
		from cte where ProductionDate BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+''' GROUP BY
		GROUPING SETS (
		(ShiftId,shiftname,MachineName,tShiftName,ProductionDate),
        (ShiftId,shiftname,MachineName,tShiftName),
        (ShiftId,shiftname,tShiftName),
        (ProductionDate,MachineName),
        (MachineName),	
		())
		union 
		select ''1900-01-01'',
		case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,
		'''',
		case when GROUPING(tShiftName) = 1 THEN ''tOverAll'' else max(tShiftName) end as tShiftName,
		''TotalLevelPrev''  DataLevel,
		'+@caseqry+'
		from cte where ProductionDate BETWEEN  '''+cast(@startdateprev as varchar)+''' AND '''+cast(@enddateprev as varchar)+'''
		group by GROUPING SETS (		
        (ShiftId,shiftname,tShiftName),
		())
		) t
		PIVOT(
		sum (OEE )
    FOR ShiftName IN ('+ @columns +')
) AS pivot_table
pivot
( min(Tooltip)
for tShiftName in ( '+ dbo.[GetDynamicColumnString](@columns,'t','',@organizationId) +')
) as p2 group by  ProductionDate,MachineName,DataLevel
		--Order by ProductionDate,MachineName'
		--select @sql
		EXECUTE sp_executesql @sql;

END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductMachinelevelDetailsByDatesHome_Optimized]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[USP_GetProductMachinelevelDetailsByDatesHome_Optimized]            
             
 @startdate datetime,            
 @enddate datetime,            
 @organizationId int,            
 @machineID int            
            
 --- Exec [USP_GetProductMachinelevelDetailsByDatesHome_Optimized] '2022-06-07','2022-06-13',5,0          
AS            
BEGIN             
declare @dayDiff int=datediff(day,@startdate,@enddate)        
print @dayDiff;      
declare @startdateprev date            
declare @enddateprev date            
if(@dayDiff<1)            
begin            
SELECT @startdateprev= DATEADD(day, -1,  @startdate)            
SELECT @enddateprev= DATEADD(day, -1,  @startdate)         
end            
else            
if(@dayDiff<8)            
begin            
SELECT @startdateprev= DATEADD(wk,DATEDIFF(wk,7,@startdate),0)            
SELECT @enddateprev=DATEADD(wk,DATEDIFF(wk,7,@startdate),6)       
print @startdateprev;      
print @enddateprev;      
end            
else if(@dayDiff>8)            
begin            
select @startdateprev=CONVERT(varchar,dateadd(d,-(day(dateadd(m,-1,@startdate-2))),dateadd(m,-1,@startdate-1)),106)            
select @enddateprev=CONVERT(varchar,dateadd(d,-(day(@startdate)),@startdate),106)            
end            
declare @machineIds varchar(500)=''            
if(@machineID=0)             
begin            
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId            
 end             
else             
begin            
 select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID            
 end        
     
 --declare @isManualEntry int=0;            
 --select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId            
 DECLARE  @columns NVARCHAR(MAX) = '', @sql     NVARCHAR(MAX) = '';            
SELECT @columns += QUOTENAME(ShiftName) + ',' FROM [Shift] where OrganizationId=@organizationId AND IsActive=1 ORDER BY ShiftName;            
SET @columns = @columns+'[OverAll]';            
--PRINT @columns;            
            
            
            
declare @caseqry varchar(max)='cast(convert(numeric(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))) as varchar) +'',''+            
  cast(convert(numeric(10,2),((sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(UtilisationLoss) - sum(ShutdownTime) - sum(Breaktime)))))as varchar) +'',''+            
  cast(convert(numeric(10,2),(((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end)))) as varchar) Tooltip,            
  CONVERT(DECIMAL(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))*            
  (sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))*            
  ((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end))) OEE            
  '            
 -- PRINT @caseqry;          
SET @sql ='            
 ;with cte as (            
  SELECT             
  d.MachineID ,d.ProductionDateOnly as ProductionDate,min(t2.DurationMin) as WorkingShiftTime,d.ShiftId,t2.ShiftName,m.MachineName,''t''+t2.shiftName as tShiftName,            
   ISNULL(CAL.ShutdownTime,0) AS ShutdownTime,ISNULL(CAL.BreakTime,0) AS BreakTime,ISNULL(CAL.UtilisationLoss,0) AS UtilisationLoss,    
   ISNULL(CAL.TotalProductointime,0) AS TotalProductointime,ISNULL(CAL.QualityLoss,0) AS QualityLoss,           
   '' ''DemandType,'''' CustomerName              
   FROM [dbo].[ProductionEntry] d  LEFT JOIN OEECalculation AS CAL ON d.ShiftId=CAL.ShiftId AND d.MachineID=CAL.MachineId AND Convert(date,d.ProductionDate)=Convert(date,CAL.ProductionDate)          
  inner join Shift t2 on d.ShiftId = t2.ShiftId            
  inner join MasterMachine m on d.MachineID=m.MachineID            
  where ProductionDateOnly BETWEEN  '''+cast(@startdateprev as varchar)+''' AND '''+cast(@enddate as varchar)+'''            
  --and d.MachineID in (select value from fn_split('''+@machineIds+''','',''))            
              
  group by d.ProductionDateOnly,d.ShiftId,d.MAchineID,ShiftName,machinename,CAL.ShutdownTime,CAL.BreakTime,CAL.UtilisationLoss,CAL.TotalProductointime,CAL.QualityLoss          
  )            
            
  select ProductionDate,MachineName,DataLevel, '+dbo.[GetDynamicColumnString](@columns,'','sum',@organizationId)+',            
  '+dbo.[GetDynamicColumnString](@columns,'t','min',@organizationId)+' from            
  (            
  select case when GROUPING(ProductionDate) = 1 THEN ''1900-01-01'' else max(ProductionDate) end as ProductionDate,              
  case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,            
  case when GROUPING(MachineName) = 1 THEN '''' else max(MachineName) end as MachineName,              
  case when GROUPING(tShiftName) = 1 THEN ''tOverAll'' else max(tShiftName) end as tShiftName,            
  case when GROUPING(MachineName) = 1 THEN ''TotalLevel'' when GROUPING(ProductionDate) = 1 THEN ''ShiftLevel'' else ''DateLevel'' end as DataLevel,            
  '+@caseqry+'            
  from cte where ProductionDate BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+''' GROUP BY            
  GROUPING SETS (            
  (ShiftId,shiftname,MachineName,tShiftName,ProductionDate),            
        (ShiftId,shiftname,MachineName,tShiftName),            
        (ShiftId,shiftname,tShiftName),            
        (ProductionDate,MachineName),            
        (MachineName),             
  ())            
  union             
  select ''1900-01-01'',            
  case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,            
  '''',            
  case when GROUPING(tShiftName) = 1 THEN ''tOverAll'' else max(tShiftName) end as tShiftName,            
  ''TotalLevelPrev''  DataLevel,            
  '+@caseqry+'            
  from cte where ProductionDate BETWEEN  '''+cast(@startdateprev as varchar)+''' AND '''+cast(@enddateprev as varchar)+'''            
  group by GROUPING SETS (              
        (ShiftId,shiftname,tShiftName),            
  ())            
  ) t            
  PIVOT(            
  sum (OEE )            
    FOR ShiftName IN ('+ @columns +')            
) AS pivot_table            
pivot            
( min(Tooltip)            
for tShiftName in ( '+ dbo.[GetDynamicColumnString](@columns,'t','',@organizationId) +')            
) as p2 group by  ProductionDate,MachineName,DataLevel            
  --Order by ProductionDate,MachineName'            
  --print @sql;        
  EXECUTE sp_executesql @sql;            
            
END   


GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductMachinelevelDetailsByDatesScheduler]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		shailendra
-- Create date: 05-12-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetProductMachinelevelDetailsByDatesScheduler]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineID int

	--- Exec [USP_GetProductMachinelevelDetailsByDatesScheduler] '2021-02-06','2021-02-12',6,0
AS
BEGIN	
--set @startdate='2021-01-06'
--set @enddate='2021-01-12'

DECLARE  @columns NVARCHAR(MAX) = '', @sql     NVARCHAR(MAX) = '';
SELECT @columns += QUOTENAME(ShiftName) + ',' FROM [Shift] where organizationid=@organizationId ORDER BY ShiftName;
SET @columns = @columns+'[OverAll]';
PRINT @columns;


declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
  declare @isManualEntry int=0;
 select @isManualEntry=IsManualProductionEntry from Organization where OrganizationId=@organizationId
 SET @sql ='
 ;with cte as (
		SELECT 
		d.MachineID ,d.ProductionDateOnly as ProductionDate,min(t2.DurationMin) as WorkingShiftTime,d.ShiftId,t2.ShiftName,m.MachineName
		,''ARt''+t2.shiftName as ARtShiftName,''PRt''+t2.shiftName as PRtShiftName,''QRt''+t2.shiftName as QRtShiftName,
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDateOnly,1,'+cast(@isManualEntry as varchar)+')),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLoss](d.ShiftId,d.MachineID ,d.ProductionDateOnly,0,'+cast(@isManualEntry as varchar)+')),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDateOnly)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDateOnly)),0) as QualityLoss,
			''''DemandType,'''' CustomerName		
			FROM [dbo].[ProductionEntry] d
		inner join Shift t2	on d.ShiftId = t2.ShiftId
		inner join MasterMachine m on d.MachineID=m.MachineID
		where ProductionDateOnly BETWEEN  '''+cast(@startdate as varchar)+''' AND '''+cast(@enddate as varchar)+'''
		and d.MachineID in (select value from fn_split('''+@machineIds+''','',''))
		
		group by d.ProductionDateOnly,d.ShiftId,d.MAchineID,ShiftName,machinename
		)
		select MachineName,DataLevel, 
		'+dbo.[GetDynamicColumnString](@columns,'','sum',@organizationId)+',
		'+dbo.[GetDynamicColumnString](@columns,'ARt','min',@organizationId)+',
		'+dbo.[GetDynamicColumnString](@columns,'PRt','min',@organizationId)+',
		'+dbo.[GetDynamicColumnString](@columns,'QRt','min',@organizationId)+'		
		from
		(
		select --case when GROUPING(ProductionDate) = 1 THEN ''1900-01-01'' else max(ProductionDate) end as ProductionDate,		
		case when GROUPING(ShiftName) = 1 THEN ''OverAll'' else max(ShiftName) end as ShiftName,
		case when GROUPING(MachineName) = 1 THEN '''' else max(MachineName) end as MachineName,		
		case when GROUPING(ARtShiftName) = 1 THEN ''ARtOverAll'' else max(ARtShiftName) end as ARtShiftName,
		case when GROUPING(PRtShiftName) = 1 THEN ''PRtOverAll'' else max(PRtShiftName) end as PRtShiftName,
		case when GROUPING(QRtShiftName) = 1 THEN ''QRtOverAll'' else max(QRtShiftName) end as QRtShiftName,
		
		case when GROUPING(MachineName) = 1 THEN ''TotalLevel''  else ''DateLevel'' end as DataLevel
		
		,cast(convert(numeric(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))) as varchar) AR
		,cast(convert(numeric(10,2),((sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))))as varchar) PR,
		cast(convert(numeric(10,2),(((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end))))as varchar) QR,
		
		CONVERT(DECIMAL(10,2),((sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime) - sum(UtilisationLoss)) / (sum(WorkingshiftTime) - sum(ShutdownTime) - sum(Breaktime)))*
		(sum(TotalProductoinTime) / (sum(WorkingShiftTime) - sum(ShutdownTime) - sum(Breaktime)))*
		((sum(TotalProductoinTime) - sum(QualityLoss)) / (case when sum(TotalProductoinTime) = 0 then 1 else sum(TotalProductoinTime) end))) OEE	
		
		from cte GROUP BY
		GROUPING SETS (
		(ShiftId,shiftname,ARtShiftName,PRtShiftName,QRtShiftName,MachineName),
        (ShiftId,shiftname,ARtShiftName,PRtShiftName,QRtShiftName),        
        (MachineName),	
		())
		) t
		PIVOT(
		sum (OEE )
    FOR ShiftName IN ('+ @columns +')
) AS pivot_table
pivot
( min(AR)
for ARtShiftName in ( '+ dbo.[GetDynamicColumnString](@columns,'ARt','',@organizationId) +')
) as p2 
pivot
( min(PR)
for PRtShiftName in (  '+ dbo.[GetDynamicColumnString](@columns,'PRt','',@organizationId) +')
) as p3 
pivot
( min(QR)
for QRtShiftName in (  '+ dbo.[GetDynamicColumnString](@columns,'QRt','',@organizationId) +')
) as p4

group by  MachineName,DataLevel
		--Order by ProductionDate,MachineName'
		--select @sql
		EXECUTE sp_executesql @sql;

END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductMachinelevelDetailsByDatesWaterfall]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		shailendra
-- Create date: 05-12-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetProductMachinelevelDetailsByDatesWaterfall]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int,
	@machineID int

	--- Exec [USP_GetProductMachinelevelDetailsByDatesWaterfall] '2021-01-06','2021-01-12',5,0
AS
BEGIN	
declare @machineIds varchar(500)=''
if(@machineID=0) 
begin
 select @machineIds= COALESCE(@machineIds + ',', '') + CAST(MachineID AS VARCHAR) from MasterMachine where OrganizationId=@organizationId
 end 
else 
begin
select @machineIds=MachineIds from UserReportsConfig where UserMachineReportID=@machineID
 end 
 --select @startdate='2021-01-06',@enddate='2021-01-12'
 ;with cte as (
		SELECT 
		d.MachineID ,d.ProductionDate,min(d.DurationMin) as WorkingShiftTime,
			d.ShiftId, 
			isnull((select [dbo].[GetToTalBreakDownTimeBySchLossEntry](d.ShiftId,d.MachineID ,d.ProductionDate,1)),0) as ShutdownTime,
			isnull((select [dbo].[GetToTalBreakTime](d.ShiftId)),0) as BreakTime,
			isnull  ((select [dbo].[GetToTalBreakDownTimeBySchLossEntry](d.ShiftId,d.MachineID ,d.ProductionDate,0)),0) as UtilisationLoss,
			isnull  ((select [dbo].[GetProducionTimebyProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as TotalProductointime,
			isnull  ((select [dbo].[GetProducionQualityLossProduct](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as QualityLoss,
			isnull  ((select [dbo].[GetToTalBreakDownTimeByHolidays](d.ShiftId,d.MachineID ,d.ProductionDate)),0) as Holidays
			
		FROM 
		(select machineId,CalendarDate ProductionDate,ShiftId,DurationMin from [dbo].MasterMachine m,Calendar c,Shift t2 where CalendarDate BETWEEN  @startdate AND @enddate and 
		MachineID in (select value from fn_split(@machineIds,',')) ) d		
		
		--d.MachineID=coalesce(12,d.MachineID)
		group by d.ProductionDate,d.ShiftId,d.MAchineID
		)
		select sum(WorkingShiftTime) Total_Time,-sum(ShutdownTime) Shutdown_Time,-sum(UtilisationLoss) Utilisation_Loss,-sum(BreakTime) Break_Time,
		 -sum(WorkingShiftTime)+sum(Holidays)+sum(BreakTime)+sum(ShutdownTime)+sum(UtilisationLoss)+sum(TotalProductointime) Production_Loss, 
		-sum(QualityLoss) Quality_Loss,-sum(Holidays) Holidays  from cte

END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetProDuctMouldRels]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetProDuctMouldRels]   
	@OrganizationId int
AS
	BEGIN
	SELECT        RecId, MouldId, OrganizationId, ProductID, CreatedBy, CreatedOn
	FROM            Product_Mould_Rel WHERE  OrganizationId=@OrganizationId
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProductShiftDetailsByDates]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Krishna
-- Create date: 29-04-2020
-- Description:	Get the Aggretages for every product based on given dates
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetProductShiftDetailsByDates]
	
	@startdate datetime,
	@enddate datetime,
	@organizationId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT t1.ProductID,t1.ProductionDate,t1.ShiftId, Max(t1.TotalProduction) AS 
	TotalProduction, Max(t1.ReWork) AS TotalReWork,  Max(t1.Rejection) AS TotalRejection
	FROM [dbo].[ProductionEntry] t1
		inner join [dbo].[MasterMachine] t2
			on t1.[MachineID] = t2.[MachineID]
			and t2.OrganizationId = @organizationId
	WHERE t1.ProductionDate BETWEEN  @startdate AND @enddate 
	GROUP BY ProductID,ProductionDate,ShiftId

/*------- Unit Test Case ------
DECLARE	@return_value int

EXEC	@return_value = [dbo].[USP_GetProductShiftDetailsByDates]
		@startdate = N'2019-01-10',
		@enddate = N'2019-03-10',
		@organizationId =5 

SELECT	'Return Value' = @return_value
*/

END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetRejectionReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetRejectionReason]  
@ReasonId INT=NULL,
@OrganizationId INT
AS  
BEGIN  
IF @ReasonId IS NULL  OR @ReasonId=0
 BEGIN  
 SELECT  * FROM [dbo].[RejectionReason]  WHERE OrganizationId=@OrganizationId and IsActive=1
 END  
ELSE  
    BEGIN  
 SELECT  * FROM [dbo].[RejectionReason] WHERE ReasonId=@ReasonId  AND OrganizationId=@OrganizationId and IsActive=1
 END  
END  




GO
/****** Object:  StoredProcedure [dbo].[USP_GetShift]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetShift]
@ShiftId INT=NULL
AS
BEGIN
IF @ShiftId IS NULL
	BEGIN
	SELECT  * FROM [dbo].[Shift]
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[Shift] WHERE ShiftId=@ShiftId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GETSHIFT_CONFIG]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[USP_GETSHIFT_CONFIG]  
AS  
SELECT S.ShiftName,S.ActualStartTime AS ShiftStartTime,S.ActualEndTime AS ShiftEndTime,S.IsOverNight AS IsNight FROM Shift AS S  
WHERE S.IsActive=1

GO
/****** Object:  StoredProcedure [dbo].[USP_GetStopageReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetStopageReason]
@ReasonId INT=NULL,
@OrganizationId INT
AS
BEGIN
IF @ReasonId IS NULL OR @ReasonId=0
	BEGIN
	SELECT  * FROM [dbo].[StopageReason] WHERE OrganizationId=@OrganizationId and IsActive=1
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[StopageReason] WHERE ReasonId=@ReasonId AND  OrganizationId=@OrganizationId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetStopageSubReason]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetStopageSubReason]
@SubReasonId INT=NULL,
@OrganizationId int
AS
BEGIN
IF @SubReasonId IS NULL or @SubReasonId=0
	BEGIN
	SELECT  * FROM [dbo].[StopageSubReason] as substp inner join [dbo].[StopageReason] as
	stras on substp.ReasonId=stras.ReasonId where OrganizationId=@OrganizationId
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[StopageSubReason] WHERE SubReasonId=@SubReasonId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetStopageSubReasonByReasonId]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetStopageSubReasonByReasonId]
@ReasonId INT=NULL
AS
BEGIN

	SELECT  * FROM [dbo].[StopageSubReason] WHERE ReasonId=@ReasonId
	
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetUser]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetUser]
@UserId INT=NULL
AS
BEGIN
IF @UserId IS NULL
	BEGIN
	SELECT  * FROM [dbo].[OrganizationUser]
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].[OrganizationUser] WHERE UserId=@UserId
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetUserType]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetUserType]
@UserTypeID INT=NULL
AS
BEGIN
IF @UserTypeID IS NULL
	BEGIN
	SELECT  * FROM [dbo].UserType
	END
ELSE
    BEGIN
	SELECT  * FROM [dbo].UserType WHERE UserTypeID=@UserTypeID
	END
END





GO
/****** Object:  StoredProcedure [dbo].[USP_GetUtilityModule]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetUtilityModule]
@OrganizationID int
AS
BEGIN
SELECT * FROM [dbo].[UtilityModule] WHERE OrganizationId=@OrganizationID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_IsSubmitted]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateBreakDownEntry]    Script Date: 3/16/2019 1:18:42 PM ******/

CREATE PROCEDURE [dbo].[USP_IsSubmitted]				
@ProductionDate datetime,
@OrganizationId int
AS
BEGIN
		SELECT Count(*) SubmitCount FROM [dbo].[ProductionSubmit] WHERE CAST(ProductionDate AS DATE)= CAST(@ProductionDate AS DATE) and OrganizationId=@OrganizationId
END





GO
/****** Object:  StoredProcedure [dbo].[USP_MachineEfficiencyReport]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MachineEfficiencyReport]--USP_MachineEfficiencyReport 0,5,'01-SEP-2019','01-SEP-2019'
@ShiftId INT=null,
@OrgId INT,
@FromDate DATETIME,
@ToDate DATETIME,
@MachineId Int=0
AS
BEGIN
--DECLARE 
--@OrgId INT=5,      
--@MachineId Int=0,      
--@FromDate DATETIME='01-SEP-2019' ,      
--@ToDate DATETIME ='01-SEP-2019',    
--@ShiftId int =null
DECLARE @AVG_AR TABLE (ROW_ID INT IDENTITY, VAL INT)      
DECLARE @AVG_PR TABLE (ROW_ID INT IDENTITY, VAL INT)      
DECLARE @AVG_QR TABLE (ROW_ID INT IDENTITY, VAL INT)      
DECLARE @AVG_OEE TABLE (ROW_ID INT IDENTITY, VAL INT)      
DECLARE @ALLDATES  TABLE (ROW_ID INT IDENTITY, DateVal DateTime)      
DECLARE @ALLDATESProD TABLE (ROW_ID INT IDENTITY,      
[ProductionId] [int] ,      
[MachineID] [int] ,      
[ProductID] [int],      
[UserID] [int] ,      
[OperatorID] [int] ,      
[SupervisorID] [int] ,      
[TimeOfEntry] [datetime] ,      
[ShiftId] [int],      
[StartTime] [datetime] ,      
[EndTime] [datetime],      
[TotalProduction] [decimal](18, 2),      
[ReWork] [int] ,      
[Rejection] [int] ,      
[ProductionDate] [datetime] ,      
[IsSaved] [bit]       
)      

IF @ShiftId<=0
BEGIN
SET @ShiftId=NULL
END
      
INSERT INTO @ALLDATESProD(ProductionId, MachineID, ProductID, UserID, OperatorID, SupervisorID, TimeOfEntry, ShiftId, StartTime, EndTime, TotalProduction, ReWork, Rejection,       
                         ProductionDate, IsSaved)      
SELECT        ProductionId, MachineID, ProductID, UserID, OperatorID, SupervisorID, TimeOfEntry, ShiftId, StartTime, EndTime, TotalProduction, ReWork, Rejection,       
                         ProductionDate, IsSaved      
FROM            ProductionEntry where ProductionDate is not null AND       
 dbo.GetOrganizationIdByMachineId(MachineID)=@OrgId AND      
 ShiftId=isnull(@ShiftId,ShiftId) and    
cast( ProductionDate as date) between  cast(@FromDate as date) and cast(@ToDate as date) ORDER BY ProductionDate      
      
--select  * from  @ALLDATESProD      
      
      
DECLARE @ResultTable TABLE ([Date] DATETIME,Machine VARCHAR(200),[Shift] VARCHAR(150),[TotalShutDownTime] Decimal(18,2) ,[TotalAvailableTime] Decimal(18,2),[TotalOperatingTime] Decimal(18,2),[TotalProduction] Decimal(18,2),[TotalRejection] Decimal(18,2),[TotalRework] Decimal(18,2),[TotalSpeedAndMinorLoss] Decimal(18,2),[NetoperatingTime] Decimal(18,2),[TotalQualityLoss] Decimal(18,2),[ValueAddedTime] Decimal(18,2),[AR] VARCHAR(150),[PR] VARCHAR(150),[QR]VARCHAR(150))      
      
INSERT INTO @ALLDATES(DateVal)      
SELECT  DISTINCT CAST(ProductionDate as date) FROM @ALLDATESProD       
      
DECLARE @DATECOUNT INT =(SELECT COUNT(*) FROM  @ALLDATES)      
DECLARE @DATEINDEX INT =1      
      
      
--select  * from  @ALLDATES      
DECLARE @AverageQualityProd DECIMAL(18,2) = 0;      
DECLARE @AverageProduction DECIMAL(18,2) = 0;      
DECLARE @AverageShiftTime DECIMAL(18,2) = 0;      
DECLARE @AverageAvailableTime DECIMAL(18,2) = 0;      
DECLARE @EntireProductionTime DECIMAL(18,2) = 0;      
DECLARE @AverageQR   DECIMAL(18,2) = 0;     
DECLARE @AverageAR    DECIMAL(18,2) = 0;     
DECLARE @AveragePR    DECIMAL(18,2) = 0;     
DECLARE @AverageOEE   DECIMAL(18,2) = 0;     
      
WHILE @DATEINDEX<= @DATECOUNT      
   BEGIN      
   DECLARE @CDATE DATE      
   DECLARE @All_Shift TABLE (ROW_ID INT , [Shift] INT)      
      
   SET @CDATE=(select DateVal from  @ALLDATES where ROW_ID=@DATEINDEX)      
   DELETE FROM @All_Shift      
   INSERT INTO @All_Shift(ROW_ID,[Shift])         
   select row_number() over (order by ShiftId),ShiftId from  Shift where OrganizationId=@OrgId    and ISNULL(IsActive,0)=1
         


  DECLARE @All_Machine TABLE (ROW_ID INT , [Machine] INT)      
  DELETE FROM  @All_Machine   
      
			IF  @MachineId=0  
					BEGIN  
  
							INSERT INTO @All_Machine(ROW_ID,Machine)      
							SELECT ROW_NUMBER() OVER( ORDER BY MACHINEID),MACHINEID        
							from MasterMachine where OrganizationId=@OrgId   and ISNULL(IsActive,0)=1         
             
					END  
			ELSE  
					BEGIN  
							INSERT INTO @All_Machine(ROW_ID,Machine)      
						SELECT ROW_NUMBER() OVER( ORDER BY MACHINEID),MACHINEID FROM       
						(select MachineID from MasterMachine where MachineID in(      
						SELECT Distinct MachineID from  @ALLDATESProD WHERE cast(ProductionDate as date)=@CDATE  ) and MachineID=@MachineId)  M    
					END  

DECLARE @MachineCount INT =(SELECT COUNT(*) FROM  @All_Machine)      
DECLARE @MachineIndex INT =1      

			WHILE @MachineIndex<=@MachineCount      
						 BEGIN      
								DECLARE @CMachine INT       
								DECLARE @TPROD INT=0      
								DECLARE @TREG INT =0      
								SET @CMachine=(SELECT Machine FROM  @All_Machine where ROW_ID=@MachineIndex )   

			   --select  * from  @All_Shift      
								   DECLARE @ShiftCount INT =(SELECT COUNT(*) FROM  @All_Shift)      
								   DECLARE @ShiftIndex INT =1      
      
      
									 WHILE @ShiftIndex<=@ShiftCount      
									   BEGIN    
      
										  
										DECLARE @TotalProductiontime    DECIMAL(18,2)=0  
										DECLARE @TotalQualityLosstime   DECIMAL(18,2)=0 
										DECLARE @TotalQualityProd		DECIMAL(18,2) = 0;  
										DECLARE @TotalShutDownTime		DECIMAL(18,2)=0  
										DECLARE @TotalProduction		INT = 0;      
										DECLARE @TotalAvailableTime		DECIMAL(18,2)=0      
										DECLARE @TotalShiftTime			DECIMAL(18,2)=0 

										DECLARE @TotalOperatingTime			    DECIMAL(18,2)=0
										DECLARE @TotalSpeedAndMinorLoss			DECIMAL(18,2)=0
										DECLARE @NetOperatingTime			    DECIMAL(18,2)=0
										DECLARE @ValueAddedtime			        DECIMAL(18,2)=0

										

										 

										

										 


										 DECLARE @AR    DECIMAL(18,2)=0      
										 DECLARE @PR    DECIMAL(18,2)=0      
										 DECLARE @QR    DECIMAL(18,2)=0      
										 DECLARE @OEE   DECIMAL(18,2)=0      
      
      
										 DECLARE @CSHIFT INT       
										 SET @CSHIFT=(SELECT Shift FROM  @All_Shift where ROW_ID=@ShiftIndex ) 
										 

										    SET @TotalShutDownTime= @TotalShutDownTime+dbo.GetToTalBreakTime(@CSHIFT)
											
                                            DECLARE @All_Product TABLE (ROW_ID INT , [Product] INT)  
											DELETE FROM @All_Product
											INSERT INTO @All_Product(ROW_ID,[Product])      
											SELECT ROW_NUMBER() OVER( ORDER BY ProductId),ProductId FROM       
											(select ProductId from MasterProduct where ProductId in(      
											SELECT Distinct Productid from  @ALLDATESProD WHERE cast(ProductionDate as date)=@CDATE      
											AND ShiftId=@CSHIFT and MachineID=@CMachine) )  P    

											DECLARE @ProductCount INT =(SELECT COUNT(*) FROM  @All_Product)      
											DECLARE @ProductIndex INT =1      
											WHILE @ProductIndex<=@ProductCount 
											BEGIN
												DECLARE @CProduct INT =0
												SET @CProduct=(SELECT Product FROM  @All_Product where ROW_ID=@ProductIndex ) 

													    DECLARE @TempTotalProduction DECIMAL(18,2)=0
														DECLARE @TempTotalRejection DECIMAL(18,2)=0
														DECLARE @TempTotalReWork DECIMAL(18,2)=0
														SET @TempTotalProduction=ISNULL((select  SUM(TotalProduction) from  @ALLDATESProD where ShiftId=@CSHIFT and ProductID=@CProduct and MachineID=@CMachine and CAST(ProductionDate as date)=CAST(@CDATE  AS DATE)),0)  
														SET @TempTotalRejection=ISNULL((select  SUM(Rejection) from  @ALLDATESProD where ShiftId=@CSHIFT and ProductID=@CProduct and MachineID=@CMachine AND CAST(ProductionDate as date)=CAST(@CDATE AS DATE) ),0)  
														SET @TempTotalReWork=ISNULL((select  SUM(ReWork) from  @ALLDATESProD where ShiftId=@CSHIFT and ProductID=@CProduct and MachineID=@CMachine AND CAST(ProductionDate as date)=CAST(@CDATE  AS DATE)),0)  
  

														IF @TempTotalProduction>0
														BEGIN
														SET  @TotalProductiontime=@TotalProductiontime+ (isnull(dbo.GetCyCleTime(@CProduct),0) *  ISNULL(@TempTotalProduction,0)/60)
														END
														
														
														
														SET  @TotalQualityLosstime=@TotalQualityLosstime+isnull(dbo.GetCyCleTime(@CProduct),0) *  (ISNULL(@TempTotalRejection,0)+ISNULL(@TempTotalReWork,0))
														SET  @TotalQualityProd=	  @TotalQualityProd  +
																										(ISNULL((select  SUM(TotalProduction) from  @ALLDATESProD where ShiftId=@CSHIFT and ProductID=@CProduct and MachineID=@CMachine AND CAST(ProductionDate as date)=CAST(@CDATE  AS DATE)),0)
																										-ISNULL((select  SUM(Rejection) from  @ALLDATESProD where ShiftId=@CSHIFT and ProductID=@CProduct and MachineID=@CMachine AND CAST(ProductionDate as date)=CAST(@CDATE  AS DATE)),0)-
																										ISNULL((select  SUM(ReWork) from  @ALLDATESProD where ShiftId=@CSHIFT and ProductID=@CProduct and MachineID=@CMachine AND CAST(ProductionDate as date)=CAST(@CDATE  AS DATE)),0))
													
													SET @TotalProduction = @TotalProduction + @TempTotalProduction
											
												SET @ProductIndex=@ProductIndex+1
											END
											SET  @TotalShutDownTime=ISNULL( @TotalShutDownTime,0) + ISNULL( dbo.GetToTalSLoss(@CSHIFT,@CDATE,@CDATE,@CMachine),0)
											SET  @TotalAvailableTime= ISNULL(dbo.GetToTalShiftTime(@CShift),0)-ISNULL( @TotalShutDownTime,0)
											SET  @TotalOperatingTime=ISNULL( @TotalAvailableTime,0)- ISNULL(dbo.GetToTalULoss(@CSHIFT,@CDATE,@CDATE,@CMachine),0)
											SET  @TotalSpeedAndMinorLoss=ISNULL( @TotalOperatingTime,0)- ISNULL(@TotalProductiontime,0)
											SET  @NetOperatingTime=ISNULL(@TotalProductiontime,0)
                                            SET  @ValueAddedtime=isnull(@NetOperatingTime,0)-(isnull(@TotalQualityLosstime,0)/60)
													  IF @TotalProduction>0
													  BEGIN
													   SET @QR= @TotalQualityProd/@TotalProduction
													  END

													  IF @TotalOperatingTime>0
													  BEGIN
													   SET @PR = @NetOperatingTime/@TotalOperatingTime
													  END
											    
													  IF @TotalAvailableTime>0
													  BEGIN
													    SET @AR = @TotalOperatingTime/@TotalAvailableTime
													  END
												
												
											  
											  
											  --SELECT 
											  --(@CDATE) DateVal,
											  --(SELECT  MachineName FROM  MasterMachine WHERE MachineID=@CMachine) Machine,
											  --(SELECT  ShiftName FROM  Shift WHERE ShiftId=@CSHIFT) [Shift],											  
											  --@TotalShutDownTime TotalShutDownTime ,
									    --       @TotalAvailableTime TotalAvailableTime,
											  -- @TotalOperatingTime  TotalOperatingTime,
											  -- @TotalSpeedAndMinorLoss TotalSpeedAndMinorLoss,
											  -- @NetOperatingTime NetOperatingTime,
											  -- @QR QR,
											  -- @PR PR,
											  -- @AR AR


								      INSERT INTO  @ResultTable(AR,Date,Machine,NetoperatingTime,PR,QR,Shift,
									  TotalAvailableTime,TotalOperatingTime,TotalShutDownTime,TotalSpeedAndMinorLoss,TotalProduction,TotalRejection,TotalRework,ValueAddedTime,TotalQualityLoss)
									  VALUES(@AR,(@CDATE),(SELECT  MachineName FROM  MasterMachine WHERE MachineID=@CMachine), @NetOperatingTime,@PR,@QR,
									  (SELECT  ShiftName FROM  Shift WHERE ShiftId=@CSHIFT),@TotalAvailableTime,@TotalOperatingTime,@TotalShutDownTime,@TotalSpeedAndMinorLoss,@TempTotalProduction,@TempTotalRejection,@TempTotalReWork,@ValueAddedtime,case when @TotalQualityLosstime is null then 0  when @TotalQualityLosstime<=0 then 0 else @TotalQualityLosstime/60 end  )



									SET @TempTotalProduction=0
									SET @TempTotalRejection=0
									SET @TempTotalReWork=0
										 
									   SET  @ShiftIndex=@ShiftIndex+1     

										
									   END   
									   
									
									   


			SET @MachineIndex=@MachineIndex+1      
						 END      
    
SET  @DATEINDEX=@DATEINDEX+1  
    
END       
       
	SELECT  CONVERT(CHAR(10), DATE, 103) [DATE],
	Machine,
	Shift,
	TotalProduction [Total Production],
	TotalRejection [Total rejection],
	TotalRework [Total Rework],
	TotalAvailableTime [Total Available Time],
	TotalOperatingTime [Total Operating Time],
	TotalSpeedAndMinorLoss [Total Speed And Minor Loss],
	NetoperatingTime [Net operating Time],
	TotalQualityLoss [Total Q L],
	ValueAddedTime [Value Added Time],
	TotalShutDownTime [Total Shut Down Time],
	AR,
	PR,
	QR,
	cast((CAST(AR AS DECIMAL(18,2)) * CAST(PR AS DECIMAL(18,2))* CAST(QR AS DECIMAL(18,2)) )as decimal(18,2)) OEE 
	FROM @ResultTable  

 END




GO
/****** Object:  StoredProcedure [dbo].[USP_SaveUtilityModule]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_SaveUtilityModule] -- 0,1,1,0,5
 @MouldId int =NULL,  
 @DieId int=NULL,  
 @OperatorId int=NULL,  
 @OperationId int =NULL,  
 @OrganizationId int  
AS  
BEGIN  
  
BEGIN TRAN T1  
  
DELETE FROM UtilityModule WHERE OrganizationId =@OrganizationId  
  
IF @MouldId IS NOT NULL AND @MouldId<>0  
BEGIN  
INSERT INTO UtilityModule(ModuleName,OrganizationId,IsActive)  
VALUES('Mould',@OrganizationId,1)  
END  
  
IF @DieId IS NOT NULL AND @DieId<>0  
BEGIN  
INSERT INTO UtilityModule(ModuleName,OrganizationId,IsActive)  
VALUES('Die',@OrganizationId,1)  
END  
  
IF @OperatorId IS NOT NULL AND @OperatorId<>0  
BEGIN  

INSERT INTO UtilityModule(ModuleName,OrganizationId,IsActive)  
VALUES('Operator',@OrganizationId,1)  
END  
  
IF @OperationId IS NOT NULL AND @OperationId<>0  
BEGIN  
print 'op id' +cast(@OperationId as varchar)
INSERT INTO UtilityModule(ModuleName,OrganizationId,IsActive)  
VALUES('Operation',@OrganizationId,1)  
END  
  
  
COMMIT TRAN T1  
END
GO
/****** Object:  StoredProcedure [dbo].[USP_SubmitProduction]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[USP_CreateUpdateBreakDownEntry]    Script Date: 3/16/2019 1:18:42 PM ******/

CREATE PROCEDURE [dbo].[USP_SubmitProduction]				
@ProductionDate datetime,
@OrganizationId int
AS--select  * from [ProductionSubmit]
BEGIN
		IF NOT EXISTS (SELECT ProductionDate FROM [dbo].[ProductionSubmit] WHERE (CAST(ProductionDate AS DATE)= CAST(@ProductionDate AS DATE)) AND OrganizationId=@OrganizationId)
				BEGIN		
							INSERT INTO ProductionSubmit(ProductionDate,OrganizationId)
							VALUES(@ProductionDate,@OrganizationId)
				END
END





GO
/****** Object:  StoredProcedure [dbo].[ValidateImportData]    Script Date: 12/29/2022 11:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ValidateImportData]
	@type varchar(20),
	@AddedBy int,
	@OrgId int
As
Begin
declare @cnt int , @MaxRecordId INT,@IdTemp int, @isExists int
	if(@type='Machine')
	begin
		
		select @cnt=min(MachineIDTemp) from MasterMachineTemp
		select @MaxRecordId=max(MachineIDTemp) from MasterMachineTemp
		while(@cnt>0 and @cnt<=@maxrecordID)
		begin
			set @isExists=0;
			set @IdTemp=@cnt;
			SELECT @isExists=Count(*)  FROM MasterMachine  WHERE NOT EXISTS 
			(SELECT MachineCode FROM MasterMachineTemp WHERE MasterMachineTemp.MachineCode = MasterMachine.MachineCode);

			if(@isExists=0)
			begin
				insert into MasterMachine(MachineName,Location,OrganizationId,MachineCode,IsActive,AddBy,CategoryId,NonStop,DemandType)
				select MachineName,Location,(select OrganizationId from Organization where OrganizationName=Organization),MachineCode,1,@AddedBy,
				(select CategoryId from [MachineCategory] where CategoryName=Category),NonStop,DemandType
				from MasterMachineTemp where MachineIDTemp=@IdTemp
			end
		end
		truncate table MasterMachineTemp
	end
	if(@type='Product')
	begin
		
		select @cnt=min(ProductIDTemp) from MasterProductTemp
		select @MaxRecordId=max(ProductIDTemp) from MasterProductTemp
		while(@cnt>0 and @cnt<=@maxrecordID)
		begin
			set @isExists=0;
			set @IdTemp=@cnt;
			SELECT @isExists=Count(*)  FROM MasterProduct  WHERE NOT EXISTS 
			(SELECT ProductCode FROM MasterProductTemp WHERE MasterProductTemp.ProductCode = MasterProduct.ProductCode);

			if(@isExists=0)
			begin
				insert into MasterProduct([ProductCode],[OrganizationId]
				,[ProductName],[CycleTime],[IsActive],[AddBy],[ProductType]
				,[PrdouctCategoryID],[UnitID],[CustomerID])

				select [ProductCode],(select OrganizationId from Organization where OrganizationName=Organization)
				  ,[ProductName],[CycleTime],1,@AddedBy,[DemandType]
				  ,(select Id from [Organization_ProductCategory] where CategoryName=ProductCategory)
				  ,(select Id from unitMAster where Unit=unit)
				  ,(select Id from [Organization_Customer] where CustomerName=Customer)
				from MasterProductTemp where ProductIDTemp=@IdTemp
			end
		end
		truncate table MasterProductTemp
	end
	
End







GO
USE [master]
GO
ALTER DATABASE [proefficient_mod] SET  READ_WRITE 
GO
