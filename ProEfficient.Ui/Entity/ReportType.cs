﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ReportType
    {
        public ReportType()
        {
            UserSendReportConfigs = new HashSet<UserSendReportConfig>();
        }

        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public int OrganizationId { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<UserSendReportConfig> UserSendReportConfigs { get; set; }
    }
}
