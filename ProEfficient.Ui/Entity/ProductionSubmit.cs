﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductionSubmit
    {
        public int Id { get; set; }
        public DateTime? ProductionDate { get; set; }
        public int? OrganizationId { get; set; }
    }
}
