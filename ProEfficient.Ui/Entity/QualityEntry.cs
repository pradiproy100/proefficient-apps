﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class QualityEntry
    {
        public int QualityEntryId { get; set; }
        public int MachineId { get; set; }
        public string RejectionReasonIds { get; set; }
        public int ShiftId { get; set; }
        public DateTime ProductionDate { get; set; }
        public int ProdCount { get; set; }
        public DateTime? TimeOfEntry { get; set; }
        public int? ProductionEntryId { get; set; }
    }
}
