﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class UnitMaster
    {
        public int Id { get; set; }
        public string Unit { get; set; }
        public int? OrganizationId { get; set; }
        public string UnitPrefix { get; set; }
        public string UnitSuffix { get; set; }
        public decimal? QtyMultiplier { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
