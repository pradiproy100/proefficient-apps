﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductionRejection
    {
        public int RejectionId { get; set; }
        public int? ProductionEntryId { get; set; }
        public int? RejectionReasonId { get; set; }
        public int? RejectionQuantity { get; set; }
    }
}
