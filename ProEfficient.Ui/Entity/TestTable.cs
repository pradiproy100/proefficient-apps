﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class TestTable
    {
        public int RowId { get; set; }
        public int? Ar { get; set; }
        public int? Pr { get; set; }
        public int? Qr { get; set; }
        public int? Qee { get; set; }
        public DateTime? Date { get; set; }
    }
}
