﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductSchedulerHistory
    {
        public int ProductScheduleHistoryId { get; set; }
        public int? ProductId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public bool? IsFullDay { get; set; }
        public bool? IsCancelled { get; set; }
        public bool? IsRescheduled { get; set; }
        public int? PastScheduleId { get; set; }
    }
}
