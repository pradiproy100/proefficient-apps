﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class AspNetUser
    {
        public AspNetUser()
        {
            SupervisorMachineMaps = new HashSet<SupervisorMachineMap>();
        }

        public int Id { get; set; }
        public int AccessFailedCount { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public string NormalizedEmail { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public string SecurityStamp { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string UserName { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public int? OrganizationId { get; set; }
        public int? UserTypeId { get; set; }

        public virtual ICollection<SupervisorMachineMap> SupervisorMachineMaps { get; set; }
    }
}
