﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class UserSendReportConfig
    {
        public int UserSendReportConfigId { get; set; }
        [Required]
        [Display(Name = "Machine Group")]
        public int ReportGroupId { get; set; }
        [Required]
        [Display(Name = "Report Type")]
        public int ReportTypeId { get; set; }
        [Required]
        [Display(Name = "Report Name")]
        public string ReportName { get; set; }
        [Required]
        [Display(Name = "Frequency")]
        public string DateRange { get; set; }
        public string ValidDays { get; set; }
        public string WeekStartDay { get; set; }
        public int? MonthStartDate { get; set; }
        [Required]
        [Display(Name = "Report Time")]
        public string ReportSentTime { get; set; }
        [NotMapped]
        [Required]
        [Display(Name = "Daily Report On")]
        public string[] arrValidDays { get; set; }



        [Required]
        [NotMapped]
        [Display(Name = "Recipients")]
        public int[] arrRecipients { get; set; }
        public string MailTo { get; set; }
        public int OrganizationId { get; set; }

        public virtual UserReportsConfig ReportGroup { get; set; }
        public virtual ReportType ReportType { get; set; }
    }
}
