﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class Shift
    {
        public Shift()
        {
            BreakInfos = new HashSet<BreakInfo>();
            SupervisorMachineMaps = new HashSet<SupervisorMachineMap>();
        }

        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public string ShiftDescription { get; set; }
        public decimal? StartTime { get; set; }
        public decimal? EndTime { get; set; }
        public int? OrganizationId { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public int? DurationMin { get; set; }
        public bool? IsOverNight { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual ICollection<BreakInfo> BreakInfos { get; set; }
        public virtual ICollection<SupervisorMachineMap> SupervisorMachineMaps { get; set; }
    }
}
