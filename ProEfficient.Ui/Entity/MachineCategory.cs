﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class MachineCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int OrganizationId { get; set; }
    }
}
