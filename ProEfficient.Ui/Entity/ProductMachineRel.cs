﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductMachineRel
    {
        public int MachineId { get; set; }
        public int ProductId { get; set; }
        public decimal CycleTime { get; set; }
        public string ProductDemandType { get; set; }
        public string MachineDemandType { get; set; }
        public DateTime StartDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
