﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class SupervisorMachineMap
    {
       
        public int SupervisorMachineId { get; set; }
        public int SupervisorId { get; set; }
        public int MachineId { get; set; }
        public int ShiftId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool? IsFullDay { get; set; }
        public bool? IsActive { get; set; }
        public string ThemeColor { get; set; }
        //public virtual MasterMachine Machine { get; set; }
        //public virtual Shift Shift { get; set; }
        //public virtual AspNetUser Supervisor { get; set; }
        //[NotMapped]
        
        public string MachineNames { get; set; }
        public string SupervisorName { get; set; }
        public string ShiftName { get; set; }
        public int OrganizationId { get; set; }
        public string MachineIds { get; set; }
        [NotMapped]
        public string[] arrMachineIds { get; set; }
        public virtual Shift Shift { get; set; }
        public virtual MasterMachine Machine { get; set; }
        public virtual AspNetUser Supervisor { get; set; }
    }
}
