﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class BreakInfo
    {
        public int BreakId { get; set; }
        public int? ShiftId { get; set; }
        public string BreakName { get; set; }
        public decimal? StartTime { get; set; }
        public decimal? EndTime { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public int? DurationMin { get; set; }

        public virtual Shift Shift { get; set; }
    }
}
