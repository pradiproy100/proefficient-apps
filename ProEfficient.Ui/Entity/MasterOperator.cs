﻿namespace ProEfficient.Ui.Entity
{
    public class MasterOperator
    {
        public int OperatorId { get; set; }

        public string EmployeeId { get; set; }

        public int? OrganizationId { get; set; }

        public string OperatorName { get; set; }

        public int? AddBy { get; set; }

        public int? EditBy { get; set; }

        public bool? IsActive { get; set; }

    }

}
