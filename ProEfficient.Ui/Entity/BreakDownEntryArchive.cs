﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class BreakDownEntryArchive
    {
        public int BreakDownId { get; set; }
        public int? MachineCategoryId { get; set; }
        public int? MachineId { get; set; }
        public int? ReasonId { get; set; }
        public int? SubReasonId { get; set; }
        public int? UserId { get; set; }
        public int? OperatorId { get; set; }
        public int? SupervisorId { get; set; }
        public DateTime? TimeOfEntry { get; set; }
        public int? ShiftId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal? TotalStoppageTime { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public DateTime? ProductionDate { get; set; }
        public bool? IsSaved { get; set; }
        public string Remarks { get; set; }
        public bool IsDeleted { get; set; }

        public virtual StopageReason Reason { get; set; }
        public virtual OrganizationUser User { get; set; }
    }
}
