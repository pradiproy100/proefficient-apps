﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProefficientContext : DbContext
    {
        public ProefficientContext()
        {
        }

        public ProefficientContext(DbContextOptions<ProefficientContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserRole> AspNetUserRoles { get; set; }
        public virtual DbSet<AuditLog> AuditLogs { get; set; }
        public virtual DbSet<BreakDownEntry> BreakDownEntries { get; set; }
        public virtual DbSet<BreakDownEntryArchive> BreakDownEntryArchives { get; set; }
        public virtual DbSet<BreakInfo> BreakInfos { get; set; }
        public virtual DbSet<Calendar> Calendars { get; set; }
        public virtual DbSet<Holidays> Holidays { get; set; }
        public virtual DbSet<MachineCategory> MachineCategories { get; set; }
        public virtual DbSet<MasterMachine> MasterMachines { get; set; }
        public virtual DbSet<MasterMachineTemp> MasterMachineTemps { get; set; }
        public virtual DbSet<MasterProduct> MasterProducts { get; set; }
        public virtual DbSet<MasterProductTemp> MasterProductTemps { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<OrganizationCustomer> OrganizationCustomers { get; set; }
        public virtual DbSet<OrganizationProductCategory> OrganizationProductCategories { get; set; }
        public virtual DbSet<OrganizationUser> OrganizationUsers { get; set; }
        public virtual DbSet<ProductMachineRel> ProductMachineRels { get; set; }
        public virtual DbSet<ProductMachineRelTemp> ProductMachineRelTemps { get; set; }
        public virtual DbSet<ProductMachineTran> ProductMachineTrans { get; set; }
        public virtual DbSet<ProductScheduler> ProductSchedulers { get; set; }
        public virtual DbSet<ProductSchedulerHistory> ProductSchedulerHistories { get; set; }
        public virtual DbSet<ProductionEntry> ProductionEntries { get; set; }
        public virtual DbSet<ProductionEntryArchive> ProductionEntryArchives { get; set; }
        public virtual DbSet<ProductionRejection> ProductionRejections { get; set; }
        public virtual DbSet<ProductionSubmit> ProductionSubmits { get; set; }
        public virtual DbSet<QualityEntry> QualityEntries { get; set; }
        public virtual DbSet<RejectionReason> RejectionReasons { get; set; }
        public virtual DbSet<ReportDeliveryLog> ReportDeliveryLogs { get; set; }
        public virtual DbSet<ReportType> ReportTypes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<StopageReason> StopageReasons { get; set; }
        public virtual DbSet<StopageSubReason> StopageSubReasons { get; set; }
        public virtual DbSet<SupervisorCalendar> SupervisorCalendars { get; set; }
        public virtual DbSet<SupervisorMachineMap> SupervisorMachineMaps { get; set; }
        public virtual DbSet<UnitMaster> UnitMasters { get; set; }
        public virtual DbSet<UserDashboard> UserDashboards { get; set; }
        public virtual DbSet<UserReportsConfig> UserReportsConfigs { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserSendReportConfig> UserSendReportConfigs { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=SHAILENDRA\\SQLEXPRESS;Initial Catalog=ProEfficient9Mar21;User ID=SA;Password=123456;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AspNetRole>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUser>(entity =>
            {
                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserRole>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });
            });

            modelBuilder.Entity<AuditLog>(entity =>
            {
                entity.HasKey(e => e.Logid);

                entity.ToTable("AuditLog");

                entity.Property(e => e.LogDate).HasColumnType("datetime");

                entity.Property(e => e.Message)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OperationName)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BreakDownEntry>(entity =>
            {
                entity.HasKey(e => e.BreakDownId);

                entity.ToTable("BreakDownEntry");

                entity.Property(e => e.BreakDownId).HasColumnName("BreakDownID");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.OperatorId).HasColumnName("OperatorID");

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.SupervisorId).HasColumnName("SupervisorID");

                entity.Property(e => e.TimeOfEntry)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TotalStoppageTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<BreakDownEntryArchive>(entity =>
            {
                entity.HasKey(e => e.BreakDownId);

                entity.ToTable("BreakDownEntryArchive");

                entity.Property(e => e.BreakDownId).HasColumnName("BreakDownID");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.OperatorId).HasColumnName("OperatorID");

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.Remarks).HasMaxLength(500);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.SupervisorId).HasColumnName("SupervisorID");

                entity.Property(e => e.TimeOfEntry).HasColumnType("datetime");

                entity.Property(e => e.TotalStoppageTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Reason)
                    .WithMany(p => p.BreakDownEntryArchives)
                    .HasForeignKey(d => d.ReasonId)
                    .HasConstraintName("FK_BreakDownEntryArchive_StopageReason");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.BreakDownEntryArchives)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_BreakDownEntryArchive_OrganizationUser");
            });

            modelBuilder.Entity<BreakInfo>(entity =>
            {
                entity.HasKey(e => e.BreakId);

                entity.ToTable("BreakInfo");

                entity.Property(e => e.BreakName).HasMaxLength(200);

                entity.Property(e => e.DurationMin).HasDefaultValueSql("((0))");

                entity.Property(e => e.EndTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.StartTime).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.BreakInfos)
                    .HasForeignKey(d => d.ShiftId)
                    .HasConstraintName("FK_BreakInfo_Shift");
            });

            modelBuilder.Entity<Calendar>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Calendar");

                entity.Property(e => e.CalendarDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Holidays>(entity =>
            {
                entity.HasKey(e => e.HolidayID);

                entity.Property(e => e.End).HasColumnType("datetime");

                entity.Property(e => e.HolidayDescription)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HolidayName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Start).HasColumnType("datetime");

                entity.Property(e => e.ThemeColor)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MachineCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("MachineCategory");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MasterMachine>(entity =>
            {
                entity.HasKey(e => e.MachineId);

                entity.ToTable("MasterMachine");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.DemandType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('None')");

                entity.Property(e => e.Location).HasMaxLength(500);

                entity.Property(e => e.MachineCode).HasMaxLength(200);

                entity.Property(e => e.MachineName).HasMaxLength(200);
            });

            modelBuilder.Entity<MasterMachineTemp>(entity =>
            {
                entity.HasKey(e => e.MachineIdtemp);

                entity.ToTable("MasterMachineTemp");

                entity.Property(e => e.MachineIdtemp).HasColumnName("MachineIDTemp");

                entity.Property(e => e.Category)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DemandType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MachineCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MachineName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Organization)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MasterProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PK_MasterProduct_1");

                entity.ToTable("MasterProduct");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.CycleTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.PrdouctCategoryId).HasColumnName("PrdouctCategoryID");

                entity.Property(e => e.ProductCode).HasMaxLength(100);

                entity.Property(e => e.ProductName).HasMaxLength(100);

                entity.Property(e => e.ProductType)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UnitId).HasColumnName("UnitID");

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.MasterProducts)
                    .HasForeignKey(d => d.OrganizationId)
                    .HasConstraintName("FK_MasterProduct_Organization");
            });

            modelBuilder.Entity<MasterProductTemp>(entity =>
            {
                entity.HasKey(e => e.ProductIdTemp);

                entity.ToTable("MasterProductTemp");

                entity.Property(e => e.Customer)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CycleTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.DemandType)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Organization)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCategory)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Unit)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Organization>(entity =>
            {
                entity.ToTable("Organization");

                entity.Property(e => e.EmailId).HasMaxLength(200);

               
                entity.Property(e => e.Location).HasMaxLength(500);

                entity.Property(e => e.OrganizationName).HasMaxLength(150);

                entity.Property(e => e.PhoneNo).HasMaxLength(20);
            });

            modelBuilder.Entity<OrganizationCustomer>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Organization_Customer");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.OrganizationId).HasColumnName("OrganizationID");
            });

            modelBuilder.Entity<OrganizationProductCategory>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Organization_ProductCategory");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.OrganizationId).HasColumnName("OrganizationID");
            });

            modelBuilder.Entity<OrganizationUser>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("OrganizationUser");

                entity.Property(e => e.ChangeCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EmailId).HasMaxLength(200);

                entity.Property(e => e.Password).HasColumnType("text");

                entity.Property(e => e.PhoneNo).HasMaxLength(20);

                entity.Property(e => e.UserName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.OrganizationUsers)
                    .HasForeignKey(d => d.OrganizationId)
                    .HasConstraintName("FK_OrganizationUser_Organization");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.OrganizationUsers)
                    .HasForeignKey(d => d.UserTypeId)
                    .HasConstraintName("FK_OrganizationUser_UserType");
            });

            modelBuilder.Entity<ProductMachineRel>(entity =>
            {
                entity.HasKey(e => new { e.MachineId, e.ProductId })
                    .HasName("PK_ProductMachineRel");

                entity.ToTable("Product_Machine_Rel");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CycleTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MachineDemandType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('None')");

                entity.Property(e => e.ProductDemandType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('None')");

                entity.Property(e => e.StartDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<ProductMachineRelTemp>(entity =>
            {
                entity.HasKey(e => e.MachineProductMapId)
                    .HasName("PK_ProductMachineRelTemp");

                entity.ToTable("Product_Machine_RelTemp");

                entity.Property(e => e.MachineProductMapId).HasColumnName("MachineProductMapID");

                entity.Property(e => e.CycleTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Machine)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MachineDemandType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Product)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductDemandType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductMachineTran>(entity =>
            {
                entity.HasKey(e => e.Mpid)
                    .HasName("PK_Product_Machine");

                entity.ToTable("Product_Machine_Tran");

                entity.Property(e => e.Mpid).HasColumnName("MPID");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CycleTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.MachineDemandType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('None')");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.MpendDate)
                    .HasColumnType("datetime")
                    .HasColumnName("MPEndDate");

                entity.Property(e => e.MpstartDate)
                    .HasColumnType("datetime")
                    .HasColumnName("MPStartDate")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProductDemandType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('None')");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");
            });

            modelBuilder.Entity<ProductScheduler>(entity =>
            {
                entity.HasKey(e => e.ProductScheduleId);

                entity.ToTable("ProductScheduler");

                entity.Property(e => e.ProductScheduleId).HasColumnName("productScheduleId");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("endDate");

                entity.Property(e => e.EndTime)
                    .HasColumnType("time(0)")
                    .HasColumnName("endTime");

                entity.Property(e => e.IsFullDay)
                    .IsRequired()
                    .HasColumnName("isFullDay")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.PastScheduleId).HasColumnName("pastScheduleId");

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_Date");

                entity.Property(e => e.StartTime)
                    .HasColumnType("time(0)")
                    .HasColumnName("startTime");
            });

            modelBuilder.Entity<ProductSchedulerHistory>(entity =>
            {
                entity.HasKey(e => e.ProductScheduleHistoryId);

                entity.ToTable("ProductSchedulerHistory");

                entity.Property(e => e.ProductScheduleHistoryId).HasColumnName("productScheduleHistoryId");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("endDate");

                entity.Property(e => e.EndTime)
                    .HasColumnType("time(0)")
                    .HasColumnName("endTime");

                entity.Property(e => e.IsCancelled).HasColumnName("isCancelled");

                entity.Property(e => e.IsFullDay).HasColumnName("isFullDay");

                entity.Property(e => e.IsRescheduled).HasColumnName("isRescheduled");

                entity.Property(e => e.PastScheduleId).HasColumnName("pastScheduleId");

                entity.Property(e => e.ProductId).HasColumnName("productId");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_Date");

                entity.Property(e => e.StartTime)
                    .HasColumnType("time(0)")
                    .HasColumnName("startTime");
            });

            modelBuilder.Entity<ProductionEntry>(entity =>
            {
                entity.HasKey(e => e.ProductionId);

                entity.ToTable("ProductionEntry");

                entity.Property(e => e.ConvertedProduction).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ConvertedRejection).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ConvertedRework).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.MachineDemandType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.OperatorId).HasColumnName("OperatorID");

                entity.Property(e => e.ProductDemandType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PRoductDemandType");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.ProductionDateOnly)
                    .HasColumnType("date")
                    .HasComputedColumnSql("(CONVERT([date],[ProductionDate],0))", false);

                entity.Property(e => e.RejectionList).HasColumnType("text");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.SupervisorId).HasColumnName("SupervisorID");

                entity.Property(e => e.TimeOfEntry).HasColumnType("datetime");

                entity.Property(e => e.TotalProduction).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ProductionEntryArchive>(entity =>
            {
                entity.HasKey(e => e.ProductionId);

                entity.ToTable("ProductionEntryArchive");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.MachineId).HasColumnName("MachineID");

                entity.Property(e => e.OperatorId).HasColumnName("OperatorID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.RejectionList).HasColumnType("text");

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.SupervisorId).HasColumnName("SupervisorID");

                entity.Property(e => e.TimeOfEntry).HasColumnType("datetime");

                entity.Property(e => e.TotalProduction).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<ProductionRejection>(entity =>
            {
                entity.HasKey(e => e.RejectionId);

                entity.ToTable("ProductionRejection");
            });

            modelBuilder.Entity<ProductionSubmit>(entity =>
            {
                entity.ToTable("ProductionSubmit");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<QualityEntry>(entity =>
            {
                entity.ToTable("QualityEntry");

                entity.Property(e => e.ProductionDate).HasColumnType("datetime");

                entity.Property(e => e.RejectionReasonIds)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeOfEntry).HasColumnType("datetime");
            });

            modelBuilder.Entity<RejectionReason>(entity =>
            {
                entity.HasKey(e => e.ReasonId);

                entity.ToTable("RejectionReason");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ReasonDescription).HasMaxLength(500);

               
                entity.Property(e => e.ReasonName)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReportDeliveryLog>(entity =>
            {
                entity.ToTable("ReportDeliveryLog");

                entity.Property(e => e.ReportDeliveryLogId).HasColumnName("ReportDeliveryLogID");

                entity.Property(e => e.ErrMsg)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ReportDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportType>(entity =>
            {
                entity.HasKey(e => e.ReportId);

                entity.ToTable("ReportType");

                entity.Property(e => e.ReportId).ValueGeneratedNever();

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ReportName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<Shift>(entity =>
            {
                entity.ToTable("Shift");

                entity.Property(e => e.DurationMin).HasDefaultValueSql("((0))");

                entity.Property(e => e.EndTime).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ShiftDescription).HasMaxLength(500);

                entity.Property(e => e.ShiftName).HasMaxLength(200);

                entity.Property(e => e.StartTime).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Organization)
                    .WithMany(p => p.Shifts)
                    .HasForeignKey(d => d.OrganizationId)
                    .HasConstraintName("FK_Shift_Organization");
            });

            modelBuilder.Entity<StopageReason>(entity =>
            {
                entity.HasKey(e => e.ReasonId);

                entity.ToTable("StopageReason");

                entity.Property(e => e.IsSchLoss).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReasonCode).HasMaxLength(50);

                entity.Property(e => e.ReasonName).HasMaxLength(500);

                entity.HasOne(d => d.AddByNavigation)
                    .WithMany(p => p.StopageReasonAddByNavigations)
                    .HasForeignKey(d => d.AddBy)
                    .HasConstraintName("FK__StopageRe__AddBy__4B7734FF");

                entity.HasOne(d => d.EdittedByNavigation)
                    .WithMany(p => p.StopageReasonEdittedByNavigations)
                    .HasForeignKey(d => d.EdittedBy)
                    .HasConstraintName("FK__StopageRe__Editt__4C6B5938");
            });

            modelBuilder.Entity<StopageSubReason>(entity =>
            {
                entity.HasKey(e => e.SubReasonId);

                entity.ToTable("StopageSubReason");

                entity.Property(e => e.ReasonDescription)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SubReasonCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SubReasonName)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SupervisorCalendar>(entity =>
            {
                entity.ToTable("SupervisorCalendar");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnType("datetime")
                    .HasColumnName("endDate");

                entity.Property(e => e.MachineId).HasColumnName("machineId");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ShiftId).HasColumnName("shiftId");

                entity.Property(e => e.StartDate)
                    .HasColumnType("datetime")
                    .HasColumnName("startDate");

                entity.Property(e => e.SupervisorId).HasColumnName("supervisorId");
            });

            modelBuilder.Entity<SupervisorMachineMap>(entity =>
            {
                entity.HasKey(e => e.SupervisorMachineId);

                entity.ToTable("SupervisorMachineMap");

                entity.Property(e => e.SupervisorMachineId).HasColumnName("SupervisorMachineID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsFullDay)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MachineIds)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MachineNames)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OrganizationId).HasDefaultValueSql("((5))");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.ThemeColor)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UnitMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("UnitMaster");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.OrganizationId).HasColumnName("OrganizationID");

                entity.Property(e => e.QtyMultiplier).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.Unit)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.UnitPrefix)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.UnitSuffix)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.HasOne(d => d.Organization)
                    .WithMany()
                    .HasForeignKey(d => d.OrganizationId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_UnitMaster_Organization");
            });

            modelBuilder.Entity<UserDashboard>(entity =>
            {
                entity.HasKey(e => e.DashboardUserId);

                entity.ToTable("UserDashboard");

                entity.Property(e => e.DashboardUserId).HasColumnName("DashboardUserID");

                entity.Property(e => e.DateRange)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserReportsConfig>(entity =>
            {
                entity.HasKey(e => e.UserMachineReportId);

                entity.ToTable("UserReportsConfig");

                entity.Property(e => e.UserMachineReportId).HasColumnName("UserMachineReportID");

                entity.Property(e => e.DateRange)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MachineIds)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MailTo)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ReportName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.ToTable("UserRole");
            });

            modelBuilder.Entity<UserSendReportConfig>(entity =>
            {
                entity.ToTable("UserSendReportConfig");

                entity.Property(e => e.DateRange)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MailTo)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ReportName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReportSentTime)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValidDays)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WeekStartDay)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.ReportGroup)
                    .WithMany(p => p.UserSendReportConfigs)
                    .HasForeignKey(d => d.ReportGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserSendReportConfig_UserReportsConfig");

                entity.HasOne(d => d.ReportType)
                    .WithMany(p => p.UserSendReportConfigs)
                    .HasForeignKey(d => d.ReportTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserSendReportConfig_ReportType");
            });

            modelBuilder.Entity<UserType>(entity =>
            {
                entity.ToTable("UserType");

                entity.Property(e => e.UserTypeId).HasColumnName("UserTypeID");

                entity.Property(e => e.PermissionModule).HasMaxLength(1000);

                entity.Property(e => e.UserTypeName).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
