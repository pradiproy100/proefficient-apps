﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class OrganizationUser
    {
        public OrganizationUser()
        {
            BreakDownEntryArchives = new HashSet<BreakDownEntryArchive>();
            StopageReasonAddByNavigations = new HashSet<StopageReason>();
            StopageReasonEdittedByNavigations = new HashSet<StopageReason>();
        }

        public int UserId { get; set; }
        public int? OrganizationId { get; set; }
        public int? UserTypeId { get; set; }
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public string UserName { get; set; }
        public string ChangeCode { get; set; }

        public virtual Organization Organization { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual ICollection<BreakDownEntryArchive> BreakDownEntryArchives { get; set; }
        public virtual ICollection<StopageReason> StopageReasonAddByNavigations { get; set; }
        public virtual ICollection<StopageReason> StopageReasonEdittedByNavigations { get; set; }
    }
}
