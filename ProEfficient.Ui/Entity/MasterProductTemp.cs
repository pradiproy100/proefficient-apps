﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class MasterProductTemp
    {
        public int ProductIdTemp { get; set; }
        public string ProductCode { get; set; }
        public string Organization { get; set; }
        public string ProductName { get; set; }
        public decimal? CycleTime { get; set; }
        public string DemandType { get; set; }
        public string ProductCategory { get; set; }
        public string Unit { get; set; }
        public string Customer { get; set; }
    }
}
