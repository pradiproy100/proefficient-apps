﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class MasterMachineTemp
    {
        public int MachineIdtemp { get; set; }
        public string MachineName { get; set; }
        public string Location { get; set; }
        public string Organization { get; set; }
        public string MachineCode { get; set; }
        public string Category { get; set; }
        public bool? NonStop { get; set; }
        public string DemandType { get; set; }
    }
}
