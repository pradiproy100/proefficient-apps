﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class Organization
    {
        public Organization()
        {
            MasterProducts = new HashSet<MasterProduct>();
            OrganizationUsers = new HashSet<OrganizationUser>();
            Shifts = new HashSet<Shift>();
        }

        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string Location { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }

        public virtual ICollection<MasterProduct> MasterProducts { get; set; }
        public virtual ICollection<OrganizationUser> OrganizationUsers { get; set; }
        public virtual ICollection<Shift> Shifts { get; set; }
    }
}
