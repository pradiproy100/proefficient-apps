﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class UserType
    {
        public UserType()
        {
            OrganizationUsers = new HashSet<OrganizationUser>();
        }

        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        public string PermissionModule { get; set; }

        public virtual ICollection<OrganizationUser> OrganizationUsers { get; set; }
    }
}
