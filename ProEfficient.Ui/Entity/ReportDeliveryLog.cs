﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ReportDeliveryLog
    {
        public int ReportDeliveryLogId { get; set; }
        public int UserSendReportConfigId { get; set; }
        public DateTime ReportDate { get; set; }
        public bool IsSent { get; set; }
        public string ErrMsg { get; set; }
    }
}
