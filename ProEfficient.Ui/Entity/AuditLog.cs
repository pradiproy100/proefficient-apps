﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class AuditLog
    {
        public long Logid { get; set; }
        public string ModuleName { get; set; }
        public string Message { get; set; }
        public int? Level { get; set; }
        public DateTime LogDate { get; set; }
        public string OperationName { get; set; }
        public int? UserId { get; set; }
    }
}
