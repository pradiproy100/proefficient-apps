﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class OrganizationCustomer
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public int? OrganizationId { get; set; }
    }
}
