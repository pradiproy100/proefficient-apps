﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class MasterProduct
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public int? OrganizationId { get; set; }
        public string ProductName { get; set; }
        public decimal? CycleTime { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public string ProductType { get; set; }
        public int? PrdouctCategoryId { get; set; }
        public int? UnitId { get; set; }
        public int? CustomerId { get; set; }

        public virtual Organization Organization { get; set; }
    }
}
