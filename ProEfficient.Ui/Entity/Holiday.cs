﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class Holidays
    {
        public int HolidayID { get; set; }

        public string HolidayName { get; set; }

        public string HolidayDescription { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public string ThemeColor { get; set; }

        public bool IsFullDay { get; set; }
        public int OrganizationId { get; set; }
    }
}
