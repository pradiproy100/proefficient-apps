﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductMachineTran
    {
        public int Mpid { get; set; }
        public int MachineId { get; set; }
        public int ProductId { get; set; }
        public decimal? CycleTime { get; set; }
        public string MachineDemandType { get; set; }
        public string ProductDemandType { get; set; }
        public DateTime MpstartDate { get; set; }
        public DateTime? MpendDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
