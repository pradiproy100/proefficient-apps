﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class UserDashboard
    {
        public int DashboardUserId { get; set; }
        public int UserId { get; set; }
        public int MachineId { get; set; }
        public string DateRange { get; set; }
    }
}
