﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class MasterMachine
    {
        public MasterMachine()
        {
            SupervisorMachineMaps = new HashSet<SupervisorMachineMap>();
        }

        public int MachineId { get; set; }
        public string MachineName { get; set; }
        public string Location { get; set; }
        public int? OrganizationId { get; set; }
        public string MachineCode { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public int? CategoryId { get; set; }
        public bool? NonStop { get; set; }
        public string DemandType { get; set; }

        public virtual ICollection<SupervisorMachineMap> SupervisorMachineMaps { get; set; }
    }
}
