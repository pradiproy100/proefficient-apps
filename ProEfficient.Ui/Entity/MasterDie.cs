﻿using System;

namespace ProEfficient.Ui.Entity
{
    public class MasterDie
    {
        public int DieId { get; set; }

        public string DieDescription { get; set; }

        public int? AvailableCavityCount { get; set; }

        public int? AddBy { get; set; }

        public DateTime? AddDate { get; set; }

        public int? EditBy { get; set; }

        public DateTime? EditDate { get; set; }

        public bool? IsActive { get; set; }

        public int? OrganizationId { get; set; }

    }

}
