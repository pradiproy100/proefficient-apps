﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class StopageSubReason
    {
        public int SubReasonId { get; set; }
        public int? ReasonId { get; set; }
        public string SubReasonName { get; set; }
        public string SubReasonCode { get; set; }
        public bool? IsActive { get; set; }
        public string ReasonDescription { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
    }
}
