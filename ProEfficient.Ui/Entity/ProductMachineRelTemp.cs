﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductMachineRelTemp
    {
        public int MachineProductMapId { get; set; }
        public string Machine { get; set; }
        public string Product { get; set; }
        public decimal? CycleTime { get; set; }
        public string ProductDemandType { get; set; }
        public string MachineDemandType { get; set; }
    }
}
