﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class StopageReason
    {
        public StopageReason()
        {
            BreakDownEntries = new HashSet<BreakDownEntry>();
            BreakDownEntryArchives = new HashSet<BreakDownEntryArchive>();
        }

        public int ReasonId { get; set; }
        public string ReasonName { get; set; }
        public string ReasonCode { get; set; }
        public bool? IsActive { get; set; }
        public int? AddBy { get; set; }
        public int? EdittedBy { get; set; }
        public bool? IsManaged { get; set; }
        public bool? IsSchLoss { get; set; }
        public int? OrganizationId { get; set; }

        public virtual OrganizationUser AddByNavigation { get; set; }
        public virtual OrganizationUser EdittedByNavigation { get; set; }
        public virtual ICollection<BreakDownEntry> BreakDownEntries { get; set; }
        public virtual ICollection<BreakDownEntryArchive> BreakDownEntryArchives { get; set; }
    }
}
