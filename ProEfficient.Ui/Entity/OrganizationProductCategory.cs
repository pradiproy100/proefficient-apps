﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProEfficient.Ui.Entity
{
    public partial class OrganizationProductCategory
    {
        [Key]
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public int OrganizationId { get; set; }
        public string RejectionReasonIds { get; set; }
    }
}
