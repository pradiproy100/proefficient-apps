﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class Calendar
    {
        public DateTime? CalendarDate { get; set; }
    }
}
