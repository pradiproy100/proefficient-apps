﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class UserReportsConfig
    {
        public UserReportsConfig()
        {
            UserSendReportConfigs = new HashSet<UserSendReportConfig>();
        }

        public int UserMachineReportId { get; set; }
        public int UserId { get; set; }
        public string ReportName { get; set; }
        public string DateRange { get; set; }
        public string MachineIds { get; set; }
        public bool IsDefault { get; set; }
        public string MailTo { get; set; }

        public virtual ICollection<UserSendReportConfig> UserSendReportConfigs { get; set; }
    }
}
