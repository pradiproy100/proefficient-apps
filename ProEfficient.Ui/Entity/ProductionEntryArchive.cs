﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class ProductionEntryArchive
    {
        public int ProductionId { get; set; }
        public int? MachineId { get; set; }
        public int? MachineCategoryId { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }
        public int? OperatorId { get; set; }
        public int? SupervisorId { get; set; }
        public DateTime? TimeOfEntry { get; set; }
        public int? ShiftId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal? TotalProduction { get; set; }
        public int? ReWork { get; set; }
        public int? Rejection { get; set; }
        public DateTime? ProductionDate { get; set; }
        public bool? IsSaved { get; set; }
        public string RejectionList { get; set; }
        public bool IsDeleted { get; set; }
    }
}
