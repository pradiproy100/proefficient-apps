﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProEfficient.Ui.Entity
{
    public partial class RejectionReason
    {
        public int ReasonId { get; set; }
        public string ReasonName { get; set; }
        public string ReasonDescription { get; set; }
        public bool? IsActive { get; set; }
        public int? OrganizationId { get; set; }
    }
}
