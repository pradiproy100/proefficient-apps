using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.FeatureManagement;
using Microsoft.IdentityModel.Tokens;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using ProEfficient.Ui.Data;
using ProEfficient.Ui.Entity;
using System;
using System.Text;

namespace ProEfficient.Ui
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var dd = Configuration.GetValue<string>("ConnectionString");
            services.AddDbContext<ProefficientContext>(options => 
            options.UseSqlServer(Configuration.GetValue<string>("ConnectionString")));
            services.AddDbContext<DataContext>(x => x.UseSqlServer
           (Configuration.GetValue<string>("ConnectionString")));
            services.AddAuthorization();
            //Membership Authentication 
            IdentityBuilder builder = services.AddIdentityCore<Core.Models.User>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 4;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
                opt.User.RequireUniqueEmail = true;


            });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);   
            builder.AddEntityFrameworkStores<DataContext>();
            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<Core.Models.User>>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                    .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddAutoMapper(typeof(AuthRepository).Assembly);
            //
            services.AddAuthentication("ApiKeyAuth")
                //.AddCookie(CookieAuthenticationDefaults.AuthenticationScheme,options =>
                //{
                //    options.LoginPath = "/Login/Login";
                //    options.AccessDeniedPath = "/Login/Login";
                //    options.SlidingExpiration = true;
                //    options.ExpireTimeSpan = TimeSpan.FromHours(1);
                //})
                .AddScheme<ApiKeyAuthOpts, ApiKeyAuthHandler>("ApiKeyAuth", "ApiKeyAuth", opts =>
                {

                });

            services.AddControllersWithViews().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(Configuration.GetValue<int>("SessionIdleTimeout"));
            });
            //services.AddSession();
            services.AddHttpContextAccessor();
            services.AddFeatureManagement();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor)
        {
            app.UseSession();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();
            //string baseDir = env.ContentRootPath;
            //System.AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Path.Combine(baseDir, "App_Data"));

            app.UseRouting();
            UserUtility.Configure(httpContextAccessor, Configuration);
            UtilityAll.Configure(Configuration);
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default", 
                    pattern: "{controller=Login}/{action=Login}/{id?}");
                    //pattern: "{controller=HomeDashBoard}/{action=HomeDashBoard}/{id?}");
            });
        }
    }
}
