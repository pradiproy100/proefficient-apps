﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class MachineEfficiencyReportController : Controller
    {
        private readonly IConfiguration _configuration;

        public MachineEfficiencyReportController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: ProdEfficiency
        public ActionResult Index()
        {

            ProductionReportAllMachine objVw = new ProductionReportAllMachine();
            objVw.dtBreakdown = new System.Data.DataTable();
            return View(objVw);
        }
        [HttpPost]
        public async Task<ActionResult> DisplaySearchResults(ProductionReportAllMachine criteria, String command)
        {
            //var model = // build list based on the properties of criteria
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            OrganizationUser usr = UserUtility.CurrentUser;
            int uid = usr.UserId;
            try
            {
                if (command == "Search")
                {
                    string startdate = criteria.StartDate;
                    string enddate = criteria.EndDate;
                    DataSet ds = new DataSet();
                    BreakDownReport bcriteria = new BreakDownReport();

                    DataSet ds1 = new DataSet();
                    try
                    {
                        SearchCriteria ReportSearch = new SearchCriteria();
                        //startdate = startdate.Split('/')[2] + startdate.Split('/')[1] + startdate.Split('/')[0];
                        //enddate = enddate.Split('/')[2] + enddate.Split('/')[1] + enddate.Split('/')[0];
                        ReportSearch.StartDate = startdate;
                        ReportSearch.EndDate = enddate;
                        // ReportSearch.MachineID = criteria.MachineID.ToStringWithNullEmpty();
                        //  ReportSearch.ShiftID = criteria.ShiftId.ToStringWithNullEmpty();

                        startdate = startdate.Split('/')[2] + "-" + startdate.Split('/')[1] + "-" + startdate.Split('/')[0];
                        enddate = enddate.Split('/')[2] + "-" + enddate.Split('/')[1] + "-" + enddate.Split('/')[0];
                        criteria.StartDate = startdate;
                        criteria.EndDate = enddate;
                        criteria.UserId = uid;
                        criteria.OrgId = UserUtility.CurrentUser.OrganizationId.ToInteger0();

                        Session_ReportSearch = ReportSearch;
                        ds = await rd.GetMachineEfficiencyReport(criteria);

                        // ds = GetEfficiencyReport(criteria);
                        //bcriteria.MachineID = criteria.MachineID;
                        //bcriteria.StartDate = criteria.StartDate;
                        //bcriteria.EndDate = criteria.EndDate;
                        //bcriteria.UserID = uid;

                        //if (!string.IsNullOrEmpty(ShiftID))
                        //    criteria.ShiftId = Convert.ToInt32(ShiftID);
                        // ds1 = rd.GetBreakDownList(bcriteria);
                    }
                    catch (Exception Ex)
                    {

                    }
                    DataTable dt = new DataTable();

                    if (ds.Tables.Count > 0)
                    {
                        criteria.dtProd = ds.Tables[0];
                        if (ds.Tables.Count > 1)
                        {
                            criteria.dtAverage = ds.Tables[1];
                        }
                    }
                    else
                    {
                        criteria.dtProd = dt;
                    }


                    return View("Index", criteria);
                }

                return View("Index", criteria);
            }
            catch (Exception)
            {
                this.CreateObjectAlert("Data not found");
                return View("Index");
            }
        }

        public async Task<string> getData()
        {
            string MachineID = Request.Query["MachineID"];
            string StartDate = Request.Query["StartDate"];
            string EndDate = Request.Query["EndDate"];
            string ShiftID = Request.Query["ShiftID"];
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                ProductionReport criteria = new ProductionReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetProductionEffList(criteria);
            }
            catch (Exception Ex)
            {

            }
            if (ds.Tables.Count > 1)
            {
                dt = ds.Tables[1];

                sb.Append("{\"type\": \"line\",\"data\":{\"labels\": [");
                int cnt = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (cnt == dt.Rows.Count)
                        sb.Append("\"" + dr["Date"].ToString() + "\"],");
                    else
                        sb.Append("\"" + dr["Date"].ToString() + "\",");
                    cnt++;
                }
                sb.Append("\"datasets\": [");
                int colcnt = 2;

                for (int x = 2; x < dt.Columns.Count; x++)
                {
                    string col = dt.Columns[x].ColumnName;
                    sb.Append("{\"label\":\"" + col + "\",\"fill\": \"false\",");
                    if (x == 2)
                        sb.Append("\"backgroundColor\": \"#553cb7\",\"borderColor\": \"#553cb7\",\"pointColor\": \"rgba(10, 214, 38, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(20,220,220,15)\",\"data\":[");
                    else if (x == 3)
                        sb.Append("\"backgroundColor\": \"#1c6b73\",\"borderColor\": \"#1c6b73\",\"pointColor\": \"rgba(110, 214, 222, 1)\",\"pointStrokeColor\": \"#f442e8\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(120,88,220,1)\",\"data\":[");
                    else if (x == 4)
                        sb.Append("\"backgroundColor\": \"#0f7d1d\",\"borderColor\": \"#0f7d1d\",\"pointColor\": \"rgba(180, 114, 222, 1)\",\"pointStrokeColor\": \"#1a2e7a\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(180,100,220,10)\",\"data\":[");
                    else if (x == 5)
                        sb.Append("\"backgroundColor\": \"#7c187d\",\"borderColor\": \"#7c187d\",\"pointColor\": \"rgba(210, 14, 222, 1)\",\"pointStrokeColor\": \"#4c2718\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,20,220,1)\",\"data\":[");
                    else if (x == 6)
                        sb.Append("\"backgroundColor\": \"#6f630c\",\"borderColor\": \"#6f630c\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#034c03\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");
                    else if (x == 7)
                        sb.Append("\"backgroundColor\": \"#773933\",\"borderColor\": \"#773933\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");
                    else if (x == 8)
                        sb.Append("\"backgroundColor\": \"#e460a3\",\"borderColor\": \"#e460a3\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");
                    else
                        sb.Append("\"backgroundColor\": \"#f39e6d\",\"borderColor\": \"#f39e6d\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");

                    cnt = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (cnt == dt.Rows.Count)
                            sb.Append(dr[col].ToString() + "]");
                        else
                            sb.Append(dr[col].ToString() + ",");
                        cnt++;
                    }
                    if (colcnt != dt.Columns.Count - 1)
                        sb.Append("},");
                    else
                        sb.Append("}]");
                    colcnt++;
                }
                sb.Append("},");
                sb.Append("\"options\": {\"responsive\": \"true\",\"title\": {\"display\": \"false\",\"text\": \"\"},\"tooltips\": {\"mode\": \"index\",");
                sb.Append("\"intersect\": \"false\"},\"hover\": {\"mode\": \"nearest\",\"intersect\": \"true\"},\"scales\": {\"xAxes\": [{\"display\": \"true\",");

                sb.Append("\"scaleLabel\": {\"display\": \"true\",\"labelString\": \"Day\"}}],\"yAxes\": [{\"display\": \"true\",\"scaleLabel\": {\"display\": \"true\",\"labelString\": \"Value\"}}]}}}");
            }

            return (sb.ToString());
        }
        public async Task<string> getProductionRejectionData()
        {
            string MachineID = Request.Query["MachineID"];
            string StartDate = Request.Query["StartDate"];
            string EndDate = Request.Query["EndDate"];
            string ShiftID = Request.Query["ShiftID"];
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                ProductionReport criteria = new ProductionReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.getProductionRejectionData(criteria);
            }
            catch
            {

            }
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

                sb.Append("{\"labels\": [");
                int cnt = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (cnt == dt.Rows.Count)
                        sb.Append("\"" + dr["ProductName"].ToString() + "\"],");
                    else
                        sb.Append("\"" + dr["ProductName"].ToString() + "\",");
                    cnt++;
                }
                sb.Append("\"datasets\": [");
                int colcnt = 1;

                for (int x = 1; x < dt.Columns.Count; x++)
                {
                    string col = dt.Columns[x].ColumnName;
                    sb.Append("{\"label\":\"" + col + "\",");
                    if (x == 1)
                        sb.Append("\"backgroundColor\": \"#08206d\",\"data\":[");
                    else if (x == 2)
                        sb.Append("\"backgroundColor\": \"#8c0f1b\",\"data\":[");
                    else if (x == 4)
                        sb.Append("\"backgroundColor\": \"rgba(150, 159, 123, 10)\",\"strokeColor\": \"rgba(210, 195, 26, 1)\",\"pointColor\": \"rgba(180, 114, 222, 1)\",\"pointStrokeColor\": \"#1a2e7a\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(180,100,220,10)\",\"data\":[");
                    else if (x == 5)
                        sb.Append("\"backgroundColor\": \"rgba(60,141,188,0.8))\",\"strokeColor\": \"rgba(110, 214, 29, 1)\",\"pointColor\": \"rgba(210, 14, 222, 1)\",\"pointStrokeColor\": \"#4c2718\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,20,220,1)\",\"data\":[");
                    else if (x == 6)
                        sb.Append("\"backgroundColor\": \"rgba(210, 214, 222, 1)\",\"strokeColor\": \"rgba(150, 214, 4, 1)\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#034c03\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");
                    else if (x == 7)
                        sb.Append("\"backgroundColor\": \"rgba(60,141,188,0.8))\",\"strokeColor\": \"rgba(20, 214, 222, 1)\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");
                    else if (x == 8)
                        sb.Append("\"backgroundColor\": \"rgba(110, 24, 122, 10)\",\"strokeColor\": \"rgba(21, 214, 222, 1)\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");
                    else
                        sb.Append("\"backgroundColor\": \"rgba(110, 114, 122, 1)\",\"strokeColor\": \"rgba(210, 214, 38, 1)\",\"pointColor\": \"rgba(210, 214, 222, 1)\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(220,220,220,1)\",\"data\":[");

                    cnt = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (cnt == dt.Rows.Count)
                            sb.Append(dr[col].ToString() + "]");
                        else
                            sb.Append(dr[col].ToString() + ",");
                        cnt++;
                    }
                    if (colcnt != dt.Columns.Count - 1)
                        sb.Append("},");
                    else
                        sb.Append("}]");
                    colcnt++;
                }
                sb.Append("}");
            }

            return (sb.ToString());
        }

        #region chart generation 
        public async Task<ActionResult> CreateBar(string MachineID, string StartDate, string EndDate, string ShiftID)
        {
            //   string MachineID =ReportSearch.MachineID;
            //string StartDate = ReportSearch.StartDate;
            //string EndDate =ReportSearch.EndDate;
            //string ShiftID = ReportSearch.ShiftID;
            //Create bar chart
            //var chart = new Chart(width: 300, height: 200)
            //.AddSeries(chartType: "bar",
            //                xValue: new[] { "10 ", "50", "30 ", "70" },
            //                yValues: new[] { "50", "70", "90", "110" })
            //                .GetBytes("png");
            //return File(chart, "image/bytes");


            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                ProductionReport criteria = new ProductionReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetProductionEffList(criteria);

                var chart = new Chart(width: 700, height: 800);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var ColumnListArr = getColumnList(ds.Tables[0]);
                    chart.AddSeries(chartType: "bar",
                           xValue: ColumnListArr,
                           yValues: new[] { "0" });

                    return File(chart.GetBytes("png"), "image/bytes");
                }


                return File(chart.GetBytes("png"), "image/bytes");


            }
            catch (Exception Ex)
            {
                var chart = new Chart(width: 300, height: 200)
              .AddSeries(chartType: "bar",
                              xValue: new[] { "0" },
                              yValues: new[] { "0" })
                              .GetBytes("png");
                return File(chart, "image/bytes");
            }
        }

        public String[] getColumnList(DataTable Dt)
        {

            var ReasonList = new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList();

            List<String> LstColumns = new List<string>();
            int index = 0;
            foreach (DataColumn c in Dt.Columns)
            {
                index++;
                if (index == 1)
                    continue;
                LstColumns.Add(ReasonList.FirstOrDefault(i => i.ReasonCode == c.ColumnName).ReasonName);

            }

            return LstColumns.ToArray();

        }

        public ActionResult CreatePie()
        {
            //Create bar chart
            var chart = new Chart(width: 300, height: 200)
            .AddSeries(chartType: "pie",
                            xValue: new[] { "10 ", "50", "30 ", "70" },
                            yValues: new[] { "50", "70", "90", "110" })
                            .GetBytes("png");
            return File(chart, "image/bytes");
        }

        public ActionResult CreateLine()
        {
            //Create bar chart
            var chart = new Chart(width: 600, height: 200)
            .AddSeries(chartType: "line",
                            xValue: new[] { "10 ", "50", "30 ", "70" },
                            yValues: new[] { "50", "70", "90", "110" })
                            .GetBytes("png");
            return File(chart, "image/bytes");
        }

        public async Task<DataSet> GetEfficiencyReport(ProductionReportAllMachine criteria)
        {
            DataSet ds = new DataSet();
            ProductionReport _criteria = new ProductionReport();
            _criteria.StartDate = criteria.StartDate;
            _criteria.EndDate = criteria.EndDate;
            _criteria.ShiftId = criteria.ShiftId;
            var AllMachines = UserUtility.GetMachineIdsByCategory(criteria.MachineID.ToInteger0());
            var reportDal = new ReportService(_configuration).DataAccessLayer;
            var AllReport = new DataTable();
            if (AllMachines != null)
            {

                foreach (var machine in AllMachines)
                {
                    _criteria.MachineID = machine.MachineID;

                    var _report = await reportDal.GetProductionEffList(_criteria);

                    if (_report != null && _report.Tables.Count > 0)
                    {
                        AllReport.Merge(_report.Tables[0]);
                    }
                }

            }
            ds.Tables.Add(AllReport);
            return ds;
        }
        #endregion
        #region Session Property
        public SearchCriteria Session_ReportSearch
        {
            get

            {
                var data = HttpContext.Session.GetString("SearchCriteria");
                return JsonConvert.DeserializeObject<SearchCriteria>(data);
            }
            set
            {

                var data = JsonConvert.SerializeObject(value);
                HttpContext.Session.SetString("SearchCriteria", data);

            }
        }
        #endregion

    }

}