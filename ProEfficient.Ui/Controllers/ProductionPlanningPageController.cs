﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductionPlanningPageController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string apiBaseUrl;
        private List<MasterProduct> ListOfProducts;
        private List<MasterMachine> ListOfMachine;
        public ProductionPlanningPageController(IConfiguration configuration)
        {
            _configuration = configuration;
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
            ListOfProducts = new List<MasterProduct>();
            ListOfMachine = new List<MasterMachine>();


            var objProduct = new ProductService(_configuration).DataAccessLayer;

            ListOfProducts = objProduct.GetMasterProductList().
                 FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId).
                 OrderByDescending(i => i.ProductId).ToList();

            var objMachine = new MachineService(_configuration).DataAccessLayer;

            ListOfMachine = objMachine.GetMasterMachineList()
                  .FindAll(i => i.OrganizationId == UserUtility.
                  CurrentUser.OrganizationId).OrderByDescending(i => i.MachineID).ToList();


        }
        // GET: ProductionPage
        [HttpGet]
        public ActionResult Index()
        {
            List<vwMachineProductPlanning> machineproductplanning = new List<vwMachineProductPlanning>();

            try
            {
                int OrganizationIDofloginuser = UserUtility.CurrentUser.OrganizationId.ToInteger0();
                int CurrentUserId = UserUtility.CurrentUser.Id;
                var proDate = DateTime.Now.ToString("yyyy-MM-dd");

                var TodaysProcution = GetTodaysProductionPlanning();
                foreach (var eachmachine in ListOfMachine)
                {
                    var todaysProductForThismachine = ListOfProducts.FirstOrDefault();

                    if (TodaysProcution != null
                        && TodaysProcution.FirstOrDefault(i => i.MachineId == eachmachine.MachineID) != null)
                    {
                        todaysProductForThismachine = ListOfProducts.FirstOrDefault(i => i.ProductId ==
                         TodaysProcution.FirstOrDefault(i => i.MachineId == eachmachine.MachineID).ProductId);
                    }
                    machineproductplanning.Add(new vwMachineProductPlanning()
                    {
                        machine = eachmachine,
                        product = todaysProductForThismachine
                    });
                }

                ProductListLoad();
                ProductPlanningLoad();
            }
            catch (Exception ex)
            {
                
            }

            return View(machineproductplanning);
        }

        private void ProductPlanningLoad()
        {

            int day = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            DateTime PlanningEndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, day);

            var ProductionPlanning = new ProductionPlanningDAL(_configuration).GetListOfProductionPlanningBySheduleDateRange(
                  DateTime.Now, PlanningEndDate, UserUtility.CurrentUser.OrganizationId.ToInteger0());

            ViewBag.ProductionPlanningList = ProductionPlanning;
        }
        private List<ProductionPlanning> GetTodaysProductionPlanning()
        {
            var ProductionPlanning = new ProductionPlanningDAL(_configuration).GetListOfProductionPlanningBySheduleDateRange(
              DateTime.Now, DateTime.Now, UserUtility.CurrentUser.OrganizationId.ToInteger0());
            return ProductionPlanning;
        }

        [HttpPost]
        public ActionResult Index(IFormCollection forms)
        {
            int OrganizationIDofloginuser = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            int CurrentUserId = UserUtility.CurrentUser.Id;

            SaveFormData(forms);


            var proDate = DateTime.Now.ToString("yyyy-MM-dd");
            List<vwMachineProductPlanning> machineproductplanning = new List<vwMachineProductPlanning>();

            var TodaysProcution = GetTodaysProductionPlanning();
            foreach (var eachmachine in ListOfMachine)
            {
                var todaysProductForThismachine = ListOfProducts.FirstOrDefault();

                if (TodaysProcution != null &&
                     TodaysProcution.FirstOrDefault(i => i.MachineId == eachmachine.MachineID) != null)
                {
                    todaysProductForThismachine = ListOfProducts.FirstOrDefault(i => i.ProductId ==
                     TodaysProcution.FirstOrDefault(i => i.MachineId == eachmachine.MachineID).ProductId);
                }




                machineproductplanning.Add(new vwMachineProductPlanning()
                {
                    machine = eachmachine
                ,
                    product = todaysProductForThismachine

                });
            }
            ProductListLoad();
            ProductPlanningLoad();
            return View(machineproductplanning);

        }

        private Boolean SaveFormData(IFormCollection forms)
        {
            DateTime PlanningStartDate = DateTime.Now;
            DateTime PlanningEndDate = DateTime.Now;
            Boolean IsRestOfTheMonth = false;
            if (forms["IsRestOfTheMonth"] == "RestOfTheMonth")
            {
                int day = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                PlanningEndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, day);
                IsRestOfTheMonth = true;
            }




            try
            {
                //using (var transaction = new TransactionScope())
                //{

                    new ProductionPlanningDAL(_configuration).DeleteProductionPlanning(new ProductionPlanning()
                    {
                        StartDate = PlanningStartDate,
                        EndDate = PlanningEndDate
          ,
                        OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0()
                    });

                    for (int i = 1; i <= ListOfMachine.Count; i++)
                    {
                        var production_Planning = new ProductionPlanning()
                        {
                            AddByUser = UserUtility.CurrentUser.Id,
                            EditByUser = UserUtility.CurrentUser.Id,
                            AddDate = DateTime.Now,
                            EditDate = DateTime.Now,
                            StartDate = PlanningStartDate,
                            EndDate = PlanningEndDate,
                            MachineId = forms["MachineId_" + i].ToString().ToInteger0(),
                            OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0(),
                            ProductId = forms["ddlProductList_" + i].ToString().ToInteger0(),
                            SheduleDate = PlanningStartDate

                        };
                        new ProductionPlanningDAL(_configuration).UpdateProductionPlanning(production_Planning, IsRestOfTheMonth);
                    }


                //    transaction.Complete();
                //}

            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }


        private void ProductListLoad()
        {
            var productOptioList = ListOfProducts.Select(item => new SelectListItem()
            {
                Value = item.ProductId.ToString(),
                Text = item.ProductName
            }).ToList();
            ViewBag.ProductList = productOptioList;
        }
    }
}