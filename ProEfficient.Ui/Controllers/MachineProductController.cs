﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using ProEfficient.Ui.ExcelUploadUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Transactions;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class MachineProductController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<MachineProductController> _logger;
        private readonly MqttClient _mqttClient;

        public MachineProductController(IConfiguration configuration, ILogger<MachineProductController> logger)
        {
            _configuration = configuration;
            _logger = logger;
            _mqttClient = new MqttClient(configuration.GetValue<string>("MqttIpAddress"));
        }
        public IActionResult Index()
        {
            LoadViewBags();
            int OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            ProductMachineViewModel model = new ProductMachineViewModel();
            var objMachine = new MachineProductService(_configuration).DataAccessLayer;
            List<PMMachineVM> machineLst = objMachine.GetMachineList(OrganizationId, "machine").ToList();
            if (machineLst.Count > 0)
            {
                model.lstproductmachineMachineVM = machineLst;
            }
            else
            {
                model.lstproductmachineMachineVM = new List<PMMachineVM>();
            }
            var objProduct = new MachineProductService(_configuration).DataAccessLayer;
            List<PMProductVM> productLst = objMachine.GetProductList(OrganizationId, "product").ToList();
            if (productLst.Count > 0)
            {
                model.lstproductmachineProductVM = productLst;
            }
            else
            {
                model.lstproductmachineProductVM = new List<PMProductVM>();
            }
            model.lstproductmachineVM = new List<ProductMachineVM>();
            model.productMachineVMSelect = new ProductMachineVMSelect();
            model.productMachineVMSelect.OrganizationId = OrganizationId;
            
            return View(model);
        }
        [HttpPost]
        public IActionResult SaveProductionEntry()
        {
            try
            {
                var objProductDal = new MachineProductService(_configuration).DataAccessLayer;
                var mappingCount = objProductDal.GetMachineProductMap(UserUtility.CurrentUser.OrganizationId.ToInteger0());
                int index = 1;

                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (var map in mappingCount)
                    {
                        String IsCheckValue = Request.Form["SelectedMapping_" + index.ToString()].ToStringx().ToLower();
                        if (String.IsNullOrEmpty(IsCheckValue))
                        {
                            IsCheckValue = "false";
                        }
                        if (IsCheckValue.ToBoolean())
                        {
                            var obj = new UtilityMasterMapping()
                            {
                                MachineId = Request.Form["hdMachineId_" + index.ToString()].ToStringx().ToInteger0(),
                                ProductId = Request.Form["ddlProductList_" + index.ToString()].ToStringx().ToInteger0(),
                                OrganizationId = UserUtility.CurrentUser.OrganizationId.ToStringx().ToInteger0(),
                                DieCavity = Request.Form["Die_Cavity_" + index.ToString()].ToStringx().ToInteger0(),
                                MouldCavity = Request.Form["Mould_Cavity_" + index.ToString()].ToStringx().ToInteger0(),
                                DieId = Request.Form["Die_" + index.ToString()].ToStringx().ToInteger0(),
                                MouldId = Request.Form["Mould_" + index.ToString()].ToStringx().ToInteger0(),
                                OperationId = Request.Form["Operations_" + index.ToString()].ToStringx().ToInteger0(),
                                OperatorId = Request.Form["Operators_" + index.ToString()].ToStringx().ToInteger0(),
                                TargetProduction = Request.Form["TargetProduction_" + index.ToString()].ToStringx().ToInteger0()
                            };
                            if (obj != null && obj.OrganizationId != null && obj.MachineId != null && obj.ProductId != null)
                            {
                                var objMachineService = new MachineProductService(_configuration).DataAccessLayer;
                                objMachineService.UpdateMasterMapping(obj);

                                // Publish to MQTT code
                                PublishToMqtt(JsonConvert.SerializeObject(obj));
                            }
                        }
                        index++;
                    }
                    scope.Complete();
                }
                TempData["SavedMachineProductMap"] = true;
                MessageUtility.ShowSuccessAlertMessage(this, "Data saved successfully...");
            }
            catch (Exception)
            {

                MessageUtility.ShowErrorAletMessage(this, "Data saving error...");
            }
            return RedirectToAction("Index");
        }
        private void PublishToMqtt(string value)
        {
            try
            {
                _logger.LogInformation($"{nameof(PublishToMqtt)} execute..");
                string topic = _configuration.GetValue<string>("PublishTopicForTargetCount");

                if (!_mqttClient.IsConnected)
                {
                    _mqttClient.Connect(topic);
                }
                var data = Encoding.UTF8.GetBytes(value);
                _mqttClient.Publish(topic, data, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(PublishToMqtt)} - Error occur..");
            }
        }
        private void LoadMachineProductViewBag()
        {
            try
            {
                var objProductDal = new MachineProductService(_configuration).DataAccessLayer;
                ViewBag.MachineProductMapList = objProductDal.GetMachineProductMap(UserUtility.CurrentUser.OrganizationId.ToInteger0());
                var productDataAccess = new ProductService(_configuration).DataAccessLayer;
                var productList = productDataAccess.GetMasterProductList(UserUtility.CurrentUser.OrganizationId);
                var productOptioList = productList.Select(item => new SelectListItem()
                {
                    Value = item.ProductId.ToString(),
                    Text = item.ProductName                    
                }).ToList();
                productOptioList.Insert(0, new SelectListItem() { Value = "0", Text = "Please select a product" });
                ViewBag.ProductList = productOptioList;
                ViewBag.ProductCycleList =JsonConvert.SerializeObject( productList.Select(i => new { productId = i.ProductId, cycletime = i.CycleTime }));
            }
            catch (Exception)
            {

               
            }

        }

        public ActionResult MachineDemandType()
        {
            int OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            var objMachine = new MachineProductService(_configuration).DataAccessLayer;
            List<PMMachineVM> productMachineLst = objMachine.GetMachineList(OrganizationId, "machine").ToList();
            if (productMachineLst.Count > 0)
            {
                return PartialView("_machines", productMachineLst);
            }
            else { return Content("No Record Found"); }

        }
        [HttpPost]
        public ActionResult MachineDemandTypePost(IList<PMMachineVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("MachineId", typeof(int));
            UDT.Columns.Add("ProductId", typeof(int));
            UDT.Columns.Add("CycleTime", typeof(decimal));
            UDT.Columns.Add("DemandTypeText", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].isChecked == true)
                {
                    DataRow DR = UDT.NewRow();
                    DR["MachineId"] = model[i].MachineID;
                    DR["ProductId"] = 0;
                    DR["CycleTime"] = 0;
                    DR["DemandTypeText"] = model[i].MachineDemandType;
                    UDT.Rows.Add(DR);
                }
            }
            int currentUserId = UserUtility.CurrentUser.Id;
            new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT, currentUserId, "machine");

            return RedirectToAction("MachineDemandType");
        }
        public ActionResult ProductDemandType()
        {
            int OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            var objMachine = new MachineProductService(_configuration).DataAccessLayer;
            List<PMProductVM> productMachineLst = objMachine.GetProductList(OrganizationId, "product").ToList();
            if (productMachineLst.Count > 0)
            {
                return PartialView("_products", productMachineLst);
            }
            else { return Content("No Record Found"); }

        }
        [HttpPost]
        public ActionResult ProductDemandTypePost(IList<PMProductVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("MachineId", typeof(int));
            UDT.Columns.Add("ProductId", typeof(int));
            UDT.Columns.Add("CycleTime", typeof(decimal));
            UDT.Columns.Add("DemandTypeText", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].isChecked == true)
                {
                    DataRow DR = UDT.NewRow();
                    DR["MachineId"] = 0;
                    DR["ProductId"] = model[i].ProductId;
                    DR["CycleTime"] = 0;
                    DR["DemandTypeText"] = model[i].ProductDemandType;
                    UDT.Rows.Add(DR);
                }
            }
            int currentUserId = UserUtility.CurrentUser.Id;
            new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT, currentUserId, "product");

            return RedirectToAction("ProductDemandType");
        }
        [HttpPost]
        public ActionResult MachineProductSelect(ProductMachineVMSelect model)
        {
            if (model.arrMachineIds != null) { model.MachineIds = string.Join(",", model.arrMachineIds); }
            if (model.arrProductIds != null) { model.ProductIds = string.Join(",", model.arrProductIds); }
            var objMachineProduct = new MachineProductService(_configuration).DataAccessLayer;
            List<ProductMachineVM> productMachineLst = objMachineProduct.GetMachineProductList(model).ToList();
            var objMachineService = new MachineProductService(_configuration).DataAccessLayer;
            var mappings = objMachineService.GetUtilityMasterMapping(UserUtility.CurrentUser.OrganizationId);
            if (productMachineLst.Count > 0)
            {
                productMachineLst.ForEach(x =>
                {
                    x.IsMapped = true;
                    x.mapping = mappings.FirstOrDefault(i => i.ProductId == x.ProductId && i.MachineId == x.MachineID);
                });
                LoadViewBags();
                return PartialView("_machineProductsList", productMachineLst);
            }
            else
            {
                LoadViewBags();
                return Content("No Record Found");
            }
            LoadMqttConfigurationData();
        }
        [HttpPost]
        public ActionResult MachineProductList(IList<ProductMachineVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("MachineId", typeof(int));
            UDT.Columns.Add("ProductId", typeof(int));
            UDT.Columns.Add("CycleTime", typeof(decimal));
            UDT.Columns.Add("DemandTypeText", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].IsMapped == true)
                {
                    DataRow DR = UDT.NewRow();
                    DR["MachineId"] = model[i].MachineID;
                    DR["ProductId"] = model[i].ProductId;
                    DR["CycleTime"] = model[i].ProductCycleTime;
                    DR["DemandTypeText"] = "";
                    UDT.Rows.Add(DR);
                }
            }
            int currentUserId = UserUtility.CurrentUser.Id;
            new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT, currentUserId, "mapping");

            List<ProductMachineVM> li = new List<ProductMachineVM>();
            LoadViewBags();
            return PartialView("_machineProductsList", li);
        }

        private void LoadViewBags()
        {
            var OperatorService = new OperatorService(_configuration).DataAccessLayer;
            var OperationsService = new OperationService(_configuration).DataAccessLayer;
            var DieService = new DieService(_configuration).DataAccessLayer;
            var MouldService = new MouldService(_configuration).DataAccessLayer;

            var AllOperators = OperatorService.
                GetMasterOperatorList(UserUtility.CurrentUser.OrganizationId.ToInteger0());


            if (AllOperators != null)
            {
                ViewBag.AllOperators = AllOperators.GetSelectList("OperatorId", "OperatorName");
            }
            var AllOperations = OperationsService.GetMasterOperationList
                (UserUtility.CurrentUser.OrganizationId.ToInteger0());
            if (AllOperations != null)
            {
                ViewBag.AllOperations = AllOperations.GetSelectList("OperationId", "OperationDescription");
            }
            var AllDies = DieService.
                  GetMasterDieList(UserUtility.CurrentUser.OrganizationId.ToInteger0());

            if (AllDies != null)
            {
                ViewBag.AllDies = AllDies;
                ViewBag.ProductDieMapping = new ProductDieService(_configuration).DataAccessLayer.GetProductDieRel(UserUtility.CurrentUser.OrganizationId.ToInteger0());
            }
            var AllMoulds = MouldService.
                GetMasterMouldList(UserUtility.CurrentUser.OrganizationId.ToInteger0());
            if (AllMoulds != null)
            {
                ViewBag.AllMoulds = AllMoulds;
                ViewBag.ProductMouldMapping = new ProductMouldService(_configuration).DataAccessLayer.GetProductMouldRel(UserUtility.CurrentUser.OrganizationId.ToInteger0());
            }

            LoadMqttConfigurationData();
            LoadMachineProductViewBag();

        }
        private void LoadMqttConfigurationData()
        {
            ViewBag.PublishTopic = UtilityAll.PublishTopic;
            ViewBag.SubscribeTopic = UtilityAll.SubscribeTopic;
            ViewBag.MqttIpAddress = UtilityAll.MqttIpAddress;
            ViewBag.MqttPortNo = UtilityAll.MqttPortNo;
            ViewBag.ClientId = UtilityAll.ClientId;
            LoadMqttMapping();
        }
        private void LoadMqttMapping()
        {
            var objMachineService = new MachineProductService(_configuration).DataAccessLayer;
            var AllModule = objMachineService.GetUtilityModule(UserUtility.CurrentUser.OrganizationId.ToInteger0());

            if (AllModule != null)
            {
                ViewBag.IsMould = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Mould".ToLower()) == null ? false : true;
                ViewBag.IsDie = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Die".ToLower()) == null ? false : true;
                ViewBag.IsOperator = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Operator".ToLower()) == null ? false : true;
                ViewBag.IsOperation = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Operation".ToLower()) == null ? false : true;
            }


        }
        [HttpGet]
        public void SaveMachineUtilityMapping(
               string id
            )
        {
            id = id.Replace("id=", "").Replace("'", "");

            var obj = JsonConvert.DeserializeObject<UtilityMasterMapping>(id);
            if (obj != null && obj.OrganizationId != null && obj.MachineId != null && obj.ProductId != null)
            {
                var objMachineService = new MachineProductService(_configuration).DataAccessLayer;
                objMachineService.UpdateMasterMapping(obj);
            }

        }
        [HttpGet]
        public int  GetMouldCavityCount(
             string id
          )
        {
            id = id.Replace("'", "").Replace("id=", "");
            var mouldMasterDal = new MouldService(_configuration).DataAccessLayer;
            var mould = mouldMasterDal.GetMasterMouldById(UserUtility.CurrentUser.OrganizationId.ToInteger0(), id.ToInteger0());
            if (mould != null)
            {
               return mould.AvailableCavityCount.ToInteger0();
            }
            return 0;

        }
        [HttpGet]
        public int GetDieCavityCount(
            string id
         )
        {
            id = id.Replace("'", "").Replace("id=","");
            var DieMasterDal = new DieService(_configuration).DataAccessLayer;
            var Die = DieMasterDal.GetMasterDieById(UserUtility.CurrentUser.OrganizationId.ToInteger0(), id.ToInteger0());
            if (Die != null)
            {
                return Die.AvailableCavityCount.ToInteger0();
            }
            return 0;

        }


        [HttpGet]
        public Boolean SaveUtilityModuleConfig(
               string id
            )
        {
            id = id.Replace("id=", "").Replace("'", "");

            try
            {
                var obj = JsonConvert.DeserializeObject<UtilityModuleParam>(id);
                obj.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
                if (obj != null && obj.OrganizationId != 0)
                {
                    var objMachineService = new MachineProductService(_configuration).DataAccessLayer;
                    objMachineService.USP_SaveUtilityModule(obj);
                }
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        [HttpGet]
        public bool SuperAdminLogin(
              string id
           )
        {
            string passWord = id.Replace("id=", "").Replace("'", "");

            if (!String.IsNullOrEmpty(passWord))
            {
                passWord = Base64Encode(passWord);
                if(passWord == UtilityAll.UtilityModulePassword)
                {
                    return true;
                }

            }
            return false;

        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        //UtilityAll.PublishTopic
    }
}
