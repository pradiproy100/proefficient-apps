﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using ProEfficient.Ui.Entity;
using System.Collections.Generic;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class AutoReportController : Controller
    {
        private readonly ProefficientContext _context;
        private readonly IConfiguration _configuration;
        private int CurrentOrgId;
        public AutoReportController(ProefficientContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            CurrentOrgId = UserUtility.CurrentUser.OrganizationId;
        }

        public SelectList GetFreequency()
        {
            List<SelectListItem> freequency = new List<SelectListItem>();
            freequency.Add(new SelectListItem() { Text = "Daily", Value = "Daily" });
            freequency.Add(new SelectListItem() { Text = "Weekly", Value = "Weekly" });
            freequency.Add(new SelectListItem() { Text = "Monthly", Value = "Monthly" });
            return new SelectList(freequency, "Value", "Text");
        }
        public SelectList GetWeekDays()
        {
            List<SelectListItem> weekdays = new List<SelectListItem>();
            weekdays.Add(new SelectListItem() { Text = "Monday", Value = "Monday" });
            weekdays.Add(new SelectListItem() { Text = "Tuesday", Value = "Tuesday" });
            weekdays.Add(new SelectListItem() { Text = "Wednesday", Value = "Wednesday" });
            weekdays.Add(new SelectListItem() { Text = "Thursday", Value = "Thursday" });
            weekdays.Add(new SelectListItem() { Text = "Friday", Value = "Friday" });
            weekdays.Add(new SelectListItem() { Text = "Saturday", Value = "Saturday" });
            weekdays.Add(new SelectListItem() { Text = "Sunday", Value = "Sunday" });
            return new SelectList(weekdays, "Value", "Text");
        }
        public ActionResult Index()
        {
            var proefficientContext = _context.UserSendReportConfigs.Include(u => u.ReportGroup).Include(u => u.ReportType);
            return View(proefficientContext.ToList());
        }

       
        // GET: AutoReport/Create
        public IActionResult Create()
        {
            ViewData["ReportGroupId"] = new SelectList(_context.UserReportsConfigs, "UserMachineReportId", "ReportName");
            ViewData["ReportTypeId"] = new SelectList(_context.ReportTypes, "ReportId", "ReportName");
            ViewData["UserIds"] = new SelectList(_context.AspNetUsers.Where(m => m.OrganizationId == CurrentOrgId), "Id", "UserName");
            ViewData["DateRange"] = GetFreequency();
            ViewData["ValidDays"] = GetWeekDays();
            return View();
        }

        // POST: AutoReport/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserSendReportConfig userSendReportConfig)
        {
            if (ModelState.IsValid)
            {
                    if (userSendReportConfig.arrValidDays != null) { userSendReportConfig.ValidDays = string.Join(",", userSendReportConfig.arrValidDays); }
                if (userSendReportConfig.arrRecipients != null) { userSendReportConfig.MailTo = string.Join(",", userSendReportConfig.arrRecipients); }
                if (string.IsNullOrEmpty(userSendReportConfig.ValidDays) && userSendReportConfig.DateRange == "Daily")
                {
                    userSendReportConfig.ValidDays = "Monday";
                }
                else if (string.IsNullOrEmpty(userSendReportConfig.WeekStartDay) && userSendReportConfig.DateRange == "Weekly")
                {
                    userSendReportConfig.ValidDays = "Monday";
                }
                else if (userSendReportConfig.DateRange == "Monthly")
                {
                    userSendReportConfig.MonthStartDate = 1;
                }
                userSendReportConfig.OrganizationId = CurrentOrgId;
                _context.Add(userSendReportConfig);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ReportGroupId"] = new SelectList(_context.UserReportsConfigs, "UserMachineReportId", "ReportName", userSendReportConfig.ReportGroupId);
            ViewData["ReportTypeId"] = new SelectList(_context.ReportTypes, "ReportId", "ReportName", userSendReportConfig.ReportTypeId);
            ViewData["UserIds"] = new SelectList(_context.AspNetUsers.Where(m => m.OrganizationId == CurrentOrgId), "Id", "UserName");
            ViewData["DateRange"] = GetFreequency();
            ViewData["ValidDays"] = GetWeekDays();
            return View(userSendReportConfig);
        }

        // GET: AutoReport/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userSendReportConfig = _context.UserSendReportConfigs.Find(id);
            if (userSendReportConfig == null)
            {
                return NotFound();
            }
            ViewData["ReportGroupId"] = new SelectList(_context.UserReportsConfigs, "UserMachineReportId", "ReportName", userSendReportConfig.ReportGroupId);
            ViewData["ReportTypeId"] = new SelectList(_context.ReportTypes, "ReportId", "ReportName", userSendReportConfig.ReportTypeId);
            ViewData["UserIds"] = new SelectList(_context.AspNetUsers.Where(m => m.OrganizationId == CurrentOrgId), "Id", "UserName");
            ViewData["DateRange"] = GetFreequency();
            ViewData["ValidDays"] = GetWeekDays();
            return View(userSendReportConfig);
        }

        // POST: AutoReport/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult Edit(int id, [Bind("UserSendReportConfigId,ReportGroupId,ReportTypeId,ReportName,DateRange,ValidDays,WeekStartDay,MonthStartDate,ReportSentTime,MailTo,OrganizationId")] UserSendReportConfig userSendReportConfig)
        {
            if (id != userSendReportConfig.UserSendReportConfigId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userSendReportConfig);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserSendReportConfigExists(userSendReportConfig.UserSendReportConfigId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ReportGroupId"] = new SelectList(_context.UserReportsConfigs, "UserMachineReportId", "ReportName", userSendReportConfig.ReportGroupId);
            ViewData["ReportTypeId"] = new SelectList(_context.ReportTypes, "ReportId", "ReportName", userSendReportConfig.ReportTypeId);
            ViewData["UserIds"] = new SelectList(_context.AspNetUsers.Where(m => m.OrganizationId == CurrentOrgId), "Id", "UserName");
            ViewData["DateRange"] = GetFreequency();
            ViewData["ValidDays"] = GetWeekDays();
            return View(userSendReportConfig);
        }

        //// GET: AutoReport/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var userSendReportConfig = await _context.UserSendReportConfigs
        //        .Include(u => u.UserSendReportConfig1)
        //        .Include(u => u.UserSendReportConfigNavigation)
        //        .FirstOrDefaultAsync(m => m.UserSendReportConfigId == id);
        //    if (userSendReportConfig == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(userSendReportConfig);
        //}

        //// POST: AutoReport/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var userSendReportConfig = await _context.UserSendReportConfigs.FindAsync(id);
        //    _context.UserSendReportConfigs.Remove(userSendReportConfig);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        private bool UserSendReportConfigExists(int id)
        {
            return _context.UserSendReportConfigs.Any(e => e.UserSendReportConfigId == id);
        }
    }
}
