﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class BreakdownEntryEditController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string apiBaseUrl;

        public BreakdownEntryEditController(IConfiguration configuration)
        {
            _configuration = configuration;
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
        }
        // GET: ProductionPage

        public ActionResult Index()
        {
            int OrganizationIDofloginuser = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            int CurrentUserId = UserUtility.CurrentUser.Id;

            var proDate = DateTime.Now.ToString("yyyy-MM-dd");
            var productionBreakDownEntryViewModel = new BreakdownEntryEditPageVM();
            // var productionEntryUI = new ProductionPageVM();           
            productionBreakDownEntryViewModel.UserId = CurrentUserId;
            productionBreakDownEntryViewModel.OrganizationId = OrganizationIDofloginuser;
            //using (var httpClient = new HttpClient())
            //{                
            //    httpClient.BaseAddress = new Uri(apiBaseUrl);
            //    httpClient.DefaultRequestHeaders.Accept.Clear();
            //    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    //GET Method  
            //    HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryUI?ProdDate=" + proDate + "&organizationID=" + OrganizationIDofloginuser);
            //    if (response.IsSuccessStatusCode)
            //    {
            //        string apiResponse = await response.Content.ReadAsStringAsync();
            //        productionEntryUI = JsonConvert.DeserializeObject<ProductionPageVM>(apiResponse);  
            //    }
            //}
            // productionBreakDownEntryViewModel.lstAllShift =productionEntryUI.lstshiftData.ToSelectList(c => c.ShiftId.ToString(), c => c.ShiftName);

            productionBreakDownEntryViewModel.lstAllMachine = UserUtility.AllMachine; //productionEntryUI.lstmachineProductData.ToSelectList(c => c.MachineId.ToString(), c => c.MachineName);
                                                                                      //productionBreakDownEntryViewModel.lstAllStopageReason = productionEntryUI.lststopageData.ToSelectList(c => c.ReasonId.ToString(), c => c.ReasonName);
                                                                                      // productionBreakDownEntryViewModel.lstAllSubStopageReason = productionEntryUI.lststopageData;
            productionBreakDownEntryViewModel.lstBreakdownEntryEditVM = new List<BreakdownEntryEditVM>();
            BreakdownSearchVM breakdownSearchViewModel = new BreakdownSearchVM();
            breakdownSearchViewModel.lstAllShift = UserUtility.AllShift; //productionEntryUI.lstshiftData.ToSelectList(c => c.ShiftId.ToString(), c => c.ShiftName);
            breakdownSearchViewModel.ProductionDate = proDate;
            productionBreakDownEntryViewModel.breakdownSearchVM = breakdownSearchViewModel;
            return View(productionBreakDownEntryViewModel);
        }
        [HttpPost]
        public ActionResult GetBreakdowns(BreakdownSearchVM model)
        {
            var objBreakdown = new BreakDownService(_configuration).DataAccessLayer;
            List<BreakdownEntryEditVM> objBreakdownlst = objBreakdown.GetBreakDownEntryEdit(model.ProductionDate, model.ShiftId).ToList();
            if (objBreakdownlst.Count > 0)
            {
                List<BreakdownEntryEditVM> objBreakdownlstFilter = new List<BreakdownEntryEditVM>();
                decimal.TryParse(_configuration["TotalStoppageTime"], out decimal totalDuration);
                if (totalDuration > 0)
                {
                    foreach (var item in objBreakdownlst)
                    {
                        string[] totalStoppage = item.TotalStoppageTime.Split(':');
                        if (totalStoppage.Length == 3 && Convert.ToDecimal(totalStoppage[0]) * 60 + Convert.ToDecimal(totalStoppage[1]) >= totalDuration)
                        {
                            objBreakdownlstFilter.Add(item);
                        }
                    }

                    ViewBag.ActiveReasons = UserUtility.ActiveReasons;
                    ViewBag.SubStoppageReasons = UserUtility.GetSubstoppagereasonlst();
                }
                return PartialView("_breakdownList", objBreakdownlstFilter);
            }
            else { return Content("No Record Found"); }
        }
        [HttpPost]
        public ActionResult EditBreakdownEntries(IList<BreakdownEntryEditVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("BreakdownId", typeof(int));
            UDT.Columns.Add("ReasonId", typeof(int));
            UDT.Columns.Add("SubReasonId", typeof(int));
            UDT.Columns.Add("Remarks", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                //if (model[i].IsMapped == true)
                //{
                DataRow DR = UDT.NewRow();
                DR["BreakdownId"] = model[i].BreakdownId;
                DR["ReasonId"] = model[i].ReasonId;
                DR["SubReasonId"] = model[i].SubReasonId;
                DR["Remarks"] = model[i].Remarks;
                UDT.Rows.Add(DR);
                // }
            }
            int currentUserId = UserUtility.CurrentUser.Id;
            bool isSaved = new BreakDownService(_configuration).DataAccessLayer.EditBreakDownEntry(UDT);
            List<BreakdownEntryEditVM> li = new List<BreakdownEntryEditVM>();
            if (isSaved)
            {
                return PartialView("_breakdownList", li);
            }
            else { return Content("Error in saving Data. Please contact Administrator"); }
        }


    }
}