﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductionBreakdownEntryController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string apiBaseUrl;

        public ProductionBreakdownEntryController(IConfiguration configuration)
        {
            _configuration = configuration;
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
        }
        // GET: ProductionPage

        public async Task<ActionResult> Index()
        {
            int OrganizationIDofloginuser = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            int CurrentUserId = UserUtility.CurrentUser.Id;

            var proDate = DateTime.Now.ToString("yyyy-MM-dd");


            var productionBreakDownEntryViewModel = new ProductionBreakDownEntryViewModel();
            var productionEntryUI = new ProductionPageVM();
            productionBreakDownEntryViewModel.ProductionDate = DateTime.ParseExact(proDate, "yyyy-MM-dd", CultureInfo.InvariantCulture); //Can also use .ToString("dd-MM-yyyy");
            productionBreakDownEntryViewModel.CurrentOrganizationId = OrganizationIDofloginuser;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryUI?ProdDate=" + proDate + "&ProdToDate=" + proDate + "&organizationID=" + OrganizationIDofloginuser);
                if (response.IsSuccessStatusCode)
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    productionEntryUI = JsonConvert.DeserializeObject<ProductionPageVM>(apiResponse);
                }
            }
            productionBreakDownEntryViewModel.lstAllShift = productionEntryUI.lstshiftData.ToSelectList(c => c.ShiftId.ToString(), c => c.ShiftName);
            productionBreakDownEntryViewModel.CurrentUserId = CurrentUserId;
            return View(productionBreakDownEntryViewModel);
        }
        [HttpGet]
        [Route("ProductionBreakdownEntryDataByShiftAndDate")]
        public async Task<List<GetProductionEntryByShiftAndDateVM>> ProductionEntryDataByShiftAndDate([FromQuery] string ProdDate, string Shiftid, string organizationID)
        {
            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            List<GetProductionEntryByShiftAndDateVM> AllProductions = new List<GetProductionEntryByShiftAndDateVM>();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ProdToDate=" + ProductionDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        GetProductionEntryByShiftAndDateVM dto = o.ToObject<GetProductionEntryByShiftAndDateVM>();

                        AllProductions.Add(dto);
                    }
                }
            }
            return AllProductions;
        }

        [HttpGet]
        [Route("GetProductionBreakdownList")]
        public ActionResult GetProductionBreakdownList(int productionId)
        {
            List<ProductionBreakdownEntryModel> productionBreakdownList = new ProductionReasonMappingService(_configuration).DataAccessLayer.GetProductionReasonMapping(productionId);
            return PartialView("_ProductionReason", productionBreakdownList);
        }
        [HttpPost]
        [Route("UpdateProductionBreakdownList")]
        public ActionResult UpdateProductionBreakdownList
                    (List<ProductionBreakdownEntryModel> productionsBrkList)
        {
            var result = new ProductReasonMappingDAL(_configuration).InsertProductionReasonMapping(productionsBrkList);
            return Json(result);
        }
    }
}