﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProdBreakViewController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string apiBaseUrl;

        public ProdBreakViewController(IConfiguration configuration)
        {
            _configuration = configuration;
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
        }
        // GET: ProductionPage

        public async Task<ActionResult> Index()
        {
            int OrganizationIDofloginuser = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            int CurrentUserId = UserUtility.CurrentUser.Id;

            var proDate = DateTime.Now.ToString("yyyy-MM-dd");
            var productionBreakDownEntryViewModel = new ProductionBreakDownEntryViewModel();
            var productionEntryUI = new ProductionPageVM();
            productionBreakDownEntryViewModel.ProductionDate = DateTime.ParseExact(proDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);  
            productionBreakDownEntryViewModel.ProductionToDate = DateTime.ParseExact(proDate, "yyyy-MM-dd", CultureInfo.InvariantCulture); 
            productionBreakDownEntryViewModel.OperatorID = CurrentUserId;
            productionBreakDownEntryViewModel.SupervisorID = CurrentUserId;
            productionBreakDownEntryViewModel.CurrentOrganizationId = OrganizationIDofloginuser;
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryUI?ProdDate=" + proDate + "&ProdToDate=" + proDate + "&organizationID=" + OrganizationIDofloginuser);
                if (response.IsSuccessStatusCode)
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    productionEntryUI = JsonConvert.DeserializeObject<ProductionPageVM>(apiResponse);
                }
            }
            productionBreakDownEntryViewModel.lstAllShift = productionEntryUI.lstshiftData.ToSelectList(c => c.ShiftId.ToString(), c => c.ShiftName);
            productionBreakDownEntryViewModel.lstAllShiftData = productionEntryUI.lstshiftData;
            productionBreakDownEntryViewModel.lstAllMachine = productionEntryUI.lstmachineProductData.ToSelectList(c => c.MachineId.ToString(), c => c.MachineName);
            productionBreakDownEntryViewModel.lstAllOperators = productionEntryUI.lstuserData.ToSelectList(c => c.UserId.ToString(), c => c.UserName);
            productionBreakDownEntryViewModel.lstAllProduct = productionEntryUI.lstmachineProductData.ToSelectList(c => c.ProductId.ToString(), c => c.ProductName);
            productionBreakDownEntryViewModel.lstvmAllProduct = productionEntryUI.lstmachineProductData;
            productionBreakDownEntryViewModel.lstAllRejectReason = productionEntryUI.lstrejectionData;
            productionBreakDownEntryViewModel.lstAllStopageReason = productionEntryUI.lststopageData.ToSelectList(c => c.ReasonId.ToString(), c => c.ReasonName);
            productionBreakDownEntryViewModel.lstAllSubStopageReason = productionEntryUI.lststopageData;
            productionBreakDownEntryViewModel.CurrentUserId = CurrentUserId;
            return View(productionBreakDownEntryViewModel);
        }
        [HttpGet]
        [Route("ProductionEntryDataByShiftAndDate")]
        public async Task<List<GetProductionEntryByShiftAndDateVM>> ProductionEntryDataByShiftAndDate([FromQuery] string ProdDate, [FromQuery] string ProdToDate, string Shiftid, string organizationID)
        {
            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            DateTime ProductionToDate = new DateTime();
            DateTime.TryParse(ProdToDate, out ProductionToDate);
            List<GetProductionEntryByShiftAndDateVM> AllProductions = new List<GetProductionEntryByShiftAndDateVM>();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ProdToDate=" + ProductionToDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        GetProductionEntryByShiftAndDateVM dto = o.ToObject<GetProductionEntryByShiftAndDateVM>();

                        AllProductions.Add(dto);
                    }
                }
            } 
            return AllProductions;
        }

        [HttpGet]
        [Route("ExportProductionEntryDataByShiftAndDate")]
        public async Task<JsonResult> ExportProductionEntryDataByShiftAndDate([FromQuery] string ProdDate, [FromQuery] string ProdToDate, string Shiftid, string organizationID)
        {
            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            DateTime ProductionToDate = new DateTime();
            DateTime.TryParse(ProdToDate, out ProductionToDate);
            List<GetProductionEntryByShiftAndDateVM> AllProductions = new List<GetProductionEntryByShiftAndDateVM>();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ProdToDate=" + ProductionToDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        GetProductionEntryByShiftAndDateVM dto = o.ToObject<GetProductionEntryByShiftAndDateVM>();
                        AllProductions.Add(dto);
                    }
                }
            }
            //save the file to server temp folder
            string fileName = "ProductionEntry_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), fileName);
            //var table = UtilityAll.ToDataTable<GetProductionEntryByShiftAndDateVM>(AllProductions);
            if (AllProductions != null && AllProductions.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {

                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("ProductionEntry");
                    PrepareExcel(worksheet, AllProductions);

                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }
            }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        private static void PrepareExcel(ExcelWorksheet ws, List<GetProductionEntryByShiftAndDateVM> productionEntryDatas)
        {
            ws.Cells[1, 1].Value = "Production Date";
            ws.Cells[1, 2].Value = productionEntryDatas.FirstOrDefault().ProductionDate;

            ws.Cells[1, 4].Value = "Shift Name";
            ws.Cells[1, 5].Value = productionEntryDatas.FirstOrDefault().ShiftName;

            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 1].Style.Font.Color.SetColor(Color.White);
            ws.Cells[1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(Color.Gray);

            ws.Cells[1, 4].Style.Font.Bold = true;
            ws.Cells[1, 4].Style.Font.Color.SetColor(Color.White);
            ws.Cells[1, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
            ws.Cells[1, 4].Style.Fill.BackgroundColor.SetColor(Color.Gray);


            int rowIndex = 3;
            // headers
            ws.Cells[rowIndex, 1].Value = "Machine";
            ws.Cells[rowIndex, 2].Value = "Operator";
            ws.Cells[rowIndex, 3].Value = "Product";
            ws.Cells[rowIndex, 4].Value = "Production";
            ws.Cells[rowIndex, 5].Value = "Rework";
            ws.Cells[rowIndex, 6].Value = "Rejection";

            using (ExcelRange range = ws.Cells[rowIndex, 1, rowIndex, 6])
            {
                range.Style.Font.Bold = true;
                range.Style.Font.Color.SetColor(Color.White);
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(22, 31, 54));
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Machine");
            dataTable.Columns.Add("Operator");
            dataTable.Columns.Add("Product");
            dataTable.Columns.Add("Production");
            dataTable.Columns.Add("Rework");
            dataTable.Columns.Add("Rejection");

            DataRow dataRow = null;
            foreach (var item in productionEntryDatas)
            {
                dataRow = dataTable.NewRow();
                dataRow["Machine"] = item.MachineName;
                dataRow["Operator"] = item.OperatorName;
                dataRow["Product"] = item.ProductName;
                dataRow["Production"] = item.TotalProduction;
                dataRow["Rework"] = item.ReWork;
                dataRow["Rejection"] = item.Rejection;

                dataTable.Rows.Add(dataRow);
            }
            ws.Cells["A4"].LoadFromDataTable(dataTable, false, OfficeOpenXml.Table.TableStyles.Light8);
        }

        [HttpGet]
        [Route("BreakDownEntryDataByShiftAndDate")]
        public async Task<List<GetBreakDownEntryByShiftAndDateVM>> BreakDownEntryDataByShiftAndDate([FromQuery] string ProdDate, [FromQuery] string ProdToDate, string Shiftid, string organizationID)
        {

            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            DateTime ProductionToDate = new DateTime();
            DateTime.TryParse(ProdToDate, out ProductionToDate);
            List<GetBreakDownEntryByShiftAndDateVM> AllBreakdowns = new List<GetBreakDownEntryByShiftAndDateVM>();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getBreakDownEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ProdToDate=" + ProductionToDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        GetBreakDownEntryByShiftAndDateVM dto = o.ToObject<GetBreakDownEntryByShiftAndDateVM>();

                        AllBreakdowns.Add(dto);
                    }
                }
            }
            decimal.TryParse(_configuration["TotalStoppageTime"], out decimal stopageTime);
            if (stopageTime > 0)
                AllBreakdowns = AllBreakdowns.Where(x => x.TotalStoppageTime >= stopageTime).ToList();
            return AllBreakdowns;
        }
    }
}