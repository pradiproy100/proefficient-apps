﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductRejectionController : Controller
    {
        private readonly IConfiguration _configuration;

        public ProductRejectionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            int OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            ProductMachineViewModel model = new ProductMachineViewModel();
            var objMachine = new MachineProductService(_configuration).DataAccessLayer;
            List<PMMachineVM> machineLst = objMachine.GetMachineList(OrganizationId, "machine").ToList();
            if (machineLst.Count > 0)
            {
                model.lstproductmachineMachineVM = machineLst;
            }
            else
            {
                model.lstproductmachineMachineVM = new List<PMMachineVM>();
            }
            var objProduct = new MachineProductService(_configuration).DataAccessLayer;
            List<PMProductVM> productLst = objMachine.GetProductList(OrganizationId, "product").ToList();
            if (productLst.Count > 0)
            {
                model.lstproductmachineProductVM = productLst;
            }
            else
            {
                model.lstproductmachineProductVM = new List<PMProductVM>();
            }
            model.lstproductmachineVM = new List<ProductMachineVM>();
            model.productMachineVMSelect = new ProductMachineVMSelect();
            model.productMachineVMSelect.OrganizationId = OrganizationId;
            return View(model);
        }
        public ActionResult MachineDemandType()
        {
            int OrganizationId= UserUtility.CurrentUser.OrganizationId.ToInteger0();
            var objMachine = new MachineProductService(_configuration).DataAccessLayer;
            List<PMMachineVM> productMachineLst = objMachine.GetMachineList(OrganizationId,"machine").ToList();
            if (productMachineLst.Count > 0)
            {
                return PartialView("_machines", productMachineLst);
            }
            else { return Content("No Record Found"); }
            
        }
        [HttpPost]
        public ActionResult MachineDemandTypePost(IList<PMMachineVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("MachineId", typeof(int));
            UDT.Columns.Add("ProductId", typeof(int));
            UDT.Columns.Add("CycleTime", typeof(decimal));
            UDT.Columns.Add("DemandTypeText", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].isChecked == true)
                {
                    DataRow DR = UDT.NewRow();
                    DR["MachineId"] = model[i].MachineID;
                    DR["ProductId"] = 0;
                    DR["CycleTime"] = 0;
                    DR["DemandTypeText"] = model[i].MachineDemandType;
                    UDT.Rows.Add(DR);
                }
            }
            int currentUserId = UserUtility.CurrentUser.Id;
            new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT, currentUserId, "machine");

            return RedirectToAction("MachineDemandType");
        }
        public ActionResult ProductDemandType()
        {
            int OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            var objMachine = new MachineProductService(_configuration).DataAccessLayer;
            List<PMProductVM> productMachineLst = objMachine.GetProductList(OrganizationId, "product").ToList();
            if (productMachineLst.Count > 0)
            {
                return PartialView("_products", productMachineLst);
            }
            else { return Content("No Record Found"); }
            
        }
        [HttpPost]
        public ActionResult ProductDemandTypePost(IList<PMProductVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("MachineId", typeof(int));
            UDT.Columns.Add("ProductId", typeof(int));
            UDT.Columns.Add("CycleTime", typeof(decimal));
            UDT.Columns.Add("DemandTypeText", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].isChecked == true)
                {
                    DataRow DR = UDT.NewRow();
                    DR["MachineId"] = 0;
                    DR["ProductId"] = model[i].ProductId;
                    DR["CycleTime"] = 0;
                    DR["DemandTypeText"] = model[i].ProductDemandType;
                    UDT.Rows.Add(DR);
                }
            }
            int currentUserId = UserUtility.CurrentUser.Id;
            new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT, currentUserId, "product");

           return RedirectToAction("ProductDemandType");
        }
        [HttpPost]
        public ActionResult MachineProductSelect(ProductMachineVMSelect model)
        {
            if (model.arrMachineIds != null) { model.MachineIds = string.Join(",", model.arrMachineIds); }
            if (model.arrProductIds != null) { model.ProductIds = string.Join(",", model.arrProductIds); }
            var objMachineProduct = new MachineProductService(_configuration).DataAccessLayer;
            List<ProductMachineVM> productMachineLst = objMachineProduct.GetMachineProductList(model).ToList();
            if (productMachineLst.Count > 0)
            {
                return PartialView("_machineProductsList", productMachineLst);
            }
            else { return Content("No Record Found"); }
        }
        [HttpPost]
        public ActionResult MachineProductList(IList<ProductMachineVM> model)
        {
            DataTable UDT = new DataTable();
            UDT.Columns.Add("MachineId", typeof(int));
            UDT.Columns.Add("ProductId", typeof(int));
            UDT.Columns.Add("CycleTime", typeof(decimal));
            UDT.Columns.Add("DemandTypeText", typeof(string));
            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].IsMapped == true)
                {
                    DataRow DR = UDT.NewRow();
                    DR["MachineId"] = model[i].MachineID;
                    DR["ProductId"] = model[i].ProductId;
                    DR["CycleTime"] = model[i].ProductCycleTime;
                    DR["DemandTypeText"] = "";
                    UDT.Rows.Add(DR);
                }
            }
            int currentUserId = UserUtility.CurrentUser.Id;           
            new MachineProductService(_configuration).DataAccessLayer.CreateMachineProductMapping(UDT,currentUserId,"mapping");
            
            List<ProductMachineVM> li = new List<ProductMachineVM>();
            
            return PartialView("_machineProductsList", li);
        }
    }
}
