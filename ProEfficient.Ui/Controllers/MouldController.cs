﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;
using System.Threading;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class MouldController : Controller
    {
        private readonly IConfiguration _configuration;

        public MouldController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Mould

        public ActionResult Index()
        {
            var objMould = new MouldService(_configuration).DataAccessLayer;

            return View(objMould.GetMasterMouldList(UserUtility.CurrentUser.OrganizationId)?.OrderByDescending(i => i?.MouldId));

        }

        public ActionResult Create()
        {

            return View(new MasterMould() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });
        }
        [HttpPost]
        public ActionResult Create(MasterMould pMould)
        {
            if (ModelState.IsValid)
            {
                var AllOps = new MouldService(_configuration).DataAccessLayer.GetMasterMouldList(UserUtility.CurrentUser.OrganizationId);                
                pMould.IsActive = true;
                pMould.AddBy = UserUtility.CurrentUser.Id;         
                pMould.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new MouldService(_configuration).DataAccessLayer.UpdateMasterMould(pMould);  
                this.CreateObjectAlert("Mould");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Mould Data is not valid");
                return View(pMould);
            }
        }
        public ActionResult Edit(int id)
        {

            var Mould = new MouldService(_configuration).DataAccessLayer.GetMasterMouldById(UserUtility.CurrentUser.OrganizationId,id);

            if (Mould == null)
            {
                return RedirectToAction("Index");
            }


            return View(Mould);
        }
        [HttpPost]
        public ActionResult Edit(MasterMould pMould)
        {
            if (ModelState.IsValid)
            {               
                pMould.EditBy = UserUtility.CurrentUser.Id;
                pMould.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new MouldService(_configuration).DataAccessLayer.UpdateMasterMould(pMould);
                this.UpdatedObjectAlert("Mould");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Mould Data is not valid");
                return View(pMould);
            }
        }
        public ActionResult Delete(int id)
        {


            var Mould = new MouldService(_configuration).DataAccessLayer.GetMasterMouldById(UserUtility.CurrentUser.OrganizationId, id);
            if (Mould != null)
            {
                Mould.IsActive = !Mould.IsActive;
                Mould.EditBy = UserUtility.CurrentUser.Id;
                new MouldService(_configuration).DataAccessLayer.UpdateMasterMould(Mould);

            }
            return RedirectToAction("Index");
        }

    }
}