﻿using ClosedXML.Excel;
using ClosedXML;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using ProEfficient.Ui.ExcelUploadUtility;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization("Admin", "Supervisor")]
    public class ReportController : Controller
    {
        private readonly IConfiguration _configuration;
        private APIUser CurrentUserObj;
        private int CurrentOrgId;
        private int userID;
        private readonly ILogger<ReportController> _logger;

        public ReportController(IConfiguration configuration, ILogger<ReportController> logger)
        {
            _configuration = configuration;
            CurrentUserObj = UserUtility.CurrentUser;
            CurrentOrgId = CurrentUserObj.OrganizationId;
            userID = CurrentUserObj.Id;
            _logger = logger;
        }

        public IActionResult Index()
        {
            //Get Machine List 
            FinalResultSetModel FinalResultset = new FinalResultSetModel();

            TypeofData typeData = new TypeofData();
            typeData = GetMomentRange();
            typeData.typeofdata = "DateWise";
            typeData.OrganizationID = CurrentOrgId.ToString();


            FinalResultset = FinalResult(typeData);

            return View(FinalResultset);

        }

        [HttpPost]
        public FinalResultSetModel FinalResult([FromBody] TypeofData typeData)
        {
            FinalResultSetModel FinalResultset = new FinalResultSetModel();
            List<FinalARPRQROEEHome> lstperformance = new List<FinalARPRQROEEHome>();


            FinalResultset.ShiftList = UserUtility.AllShift;
            FinalResultset.MachineList = GetReportsConfig(typeData);
            typeData.OrganizationID = CurrentOrgId.ToString();

            var dates = GetStartEndDates(typeData);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            lstperformance = objCategoryDAL.GetOEEReport(staretdate, endate, CurrentOrgId, MachineID);
            FinalResultset.PerformanceReportListHome = lstperformance;

            return FinalResultset;
        }
        [HttpPost]
        public JsonResult ExportExcel([FromBody] TypeofData typeData)
        {
            string grpName = GetReportsConfig(typeData).Where(a => a.Value == typeData.MachineID).Select(b => b.Text).FirstOrDefault();
            List<FinalARPRQROEEHome> lstperformance = new List<FinalARPRQROEEHome>();
            var dates = GetStartEndDates(typeData);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var resultList = objCategoryDAL.GetOEEReport(staretdate, endate, CurrentOrgId, MachineID);
            var fileName = grpName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), fileName);
            var filteredList = resultList.Where(a => a.DataLevel == "DateLevel").ToList();
            List<FinalARPRQROEEExcel> lstexcelRes = new List<FinalARPRQROEEExcel>();
            foreach (FinalARPRQROEEHome m in filteredList)
            {
                FinalARPRQROEEExcel em = new FinalARPRQROEEExcel();
                em.Date = m.RecordDate;
                em.MachineName = m.MachineName;

                if (!string.IsNullOrEmpty(m.tShiftA))
                {
                    string[] ABC = m.tShiftA.Split(",");
                    em.ShiftAAR = ABC[0];
                    em.ShiftAPR = ABC[1];
                    em.ShiftAQR = ABC[2];
                }
                em.ShiftAOEE = m.ShiftA;

                if (!string.IsNullOrEmpty(m.tShiftB))
                {
                    string[] ABC = m.tShiftB.Split(",");
                    em.ShiftBAR = ABC[0];
                    em.ShiftBPR = ABC[1];
                    em.ShiftBQR = ABC[2];
                }
                em.ShiftBOEE = m.ShiftB;

                if (!string.IsNullOrEmpty(m.tShiftC))
                {
                    string[] ABC = m.tShiftC.Split(",");
                    em.ShiftCAR = ABC[0];
                    em.ShiftCPR = ABC[1];
                    em.ShiftCQR = ABC[2];
                }
                em.ShiftCOEE = m.ShiftC;

                if (!string.IsNullOrEmpty(m.tOverAll))
                {
                    string[] ABC = m.tOverAll.Split(",");
                    em.OverAllAR = ABC[0];
                    em.OverAllPR = ABC[1];
                    em.OverAllQR = ABC[2];
                }
                em.OverAllOEE = m.OverAll;
                lstexcelRes.Add(em);

            }
            var table = Core.Utility.UtilityAll.ToDataTable<FinalARPRQROEEExcel>(lstexcelRes);
            var errorMessage = "return the errors in here!";

            if (lstexcelRes != null && lstexcelRes.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("OEE");
                        loadData(worksheet, table);
                    }

                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }
            }
            else { return Json(new { fileName = "", errorMessage = "No Data Found" }); }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpPost]
        public JsonResult ExportExcelBreak([FromBody] TypeofData typeData)
        {
            string grpName = GetReportsConfig(typeData).Where(a => a.Value == typeData.MachineID).Select(b => b.Text).FirstOrDefault();

            var dates = GetStartEndDates(typeData);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var resultList = objCategoryDAL.GetLossReportRptDetails(staretdate, endate, CurrentOrgId, MachineID);
            var fileName = grpName + "_Breakdown_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), fileName);
            var table = Core.Utility.UtilityAll.ToDataTable<LossReportDetailsModel>(resultList);
            var errorMessage = "return the errors in here!";
            if (resultList != null && resultList.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Breakdown");
                        loadBreakData(worksheet, table);
                    }
                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }
            }
            else
            {
                return Json(new { fileName = "", errorMessage = "No Data Found" });
            }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpPost]
        public JsonResult ExportExcelRej([FromBody] TypeofData typeData)
        {
            string grpName = GetReportsConfig(typeData).Where(a => a.Value == typeData.MachineID).Select(b => b.Text).FirstOrDefault();

            var dates = GetStartEndDates(typeData);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var resultList = objCategoryDAL.GetRejectionReportRpt(staretdate, endate, CurrentOrgId, MachineID);
            var fileName = grpName + "_Quality_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), fileName);
            var filteredList = resultList.Where(a => a.MachineName != "Total").ToList();
            var table = Core.Utility.UtilityAll.ToDataTable<RejectionReasonDetailsModel>(filteredList);
            var errorMessage = "return the errors in here!";
            if (resultList != null && resultList.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Quality");
                        loadRejData(worksheet, table);
                    }

                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }
            }
            else { return Json(new { fileName = "", errorMessage = "No Data Found" }); }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpGet]
        public ActionResult Download(string file)
        {
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), file);
            byte[] fileByteArray = System.IO.File.ReadAllBytes(fullPath);
            System.IO.File.Delete(fullPath);
            return File(fileByteArray, "application/vnd.ms-excel", file);
        }

        public ActionResult DownloadFullPath(string fullPath)
        {

            byte[] fileByteArray = System.IO.File.ReadAllBytes(fullPath);
            System.IO.File.Delete(fullPath);
            return File(fileByteArray, "application/vnd.ms-excel", "ProductionExcel");
        }
        public IActionResult LossIndex()
        {
            //Get Machine List 
            FinalResultSetModel FinalResultset = new FinalResultSetModel();
            TypeofData typeData = new TypeofData();
            typeData = GetMomentRange();
            typeData.typeofdata = "DateWise";
            typeData.OrganizationID = CurrentOrgId.ToString();

            FinalResultset = LossFinalResult(typeData);
            //FinalResultset.LossReportRptList = lstperformance;

            return View(FinalResultset);

        }
        [HttpGet]
        public IActionResult ProductionReport()
        {
            //Get Machine List 
            TypeofData typeData;
            typeData = GetMomentRange();
            typeData.typeofdata = "DateWise";
            typeData.OrganizationID = CurrentOrgId.ToString();
            ViewBag.DateRange = String.Empty;

            return View(new List<ProductionReportResult>());

        }

        [HttpPost]
        public IActionResult ProductionReportSearch()
        {
            List<ProductionReportResult> Reports = null;
            try
            {
                DateTime StartDate = HttpContext.Request.Form["rptStartDate"].ToString().ToDateTime_yyyyMMdd();
                DateTime EndDate = HttpContext.Request.Form["rptEndDate"].ToString().ToDateTime_yyyyMMdd();
                ViewBag.DateRange = StartDate.ToString("yyyy-MM-dd") + " to " + EndDate.ToString("yyyy-MM-dd");
                ViewBag.SelectedStartDate = StartDate.ToString("yyyy-MM-dd");
                ViewBag.SelectedEndDate = EndDate.ToString("yyyy-MM-dd");
                String Action = HttpContext.Request.Form["rptAction"].ToString();

                var reportService = new ProductionReportService(_configuration).DataAccessLayer;
                Reports = reportService.GetProductionReport(CurrentOrgId, StartDate, EndDate);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(ReportController)} - {nameof(ProductionReportSearch)} Error occur..");
            }
            return View("ProductionReport", Reports);
        }
        [HttpPost]
        public JsonResult ExportexcelProduction([FromBody] TypeofData typedata)
        {
            DateTime staretdate = typedata.StartDate.ToDateTime_yyyyMMdd();
            DateTime endate = typedata.EndDate.ToDateTime();
            int machineid = typedata.MachineID.ToInteger0();
            var reportservice = new ProductionReportService(_configuration).DataAccessLayer;
            var reports = reportservice.GetProductionReport(CurrentOrgId, staretdate, endate);

            var filename = "productionreport" + DateTime.Now.ToString("yyyymmddhhmmss") + ".xlsx";

            //save the file to server temp folder
            string fullpath = Path.Combine(_configuration["excelsavepath"].ToString(), filename);
            var table = UtilityAll.ToDataTable(reports);
            if (reports != null && reports.Count() > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("production");
                        loadProductionData(worksheet, table);
                    }
                    //write the file to the disk
                    FileInfo fi = new FileInfo(fullpath);
                    package.SaveAs(fi);
                }
            }
            else
            {
                return Json(new { filename = "", errormessage = "no data found" });
            }
            //return the excel file name
            return Json(new { filename = filename, errormessage = "" });
        }
        [HttpPost]
        public JsonResult ExportExcelProductionDaily([FromBody] TypeofData typeData)
        {
            DateTime staretdate = typeData.StartDate.ToDateTime_yyyyMMdd();
            DateTime endate = typeData.EndDate.ToDateTime_yyyyMMdd();
            int MachineID = typeData.MachineID.intTP();
            var reportService = new ProductionReportService(_configuration).DataAccessLayer;
            var Reports = reportService.GetProductionReportWithBreakDown(CurrentOrgId, staretdate, endate);

            var fileName = "ProductionReport" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), fileName);

            if (Reports != null && Reports.Rows.Count > 0)
            {
                if (Reports.Rows.Count > 0)
                {
                    XLWorkbook wb = new XLWorkbook();
                    wb.Worksheets.Add(loadProductionWithBreakDownData(Reports));   
                    var Sheet = wb.Worksheets.FirstOrDefault();
                    Sheet.Cell(4 + Reports.Rows.Count, 13).FormulaA1 = @$"=AVERAGE(M2:M{3 + Reports.Rows.Count})";
                    Sheet.Cell(4 + Reports.Rows.Count, 14).FormulaA1 = @$"=AVERAGE(N2:N{3 + Reports.Rows.Count})";
                    Sheet.Cell(4 + Reports.Rows.Count, 7).FormulaA1 = @$"=SUM(G2:G{3 + Reports.Rows.Count})";
                    Sheet.Cell(4 + Reports.Rows.Count, 12).Value = "AVERAGE";
                    Sheet.Cell(4 + Reports.Rows.Count, 6).Value = "Production Inefficiency";
                    // Sheet.Cell(4 + Reports.Rows.Count, 12).Style.Font=Font

                    for (int i = 1; i <= Reports.Columns.Count; i++)
                    {
                        Sheet.Cell(1, i).WorksheetColumn().Width = 35;
                        Sheet.Cell(1, i).WorksheetColumn().Style.Font.SetBold();
                        Sheet.Cell(1, i).WorksheetColumn().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    }

                    FileInfo fi = new FileInfo(fullPath);
                    wb.SaveAs(fullPath);
                }
            }
            else
            {
                return Json(new { fileName = "", errorMessage = "No Data Found" });
            }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpPost]
        public JsonResult ExportEmailProduction([FromBody] TypeofData typeData)
        {
            DateTime staretdate = typeData.StartDate.ToDateTime_yyyyMMdd();
            DateTime endate = typeData.EndDate.ToDateTime_yyyyMMdd();
            String emailId = typeData.EmailId.ToStringx();
            int MachineID = typeData.MachineID.intTP();
            var reportService = new ProductionReportService(_configuration).DataAccessLayer;
            var Reports = reportService.GetProductionReportWithBreakDown(CurrentOrgId, staretdate, endate);

            var fileName = "ProductionReport" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(_configuration["ExcelSavePath"].ToString(), fileName);

            if (Reports != null && Reports.Rows.Count > 0)
            {
                if (Reports.Rows.Count > 0)
                {
                    XLWorkbook wb = new XLWorkbook();
                    wb.Worksheets.Add(loadProductionWithBreakDownData(Reports));

                    var Sheet = wb.Worksheets.FirstOrDefault();
                    for (int i = 1; i <= Reports.Columns.Count; i++)
                    {
                        Sheet.Cell(1, i).WorksheetColumn().Width = 35;
                        Sheet.Cell(1, i).WorksheetColumn().Style.Font.SetBold();
                        Sheet.Cell(1, i).WorksheetColumn().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    }

                    FileInfo fi = new FileInfo(fullPath);
                    wb.SaveAs(fullPath);
                    if (!String.IsNullOrEmpty(fullPath) && !String.IsNullOrEmpty(emailId))
                    {
                    var result=    new EmailUtility(configuration:_configuration).SendMail(emailId, "Production Report with BreakDown",
                            "Hi, <br /> Please find attachement for <b>Production Report</b><br /><br />For any further information , Please contact to Administrator."
                            , fullPath);
                        if(!result)
                        {
                            return Json(new { fileName = "", errorMessage = "Email sending error" });
                        }
                    }
                }
            }
            else
            {
                return Json(new { fileName = "", errorMessage = "Email sending error" });
            }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpPost]
        public FinalResultSetModel LossFinalResult([FromBody] TypeofData typeData)
        {
            FinalResultSetModel FinalResultset = new FinalResultSetModel();

            FinalResultset.ShiftList = UserUtility.AllShift;
            FinalResultset.MachineList = GetReportsConfig(typeData);
            if (CurrentUserObj.HasBreakdown)
            {
                var dates = GetStartEndDates(typeData);
                DateTime staretdate = dates.Item1;
                DateTime endate = dates.Item2;
                int MachineID = typeData.MachineID.intTP();

                // ViewBag.rptDate = strReturn;
                var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                var CategoryList = objCategoryDAL.GetLossReportRpt(staretdate, endate, CurrentOrgId, MachineID);
                FinalResultset.LossReportRptList = CategoryList;

            }
            else
            {
                FinalResultset.LossReportRptList = new List<LossReportModel>();
            }
            return FinalResultset;

        }

        public IActionResult RejIndex()
        {
            //Get Machine List 
            FinalResultSetModel FinalResultset = new FinalResultSetModel();

            TypeofData typeData = new TypeofData();
            typeData = GetMomentRange();
            typeData.typeofdata = "DateWise";
            typeData.OrganizationID = CurrentOrgId.ToString();

            FinalResultset = GetRejectionReport(typeData);

            return View(FinalResultset);

        }

        [HttpPost]
        public FinalResultSetModel GetRejectionReport([FromBody] TypeofData typeData)
        {
            FinalResultSetModel FinalResultset = new FinalResultSetModel();

            FinalResultset.ShiftList = UserUtility.AllShift;
            FinalResultset.MachineList = GetReportsConfig(typeData);
            if (CurrentUserObj.HasRejection)
            {
                var dates = GetStartEndDates(typeData);
                DateTime staretdate = dates.Item1;
                DateTime endate = dates.Item2;
                string strReturn = staretdate.ToString("yyyy-MM-dd") + " to " + endate.ToString("yyyy-MM-dd");
                //  int orgid = typeData.OrganizationID.intTP();
                int MachineID = typeData.MachineID.intTP();
                var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                var CategoryList = objCategoryDAL.GetRejectionReportRpt(staretdate, endate, CurrentOrgId, MachineID);
                FinalResultset.RejectionReasonRpt = CategoryList;

            }
            else
            {
                FinalResultset.RejectionReasonRpt = new List<RejectionReasonDetailsModel>();
            }
            return FinalResultset;
        }

        private List<SelectListItem> GetReportsConfig(TypeofData typeofData)
        {
            var objreportDAL = new UserMachineReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;

            var Reportlistdata = objreportDAL.GetReportConfigList(userID);

            var reports = Reportlistdata.Select(m => new SelectListItem
            {
                Text = m.ReportName,
                Value = m.UserMachineReportID.ToString(),
                Selected = m.IsDefault
                //Selected = Reportlistdata.Exists(z => z.UserMachineReportID == Convert.ToInt32(typeofData.MachineID))
            }).ToList();

            reports.Add(new SelectListItem { Text = "+ Add New", Value = "Add New" });
            reports.Insert(0, new SelectListItem { Text = "All", Value = "0" });
            return reports;
        }
        private TypeofData GetMomentRange()
        {
            UserReportConfig userDashboard = new UserReportConfig();
            string preDefinedLabel = "";
            var objCategoryDAL = new UserMachineReportService(_configuration).DataAccessLayer;

            string strReturn = "";
            DateTime now = DateTime.Now;
            DateTime startDate;
            DateTime endDate;
            userDashboard = objCategoryDAL.GetReportConfigList(userID).Where(m => m.IsDefault == true).FirstOrDefault();
            if (userDashboard != null)
            {
                preDefinedLabel = userDashboard.DateRange;


                if (preDefinedLabel == "Today")
                {
                    startDate = DateTime.Now;
                    endDate = DateTime.Now;
                }
                else if (preDefinedLabel == "Yesterday")
                {
                    startDate = DateTime.Now.AddDays(-1);
                    endDate = DateTime.Now.AddDays(-1);
                }
                else if (preDefinedLabel == "This Week")
                {
                    DayOfWeek currentDay = now.DayOfWeek;
                    int daysTillCurrentDay = currentDay - DayOfWeek.Monday;
                    startDate = now.AddDays(-daysTillCurrentDay);
                    endDate = now;
                }
                else if (preDefinedLabel == "Last 7 Days")
                {
                    startDate = now.AddDays(-6);
                    endDate = now;
                }
                else if (preDefinedLabel == "Last Week")
                {
                    DateTime date = DateTime.Now.AddDays(-7);
                    while (date.DayOfWeek != DayOfWeek.Monday)
                    {
                        date = date.AddDays(-1);
                    }

                    startDate = date;
                    endDate = date.AddDays(7);

                }
                else if (preDefinedLabel == "This Month")
                {
                    startDate = new DateTime(now.Year, now.Month, 1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
                else if (preDefinedLabel == "Last Month")
                {
                    var month = new DateTime(now.Year, now.Month, 1);
                    startDate = month.AddMonths(-1);
                    endDate = month.AddDays(-1);
                }
                else if (preDefinedLabel == "This Year")
                {
                    int year = now.Year;
                    startDate = new DateTime(year, 1, 1);
                    endDate = now;
                }
                else
                {
                    startDate = new DateTime(now.Year, now.Month, 1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
            }
            else
            {
                startDate = new DateTime(now.Year, now.Month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }

            strReturn = startDate.ToString("yyyy-MM-dd") + " to " + endDate.ToString("yyyy-MM-dd");

            // string strDateRange = GetMomentRange();
            TypeofData typeData = new TypeofData();
            if (!string.IsNullOrEmpty(strReturn))
            {

                var datearray = strReturn.Split("to");
                typeData.StartDate = datearray[0].Trim();
                typeData.EndDate = datearray[1].Trim();
                ViewBag.rptMonth = startDate.Month;
                ViewBag.rptDate = strReturn;
                if (userDashboard != null)
                {
                    ViewBag.rptMachine = userDashboard.UserMachineReportID;
                }
                else { ViewBag.rptMachine = 0; }
            }
            ViewBag.HasBreakdown = CurrentUserObj.HasBreakdown;
            ViewBag.HasRejection = CurrentUserObj.HasRejection;
            typeData.MachineID = ViewBag.rptMachine.ToString();
            return typeData;
        }
        (DateTime, DateTime) GetStartEndDates(TypeofData typeData)
        {
            DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            DateTime endate = DateTime.Now;
            if (typeData.typeofdata.ToLower() == "monthwise")
            {
                //Then Make Final Start and end Date
                int monnumber = typeData.monthnumber + 1;

                //Get last day of that month
                int curyear = DateTime.Now.Year;
                int lastday = DateTime.DaysInMonth(curyear, monnumber);
                staretdate = new DateTime(curyear, monnumber, 1);
                endate = new DateTime(curyear, monnumber, lastday);

            }
            else if (typeData.typeofdata.ToLower() == "datewise")
            {
                if (typeData.StartDate.dtTP().Year > 2001)
                {
                    staretdate = typeData.StartDate.dtTP();
                }
                if (typeData.EndDate.dtTP().Year > 2001)
                {
                    endate = typeData.EndDate.dtTP();
                }

            }
            return (staretdate, endate); // tuple literal
        }

        // Functions for write to Excel sheet/s
        private static void loadData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int productionDateIndex = 1;
            int machineNameIndex = 2;
            int ShiftAIndex = 3;
            int ShiftBIndex = 7;
            int ShiftCIndex = 11;
            int overAllIndex = 15;

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, productionDateIndex].Value = "Date";
            ws.Cells[rowIndex, machineNameIndex].Value = "Line";
            ws.Cells[rowIndex, ShiftAIndex].Value = "Shift A";
            ws.Cells[rowIndex, ShiftBIndex].Value = "Shift B";
            ws.Cells[rowIndex, ShiftCIndex].Value = "Shift C";
            ws.Cells[rowIndex, overAllIndex].Value = "Over All";
            ws.Cells["C1:F1"].Merge = true;
            ws.Cells["G1:J1"].Merge = true;
            ws.Cells["K1:N1"].Merge = true;
            ws.Cells["O1:R1"].Merge = true;
            ws.Cells["A1:R1"].Style.Font.Size = 12;
            ws.Cells["A1:R1"].Style.Font.Bold = true;


            ws.Column(2).Width = 35;
            using (ExcelRange Rng = ws.Cells[1, 1, 1, 18])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Orange);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
            using (ExcelRange Rng = ws.Cells[2, 1, 2, 18])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
            // sub headers
            rowIndex = 2;
            string subHeaderVal = "AR";
            for (int subIncrement = 0; subIncrement < 4; subIncrement++)
            {
                if (subIncrement == 1) { subHeaderVal = "PR"; }
                else if (subIncrement == 2) { subHeaderVal = "QR"; }
                else if (subIncrement == 3) { subHeaderVal = "OEE"; }
                ws.Cells[rowIndex, ShiftAIndex + subIncrement].Value = subHeaderVal;
                ws.Cells[rowIndex, ShiftBIndex + subIncrement].Value = subHeaderVal;
                ws.Cells[rowIndex, ShiftCIndex + subIncrement].Value = subHeaderVal;
                ws.Cells[rowIndex, overAllIndex + subIncrement].Value = subHeaderVal;
            }
            ws.Cells["A3"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        private static void loadBreakData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Reason";
            ws.Cells[rowIndex, 4].Value = "Start";
            ws.Cells[rowIndex, 5].Value = "End";
            ws.Cells[rowIndex, 6].Value = "Duration";


            ws.Column(2).Width = 35;
            ws.Column(3).Width = 35;
            using (ExcelRange Rng = ws.Cells[1, 1, 1, 6])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        private static DataTable loadProductionWithBreakDownData(DataTable table)
        {

            table.Columns.RemoveAt(table.Columns.IndexOf("TimeOfEntry"));
            table.Columns.RemoveAt(table.Columns.IndexOf("OperatorID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("MachineID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("ProductID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("ShiftID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("OperationID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("DieID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("MouldID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("DD_DateOfEntry"));
            table.Columns.RemoveAt(table.Columns.IndexOf("DieName"));
            table.Columns.RemoveAt(table.Columns.IndexOf("MouldName"));
            table.Columns.RemoveAt(table.Columns.IndexOf("MouldCavity"));
            table.Columns.RemoveAt(table.Columns.IndexOf("DieCavity"));

            foreach (DataColumn col in table.Columns)
            {
                col.ColumnName = col.ColumnName.Replace("_", Environment.NewLine);
            }



            return table;


        }
        private static void loadProductionData(ExcelWorksheet ws, DataTable table)
        {
            int rowIndex = 1;
            table.Columns.RemoveAt(table.Columns.IndexOf("TimeOfEntry"));
            table.Columns.RemoveAt(table.Columns.IndexOf("OperatorID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("MachineID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("ProductID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("ShiftID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("OperationID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("DieID"));
            table.Columns.RemoveAt(table.Columns.IndexOf("MouldID"));

            ws.Cells[rowIndex, 1].Value = "DateOfEntry";
            ws.Cells[rowIndex, 2].Value = "OperatorName";
            ws.Cells[rowIndex, 3].Value = "MachineName";
            ws.Cells[rowIndex, 4].Value = "ProductCode";
            ws.Cells[rowIndex, 5].Value = "ProductName";
            ws.Cells[rowIndex, 6].Value = "ShiftName";
            ws.Cells[rowIndex, 7].Value = "OperationName";
            ws.Cells[rowIndex, 8].Value = "DieName";
            ws.Cells[rowIndex, 9].Value = "MouldName";
            ws.Cells[rowIndex, 10].Value = "TotalProduction";
            ws.Cells[rowIndex, 11].Value = "TargetProduction";
            ws.Cells[rowIndex, 12].Value = "MouldCavity";
            ws.Cells[rowIndex, 13].Value = "DieCavity";

            ws.Column(2).Width = 35;
            ws.Column(3).Width = 35;
            using (ExcelRange range = ws.Cells[1, 1, 1, 13])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);
        }
        private static void loadRejData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Reason";
            ws.Cells[rowIndex, 4].Value = "Count";

            using (ExcelRange Rng = ws.Cells[1, 1, 1, 4])
            {

                Rng.Style.Font.Bold = true;

                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Column(2).Width = 35;
            ws.Column(3).Width = 35;

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }


    }

}
