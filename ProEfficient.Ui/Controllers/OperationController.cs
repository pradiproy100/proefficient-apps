﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;
using System.Threading;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class OperationController : Controller
    {
        private readonly IConfiguration _configuration;

        public OperationController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Operation

        public ActionResult Index()
        {
            var objOperation = new OperationService(_configuration).DataAccessLayer;

            return View(objOperation.GetMasterOperationList(UserUtility.CurrentUser.OrganizationId)?.OrderByDescending(i => i?.OperationId));

        }

        public ActionResult Create()
        {

            return View(new MasterOperation() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });
        }
        [HttpPost]
        public ActionResult Create(MasterOperation pOperation)
        {
            if (ModelState.IsValid)
            {
                var AllOps = new OperationService(_configuration).DataAccessLayer.GetMasterOperationList(UserUtility.CurrentUser.OrganizationId);               
                pOperation.IsActive = true;
                pOperation.AddBy = UserUtility.CurrentUser.Id;         
                pOperation.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new OperationService(_configuration).DataAccessLayer.UpdateMasterOperation(pOperation);  
                this.CreateObjectAlert("Operation");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Operation Data is not valid");
                return View(pOperation);
            }
        }
        public ActionResult Edit(int id)
        {

            var Operation = new OperationService(_configuration).DataAccessLayer.GetMasterOperationById(UserUtility.CurrentUser.OrganizationId,id);

            if (Operation == null)
            {
                return RedirectToAction("Index");
            }


            return View(Operation);
        }
        [HttpPost]
        public ActionResult Edit(MasterOperation pOperation)
        {
            if (ModelState.IsValid)
            {
                pOperation.EditBy = UserUtility.CurrentUser.Id;
                pOperation.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new OperationService(_configuration).DataAccessLayer.UpdateMasterOperation(pOperation);
                this.UpdatedObjectAlert("Operation");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Operation Data is not valid");
                return View(pOperation);
            }
        }
        public ActionResult Delete(int id)
        {


            var Operation = new OperationService(_configuration).DataAccessLayer.GetMasterOperationById(UserUtility.CurrentUser.OrganizationId, id);
            if (Operation != null)
            {
                Operation.IsActive = !Operation.IsActive;
                Operation.EditBy = UserUtility.CurrentUser.Id;
                new OperationService(_configuration).DataAccessLayer.UpdateMasterOperation(Operation);

            }
            return RedirectToAction("Index");
        }

    }
}