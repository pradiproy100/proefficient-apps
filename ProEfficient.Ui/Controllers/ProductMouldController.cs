﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ProEfficient.Ui.Controllers
{
    public class ProductMouldController : Controller
    {
        private readonly IConfiguration _configuration;

        public ProductMouldController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            LoadViewBag();
            return View();
        }
        public Boolean SaveJsonData(string id)
        {

            id = id.Replace("id=", "").Replace("'", "");

            var obj = JsonConvert.DeserializeObject<List<ProductMouldMap>>(id);
            if (obj != null)
            {
                try
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        var productMouldService = new ProductMouldService(_configuration).DataAccessLayer;

                        obj = obj.OrderBy(i => i.productId).ToList();
                        int prodid = 0;
                        foreach (ProductMouldMap map in obj)
                        {
                            map.MouldId = map.MouldId.Replace("Mould", "");
                            map.productId = map.productId.Replace("product", "");
                            if (map.productId.ToInteger0() != prodid)
                            {
                                prodid = map.productId.ToInteger0();
                                productMouldService.USP_DeleteProductMouldRel(UserUtility.CurrentUser.OrganizationId.ToInteger0(), prodid);

                            }
                           
                            productMouldService.USP_SaveProductMould(new Product_Mould_Rel()
                            {
                                CreatedBy = UserUtility.CurrentUser.Id.ToInteger0(),
                                CreatedOn = DateTime.Now,
                                MouldId = map.MouldId.ToInteger(),
                                OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0(),
                                ProductID = map.productId.ToInteger()
                            });
                        }
                        scope.Complete();
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
               
            }
            return false;

        }

        private void LoadViewBag()
        {
            var productDataAccess = new ProductService(_configuration).DataAccessLayer;
            ViewBag.ProductList = productDataAccess.GetMasterProductList().FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId);
            var MouldDataAccess = new MouldService(_configuration).DataAccessLayer;
            ViewBag.MouldList = MouldDataAccess.GetMasterMouldList(UserUtility.CurrentUser.OrganizationId);
        }

    }
    public class ProductMouldMap
    {
        public String productId { get; set; }
        public String MouldId { get; set; }
    }
}
