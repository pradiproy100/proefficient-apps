﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class HomeDashBoardController : Controller
    {
        private readonly IConfiguration _configuration;

        public HomeDashBoardController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IActionResult HomeDashBoard()
        {

            //Get Machine List 
            FinalResultSetModel FinalResultset = new FinalResultSetModel();
            /*
            var objMachinedata = new MachineService(_configuration).DataAccessLayer;
            var Machinelistdata = objMachinedata.GetMasterMachineList();
                     
            var machines = Machinelistdata.Where(m => m.OrganizationId == 5).Select(m => new SelectListItem
            {
                Text = m.MachineName,
                Value = m.MachineID.ToString()
            }).ToList();
            machines.Insert(0, new SelectListItem { Text = "All Machine", Value = "All Machine" });
            FinalResultset.MachineList=machines;

           */
           
            FinalResultset.MachineList = GetReportsConfig();
            List<FinalARPRQROEE> lstperformance = new List<FinalARPRQROEE>();
            List<LossReportDashboardModel> lstLoss = new List<LossReportDashboardModel>();
            List<FinalWeekWiseOEE> lstWeek = new List<FinalWeekWiseOEE>();
            TypeofData typeData = new TypeofData();
            typeData = GetMomentRange();
            typeData.typeofdata = "DateWise";
            typeData.OrganizationID = 5.ToString();
            //typeData.MachineID = 0.ToString();
            lstperformance = GetPerformance(typeData);
            FinalResultset.PerformanceReportList = lstperformance;
            lstLoss = GetLossReport(typeData);
            FinalResultset.LossReportList = lstLoss;
            lstWeek = GetWeekOEE(typeData);
            FinalResultset.PerformanceReportWEEKList = lstWeek;
            return View(FinalResultset);

        }


        //public string GetLastFifteenDaysData()
        //{
        //    List<FinalARPRQROEE> lstfianl = new List<FinalARPRQROEE>();
        //    List<SelectListItem> lstMachine = JsonConvert.DeserializeObject<List<SelectListItem>>(TempData["MachineList"] as string);
        //    TempData.Keep();
        //    //Fetch Machine Data Of Last 15 days
        //    TypeofData typeData = new TypeofData();            
                     
        //    typeData.typeofdata = "DateWise";
        //    typeData.OrganizationID = 5.ToString();
        //    typeData.MachineID = 0.ToString();
        //    lstfianl = GetPerformance(typeData);
        //    string result = JsonConvert.SerializeObject(lstfianl);
        //    return result;


        //}      
        private List<SelectListItem> GetReportsConfig()
        {
            var objreportDAL = new UserMachineReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;

            var Reportlistdata = objreportDAL.GetReportConfigList(userID);
            var reports = Reportlistdata.Select(m => new SelectListItem
            {
                Text = m.ReportName,
                Value = m.UserMachineReportID.ToString()
            }).ToList();
            
            reports.Add(new SelectListItem { Text = "Add New", Value = "Add New" });
            reports.Insert(0, new SelectListItem { Text = "All", Value = "All" });
            return reports;
        }
        [HttpPost]
        public FinalResultSetModel FinalResult([FromBody]TypeofData typeData)
        {
            FinalResultSetModel FinalResultset = new FinalResultSetModel();
            List<FinalARPRQROEE> lstfianl = new List<FinalARPRQROEE>();
            List<LossReportDashboardModel> lstloss = new List<LossReportDashboardModel>();
            List<FinalWeekWiseOEE> lstWeek = new List<FinalWeekWiseOEE>();
            //var objMachinedata = new MachineService(_configuration).DataAccessLayer;
            //var Machinelistdata = objMachinedata.GetMasterMachineList();
            //var machines = Machinelistdata.Where(m => m.OrganizationId == 5).Select(m => new SelectListItem
            //{
            //    Text = m.MachineName,
            //    Value = m.MachineID.ToString()
            //}).ToList();
            //machines.Insert(0, new SelectListItem { Text = "All Machine", Value = "All Machine" });
            // FinalResultset.MachineList = machines;
            FinalResultset.MachineList = GetReportsConfig();
            typeData.OrganizationID = "5";        
            lstfianl = GetPerformance(typeData);
            lstloss = GetLossReport(typeData);
           // FinalResultset.MachineList = lstMachine;
            FinalResultset.PerformanceReportList = lstfianl;
            FinalResultset.LossReportList = lstloss;
            lstWeek = GetWeekOEE(typeData);
            FinalResultset.PerformanceReportWEEKList = lstWeek;
            return FinalResultset;
            //string result = JsonConvert.SerializeObject(FinalResultset);
            //return result;

        }
        public List<FinalARPRQROEE> GetPerformance(TypeofData typeData)
        {
            
            DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            DateTime endate = DateTime.Now;
            if (typeData.typeofdata.ToLower() == "monthwise")
            {
                //Then Make Final Start and end Date
                int monnumber = typeData.monthnumber + 1;

                //Get last day of that month
                int curyear = DateTime.Now.Year;
                int lastday = DateTime.DaysInMonth(curyear, monnumber);
                staretdate = new DateTime(curyear, monnumber, 1);
                endate = new DateTime(curyear, monnumber, lastday);

            }
            else if (typeData.typeofdata.ToLower() == "datewise")
            {
                if (typeData.StartDate.dtTP().Year > 2001)
                {
                    staretdate = typeData.StartDate.dtTP();
                }
                if (typeData.EndDate.dtTP().Year > 2001)
                {
                    endate = typeData.EndDate.dtTP();
                }

            }
            string strReturn = staretdate.ToString("yyyy-MM-dd") + " to " + endate.ToString("yyyy-MM-dd");
            int orgid = typeData.OrganizationID.intTP(); 
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetReportList(staretdate, endate, orgid, MachineID);
            var CategoryListYTD = objCategoryDAL.GetReportListYTD(staretdate, endate, orgid, MachineID);
            // FinalResultset.Reportlst = CategoryList;
            List<FinalARPRQROEE> lstfianl = new List<FinalARPRQROEE>();
            
            if (CategoryList != null && CategoryList.Count > 0)
            {

                string[] strDemandType = { "Runner", "Repeater", "Stranger" };
                //string[] strDemandType = CategoryList.Select(std => std.DemandType).Distinct().ToArray();
                string[] strCustomerType = CategoryList.Select(std => std.CustomerName).Distinct().ToArray();


                double TotalWorkShiftTime = CategoryList.Sum(a => a.WorkShiftTime).ToString().doubleTP();
                double TotalShudownTime = CategoryList.Sum(a => a.ShutdownTime.doubleTP()).ToString().doubleTP();
                double TotalShiftBeakTime = CategoryList.Sum(a => a.ShiftBreakTime.doubleTP()).ToString().doubleTP();
                double TotalUtilisationLoss = CategoryList.Sum(a => a.Utliizationloss.doubleTP()).ToString().doubleTP();
                double TotalProductionTime = CategoryList.Sum(a => a.TotalProduction.doubleTP()).ToString().doubleTP();
                double TotalQualtiyLoss = CategoryList.Sum(a => a.Qualityloss.doubleTP()).ToString().doubleTP();

                double PlantAR = Math.Round((TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime - TotalUtilisationLoss) /
                    (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
                double PlantPR = Math.Round(TotalProductionTime / (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
                double PlantQR = Math.Round((TotalProductionTime - TotalQualtiyLoss) / (TotalProductionTime == 0 ? 1 : TotalProductionTime), 2);
                double PlantOEE = Math.Round(PlantAR * PlantQR * PlantPR, 2);
                lstfianl = new List<FinalARPRQROEE>() { new FinalARPRQROEE { AR = PlantAR, PR = PlantPR, QR = PlantQR, OEE = PlantOEE, DataLevel = "PlantLevel",rptDate= strReturn } };

                foreach (string strdType in strDemandType)
                {
                    TotalWorkShiftTime = CategoryList.Sum(a => (a.DemandType == strdType ? a.WorkShiftTime : 0)).ToString().doubleTP();
                    TotalShudownTime = CategoryList.Sum(a => (a.DemandType == strdType ? a.ShutdownTime.doubleTP() : 0)).ToString().doubleTP();
                    TotalShiftBeakTime = CategoryList.Sum(a => (a.DemandType == strdType ? a.ShiftBreakTime.doubleTP() : 0)).ToString().doubleTP();
                    TotalUtilisationLoss = CategoryList.Sum(a => (a.DemandType == strdType ? a.Utliizationloss.doubleTP() : 0)).ToString().doubleTP();
                    TotalProductionTime = CategoryList.Sum(a => (a.DemandType == strdType ? a.TotalProduction.doubleTP() : 0)).ToString().doubleTP();
                    TotalQualtiyLoss = CategoryList.Sum(a => (a.DemandType == strdType ? a.Qualityloss.doubleTP() : 0)).ToString().doubleTP();

                    PlantAR = Math.Round((TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime - TotalShiftBeakTime) /
                       (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
                    PlantPR = Math.Round(TotalProductionTime / (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
                    PlantQR = Math.Round((TotalProductionTime - TotalQualtiyLoss) / (TotalProductionTime==0?1: TotalProductionTime), 2);
                    PlantOEE = Math.Round(PlantAR * PlantQR * PlantPR, 2);
                    if (TotalWorkShiftTime == 0)
                    {
                        lstfianl.Add(new FinalARPRQROEE { AR = 0.00, PR = 0.00, QR = 0.00, OEE = 0.00, DataLevel = strdType });
                    }
                    else
                    {
                        lstfianl.Add(new FinalARPRQROEE { AR = PlantAR, PR = PlantPR, QR = PlantQR, OEE = PlantOEE, DataLevel = strdType });
                    }
                }
                foreach (string strcType in strCustomerType)
                {
                    TotalWorkShiftTime = CategoryList.Sum(a => (a.CustomerName == strcType ? a.WorkShiftTime : 0)).ToString().doubleTP();
                    TotalShudownTime = CategoryList.Sum(a => (a.CustomerName == strcType ? a.ShutdownTime.doubleTP() : 0)).ToString().doubleTP();
                    TotalShiftBeakTime = CategoryList.Sum(a => (a.CustomerName == strcType ? a.ShiftBreakTime.doubleTP() : 0)).ToString().doubleTP();
                    TotalUtilisationLoss = CategoryList.Sum(a => (a.CustomerName == strcType ? a.Utliizationloss.doubleTP() : 0)).ToString().doubleTP();
                    TotalProductionTime = CategoryList.Sum(a => (a.CustomerName == strcType ? a.TotalProduction.doubleTP() : 0)).ToString().doubleTP();
                    TotalQualtiyLoss = CategoryList.Sum(a => (a.CustomerName == strcType ? a.Qualityloss.doubleTP() : 0)).ToString().doubleTP();

                    PlantAR = Math.Round((TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime - TotalShiftBeakTime) /
                       (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
                    PlantPR = Math.Round(TotalProductionTime / (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
                    PlantQR = Math.Round((TotalProductionTime - TotalQualtiyLoss) / (TotalProductionTime == 0 ? 1 : TotalProductionTime), 2);
                    PlantOEE = Math.Round(PlantAR * PlantQR * PlantPR, 2);
                    lstfianl.Add(new FinalARPRQROEE { AR = PlantAR, PR = PlantPR, QR = PlantQR, OEE = PlantOEE, DataLevel = strcType });
                }
                if (CategoryListYTD != null && CategoryListYTD.Count > 0)
                {
                    lstfianl.Add(new FinalARPRQROEE { AR = CategoryListYTD[0].AR, PR = CategoryListYTD[0].PR, QR = CategoryListYTD[0].QR, OEE = CategoryListYTD[0].OEE, DataLevel = "YTD" } );
                }
                else
                {
                    lstfianl.Add(new FinalARPRQROEE { AR = 0.0, PR = 0.0, QR = 0.0, OEE = 0.0, DataLevel = "YTD" });
                }
            }
            else
            {

                lstfianl = new List<FinalARPRQROEE>() { new FinalARPRQROEE { AR = 0.0, PR = 0.0, QR = 0.0, OEE = 0.0, DataLevel = "PlantLevel", rptDate = strReturn } };
                if (CategoryListYTD != null && CategoryListYTD.Count > 0)
                {
                    lstfianl.Add(new FinalARPRQROEE { AR = CategoryListYTD[0].AR, PR = CategoryListYTD[0].PR, QR = CategoryListYTD[0].QR, OEE = CategoryListYTD[0].OEE, DataLevel = "YTD" });
                }
                else
                {
                    lstfianl.Add(new FinalARPRQROEE { AR = 0.0, PR = 0.0, QR = 0.0, OEE = 0.0, DataLevel = "YTD" });
                }
            }
           
            return lstfianl;
        }
        public List<LossReportDashboardModel> GetLossReport(TypeofData typeData)
        {            
            DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            DateTime endate = DateTime.Now;
            if (typeData.typeofdata.ToLower() == "monthwise")
            {
                //Then Make Final Start and end Date
                int monnumber = typeData.monthnumber + 1;

                //Get last day of that month
                int curyear = DateTime.Now.Year;
                int lastday = DateTime.DaysInMonth(curyear, monnumber);
                staretdate = new DateTime(curyear, monnumber, 1);
                endate = new DateTime(curyear, monnumber, lastday);

            }
            else if (typeData.typeofdata.ToLower() == "datewise")
            {
                if (typeData.StartDate.dtTP().Year > 2001)
                {
                    staretdate = typeData.StartDate.dtTP();
                }
                if (typeData.EndDate.dtTP().Year > 2001)
                {
                    endate = typeData.EndDate.dtTP();
                }

            }
            int orgid = 5;
            int MachineID = typeData.MachineID.intTP();
          
           // ViewBag.rptDate = strReturn;
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetLossReport(staretdate, endate, orgid, MachineID);
            var res = from element in CategoryList
                      group element by element.ReasonId
                  into groups
                      select groups.First();
            List<LossReportDashboardModel> lstParent = new List<LossReportDashboardModel>();

            foreach (var el in res)
            {
                LossReportDashboardModel m = new LossReportDashboardModel();
                //List<LossReportDashboardChildModel> lstchild = new List<LossReportDashboardChildModel>();
                m.LossDetails = new List<LossReportDashboardChildModel>();
                m.Reason = el.Reason;
                m.Duration = el.Duration;
                m.Freequency = el.Freequency;
               // m.rptDate = strReturn;
                foreach (var items in CategoryList)
                {
                    if (el.ReasonId == items.ReasonId)
                    {
                        LossReportDashboardChildModel c = new LossReportDashboardChildModel();
                        c.MachineName = items.MachineName;
                        c.ProductionDate = items.ProductionDate;
                        c.SubReason = items.SubReason;
                        c.Remarks = items.Remarks;
                        c.TotalStoppagetime = items.TotalStoppagetime;
                        c.MAchineID = items.MAchineID;
                        c.SubReasonId = items.SubReasonId;
                        c.Reason = items.Reason;
                       // lstchild.Add(c);
                        m.LossDetails.Add(c);
                    }
                }
                lstParent.Add(m);
            }
            lstParent= lstParent.OrderByDescending(x => x.Duration).ToList();
            return lstParent;
            //string result = JsonConvert.SerializeObject(lstParent);
            //return result;
        }
        public List<FinalWeekWiseOEE> GetWeekOEE(TypeofData typeData)
        {
            //DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            //DateTime endate = DateTime.Now;
            //if (typeData.typeofdata.ToLower() == "monthwise")
            //{
            //    //Then Make Final Start and end Date
            //    int monnumber = typeData.monthnumber + 1;

            //    //Get last day of that month
            //    int curyear = DateTime.Now.Year;
            //    int lastday = DateTime.DaysInMonth(curyear, monnumber);
            //    staretdate = new DateTime(curyear, monnumber, 1);
            //    endate = new DateTime(curyear, monnumber, lastday);

            //}
            //else if (typeData.typeofdata.ToLower() == "datewise")
            //{
            //    if (typeData.StartDate.dtTP().Year > 2001)
            //    {
            //        staretdate = typeData.StartDate.dtTP();
            //    }
            //    if (typeData.EndDate.dtTP().Year > 2001)
            //    {
            //        endate = typeData.EndDate.dtTP();
            //    }

            //}
            //int orgid = typeData.OrganizationID.intTP();
            //int MachineID = typeData.MachineID.intTP();
            //var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            //var CategoryList = objCategoryDAL.GetReportListWEEK(staretdate, endate, orgid, MachineID);
            //List<FinalWeekWiseOEE> lstfianl = new List<FinalWeekWiseOEE>();

            //if (CategoryList != null && CategoryList.Count > 0)
            //{


            //    lstfianl = new List<FinalWeekWiseOEE>();

            //    foreach (FinalWeekWiseOEE week in CategoryList)
            //    {
                   
            //            lstfianl.Add(new FinalWeekWiseOEE { weekno = week.weekno, OEE = week.OEE, DataLevel = week.DataLevel });
                    
            //    }
               
            //}
            List<FinalWeekWiseOEE> lstfianl = new List<FinalWeekWiseOEE>();
            return lstfianl;
        }
        [HttpPost]
        public bool SaveDashboardView([FromBody]UserDashboard userDashboard)
        {
            bool result = false;
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;
            if (userID > 0)
            {if (userDashboard != null)
                {
                    result = objCategoryDAL.SaveUserDashboardSearch(userDashboard.machineId, userID, userDashboard.dateRange);
                }
            }
            return result;
        }
        //public UserDashboard GetUserDashboard()
        //{
        //    UserDashboard userDashboard = new UserDashboard() ;
        //    var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
        //    int userID = UserUtility.CurrentUser.UserId;
        //    if (userID > 0)
        //    {

        //        userDashboard = objCategoryDAL.GetUserDashboardSearch(userID);
               
        //    }
        //    return userDashboard;
        //}
        private TypeofData GetMomentRange()
        {
            UserReportConfig userDashboard = new UserReportConfig();
            string preDefinedLabel = "";
            var objCategoryDAL = new UserMachineReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;
            string strReturn = "";
            DateTime now = DateTime.Now;
            DateTime startDate;
            DateTime endDate;
            userDashboard = objCategoryDAL.GetReportConfigList(userID).Where(m=> m.IsDefault==true).FirstOrDefault();
            if (userDashboard != null)
            {
                preDefinedLabel = userDashboard.DateRange;

              
                if (preDefinedLabel == "This Week")
                {
                    DayOfWeek currentDay = now.DayOfWeek;
                    int daysTillCurrentDay = currentDay - DayOfWeek.Monday;
                    startDate = now.AddDays(-daysTillCurrentDay);
                    endDate = now;
                }
                else if (preDefinedLabel == "Last 7 Days")
                {
                    startDate = now.AddDays(-6);
                    endDate = now;
                }
                else if (preDefinedLabel == "Last Week")
                {
                    DateTime date = DateTime.Now.AddDays(-7);
                    while (date.DayOfWeek != DayOfWeek.Monday)
                    {
                        date = date.AddDays(-1);
                    }

                    startDate = date;
                    endDate = date.AddDays(7);

                }
                else if (preDefinedLabel == "This Month")
                {
                    startDate = new DateTime(now.Year, now.Month, 1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
                else if (preDefinedLabel == "Last Month")
                {
                    var month = new DateTime(now.Year, now.Month, 1);
                    startDate = month.AddMonths(-1);
                    endDate = month.AddDays(-1);
                }
                else if (preDefinedLabel == "This Year")
                {
                    int year = now.Year;
                    startDate = new DateTime(year, 1, 1);
                    endDate = now;
                }
                else
                {
                    startDate = new DateTime(now.Year, now.Month, 1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
            }
            else
            {
                startDate = new DateTime(now.Year, now.Month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }
           
            strReturn = startDate.ToString("yyyy-MM-dd") + " to " + endDate.ToString("yyyy-MM-dd");
           
            // string strDateRange = GetMomentRange();
            TypeofData typeData = new TypeofData();
            if (!string.IsNullOrEmpty(strReturn))
            {

                var datearray = strReturn.Split("to");
                typeData.StartDate = datearray[0].Trim();
                typeData.EndDate = datearray[1].Trim();
                ViewBag.rptMonth = startDate.Month;
                ViewBag.rptDate = strReturn;
                if (userDashboard != null)
                {
                    ViewBag.rptMachine = userDashboard.UserMachineReportID;
                }
                else { ViewBag.rptMachine = 0; }
            }
            typeData.MachineID = ViewBag.rptMachine.ToString();
            return typeData;
        }
    }
}