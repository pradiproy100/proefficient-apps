﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace ProEfficient.Ui.Controllers
{
    public class ProductDieController : Controller
    {
        private readonly IConfiguration _configuration;

        public ProductDieController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            LoadViewBag();
            return View();
        }
        public Boolean SaveJsonData(string id)
        {

            id = id.Replace("id=", "").Replace("'", "");

            var obj = JsonConvert.DeserializeObject<List<ProductDieMap>>(id);
            if (obj != null)
            {
                try
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        var productDieService = new ProductDieService(_configuration).DataAccessLayer;

                        obj = obj.OrderBy(i => i.productId).ToList();
                        int prodid = 0;
                        foreach (ProductDieMap map in obj)
                        {
                            map.dieId = map.dieId.Replace("Die", "");
                            map.productId = map.productId.Replace("product", "");
                            if (map.productId.ToInteger0() != prodid)
                            {
                                prodid = map.productId.ToInteger0();
                                productDieService.USP_DeleteProductDieRel(UserUtility.CurrentUser.OrganizationId.ToInteger0(), prodid);

                            }

                            productDieService.USP_SaveProductDie(new Product_Die_Rel()
                            {
                                CreatedBy = UserUtility.CurrentUser.Id.ToInteger0(),
                                CreatedOn = DateTime.Now,
                                DieId = map.dieId.ToInteger(),
                                OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0(),
                                ProductID = map.productId.ToInteger()
                            });
                        }
                        scope.Complete();
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }

            }
            return false;

        }

        private void LoadViewBag()
        {
            var productDataAccess = new ProductService(_configuration).DataAccessLayer;
            ViewBag.ProductList = productDataAccess.GetMasterProductList().FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId);
            var dieDataAccess = new DieService(_configuration).DataAccessLayer;
            ViewBag.DieList = dieDataAccess.GetMasterDieList(UserUtility.CurrentUser.OrganizationId);
        }

    }
    public class ProductDieMap
    {
        public String productId { get; set; }
        public String dieId { get; set; }
    }
}
