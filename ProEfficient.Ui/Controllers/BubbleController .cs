﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Ui.Controllers
{
    public class BubbleController : Controller
    {
        private readonly IConfiguration _configuration;

        public BubbleController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            ChartsModel m = new ChartsModel();
            List<WaterFallModel> lstWaterFallModel = new List<WaterFallModel>();
            List<RejectionReasonReportModel> lstBubbleChartModel = new List<RejectionReasonReportModel>();
            TypeofData typeData = new TypeofData();
            typeData = GetMomentRange();
            typeData.typeofdata = "DateWise";
            typeData.OrganizationID = 5.ToString();
           // lstWaterFallModel = GetWaterFall(typeData);
            lstBubbleChartModel = GetBubbleChart(typeData);
            m.waterFallList = lstWaterFallModel;
            m.bubbleChartList = lstBubbleChartModel;

            return View(m);
        }
        public List<WaterFallModel> GetWaterFall(TypeofData typeData)
        {

            DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            DateTime endate = DateTime.Now;
            if (typeData.typeofdata.ToLower() == "monthwise")
            {
                //Then Make Final Start and end Date
                int monnumber = typeData.monthnumber + 1;

                //Get last day of that month
                int curyear = DateTime.Now.Year;
                int lastday = DateTime.DaysInMonth(curyear, monnumber);
                staretdate = new DateTime(curyear, monnumber, 1);
                endate = new DateTime(curyear, monnumber, lastday);

            }
            else if (typeData.typeofdata.ToLower() == "datewise")
            {
                if (typeData.StartDate.dtTP().Year > 2001)
                {
                    staretdate = typeData.StartDate.dtTP();
                }
                if (typeData.EndDate.dtTP().Year > 2001)
                {
                    endate = typeData.EndDate.dtTP();
                }

            }
            string strReturn = staretdate.ToString("yyyy-MM-dd") + " to " + endate.ToString("yyyy-MM-dd");
            int orgid = typeData.OrganizationID.intTP();
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetWaterfall(staretdate, endate, orgid, MachineID);
           
            return CategoryList;
        }
        public List<RejectionReasonReportModel> GetBubbleChart(TypeofData typeData)
        {

            DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            DateTime endate = DateTime.Now;
            if (typeData.typeofdata.ToLower() == "monthwise")
            {
                //Then Make Final Start and end Date
                int monnumber = typeData.monthnumber + 1;

                //Get last day of that month
                int curyear = DateTime.Now.Year;
                int lastday = DateTime.DaysInMonth(curyear, monnumber);
                staretdate = new DateTime(curyear, monnumber, 1);
                endate = new DateTime(curyear, monnumber, lastday);

            }
            else if (typeData.typeofdata.ToLower() == "datewise")
            {
                if (typeData.StartDate.dtTP().Year > 2001)
                {
                    staretdate = typeData.StartDate.dtTP();
                }
                if (typeData.EndDate.dtTP().Year > 2001)
                {
                    endate = typeData.EndDate.dtTP();
                }

            }
            string strReturn = staretdate.ToString("yyyy-MM-dd") + " to " + endate.ToString("yyyy-MM-dd");
            int orgid = typeData.OrganizationID.intTP();
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetRejectionReport(staretdate, endate, orgid, MachineID);

            return CategoryList;
        }
        private TypeofData GetMomentRange()
        {
            UserReportConfig userDashboard = new UserReportConfig();
            string preDefinedLabel = "";
            var objCategoryDAL = new UserMachineReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;
            string strReturn = "";
            DateTime now = DateTime.Now;
            DateTime startDate;
            DateTime endDate;
            userDashboard = objCategoryDAL.GetReportConfigList(userID).Where(m => m.IsDefault == true).FirstOrDefault();
            if (userDashboard != null)
            {
                preDefinedLabel = userDashboard.DateRange;


                if (preDefinedLabel == "Today")
                {
                    startDate = DateTime.Now;
                    endDate = DateTime.Now;
                }
                else if (preDefinedLabel == "Yesterday")
                {
                    startDate = DateTime.Now.AddDays(-1);
                    endDate = DateTime.Now.AddDays(-1);
                }
                else if (preDefinedLabel == "This Week")
                {
                    DayOfWeek currentDay = now.DayOfWeek;
                    int daysTillCurrentDay = currentDay - DayOfWeek.Monday;
                    startDate = now.AddDays(-daysTillCurrentDay);
                    endDate = now;
                }
                else if (preDefinedLabel == "Last 7 Days")
                {
                    startDate = now.AddDays(-6);
                    endDate = now;
                }
                else if (preDefinedLabel == "This Month")
                {
                    startDate = new DateTime(now.Year, now.Month, 1);
                    //endDate = startDate.AddMonths(1).AddDays(-1);
                    endDate = now;
                }
                else if (preDefinedLabel == "Last Month")
                {
                    var month = new DateTime(now.Year, now.Month, 1);
                    startDate = month.AddMonths(-1);
                    endDate = month.AddDays(-1);
                }
                else if (preDefinedLabel == "This Year")
                {
                    int year = now.Year;
                    startDate = new DateTime(year, 1, 1);
                    endDate = now;
                }
                else
                {
                    startDate = new DateTime(now.Year, now.Month, 1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
            }
            else
            {
                startDate = new DateTime(now.Year, now.Month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }

            strReturn = startDate.ToString("yyyy-MM-dd") + " to " + endDate.ToString("yyyy-MM-dd");

            // string strDateRange = GetMomentRange();
            TypeofData typeData = new TypeofData();
            if (!string.IsNullOrEmpty(strReturn))
            {

                var datearray = strReturn.Split("to");
                typeData.StartDate = datearray[0].Trim();
                typeData.EndDate = datearray[1].Trim();
                ViewBag.rptMonth = startDate.Month;
                ViewBag.rptDate = strReturn;
                if (userDashboard != null)
                {
                    ViewBag.rptMachine = userDashboard.UserMachineReportID;
                }
                else { ViewBag.rptMachine = 0; }
            }
            typeData.MachineID = ViewBag.rptMachine.ToString();
            return typeData;
        }
    }
}
