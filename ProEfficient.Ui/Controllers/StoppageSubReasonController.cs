﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Dal.Utility;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class StoppageSubReasonController : Controller
    {
        private readonly IConfiguration _configuration;

        public StoppageSubReasonController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: StopageSubReason
        public ActionResult Index()
        {

            return View(new StopageSubReasonService(_configuration).DataAccessLayer.GetStopageSubReasonList());
        }

        public ActionResult Create()
        {

            return View(new StopageSubReason());
        }

        [HttpPost]
        public ActionResult Create(StopageSubReason pStopageSubReason)
        {


            if (new StopageSubReasonService(_configuration).DataAccessLayer.GetStopageSubReasonList().FirstOrDefault(x => x.SubReasonCode.Trim() == pStopageSubReason.SubReasonCode.Trim()) != null)
            {
                ModelState.AddModelError("ReasonCode", "Duplicate Reason Code");
                return View(pStopageSubReason);
            }
            pStopageSubReason.IsActive = true;
            pStopageSubReason.AddBy = UserUtility.CurrentUser.Id;
            pStopageSubReason.EdittedBy = UserUtility.CurrentUser.Id;
            new StopageSubReasonService(_configuration).DataAccessLayer.UpdateStopageSubReason(pStopageSubReason);
            this.CreateObjectAlert("Stopage Sub Reason");
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {

            var StopageSubReason = (new StopageSubReasonService(_configuration).DataAccessLayer.GetStopageSubReason(new StopageSubReason() { SubReasonId = id }));
            if (StopageSubReason == null)
            {
                return RedirectToAction("Index");
            }
            return View(StopageSubReason);
        }


        [HttpPost]
        public ActionResult Edit(StopageSubReason pStopageSubReason)
        {

            pStopageSubReason.EdittedBy = UserUtility.CurrentUser.Id;
            new StopageSubReasonService(_configuration).DataAccessLayer.UpdateStopageSubReason(pStopageSubReason);
            this.UpdatedObjectAlert("Stopage Reason");
            return RedirectToAction("Index");
        }

    }
}