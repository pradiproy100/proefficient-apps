﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Ui.Entity;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductCategoriesController : Controller
    {
        private readonly ProefficientContext _context;

        public ProductCategoriesController(ProefficientContext context)
        {
            _context = context;
        }

        // GET: ProductCategories
        public async Task<IActionResult> Index()
        {
            List<OrganizationProductCategory> lst = await _context.OrganizationProductCategories.ToListAsync();
           
            var lstvm = lst.Select(item => new ProductCategoryViewModel()
            {
                Id = item.Id,
                CategoryName = item.CategoryName,
                OrganizationId=item.OrganizationId,
                RejectionReasonIds=item.RejectionReasonIds,
                arrRejectionReasonIds =item.RejectionReasonIds!=null? item.RejectionReasonIds.Split(',').Select(int.Parse).ToArray():null
        }).ToList();
           
            ViewData["RejectionReasons"] = new SelectList(_context.RejectionReasons, "ReasonId", "ReasonName");
            return View(lstvm);
        }

        // GET: ProductCategories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organizationProductCategory = await _context.OrganizationProductCategories
                .FirstOrDefaultAsync(m => m.Id == id);
            if (organizationProductCategory == null)
            {
                return NotFound();
            }

            return View(organizationProductCategory);
        }

        // GET: ProductCategories/Create
        public IActionResult Create()
        {
            ViewData["RejectionReasons"] = new SelectList(_context.RejectionReasons, "ReasonId", "ReasonName");
            ProductCategoryViewModel vm = new ProductCategoryViewModel();
            vm.OrganizationId = Dal.Utility.UserUtility.CurrentUser.OrganizationId;
            return View(vm);
        }

        // POST: ProductCategories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CategoryName,OrganizationId,arrRejectionReasonIds")] ProductCategoryViewModel organizationProductCategory)
        {
            if (ModelState.IsValid)
            {
                OrganizationProductCategory em = new OrganizationProductCategory();
                em.Id = organizationProductCategory.Id;
                em.CategoryName = organizationProductCategory.CategoryName;
                em.OrganizationId = organizationProductCategory.OrganizationId;
                if (organizationProductCategory.arrRejectionReasonIds != null) { em.RejectionReasonIds = string.Join(",", organizationProductCategory.arrRejectionReasonIds); }
                _context.Add(em);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["RejectionReasons"] = new SelectList(_context.RejectionReasons, "ReasonId", "ReasonName");
            return View(organizationProductCategory);
        }

        // GET: ProductCategories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ProductCategoryViewModel vm = new ProductCategoryViewModel();
            var organizationProductCategory = await _context.OrganizationProductCategories.FindAsync(id);
            if (organizationProductCategory == null)
            {
                return NotFound();
            }
            else
            {
                vm.Id = organizationProductCategory.Id;
                vm.OrganizationId = organizationProductCategory.OrganizationId;
                vm.CategoryName = organizationProductCategory.CategoryName;
                vm.RejectionReasonIds = organizationProductCategory.RejectionReasonIds;

            }
            if (!string.IsNullOrEmpty(vm.RejectionReasonIds))
            {
                vm.arrRejectionReasonIds = vm.RejectionReasonIds.Split(',').Select(int.Parse).ToArray();
            }
            ViewData["RejectionReasons"] = new SelectList(_context.RejectionReasons, "ReasonId", "ReasonName");
            return View(vm);
        }

        // POST: ProductCategories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryName,OrganizationId,arrRejectionReasonIds")] ProductCategoryViewModel organizationProductCategory)
        {
            if (id != organizationProductCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                OrganizationProductCategory em = new OrganizationProductCategory();
                em.Id = organizationProductCategory.Id;
                em.CategoryName = organizationProductCategory.CategoryName;
                em.OrganizationId = organizationProductCategory.OrganizationId;                
                if (organizationProductCategory.arrRejectionReasonIds != null) { em.RejectionReasonIds = string.Join(",", organizationProductCategory.arrRejectionReasonIds); }
                try
                {
                    _context.Update(em);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrganizationProductCategoryExists(em.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RejectionReasons"] = new SelectList(_context.RejectionReasons, "ReasonId", "ReasonName");
            return View(organizationProductCategory);
        }

        // GET: ProductCategories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organizationProductCategory = await _context.OrganizationProductCategories
                .FirstOrDefaultAsync(m => m.Id == id);
            if (organizationProductCategory == null)
            {
                return NotFound();
            }

            return View(organizationProductCategory);
        }

        // POST: ProductCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var organizationProductCategory = await _context.OrganizationProductCategories.FindAsync(id);
            _context.OrganizationProductCategories.Remove(organizationProductCategory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrganizationProductCategoryExists(int id)
        {
            return _context.OrganizationProductCategories.Any(e => e.Id == id);
        }
    }
}
