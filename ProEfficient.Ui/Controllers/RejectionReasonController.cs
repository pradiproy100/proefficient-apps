﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Dal.Utility;
using RejectionReason = ProEfficient.Core.Models.RejectionReason;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class RejectionReasonController : Controller
    {
        private readonly IConfiguration _configuration;

        public RejectionReasonController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: RejectionReason
        public ActionResult Index()
        {
            return View(new RejectionReasonService(_configuration).DataAccessLayer.GetRejectionReasonList());
        }
        public ActionResult Create()
        {
            var newobj = new RejectionReason();
            newobj.IsActive = true;
            return View(newobj);
        }
        [HttpPost]
        public ActionResult Create(RejectionReason pRejectionReason)
        {
            pRejectionReason.IsActive = true;

            new RejectionReasonService(_configuration).DataAccessLayer.UpdateRejectionReason(pRejectionReason);
            this.CreateObjectAlert("Rejection Reason");
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var RejectionReason = new RejectionReasonService(_configuration).DataAccessLayer.GetRejectionReason(new RejectionReason() { ReasonId = id });

            if (RejectionReason == null)
            {
                return RedirectToAction("Index");
            }


            return View(RejectionReason);
        }
        [HttpPost]
        public ActionResult Edit(RejectionReason pRejectionReason)
        {

            new RejectionReasonService(_configuration).DataAccessLayer.UpdateRejectionReason(pRejectionReason);
            this.CreateObjectAlert("Rejection Reason");
            return RedirectToAction("Index");
        }

    }
}