﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ProductController> _logger;

        public ProductController(IConfiguration configuration, ILogger<ProductController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        // GET: Product
        public ActionResult Index()
        {
            try
            {
                var objProduct = new ProductService(_configuration).DataAccessLayer;
                List<MasterProduct> masterProductslst = objProduct.GetMasterProductList().
                     FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId).
                     OrderByDescending(i => i.ProductId).ToList();
                return View(masterProductslst);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(ProductController)}-{nameof(Index)} - Error occur..");
                return View(new List<MasterProduct>());
            }
        }

        public ActionResult Create()
        {
            MasterProduct prodviewmodel = new MasterProduct();
            prodviewmodel = new MasterProduct() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() };
            return View(prodviewmodel);
        }

        [HttpPost]
        public ActionResult Create(MasterProduct pProduct)
        {


            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Product data is not valid");
                return View(pProduct);
            }

            pProduct.ProductType = pProduct.ProductTypeName.ToString();

            pProduct.IsActive = true;
            pProduct.AddBy = UserUtility.CurrentUser.Id;
            if (new ProductService(_configuration).DataAccessLayer.GetMasterProductList().FirstOrDefault(i => i.ProductCode == pProduct.ProductCode && i.OrganizationId == pProduct.OrganizationId) != null)
            {
                ModelState.AddModelError("ProductCode", "Duplicate Product Code For Same Organization");
                return View(pProduct);
            }
            if (pProduct.OrganizationId == 0)
            {
                ModelState.AddModelError("OrganizationId", "Please select the organization");
                return View(pProduct);
            }
            pProduct.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            new ProductService(_configuration).DataAccessLayer.UpdateMasterProduct(pProduct);

            this.CreateObjectAlert("Product");
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {

            var Product = new ProductService(_configuration).DataAccessLayer.GetMasterProduct(new MasterProduct() { ProductId = id });

            if (Product == null)
            {
                return RedirectToAction("Index");
            }


            return View(Product);
        }
        [HttpPost]
        public ActionResult Edit(MasterProduct pProduct)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Product data is not valid");
                return View(pProduct);
            }
            pProduct.ProductType = pProduct.ProductTypeName.ToString();
            if (new ProductService(_configuration).DataAccessLayer.GetMasterProductList().FirstOrDefault(i => i.ProductCode == pProduct.ProductCode && i.OrganizationId == pProduct.OrganizationId && i.ProductId != pProduct.ProductId) != null)
            {
                ModelState.AddModelError("ProductCode", "Duplicate Product Code For Same Organization");
                return View(pProduct);
            }
            if (pProduct.OrganizationId == 0)
            {
                ModelState.AddModelError("OrganizationId", "Please select the organization");
                return View(pProduct);
            }
            pProduct.EdittedBy = UserUtility.CurrentUser.Id;
            pProduct.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            new ProductService(_configuration).DataAccessLayer.UpdateMasterProduct(pProduct);
            this.UpdatedObjectAlert("Product");
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {

            var Product = new ProductService(_configuration).DataAccessLayer.GetMasterProduct(new MasterProduct() { ProductId = id });
            if (Product != null)
            {
                Product.IsActive = !Product.IsActive;
                Product.EdittedBy = UserUtility.CurrentUser.Id;
                new ProductService(_configuration).DataAccessLayer.UpdateMasterProduct(Product);

            }
            return RedirectToAction("Index");
        }

    }
}