﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class SupervisorCalendarController : Controller
    {
        private readonly IConfiguration _configuration;
        int orgID = 0;
        public SupervisorCalendarController(IConfiguration configuration)
        {
            _configuration = configuration;
            orgID = UserUtility.CurrentUser.OrganizationId.ToInteger0();
        }
        public IActionResult Index()
        {
            SupervisorCalendarViewModel model = new SupervisorCalendarViewModel();
            model.AllMachine = GetMachineMapping(orgID);
            model.AllShift = GetAllShift(orgID);
            return View(model);
        }
        //public bool SaveSupervisorCalendar([FromBody] string eventString)
        //{
        //    List<SupervisorCalendarSaveViewModel> EventEntries = JsonConvert.DeserializeObject<List<SupervisorCalendarSaveViewModel>>(eventString);
        //    bool save = false;            
        //    if (EventEntries == null)
        //    {
        //        return save;
        //    }

        //    try
        //    {
        //        for (int k = 0; k < EventEntries.Count; k++)
        //        {
        //            if (EventEntries[k].id.Contains("new"))
        //            {
        //                EventEntries[k].id = "0";
        //            }
        //        }
                
        //        for (int j = 0; j < EventEntries.Count; j++)
        //        {
        //            SupervisorCalendarViewModel objEvent = new SupervisorCalendarViewModel();
        //            if (EventEntries[j].id != "0")
        //            {
        //                objEvent = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryById(new ProductionEntry() { ProductionId = ProductionEntry[j].ProductionId.ToInteger0() });
                       
        //            }
        //            else
        //            {
        //                objEvent.id = 0;
        //                objEvent.ModifiedOn = DateTime.Now;
        //            }
                   
        //            if (new ProductionService(_configuration).DataAccessLayer.UpdateProductionEntryThroughApi(objEvent))
        //            {
        //                save = true;
        //            }
        //            else
        //                save = false;
        //        }


               
        //    }
        //    catch (Exception ex)
        //    {

        //        save = false;
        //    }

        //    return save;
        //}
        public List<SelectListItem> GetMachineMapping(int organizationID)
        {
            var objMachinedata = new MachineService(_configuration).DataAccessLayer;
            var Machinelistdata = objMachinedata.GetMasterMachineList();
          
            var machines = Machinelistdata.Where(m => m.OrganizationId == 5).Select(m => new SelectListItem
            {
                Text = m.MachineName,
                Value = m.MachineID.ToString()
            }).ToList();
           
            return machines;
        }
        public List<SelectListItem> GetAllShift(int organizationid)
        {

            var AllShiftType = new ShiftService(_configuration).DataAccessLayer.GetShiftList().FindAll(i => i.OrganizationId == organizationid).ToList();
            var shifts = AllShiftType.Select(m => new SelectListItem
            {
                Text = m.ShiftName,
                Value = m.ShiftId.ToString()
            }).ToList();

            return shifts;
        }
    }
}
