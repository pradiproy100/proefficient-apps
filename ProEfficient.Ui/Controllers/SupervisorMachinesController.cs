﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Ui.Entity;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Dal.Utility;
using ProEfficient.Core.CustomAuthorization;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class SupervisorMachinesController : Controller
    {
        private readonly ProefficientContext _context;
        private readonly IConfiguration _configuration;
        private int CurrentOrgId;
        public SupervisorMachinesController(ProefficientContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            CurrentOrgId = UserUtility.CurrentUser.OrganizationId;
        }
        public JsonResult GetEvents(SupervisorScheduleSearch model)
        {
            if (model.MachineIdsArr != null) { model.MachineIds = string.Join(",", model.MachineIdsArr); }
            if (model.ShiftIdsArr != null) { model.ShiftIds = string.Join(",", model.ShiftIdsArr); }
            if (model.SupervisorIdsArr != null) { model.SupervisorIds = string.Join(",", model.SupervisorIdsArr); }
            model.OrganizationId = CurrentOrgId;
            var objService = new SupervisorSchedulerService(_configuration).DataAccessLayer;
            List<SupervisorMachineMapVM> events = objService.GetSupervisorScheduleList(model).ToList();

            // var events = _context.SupervisorMachineMaps.Include(s => s.Machine).Include(s => s.Shift).Include(s => s.Supervisor).ToList();
            var m = events.Select(i => new
            {
                i.SupervisorMachineId,
                i.MachineId,
                i.SupervisorId,
                i.ShiftId,
                i.StartDate,
                i.EndDate,
                i.IsFullDay,
                i.IsActive,
                i.ThemeColor,
                i.MachineName,
                i.SupervisorName,
                i.ShiftName,
                arrMachineIds = i.MachineIds.Split(",")

            });
            // var events = _context.SupervisorMachineMaps.ToList();
            // return Json( events );
            //return new JsonResult(events,new JsonSerializerSettings());
            // return Json(new { data = events });
            return new JsonResult(new { data = m });
        }
        [HttpPost]
        public JsonResult SaveEvent(SupervisorMachineMapVM e)
        {
            var status = false;
            if (e.ShiftId == 1)
            { e.ThemeColor = "Green"; }
            else if (e.ShiftId == 2)
            { e.ThemeColor = "Blue"; }
            else if (e.ShiftId == 3)
            { e.ThemeColor = "Orange"; }
            else
            { e.ThemeColor = "Red"; }
            if (e.arrMachineIds != null) { e.MachineIds = string.Join(",", e.arrMachineIds); }
            var objService = new SupervisorSchedulerService(_configuration).DataAccessLayer;
            string msg_Error = string.Empty;
            e.EndDate.AddMinutes(1439);
            e.OrganizationId = CurrentOrgId;
            try
            {
                status = objService.UpdateSupervisorMachine(e, ref msg_Error);
            }
            catch (Exception ex)
            {
                status = false;
            }


            return Json(new { data = new { status = status, msgerror = msg_Error } });
        }
        // GET: SupervisorMachines
        public IActionResult Index()
        {
            ViewData["MachineId"] = new SelectList(_context.MasterMachines.Where(h => h.OrganizationId == CurrentOrgId), "MachineId", "MachineName");
            ViewData["ShiftId"] = new SelectList(_context.Shifts.Where(h => h.OrganizationId == CurrentOrgId && h.IsActive == true), "ShiftId", "ShiftName");
            ViewData["SupervisorId"] = new SelectList(_context.AspNetUsers.Where(u => u.UserTypeId == 2 && u.OrganizationId == CurrentOrgId), "Id", "UserName");
            //var proefficientContext = _context.SupervisorMachineMaps.Include(s => s.Machine).Include(s => s.Shift).Include(s => s.Supervisor);
            return View();
        }
        [HttpPost]
        public JsonResult DeleteEvent(int supervisorMachineId)
        {
            var objService = new SupervisorSchedulerService(_configuration).DataAccessLayer;
            var status = false;
            try
            {
                status = objService.DeleteSupervisorMachine(supervisorMachineId);
            }
            catch (Exception ex)
            {
                status = false;
            }


            return Json(new { data = new { status = status } });
        }



    }
}
