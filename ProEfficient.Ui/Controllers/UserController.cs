﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProEfficient.Business;
using ProEfficient.Core.Auth.DTO;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class UserController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string apiBaseUrl;
        private int CurrentOrgId;
        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;            
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
            CurrentOrgId = UserUtility.CurrentUser.OrganizationId;
        }

        // GET: User
        [HttpGet]
        public async Task<ActionResult> IndexAsync()
        {

            if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
            {
                return RedirectToAction("Login", "Login");
            }
            List<UserForListDTO> UsrList = new List<UserForListDTO>();
            //var objUser = new UserManagementService(_configuration).DataAccessLayer;
            //List<OrganizationUser> UsrList = new List<OrganizationUser>();
            //UsrList = objUser.GetUserList().FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId);
            //if (UsrList == null)
            //{
            //    UsrList = new List<OrganizationUser>();
            //}
            using (var httpClient = new HttpClient())
            {
              //  string accessToken = await HttpContext.GetTokenAsync("access_token");
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
              //  httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                //var json = JsonConvert.SerializeObject(pLogin);
               // var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
               var response = await httpClient.GetAsync("/api/admin/getUserList");
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        UserForListDTO dto = o.ToObject<UserForListDTO>();
                        if (dto.OrganizationId == CurrentOrgId)
                        {
                            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
                            {
                                if (!UserUtility.IsSuperAdmin(dto.UserTypeId.ToInteger0()))
                                {
                                    UsrList.Add(dto);
                                }
                            }
                            else { UsrList.Add(dto); }
                        }
                    }                    

                   // lstuser = user;//JsonConvert.DeserializeObject<APIUser>(apiResponse);
                }
            }
            return View(UsrList);
        }
        public ActionResult Create()
        {

            if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
            {
                return RedirectToAction("Login", "Login");
            }
            var org = new UserForRegistrationDTO();
            org.OrganizationId = CurrentOrgId;
            org.AddBy = UserUtility.CurrentUser.Id;
            return View(org);
        }
        [HttpPost]
        public async Task<ActionResult> Create(UserForRegistrationDTO pUser)
        {
            if (ModelState.IsValid)
            {

                if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
                {
                    return RedirectToAction("Login", "Login");
                }
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(apiBaseUrl);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method  
                    var json = JsonConvert.SerializeObject(pUser);
                    var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync("/api/admin/register", stringContent);
                    if (response.IsSuccessStatusCode)
                    {
                        var apiResponse = response.Content.ReadAsStringAsync().Result;
                        var obj = JObject.Parse(apiResponse);
                        var user = obj["user"].ToObject<APIUser>();
                        this.CreateObjectAlert("User");
                        return RedirectToAction("Index");

                    }
                    else
                    {
                        var apiResponse = response.Content.ReadAsStringAsync().Result;
                        var obj = JArray.Parse(apiResponse);
                        for(int i=0;i<obj.Count;i++)
                        {
                            ModelState.AddModelError(string.Empty, obj[i]["Description"].ToString());
                        }
                       // string error = obj[0]["Description"].ToString();
                       //// this.ErrorMessage(error);
                        return View(pUser);
                    }
                }
            }
            else { return View(pUser); }
           
        }
        
        public async Task<ActionResult> Delete(int id)
        {

            if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
            {
                return RedirectToAction("Login", "Login");
            }
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method                
                var response = await httpClient.GetAsync("/api/admin/deactivateUser?userId=" + id);
            }
            
            
            return RedirectToAction("Index");
        }
        public async Task<ActionResult> Edit(int id)
        {
            if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
            {
                return RedirectToAction("Login", "Login");
            }
            //var User = new UserManagementService(_configuration).DataAccessLayer.GetUser(new OrganizationUser() { UserId = id });
            //User.OrganizationId = UserUtility.CurrentUser.OrganizationId;
            UserForRegistrationDTO User = new UserForRegistrationDTO();
            using (var httpClient = new HttpClient())
            {
               
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method                
                var response = await httpClient.GetAsync("/api/admin/getUserById?userId="+id);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JObject o = JObject.Parse(apiResponse);
                  
                      User   = o.ToObject<UserForRegistrationDTO>();
                    User.EdittedBy = UserUtility.CurrentUser.Id;
                }
            }
            if (User == null || User.Id==0)
            {
                return RedirectToAction("Index");
            }

            return View(User);
        }
        [HttpPost]
        public async Task<ActionResult> Edit(UserForRegistrationDTO pUser)
        {
            if(ModelState.IsValid)
            { 
            if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
            {
                return RedirectToAction("Login", "Login");
            }
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(apiBaseUrl);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method  
                    var json = JsonConvert.SerializeObject(pUser);
                    var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync("/api/admin/updateUser", stringContent);
                    if (response.IsSuccessStatusCode)
                    {
                        var apiResponse = response.Content.ReadAsStringAsync().Result;
                        var obj = JObject.Parse(apiResponse);
                        var user = obj["user"].ToObject<APIUser>();
                        this.CreateObjectAlert("User");
                        return RedirectToAction("Index");

                    }
                    else
                    {
                        var apiResponse = response.Content.ReadAsStringAsync().Result;
                        var obj = JArray.Parse(apiResponse);
                        for (int i = 0; i < obj.Count; i++)
                        {
                            ModelState.AddModelError(string.Empty, obj[i]["Description"].ToString());
                        }
                        // string error = obj[0]["Description"].ToString();
                        //// this.ErrorMessage(error);
                        return View(pUser);
                    }
                }
            }
            else { return View(pUser); }
           

        }

        public async Task<ActionResult> Details(int id)
        {


            if ((!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())) && (!UserUtility.IsAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0())))
            {
                return RedirectToAction("Login", "Login");
            }
            UserForRegistrationDTO User = new UserForRegistrationDTO();//new UserManagementService(_configuration).DataAccessLayer.GetUser(new OrganizationUser() { UserId = id });
            using (var httpClient = new HttpClient())
            {

                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method                
                var response = await httpClient.GetAsync("/api/admin/getUserById?userId=" + id);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JObject o = JObject.Parse(apiResponse);

                    User = o.ToObject<UserForRegistrationDTO>();                   
                }
            }
            if (User.Id > 0)
            {
                var SelectedOrganization = new OrganizationService(_configuration).DataAccessLayer.GetOrganizationList().FirstOrDefault(i => i.OrganizationId == User.OrganizationId.ToInteger0());
                var SelectedUserType = new UserManagementService(_configuration).DataAccessLayer.GetAllUserType().FirstOrDefault(i => i.UserTypeID == User.UserTypeId);
                if (SelectedUserType != null)
                {
                    User.UserTypeName = SelectedUserType.UserTypeName;
                }

                if (SelectedOrganization != null)
                {
                    User.OrganizationName = SelectedOrganization.OrganizationName;
                }
            }
            return View(User);

        }
        
        public ActionResult Logout()
        {
            UserUtility.CurrentUser = null;
            //return View();
             return RedirectToAction("Login", "Login");
        }
    }
}