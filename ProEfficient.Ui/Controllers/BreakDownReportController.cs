﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using System;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class BreakDownReportController : Controller
    {
        private readonly IConfiguration _configuration;

        public BreakDownReportController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: BreakDownReport
        public ActionResult Index()
        {

            BreakDownReport objVw = new BreakDownReport();
            objVw.dtBreakdown = new System.Data.DataTable();
            return View(objVw);
        }

        // GET: BreakDownReport/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BreakDownReport/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BreakDownReport/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BreakDownReport/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BreakDownReport/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BreakDownReport/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BreakDownReport/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public async Task<ActionResult> DisplaySearchResults(BreakDownReport criteria, String command)
        {
            //var model = // build list based on the properties of criteria
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;

            string startdate = criteria.StartDate;
            string enddate = criteria.EndDate;
            DataSet ds = new DataSet();
            try
            {
                startdate = startdate.Split('/')[2] + "-" + startdate.Split('/')[1] + "-" + startdate.Split('/')[0];
                enddate = enddate.Split('/')[2] + "-" + enddate.Split('/')[1] + "-" + enddate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                ds = await rd.GetBreakDownList(criteria);
            }
            catch
            { }
            DataTable dt = new DataTable();
            if (ds.Tables.Count > 0)
                criteria.dtBreakdown = ds.Tables[0];
            else
                criteria.dtBreakdown = dt;
            return View("Index", criteria);
        }

        public async Task<string> getLossData()
        {
            string MachineID = Request.Query["MachineID"];
            string StartDate = Request.Query["StartDate"];
            string EndDate = Request.Query["EndDate"];
            string ShiftID = Request.Query["ShiftID"];
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);
            }
            catch
            {

            }
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

                sb.Append("{\"labels\": [");
                int cnt = 0;
                for (int x = 0; x < dt.Columns.Count; x++)
                {
                    string col = dt.Columns[x].ColumnName;
                    if (cnt == dt.Columns.Count - 1)
                        sb.Append("\"" + col + "\"],");
                    else
                        sb.Append("\"" + col + "\",");
                    cnt++;
                }
                sb.Append("\"datasets\": [");
                sb.Append("{\"label\":\"Loss\",");

                sb.Append("\"backgroundColor\": \"#e87633\",\"strokeColor\": \"#08206d\",\"pointColor\": \"#08206d\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(20,220,220,15)\",\"data\":[");

                cnt = 1;
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    foreach (DataColumn dc in dt.Columns)
                    {
                        decimal val = decimal.Parse(string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) == true ? "0" : dr[dc.ColumnName].ToString());
                        val = Math.Round(val, 2);
                        if (cnt == dt.Columns.Count)
                            sb.Append(val.ToString() + "]");
                        else
                            sb.Append(val.ToString() + ",");
                        cnt++;
                    }

                    sb.Append("}]");
                }
            }
            sb.Append("}");


            return (sb.ToString());
        }

        public async Task<string> getLossDataDonutChart()
        {
            string MachineID = Request.Query["MachineID"];
            string StartDate = Request.Query["StartDate"];
            string EndDate = Request.Query["EndDate"];
            string ShiftID = Request.Query["ShiftID"];

            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);
            }
            catch
            {

            }
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

                sb.Append("{\"type\": \"pie\",\"data\": {\"datasets\": [{\"data\": [");
                int cnt = 0;

                //if (dt.Rows.Count > 0)
                //{
                //    DataRow dr = dt.Rows[0];
                //    for (int x = 0; x < dt.Columns.Count; x++)
                //    {
                //        string col = dt.Columns[x].ColumnName;
                //        decimal val = decimal.Parse(string.IsNullOrEmpty(dr[col].ToString()) == true ? "0" : dr[col].ToString());
                //        val = Math.Round(val, 2);
                //        string[] colors = { "#f56954", "#00a65a", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de", "#727208", "#213d05", "#076d50", "#9b0a99", "#511af4", "#e8ed63", "#ef8051", "#7adde8", "#f26080", "#b980ce", "#420459", "#420459", "#590d0f", "#434402", "#60605d", "#a0fc44", "#efbdc9" };
                //        if (cnt == dt.Columns.Count - 1)
                //            sb.Append("{\"value\": \"" + val + "\",\"color\": \"" + colors[x] + "\",\"highlight\": \"" + colors[x] + "\",\"label\": \"" + col + "\"}]");
                //        else
                //            sb.Append("{\"value\": \"" + val + "\",\"color\": \"" + colors[x] + "\",\"highlight\": \"" + colors[x] + "\",\"label\": \"" + col + "\"},");
                //        cnt++;
                //    }

                //}
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    DataTable dtNew = new DataTable();
                    for (int x = 0; x < dt.Columns.Count; x++)
                    {
                        string col = dt.Columns[x].ColumnName;
                        decimal val = decimal.Parse(string.IsNullOrEmpty(dr[col].ToString()) == true ? "0" : dr[col].ToString());
                        val = Math.Round(val, 2);
                        if (val > 0)
                        {
                            dtNew.Columns.Add(col);
                        }

                    }
                    DataRow newRow = dtNew.NewRow();
                    for (int x = 0; x < dtNew.Columns.Count; x++)
                    {
                        newRow[dtNew.Columns[x].ColumnName] = dr[dtNew.Columns[x].ColumnName];
                    }
                    dtNew.Rows.Add(newRow);

                    for (int x = 0; x < dtNew.Columns.Count; x++)
                    {
                        string col = dtNew.Columns[x].ColumnName;
                        decimal val = decimal.Parse(string.IsNullOrEmpty(newRow[col].ToString()) == true ? "0" : newRow[col].ToString());
                        val = Math.Round(val, 2);

                        if (cnt == dtNew.Columns.Count - 1)
                            sb.Append("\"" + val + "\"],");
                        else
                            sb.Append("\"" + val + "\",");
                        cnt++;
                    }
                    sb.Append("\"backgroundColor\":[");
                    string[] colors = { "#00a65a", "#f56954", "#f39c12", "#00c0ef", "#3c8dbc", "#d2d6de", "#727208", "#213d05", "#076d50", "#9b0a99", "#511af4", "#e8ed63", "#ef8051", "#7adde8", "#f26080", "#b980ce", "#420459", "#420459", "#590d0f", "#434402", "#60605d", "#a0fc44", "#efbdc9" };
                    cnt = 0;
                    for (int x = 0; x < dtNew.Columns.Count; x++)
                    {
                        string col = dtNew.Columns[x].ColumnName;
                        if (cnt == dtNew.Columns.Count - 1)
                            sb.Append("\"" + colors[x] + "\"]");
                        else
                            sb.Append("\"" + colors[x] + "\",");
                        cnt++;
                    }
                    cnt = 0;
                    sb.Append("}],\"labels\": [");
                    for (int x = 0; x < dtNew.Columns.Count; x++)
                    {
                        string col = dtNew.Columns[x].ColumnName;
                        if (cnt == dtNew.Columns.Count - 1)
                            sb.Append("\"" + col + "\"]},");
                        else
                            sb.Append("\"" + col + "\",");
                        cnt++;
                    }
                    sb.Append("\"options\": {\"responsive\": \"true\"}}");


                }



            }
            return (sb.ToString());
        }


        public async Task<string> getLossFrequencyData()
        {
            string MachineID = Request.Query["MachineID"];
            string StartDate = Request.Query["StartDate"];
            string EndDate = Request.Query["EndDate"];
            string ShiftID = Request.Query["ShiftID"];
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);
            }
            catch
            {

            }
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

                sb.Append("{\"labels\": [");
                int cnt = 0;
                for (int x = 0; x < dt.Columns.Count; x++)
                {
                    string col = dt.Columns[x].ColumnName;
                    if (cnt == dt.Columns.Count - 1)
                        sb.Append("\"" + col + "\"],");
                    else
                        sb.Append("\"" + col + "\",");
                    cnt++;
                }
                sb.Append("\"datasets\": [");
                sb.Append("{\"label\":\"Loss\",");

                sb.Append("\"backgroundColor\": \"#08206d\",\"strokeColor\": \"#08206d\",\"pointColor\": \"#08206d\",\"pointStrokeColor\": \"#c1c7d1\",\"pointHighlightFill\": \"#fff\",\"pointHighlightStroke\": \"rgba(20,220,220,15)\",\"data\":[");

                cnt = 1;
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[1];
                    foreach (DataColumn dc in dt.Columns)
                    {
                        decimal val = decimal.Parse(string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) == true ? "0" : dr[dc.ColumnName].ToString());
                        val = Math.Round(val, 2);
                        if (cnt == dt.Columns.Count)
                            sb.Append(val.ToString() + "]");
                        else
                            sb.Append(val.ToString() + ",");
                        cnt++;
                    }
                }


                sb.Append("}]");

            }
            sb.Append("}");


            return (sb.ToString());
        }

        public async Task<string> getLossDataRaderChart()
        {
            string MachineID = Request.Query["MachineID"];
            string StartDate = Request.Query["StartDate"];
            string EndDate = Request.Query["EndDate"];
            string ShiftID = Request.Query["ShiftID"];
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);
                //startdate = StartDate.Split('/')[2] + StartDate.Split('/')[1] + StartDate.Split('/')[0];
                //enddate = EndDate.Split('/')[2] + EndDate.Split('/')[1] + EndDate.Split('/')[0];
                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);
            }
            catch
            {

            }
            if (ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];

                sb.Append("{\"labels\": [");
                int cnt = 0;
                for (int x = 0; x < dt.Columns.Count; x++)
                {
                    string col = dt.Columns[x].ColumnName;
                    if (cnt == dt.Columns.Count - 1)
                        sb.Append("\"" + col + "\"],");
                    else
                        sb.Append("\"" + col + "\",");
                    cnt++;
                }
                sb.Append("\"datasets\": [");
                sb.Append("{\"label\":\"Loss\",");

                sb.Append("\"backgroundColor\": \"#376ec6\",");
                sb.Append("\"borderColor\": \"#376ec6\", \"fill\": \"true\", \"radius\": \"6\", \"pointRadius\": \"6\", \"pointBorderWidth\": \"3\",");
                sb.Append("\"pointBackgroundColor\": \"orange\",\"pointBorderColor\": \"rgba(200, 10, 20, 0.6)\",\"pointHoverRadius\": \"10\",\"data\":[ ");

                cnt = 1;
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    foreach (DataColumn dc in dt.Columns)
                    {
                        decimal val = decimal.Parse(string.IsNullOrEmpty(dr[dc.ColumnName].ToString()) == true ? "0" : dr[dc.ColumnName].ToString());
                        val = Math.Round(val, 2);
                        if (cnt == dt.Columns.Count)
                            sb.Append(val.ToString() + "]");
                        else
                            sb.Append(val.ToString() + ",");
                        cnt++;
                    }
                }

                sb.Append("}]");

            }
            sb.Append("}");


            return (sb.ToString());
        }
        [HttpPost]
        public ActionResult getBreakDownReport(String MachineId, String ShiftId, String StartDate, String EndDate, String ReasonId)
        {
            DateTime startDate, endDate;
            startDate = DateTime.ParseExact(StartDate, "dd/MM/yyyy", null);
            endDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", null);
            var LstBreakDownEntry = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryFilter(startDate, endDate, MachineId.ToInteger0(), ShiftId.ToInteger0(), ReasonId);
            return PartialView(LstBreakDownEntry);
        }
    }
}
