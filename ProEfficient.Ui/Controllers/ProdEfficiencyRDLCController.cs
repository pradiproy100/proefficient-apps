﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.DAL;
using ProEfficient.Dal.Utility;
using Rotativa.AspNetCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProdEfficiencyRDLCController : Controller
    {
        private readonly IConfiguration _configuration;

        public ProdEfficiencyRDLCController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: ProdEfficiency
        public ActionResult Index()
        {

            ProductionReport objVw = new ProductionReport();
            objVw.dtBreakdown = new DataTable();
            objVw.SearchCriteria = Session_ReportSearch;
            return View(objVw);
        }
        [HttpPost]
        public async Task<ActionResult> DisplaySearchResults(ProductionReport criteria, String command)
        {
            //var model = // build list based on the properties of criteria
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;

            try
            {
                if (command == "Search")
                {
                    #region Search                 
                    string startdate = criteria.StartDate;
                    string enddate = criteria.EndDate;
                    DataSet ds = new DataSet();
                    BreakDownReport bcriteria = new BreakDownReport();

                    DataSet ds1 = new DataSet();
                    try
                    {
                        SearchCriteria ReportSearch = new SearchCriteria();
                        //startdate = startdate.Split('/')[2] + startdate.Split('/')[1] + startdate.Split('/')[0];
                        //enddate = enddate.Split('/')[2] + enddate.Split('/')[1] + enddate.Split('/')[0];
                        ReportSearch.StartDate = startdate;
                        ReportSearch.EndDate = enddate;
                        ReportSearch.MachineID = criteria.MachineID.ToStringWithNullEmpty();
                        ReportSearch.ShiftID = criteria.ShiftId.ToStringWithNullEmpty();

                        startdate = startdate.Split('/')[2] + "-" + startdate.Split('/')[1] + "-" + startdate.Split('/')[0];
                        enddate = enddate.Split('/')[2] + "-" + enddate.Split('/')[1] + "-" + enddate.Split('/')[0];
                        criteria.StartDate = startdate;
                        criteria.EndDate = enddate;


                        Session_ReportSearch = ReportSearch;
                        ds = await rd.GetProductionEffList(criteria);


                        bcriteria.MachineID = criteria.MachineID;
                        bcriteria.StartDate = criteria.StartDate;
                        bcriteria.EndDate = criteria.EndDate;


                        //if (!string.IsNullOrEmpty(ShiftID))
                        //    criteria.ShiftId = Convert.ToInt32(ShiftID);
                        ds1 = await rd.GetBreakDownList(bcriteria);
                    }
                    catch
                    { }
                    DataTable dt = new DataTable();

                    if (ds.Tables.Count > 0)
                        criteria.dtProd = ds.Tables[0];
                    else
                        criteria.dtProd = dt;
                    if (ds1.Tables.Count > 0)
                        criteria.dtBreakdown = ds1.Tables[0];
                    else
                        criteria.dtBreakdown = new DataTable();

                    return View("Index", criteria);

                    #endregion
                }
                else if (command.ToLower() == "ExportExcel".ToLower())
                {
                    #region Export To Excel 
                    string startdate = criteria.StartDate;
                    string enddate = criteria.EndDate;
                    DataSet ds = new DataSet();
                    try
                    {
                        //startdate = startdate.Split('/')[2] + startdate.Split('/')[1] + startdate.Split('/')[0];
                        //enddate = enddate.Split('/')[2] + enddate.Split('/')[1] + enddate.Split('/')[0];
                        startdate = startdate.Split('/')[2] + "-" + startdate.Split('/')[1] + "-" + startdate.Split('/')[0];
                        enddate = enddate.Split('/')[2] + "-" + enddate.Split('/')[1] + "-" + enddate.Split('/')[0];
                        criteria.StartDate = startdate;
                        criteria.EndDate = enddate;
                        ds = await rd.GetProductionEffListExcel(criteria);
                    }
                    catch
                    { }
                    DataTable dt = new DataTable();
                    if (ds.Tables.Count > 0)
                    {

                        var stream = new MemoryStream();

                        using (var package = new ExcelPackage(stream))
                        {
                            var workSheet = package.Workbook.Worksheets.Add("data");
                            workSheet.Cells.LoadFromDataTable(ds.Tables[0], true);
                            package.Save();
                        }
                        stream.Position = 0;
                        string excelName = $"DemoExcel.xlsx";

                        //return File(stream, "application/octet-stream", excelName);  
                        return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);

                        //var gv = new GridView();
                        //gv.DataSource = ds.Tables[0];
                        //gv.DataBind();
                        //Response.ClearContent();
                        //Response.Buffer = true;
                        //Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
                        //Response.ContentType = "application/ms-excel";
                        //Response.Charset = "";
                        //StringWriter objStringWriter = new StringWriter();
                        //HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                        //gv.RenderControl(objHtmlTextWriter);
                        //Response.Output.Write(objStringWriter.ToString());
                        //Response.Flush();
                        //Response.End();
                        //return View("Index");
                    }
                    else
                    {
                        ProductionReport objVw = new ProductionReport();
                        objVw.dtBreakdown = new System.Data.DataTable();
                        return View(objVw);
                    }
                    #endregion
                }
                else
                {
                    #region Export To PDF
                    string startdate = criteria.StartDate;
                    string enddate = criteria.EndDate;
                    DataSet ds = new DataSet();
                    BreakDownReport bcriteria = new BreakDownReport();

                    DataSet ds1 = new DataSet();
                    try
                    {
                        //startdate = startdate.Split('/')[2] + startdate.Split('/')[1] + startdate.Split('/')[0];
                        //enddate = enddate.Split('/')[2] + enddate.Split('/')[1] + enddate.Split('/')[0];
                        startdate = startdate.Split('/')[2] + "-" + startdate.Split('/')[1] + "-" + startdate.Split('/')[0];
                        enddate = enddate.Split('/')[2] + "-" + enddate.Split('/')[1] + "-" + enddate.Split('/')[0];
                        criteria.StartDate = startdate;
                        criteria.EndDate = enddate;
                        ds = await rd.GetProductionEffList(criteria);


                        bcriteria.MachineID = criteria.MachineID;
                        bcriteria.StartDate = criteria.StartDate;
                        bcriteria.EndDate = criteria.EndDate;
                        bcriteria.MachineID = criteria.MachineID;

                        //if (!string.IsNullOrEmpty(ShiftID))
                        //    criteria.ShiftId = Convert.ToInt32(ShiftID);
                        ds1 = await rd.GetBreakDownList(bcriteria);
                    }
                    catch
                    { }
                    DataTable dt = new DataTable();

                    if (ds.Tables.Count > 0)
                        criteria.dtProd = ds.Tables[0];
                    else
                        criteria.dtProd = dt;
                    if (ds1.Tables.Count > 0)
                        criteria.dtBreakdown = ds1.Tables[0];
                    else
                        criteria.dtBreakdown = new DataTable();

                    return new ViewAsPdf("IndexPdf", criteria);

                    #endregion
                }
            }
            catch (Exception)
            {
                this.CreateObjectAlert("Data not found");
                return View("Index");
            }
        }

        #region chart generation 
        public async Task<ActionResult> CreateColumn(string MachineID, string StartDate, string EndDate, string ShiftID)
        {


            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);

                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);

                var chart = new Chart(width: 1000, height: 800, theme: ChartTheme.Green);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var ColumnListArr = getColumnList(ds.Tables[0]);
                    var ColumnValueListArr = getColumnValueList(ds.Tables[0]);
                    chart.AddTitle("Loss Duration");
                    chart.AddSeries(chartType: "column",
                           xValue: ColumnListArr,
                           yValues: ColumnValueListArr);

                    return File(chart.GetBytes("png"), "image/bytes");
                }
                return File(chart.GetBytes("png"), "image/bytes");
            }
            catch (Exception Ex)
            {
                var chart = new Chart(width: 300, height: 200)
              .AddSeries(chartType: "bar",
                              xValue: new[] { "0" },
                              yValues: new[] { "0" })
                              .GetBytes("png");
                return File(chart, "image/bytes");
            }
        }
        public async Task<ActionResult> CreateColumnLossFrequency(string MachineID, string StartDate, string EndDate, string ShiftID)
        {
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);

                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);

                var chart = new Chart(width: 1000, height: 800, theme: ChartTheme.Blue);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var ColumnListArr = getColumnList(ds.Tables[0]);
                    var ColumnValueListArr = getColumnValueList(ds.Tables[0]);
                    chart.AddTitle("Loss Frequency");
                    chart.AddSeries(chartType: "column",
                           xValue: ColumnListArr,
                           yValues: ColumnValueListArr);

                    return File(chart.GetBytes("png"), "image/bytes");
                }
                return File(chart.GetBytes("png"), "image/bytes");
            }
            catch (Exception Ex)
            {
                var chart = new Chart(width: 300, height: 200)
              .AddSeries(chartType: "bar",
                              xValue: new[] { "0" },
                              yValues: new[] { "0" })
                              .GetBytes("png");
                return File(chart, "image/bytes");
            }
        }

        public async Task<ActionResult> CreatePieForLoss(string MachineID, string StartDate, string EndDate, string ShiftID)
        {
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);

                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);

                var chart = new Chart(width: 1000, height: 800, theme: ChartTheme.Green);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var ColumnListArr = getColumnList(ds.Tables[0]);
                    var ColumnValueListArr = getColumnValueList(ds.Tables[0]);


                    ArrayList myListString = new ArrayList(ColumnListArr);
                    ArrayList myListInt = new ArrayList(ColumnValueListArr);

                    ArrayList indexArray = new ArrayList();
                    int i = -1;
                    foreach (int item in myListInt)
                    {
                        i++;
                        if (item <= 0)
                        {
                            indexArray.Add(i);
                        }
                    }
                    indexArray.Reverse();
                    foreach (int item in indexArray)
                    {
                        // Removing the element present at index 4 
                        myListString.RemoveAt(item);
                        myListInt.RemoveAt(item);
                    }
                    chart.AddTitle("Loss Duration");
                    //chart.AddSeries(chartType: "pie",
                    //       xValue: ColumnListArr,
                    //       yValues: ColumnValueListArr);

                    chart.AddSeries(chartType: "pie",
                          xValue: myListString.ToArray(),
                          yValues: myListInt.ToArray());

                    return File(chart.GetBytes("png"), "image/bytes");
                }
                return File(chart.GetBytes("png"), "image/bytes");
            }
            catch (Exception Ex)
            {
                var chart = new Chart(width: 300, height: 200)
              .AddSeries(chartType: "bar",
                              xValue: new[] { "0" },
                              yValues: new[] { "0" })
                              .GetBytes("png");
                return File(chart, "image/bytes");
            }
        }
        public async Task<ActionResult> CreateRadarForLoss(string MachineID, string StartDate, string EndDate, string ShiftID)
        {
            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                BreakDownReport criteria = new BreakDownReport();
                criteria.MachineID = Convert.ToInt32(MachineID);

                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetBreakDownList(criteria);

                var chart = new Chart(width: 1000, height: 800, theme: ChartTheme.Green);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var ColumnListArr = getColumnList(ds.Tables[0]);
                    var ColumnValueListArr = getColumnValueList(ds.Tables[0]);
                    chart.AddTitle("Loss Data");
                    chart.AddSeries(chartType: "radar",
                           xValue: ColumnListArr,
                           yValues: ColumnValueListArr);

                    return File(chart.GetBytes("png"), "image/bytes");
                }
                return File(chart.GetBytes("png"), "image/bytes");
            }
            catch (Exception Ex)
            {
                var chart = new Chart(width: 300, height: 200)
              .AddSeries(chartType: "bar",
                              xValue: new[] { "0" },
                              yValues: new[] { "0" })
                              .GetBytes("png");
                return File(chart, "image/bytes");
            }
        }
        public async Task<ActionResult> CreateLineForLoss(string MachineID, string StartDate, string EndDate, string ShiftID)
        {


            StringBuilder sb = new StringBuilder();
            string startdate = "";
            string enddate = "";
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ReportDAL rd = new ReportService(_configuration).DataAccessLayer;
            try
            {
                ProductionReport criteria = new ProductionReport();
                criteria.MachineID = Convert.ToInt32(MachineID);

                startdate = StartDate.Split('/')[2] + "-" + StartDate.Split('/')[1] + "-" + StartDate.Split('/')[0];
                enddate = EndDate.Split('/')[2] + "-" + EndDate.Split('/')[1] + "-" + EndDate.Split('/')[0];
                criteria.StartDate = startdate;
                criteria.EndDate = enddate;
                if (!string.IsNullOrEmpty(ShiftID))
                    criteria.ShiftId = Convert.ToInt32(ShiftID);
                ds = await rd.GetProductionEffList(criteria);

                var chart = new Chart(width: 1000, height: 800, theme: ChartTheme.Green);

                if (ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    string[] ColumnListArr = getColumnList(ds.Tables[1], 2);
                    int[] ColumnValueListArr = getColumnValueList(ds.Tables[1], 2);

                    chart.AddTitle("EFFICIENCY PROFILE");
                    chart.AddSeries(chartType: "line",
                           xValue: ColumnListArr,
                           yValues: ColumnValueListArr);

                    return File(chart.GetBytes("png"), "image/bytes");



                }


                return File(chart.GetBytes("png"), "image/bytes");


            }
            catch (Exception Ex)
            {
                var chart = new Chart(width: 300, height: 200)
              .AddSeries(chartType: "bar",
                              xValue: new[] { "0" },
                              yValues: new[] { "0" })
                              .GetBytes("png");
                return File(chart, "image/bytes");
            }
        }

        public string[] getColumnList(DataTable Dt, int OverLook = 0)
        {
            var ReasonList = new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList();
            List<string> LstColumns = new List<string>();
            int index = 0;
            foreach (DataColumn c in Dt.Columns)
            {
                if (OverLook > 0)
                {
                    if (index < OverLook)
                    {
                        index++;
                        continue;
                    }

                }
                index++;
                var ColumnNameVal = c.ColumnName;
                LstColumns.Add(ColumnNameVal);
            }
            return LstColumns.ToArray();
        }
        public int[] getColumnValueList(DataTable Dt, int OverLook = 0)
        {
            var ReasonList = new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList();
            List<int> LstValueColumns = new List<int>();
            for (int index = 0; index < Dt.Columns.Count; index++)
            {
                if (OverLook > 0)
                {
                    if (index < OverLook)
                        continue;
                }
                int RowValue = 0;
                foreach (DataRow Row in Dt.Rows)
                {
                    RowValue += Math.Round(Row[index].ToDecimal0()).ToInteger0();
                }
                LstValueColumns.Add(RowValue);
            }

            return LstValueColumns.ToArray();

        }

        //public ActionResult CreatePie()
        //{
        //    //Create bar chart
        //    var chart = new Chart(width: 300, height: 200)
        //    .AddSeries(chartType: "pie",
        //                    xValue: new[] { "10 ", "50", "30 ", "70" },
        //                    yValues: new[] { "50", "70", "90", "110" })
        //                    .GetBytes("png");
        //    return File(chart, "image/bytes");
        //}

        //public ActionResult CreateLine()
        //{
        //    //Create bar chart
        //    var chart = new Chart(width: 600, height: 200)
        //    .AddSeries(chartType: "line",
        //                    xValue: new[] { "10 ", "50", "30 ", "70" },
        //                    yValues: new[] { "50", "70", "90", "110" })
        //                    .GetBytes("png");
        //    return File(chart, "image/bytes");
        //}
        #endregion
        #region Session Property
        public SearchCriteria Session_ReportSearch
        {
            get

            {
                var data = HttpContext.Session.GetString("SearchCriteria");
                return JsonConvert.DeserializeObject<SearchCriteria>(data);
            }
            set
            {

                var data = JsonConvert.SerializeObject(value);
                HttpContext.Session.SetString("SearchCriteria", data);

            }
        }
        #endregion

    }

}