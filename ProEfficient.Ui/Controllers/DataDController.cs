﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization("Admin", "SuperAdmin")]
    public class DataDController : Controller
    {
        private readonly IConfiguration _configuration;
        private APIUser CurrentUserObj;
        private int CurrentOrgId;
        private int userID;
        public DataDController(IConfiguration configuration)
        {
            _configuration = configuration;
            CurrentUserObj = UserUtility.CurrentUser;
            CurrentOrgId = CurrentUserObj.OrganizationId;
            userID = CurrentUserObj.Id;
        }

        public IActionResult Index()
        {
          

            return View("Index");

        }

        [HttpPost]
        public JsonResult ExportExcelProduction()
        {           
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var resultList = objCategoryDAL.GetRawProduction();
            var fileName = "RawProduction_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "temp", fileName);
            var table = Core.Utility.UtilityAll.ToDataTable<RawProduction>(resultList);
            if (resultList != null && resultList.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Production");
                        loadProdData(worksheet, table);
                    }

                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }
            }
            else { return Json(new { fileName = "", errorMessage = "No Data Found" }); }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpPost]
        public JsonResult ExportExcelBreakdown()
        {
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var resultList = objCategoryDAL.GetRawBreakdown();
            var fileName = "RawBreakdown_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "temp", fileName);
            var table = Core.Utility.UtilityAll.ToDataTable<RawBreakdown>(resultList);
            if (resultList != null && resultList.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Breakdown");
                        loadBreakData(worksheet, table);
                    }

                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }
            }
            else { return Json(new { fileName = "", errorMessage = "No Data Found" }); }
            //return the Excel file name
            return Json(new { fileName = fileName, errorMessage = "" });
        }
        [HttpPost]
        public JsonResult ExportExcelRejection()
        {
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var resultList = objCategoryDAL.GetRawRejection();
            var fileName = "RawProduction_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";

            //save the file to server temp folder
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "temp", fileName);
            var table = Core.Utility.UtilityAll.ToDataTable<RawRejection>(resultList);
            if (resultList != null && resultList.Count > 0)
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    if (table.Rows.Count > 0)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Rejection");
                        loadRejData(worksheet, table);
                    }

                    //Write the file to the disk
                    FileInfo fi = new FileInfo(fullPath);
                    package.SaveAs(fi);
                }

                //return the Excel file name
                return Json(new { fileName = fileName, errorMessage = "" });
            }
            else { return Json(new { fileName = "", errorMessage = "No Data Found" }); }
        }
        [HttpGet]        
        public ActionResult Download(string file)
        {            
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory +"temp", file);
            byte[] fileByteArray = System.IO.File.ReadAllBytes(fullPath);
            System.IO.File.Delete(fullPath);
            return File(fileByteArray, "application/vnd.ms-excel", file);
        }

        private static void loadProdData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Shift";
            ws.Cells[rowIndex, 4].Value = "Product";
            ws.Cells[rowIndex, 5].Value = "Production";
            ws.Cells[rowIndex, 6].Value = "Rejection";

            using (ExcelRange Rng = ws.Cells[1, 1, 1, 6])
            {

                Rng.Style.Font.Bold = true;

                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Column(1).Width = 15;
            ws.Column(2).Width = 35;
            ws.Column(4).Width = 35;

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        private static void loadBreakData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Shift";
            ws.Cells[rowIndex, 4].Value = "Reason";
            ws.Cells[rowIndex, 5].Value = "Duration";
            ws.Cells[rowIndex, 6].Value = "Start";
            ws.Cells[rowIndex, 7].Value = "End";


            ws.Column(1).Width = 15;
            ws.Column(2).Width = 35;
            ws.Column(4).Width = 35;
            using (ExcelRange Rng = ws.Cells[1, 1, 1, 7])
            {

                Rng.Style.Font.Bold = true;
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        private static void loadRejData(ExcelWorksheet ws, System.Data.DataTable table)
        {

            int rowIndex = 1;

            // headers
            ws.Cells[rowIndex, 1].Value = "Date";
            ws.Cells[rowIndex, 2].Value = "Line";
            ws.Cells[rowIndex, 3].Value = "Shift";
            ws.Cells[rowIndex, 4].Value = "Reason";
            ws.Cells[rowIndex, 5].Value = "Rejection";
           
            using (ExcelRange Rng = ws.Cells[1, 1, 1, 5])
            {
              
                Rng.Style.Font.Bold = true;
               
                Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                Rng.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                Rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                Rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
           
            ws.Column(1).Width = 15;
            ws.Column(2).Width = 35;
            ws.Column(4).Width = 35;
            ws.Cells["A2"].LoadFromDataTable(table, false, OfficeOpenXml.Table.TableStyles.Light8);

        }
        [HttpPost]
        public IActionResult DropAllData()
        {
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
           var res=  objCategoryDAL.DropAllData(CurrentOrgId);
            if (res)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

    }
  
}
