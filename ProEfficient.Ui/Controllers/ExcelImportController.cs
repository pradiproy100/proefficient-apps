﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProEfficient.Ui.ExcelUploadUtility;

namespace ProEfficient.Ui.Controllers
{
    public class ExcelImportController : Controller
    {
        [HttpGet]
        public ActionResult UploadExcel()
        {
            return View();
        }

        [ActionName("Importexcel")]
        [HttpPost]
        public ActionResult Importexcel1(string Type)
        {
            IFormFile file = Request.Form.Files[0];
            //ExcelDataReader works on binary excel file
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            Stream stream = file.OpenReadStream();
            //We need to written the Interface.
            IExcelDataReader reader = null;
            if (file.FileName.EndsWith(".xls"))
            {
                //reads the excel file with .xls extension
                reader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (file.FileName.EndsWith(".xlsx"))
            {
                //reads excel file with .xlsx extension
                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }
            else
            {
                //Shows error if uploaded file is not Excel file
                ModelState.AddModelError("File", "This file format is not supported");
                return View();
            }
            //treats the first row of excel file as Coluymn Names
            var result = reader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    UseHeaderRow = true
                }
            });
            //Adding reader data to DataSet() 
            DataTable DT = result.Tables[0];

            UploadExcelData excelData = new UploadExcelData();
            excelData.UploadDataToServer(Type, DT);

            TempData["ExcelDataTable"] = DT;
            return Ok("Result Declared");
        }

    }
}