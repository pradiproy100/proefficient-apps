﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductionPageController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly string apiBaseUrl;
        private readonly ILogger<ProductionPageController> _logger;

        public ProductionPageController(IConfiguration configuration, ILogger<ProductionPageController> logger)
        {
            _configuration = configuration;
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
            _logger = logger;
        }
        // GET: ProductionPage

        public async Task<ActionResult> Index()
        {
            var productionBreakDownEntryViewModel = new ProductionBreakDownEntryViewModel();

            try
            {
                int OrganizationIDofloginuser = UserUtility.CurrentUser.OrganizationId.ToInteger0();
                int CurrentUserId = UserUtility.CurrentUser.Id;

                var proDate = DateTime.Now.ToString("yyyy-MM-dd");
                var productionEntryUI = new ProductionPageVM();
                productionBreakDownEntryViewModel.ProductionDate = DateTime.ParseExact(proDate, "yyyy-MM-dd", CultureInfo.InvariantCulture); //Can also use .ToString("dd-MM-yyyy");
                productionBreakDownEntryViewModel.OperatorID = CurrentUserId;
                productionBreakDownEntryViewModel.SupervisorID = CurrentUserId;
                productionBreakDownEntryViewModel.CurrentOrganizationId = OrganizationIDofloginuser;
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(apiBaseUrl);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method  
                    HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryUI?ProdDate=" + proDate + "&ProdToDate=" + proDate + "&organizationID=" + OrganizationIDofloginuser);
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        productionEntryUI = JsonConvert.DeserializeObject<ProductionPageVM>(apiResponse);
                    }
                }
                productionBreakDownEntryViewModel.lstAllShift = productionEntryUI.lstshiftData.ToSelectList(c => c.ShiftId.ToString(), c => c.ShiftName);
                productionBreakDownEntryViewModel.lstAllShiftData = productionEntryUI.lstshiftData;
                productionBreakDownEntryViewModel.lstAllMachine = productionEntryUI.lstmachineProductData.ToSelectList(c => c.MachineId.ToString(), c => c.MachineName);
                productionBreakDownEntryViewModel.lstAllOperators = productionEntryUI.lstuserData.ToSelectList(c => c.UserId.ToString(), c => c.UserName);
                productionBreakDownEntryViewModel.lstAllProduct = productionEntryUI.lstmachineProductData.ToSelectList(c => c.ProductId.ToString(), c => c.ProductName);
                productionBreakDownEntryViewModel.lstvmAllProduct = productionEntryUI.lstmachineProductData;
                productionBreakDownEntryViewModel.lstAllRejectReason = productionEntryUI.lstrejectionData;
                productionBreakDownEntryViewModel.lstAllStopageReason = productionEntryUI.lststopageData.ToSelectList(c => c.ReasonId.ToString(), c => c.ReasonName);
                productionBreakDownEntryViewModel.lstAllSubStopageReason = productionEntryUI.lststopageData;
                productionBreakDownEntryViewModel.lstAllRejectionReason = productionEntryUI.lstRejectionReason;
                productionBreakDownEntryViewModel.CurrentUserId = CurrentUserId;
                return View(productionBreakDownEntryViewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(ProductionPageController)}-{nameof(Index)} - Error occur..");
                return View(productionBreakDownEntryViewModel);
            }
        }
        public async Task<List<SelectListItem>> GetMachineMapping( string ProdDate, int organizationID)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<ProductMachineVM> reservationList = new List<ProductMachineVM>();
            using (var httpClient = new HttpClient())
            {
                //using (var response = await httpClient.GetAsync(apiBaseUrl+ "/api/getMachineProductMapping?ProdDate=" + ProdDate + "&organizationID=" + organizationID))
                //{
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method  
                    HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getMachineProductMapping?ProdDate=" + ProdDate + "&organizationID=" + organizationID);
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                    reservationList = JsonConvert.DeserializeObject<List<ProductMachineVM>>(apiResponse);
                    var distinct = reservationList.GroupBy(x => x.MachineID).Select(x => x.FirstOrDefault());
                    foreach (var Type in distinct)
                    {

                        list.Add(new SelectListItem()
                        {
                            Text = Type.MachineName,
                            Value = Type.MachineID.ToStringWithNullEmpty()
                        });


                    }
                   
                }
            }
            return list;
        }
        #region Production Entry
        public ActionResult Create(String ShiftId = "", String MachineId = "", String ProdDate = "")
        {
            var ProductionData = new ProductionEntry();
            ProductionData.ShiftId = ShiftId.IsUndefinedOrnull() ? 0 : ShiftId.ToInteger0();
            ProductionData.ProductionDate = ProdDate.IsUndefinedOrnull() ? DateTime.Now : DateTime.ParseExact(ProdDate, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            ProductionData.MachineID = (MachineId.IsUndefinedOrnull() ? 0 : MachineId.ToInteger0());
            ProductionData.OperatorID = UserUtility.CurrentUser.Id;
            if (!ShiftId.IsUndefinedOrnull())
            {
                var ShiftData = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = ShiftId.ToInteger0() });
                ProductionData.ProductionStartTime = ShiftData.StartTime.ToDecimal0();
                ProductionData.ProductionEndTime = ShiftData.EndTime.ToDecimal0();
            }
            LoadShiftData(ProductionData.ShiftId.ToInteger0());
            return View("Create", ProductionData);


        }

        [HttpGet]
        public string GetAllShift(int organizationid)
        {

            var AllShiftType = new ShiftService(_configuration).DataAccessLayer.GetShiftList().FindAll(i => i.OrganizationId == organizationid).ToList();
            ViewBag.AllShifList = AllShiftType;
            var shiftID = AllShiftType.FirstOrDefault().ShiftId;
            DateTime _ProductionDate = DateTime.Now.AddDays(-2);
            var ProductionList = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryNew
            (new ProductionEntry() { ShiftId = shiftID, OrganizationId = organizationid });
            string data = JsonConvert.SerializeObject(ProductionList);

            return data;
        }

        public void LoadShiftData(int shiftid)
        {
            Decimal ShiftStart = 0.00m;
            Decimal ShiftEnd = 100.00m;
            Boolean IsOverNight = false;
            Decimal OverNightValue = 0;

            Shift objShift = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = shiftid });
            if (objShift != null)
            {
                if (!objShift.isOverNight)
                {
                    ShiftStart = objShift.StartTime.ToDecimal0();
                    ShiftEnd = objShift.EndTime.ToDecimal0();

                }
                else
                {
                    IsOverNight = objShift.isOverNight;
                    OverNightValue = 24;
                }
            }


            ViewBag._isOverNight = IsOverNight;
            ViewBag._shiftStart = ShiftStart;
            ViewBag._shiftEnd = ShiftEnd;
        }

        public ActionResult Edit(String id)
        {
            var ProductionData = new ProductionService(_configuration).DataAccessLayer.GetProductionEntryById(new ProductionEntry() { ProductionId = id.ToInteger0() });
            ProductionData.ProductionStartTime = (ProductionData.StartTime.ToDateTime().Hour.ToInteger0().ToStringWithNullEmpty() + "."
                + ProductionData.StartTime.ToDateTime().Minute.ToInteger0().ToStringWithNullEmpty()).ToDecimal0();

            ProductionData.ProductionEndTime = (ProductionData.EndTime.ToDateTime().Hour.ToInteger0().ToStringWithNullEmpty() + "."
              + ProductionData.EndTime.ToDateTime().Minute.ToInteger0().ToStringWithNullEmpty()).ToDecimal0();
            LoadShiftData(ProductionData.ShiftId.ToInteger0());
            return View("Edit", ProductionData);
        }
        [HttpPost]
        public ActionResult Create(ProductionEntry model)
        {
            if (ModelState.IsValid)
            {
                //model. = UserUtility.CurrentUser.UserId;
                model.TimeOfEntry = DateTime.UtcNow;
                model.IsSaved = false;
                int AddDay = 0;
                if (model.ProductionStartTime > model.ProductionEndTime)
                {
                    AddDay = 1;
                }
                model.StartTime = new DateTime(model.ProductionDate.ToDateTime().Year,
                model.ProductionDate.ToDateTime().Month, model.ProductionDate.ToDateTime().Day,
                model.ProductionStartTime.GetIntPart(), model.ProductionStartTime.GetFractionPart(), 0);

                model.EndTime = new DateTime(model.ProductionDate.ToDateTime().Year,
                 model.ProductionDate.ToDateTime().Month, model.ProductionDate.ToDateTime().Day,
                 model.ProductionEndTime.GetIntPart(), model.ProductionEndTime.GetFractionPart(), 0);
                if (AddDay > 0)
                    model.EndTime = model.EndTime.ToDateTime().AddDays(AddDay);
                var _ShiftDataDetails = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = model.ShiftId.ToInteger0() });
                var TimeOfShift = UtilityAll.getTimeDifference(_ShiftDataDetails.EndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), _ShiftDataDetails.StartTime.ToDecimal());
                var TimeOfProductionEntry = UtilityAll.getTimeDifference(model.ProductionEndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), model.ProductionStartTime.ToDecimal());
                LoadShiftData(model.ShiftId.ToInteger0());
                if (model.MachineID.ToInteger0() <= 0)
                {
                    this.ErrorMessage("Please select a Machine");
                    return View("Create", model);

                }

                new ProductionService(_configuration).DataAccessLayer.UpdateProductionEntry(model);
                this.ShowSuccessMessage("Production details has been saved successfully.");
                return Create("", "", "");
            }
            else
            {
                String _ErrorMessage = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                this.ErrorMessage(_ErrorMessage);
                return View("Create", model);
            }

        }
        [HttpGet]
        public ActionResult ProductionList(String ShiftId = "", String ProdDate = "", String MachineId = "")
        {
            DateTime _ProductionDate = ProdDate.IsUndefinedOrnull() ? DateTime.Now : DateTime.ParseExact(ProdDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ProductionList = new ProductionService(_configuration).DataAccessLayer.GetProductionEntry(new ProductionEntry() { ShiftId = ShiftId.ToInteger0(), ProductionDate = _ProductionDate }, UserUtility.CurrentUser.OrganizationId.ToInteger0());
            if (ProductionList == null)
            {
                ProductionList = new List<ProductionEntry>();
            }
            if (MachineId != "" && MachineId != null)
            {
                ProductionList = ProductionList.FindAll(i => i.MachineID == MachineId.ToInteger0());
            }
            HttpContext.Session.SetString("CurrentProductionDate", _ProductionDate.ToString());
            return View(ProductionList);
        }

        [HttpGet]
        public JsonResult RefreshStoppageSubReason(int id = 0)
        {
            if (id > 0)
            {
                var AllStoppageSSubReason = new StopageSubReasonService(_configuration).DataAccessLayer.GetStopageSubReasonByReasonId(new StopageSubReason()
                {
                    ReasonId = id.ToInteger0()
                });

                return Json(AllStoppageSSubReason);
            }
            else
            {
                return Json(new List<StopageSubReason>());
            }

        }
        [HttpGet]
        public JsonResult getRejectionReasonQuantityList(int id)
        {

            var ReasonList = new RejectionReasonService(_configuration).DataAccessLayer.GetRejectionReasonList();
            var SavedRejectionList = new RejectionReasonService(_configuration).DataAccessLayer.GetProductionRejectionReasonList(id);
            if (SavedRejectionList == null)
            {
                SavedRejectionList = new List<ProductionRejection>();
            }
            var RejectionReasonModelList = new List<RejectionReasonModel>();
            foreach (var reason in ReasonList)
            {
                if (!reason.IsActive.ToBoolean())
                {
                    continue;
                }
                var foundsaved = SavedRejectionList.FirstOrDefault(i => i.RejectionReasonId == reason.ReasonId);
                var reasonmodel = new RejectionReasonModel();
                if (foundsaved != null)
                {
                    reasonmodel.ReasonQuantity = foundsaved.RejectionQuantity.ToInteger0();
                }
                else
                {
                    reasonmodel.ReasonQuantity = 0;
                }
                reasonmodel.ReasonId = reason.ReasonId;
                reasonmodel.ReasonName = reason.ReasonName;
                RejectionReasonModelList.Add(reasonmodel);

            }

            return Json(RejectionReasonModelList);
        }
        [HttpGet]
        public Boolean isTimeValidProduction(
            string id,
            string optype,
            string startTime,
            String endTime,
            string shiftid,
            string machineid,
            string _Date)
        {
            try
            {

                var CurrentData = _Date.IsUndefinedOrnull() ? DateTime.Now : DateTime.ParseExact(_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime StartTime = new DateTime(DateTime.Now.Year,
                     DateTime.Now.Month, DateTime.Now.Day,
                      startTime.ToDecimal0().GetIntPart(), startTime.ToDecimal0().GetFractionPart(), 0);

                DateTime EndTime = new DateTime(DateTime.Now.Year,
                    DateTime.Now.Month, DateTime.Now.Day,
                 endTime.ToDecimal0().GetIntPart(), endTime.ToDecimal0().GetFractionPart(), 0);

                if (StartTime > EndTime)
                {
                    EndTime = EndTime.AddDays(1);
                }
                TimeSpan T = EndTime.Subtract(StartTime);
                double TotalMinutesWouldBeEntered = T.TotalMinutes;
                var Shift = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = shiftid.ToInteger0() });
                double ActualTimeMinutes = getShiftTimeDiff(Shift.StartTime.ToStringx(), (Shift.EndTime.ToDecimal0() > 24 ? Shift.EndTime.ToDecimal0() - 24 : Shift.EndTime.ToDecimal0()).ToStringx());
                double AlreadyEntered = TotalMinutesProductionEntry(shiftid, machineid, CurrentData.ToDateTime(), id.ToInteger0());
                if ((AlreadyEntered + TotalMinutesWouldBeEntered) > ActualTimeMinutes)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {

                return true;
            }

        }

        public Boolean isTimeValidBreakDown(
          string id,
          string optype,
          string startTime,
          String endTime,
          string shiftid,
          string machineid,
          string _Date)
        {
            try
            {

                var CurrentData = _Date.IsUndefinedOrnull() ? DateTime.Now : DateTime.ParseExact(_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime StartTime = new DateTime(DateTime.Now.Year,
                     DateTime.Now.Month, DateTime.Now.Day,
                      startTime.ToDecimal0().GetIntPart(), startTime.ToDecimal0().GetFractionPart(), 0);

                DateTime EndTime = new DateTime(DateTime.Now.Year,
                    DateTime.Now.Month, DateTime.Now.Day,
                 endTime.ToDecimal0().GetIntPart(), endTime.ToDecimal0().GetFractionPart(), 0);

                if (StartTime > EndTime)
                {
                    EndTime = EndTime.AddDays(1);
                }
                TimeSpan T = EndTime.Subtract(StartTime);
                double TotalMinutesWouldBeEntered = T.TotalMinutes;
                var Shift = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = shiftid.ToInteger0() });
                double ActualTimeMinutes = getShiftTimeDiff(Shift.StartTime.ToStringx(), (Shift.EndTime.ToDecimal0() > 24 ? Shift.EndTime.ToDecimal0() - 24 : Shift.EndTime.ToDecimal0()).ToStringx());
                double AlreadyEntered = TotalMinutesProductionEntry(shiftid, machineid, CurrentData.ToDateTime(), id.ToInteger0());
                if ((AlreadyEntered + TotalMinutesWouldBeEntered) > ActualTimeMinutes)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {

                return true;
            }

        }
        public Double TotalMinutesProductionEntry(String ShiftId, String MachineId, DateTime _ProductionDate, int id = 0)
        {
            Double TT = 0;
            var ProductionList = new ProductionService(_configuration).DataAccessLayer.GetProductionEntry(new ProductionEntry() { ShiftId = ShiftId.ToInteger0(), ProductionDate = _ProductionDate, MachineID = MachineId.ToInteger0() }, UserUtility.CurrentUser.OrganizationId.ToInteger0());
            if (ProductionList == null)
            {
                ProductionList = new List<ProductionEntry>();
            }
            if (MachineId != "" && MachineId != null)
            {
                ProductionList = ProductionList.FindAll(i => i.MachineID == MachineId.ToInteger0());
            }
            if (ProductionList != null)
            {

                foreach (var pro in ProductionList)
                {
                    if (id != 0)
                    {
                        if (pro.ProductionId == id)
                        {
                            continue;
                        }
                    }
                    TT += getShiftTimeDiff(pro.StartTime.ToDateTime(), pro.EndTime.ToDateTime());
                }
            }
            return TT;

        }

        public Double TotalMinutesBreakDownEntry(String ShiftId, String MachineId, DateTime _ProductionDate, int id = 0)
        {
            Double TT = 0;
            var BreakDownList = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryListDateTime(_ProductionDate);
            if (BreakDownList == null)
            {
                BreakDownList = new List<BreakDownEntry>();
            }

            if (MachineId != "" && MachineId != null)
            {
                BreakDownList = BreakDownList.FindAll(i => i.MachineID == MachineId.ToInteger0());
            }
            if (UserUtility.CurrentUser != null && UserUtility.CurrentUser.OrganizationId.ToInteger0() > 0)
            {
                BreakDownList = BreakDownList.FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId.ToInteger0());
            }
            if (BreakDownList != null)
            {

                foreach (var pro in BreakDownList)
                {
                    if (id != 0)
                    {
                        if (pro.BreakDownID == id)
                        {
                            continue;
                        }
                    }
                    TT += getShiftTimeDiff(pro.StartTime.ToDateTime(), pro.EndTime.ToDateTime());
                }
            }
            return TT;

        }
        public double getShiftTimeDiff(string startTime, String endTime)
        {
            DateTime StartTime = new DateTime(DateTime.Now.Year,
                DateTime.Now.Month, DateTime.Now.Day,
                 startTime.ToDecimal0().GetIntPart(), startTime.ToDecimal0().GetFractionPart(), 0);

            DateTime EndTime = new DateTime(DateTime.Now.Year,
                DateTime.Now.Month, DateTime.Now.Day,
             endTime.ToDecimal0().GetIntPart(), endTime.ToDecimal0().GetFractionPart(), 0);

            int dayadd = 0;

            if (StartTime > EndTime)
            {
                EndTime = EndTime.AddDays(1);
            }
            TimeSpan T = EndTime.Subtract(StartTime);
            double TotalMinutes = T.TotalMinutes;
            return TotalMinutes;

        }
        public double getShiftTimeDiff(DateTime StartTime, DateTime EndTime)
        {
            if (StartTime > EndTime)
            {
                EndTime = EndTime.AddDays(1);
            }
            TimeSpan T = EndTime.Subtract(StartTime);
            double TotalMinutes = T.TotalMinutes;
            return TotalMinutes;

        }
        public ActionResult DeleteProduction(int id)
        {
            new ProductionService(_configuration).DataAccessLayer.DeleteProductionEntry(id);

            return Create("", "", "");
        }
        public ActionResult DeleteBreakDown(int id)
        {
            new BreakDownService(_configuration).DataAccessLayer.DeleteBreakDownEntry(id);
            return CreateBreakDown("", "", "");
        }
        #endregion
        public ActionResult CreateBreakDown(String ShiftId = "", String MachineId = "", String ProdDate = "")
        {
            var BreakdownData = new BreakDownEntry();
            BreakdownData.ShiftId = ShiftId.IsUndefinedOrnull() ? 0 : ShiftId.ToInteger0();
           
            BreakdownData.ProductionDate = ProdDate.IsUndefinedOrnull() ? DateTime.Now : DateTime.ParseExact(ProdDate, "dd/MM/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
            BreakdownData.MachineID = (MachineId.IsUndefinedOrnull() ? 0 : MachineId.ToInteger0());
            BreakdownData.OperatorID = UserUtility.CurrentUser.Id;
            if (!ShiftId.IsUndefinedOrnull())
            {
                var ShiftData = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = ShiftId.ToInteger0() });
                BreakdownData.BreakDownStartTime = ShiftData.StartTime.ToDecimal0();
                BreakdownData.BreakDownEndTime = ShiftData.EndTime.ToDecimal0();
            }
            LoadShiftData(BreakdownData.ShiftId.ToInteger0());
            return View("CreateBreakDown", BreakdownData);


        }
        public ActionResult EditBreakDown(String id)
        {
            var BreakdownData = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntry(new BreakDownEntry() { BreakDownID = id.ToInteger0() });

            BreakdownData.BreakDownStartTime = (BreakdownData.StartTime.ToDateTime().Hour.ToInteger0().ToStringWithNullEmpty() + "."
                + BreakdownData.StartTime.ToDateTime().Minute.ToInteger0().ToStringWithNullEmpty()).ToDecimal0();
            BreakdownData.BreakDownEndTime = (BreakdownData.EndTime.ToDateTime().Hour.ToInteger0().ToStringWithNullEmpty() + "."
                + BreakdownData.EndTime.ToDateTime().Minute.ToInteger0().ToStringWithNullEmpty()).ToDecimal0();


            LoadShiftData(BreakdownData.ShiftId.ToInteger0());
            return View("EditBreakDown", BreakdownData);
        }
        [HttpPost]
        public ActionResult CreateBreakDown(BreakDownEntry breakdownentry)
        {
            if (ModelState.IsValid)
            {
                //breakdownentry. = UserUtility.CurrentUser.UserId;
                breakdownentry.TimeOfEntry = DateTime.UtcNow;
                breakdownentry.IsSaved = false;
                int AddDay = 0;
                if (breakdownentry.BreakDownStartTime > breakdownentry.BreakDownEndTime)
                {
                    AddDay = 1;
                }
                breakdownentry.StartTime = new DateTime(breakdownentry.ProductionDate.ToDateTime().Year,
                breakdownentry.ProductionDate.ToDateTime().Month, breakdownentry.ProductionDate.ToDateTime().Day,
                breakdownentry.BreakDownStartTime.GetIntPart(), breakdownentry.BreakDownStartTime.GetFractionPart(), 0);

                breakdownentry.EndTime = new DateTime(breakdownentry.ProductionDate.ToDateTime().Year,
                 breakdownentry.ProductionDate.ToDateTime().Month, breakdownentry.ProductionDate.ToDateTime().Day,
                 breakdownentry.BreakDownEndTime.GetIntPart(), breakdownentry.BreakDownEndTime.GetFractionPart(), 0);
                if (AddDay > 0)
                    breakdownentry.EndTime = breakdownentry.EndTime.ToDateTime().AddDays(AddDay);
                breakdownentry.IsActive = true;
                var _ShiftDataDetails = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = breakdownentry.ShiftId.ToInteger0() });
                var TimeOfShift = UtilityAll.getTimeDifference(_ShiftDataDetails.EndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), _ShiftDataDetails.StartTime.ToDecimal());
                var TimeOfProductionEntry = UtilityAll.getTimeDifference(breakdownentry.BreakDownEndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), breakdownentry.BreakDownStartTime.ToDecimal());

                if (breakdownentry.MachineID.ToInteger0() <= 0)
                {
                    this.ErrorMessage("Please select a Machine");
                    LoadShiftData(breakdownentry.ShiftId.ToInteger0());
                    return View("CreateBreakDown", breakdownentry);

                }
                if (breakdownentry.OrganizationId == null)
                {
                    breakdownentry.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                }
                breakdownentry.UserID = breakdownentry.OperatorID;
                breakdownentry.AddBy = breakdownentry.OperatorID;
                breakdownentry.EdittedBy = breakdownentry.OperatorID;
                LoadShiftData(breakdownentry.ShiftId.ToInteger0());
                new BreakDownService(_configuration).DataAccessLayer.UpdateBreakDownEntry(breakdownentry);
                this.ShowSuccessMessage("BreakDown details has been saved successfully.");
                return Create("", "", "");
            }
            else
            {
                String _ErrorMessage = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                this.ErrorMessage(_ErrorMessage);
                LoadShiftData(breakdownentry.ShiftId.ToInteger0());
                return View("CreateBreakDown", breakdownentry);
            }
        }
        [HttpGet]
        public ActionResult BreakDownList([FromQuery]String ShiftId = "", String ProdDate = "", String MachineId = "")
        {
//            DateTime _ProductionDate
//;
//            if (DateTime.TryParse(ProdDate, out _ProductionDate)) 
//            {
//            }
               // DateTime _ProductionDate = ProdDate.IsUndefinedOrnull() ? DateTime.Now : DateTime.ParseExact(ProdDate, "dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
            var BreakDownList = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryListFormMaxProdToTwoDays();
            if (BreakDownList == null)
            {
                BreakDownList = new List<BreakDownEntry>();
            }
            if (ShiftId != "" && ShiftId != null)
            {
                BreakDownList = BreakDownList.FindAll(i => i.ShiftId == ShiftId.ToInteger0());
            }
            if (MachineId != "" && MachineId != null)
            {
                BreakDownList = BreakDownList.FindAll(i => i.MachineID == MachineId.ToInteger0());
            }
            return View(BreakDownList);
        }
        [HttpGet]
        [Route("ProductionEntryDataByShiftAndDatep")]
        public async Task<List<GetProductionEntryByShiftAndDateVM>> ProductionEntryDataByShiftAndDatep([FromQuery] string ProdDate, string Shiftid, string organizationID)
        {

            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            List<GetProductionEntryByShiftAndDateVM> AllProductions = new List<GetProductionEntryByShiftAndDateVM>();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getProductionEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        GetProductionEntryByShiftAndDateVM dto = o.ToObject<GetProductionEntryByShiftAndDateVM>();

                        AllProductions.Add(dto);
                    }
                }
            }
            //List<RejectionEntryByShiftAndDateVM> rejList = new List<RejectionEntryByShiftAndDateVM>();
            //using (var httpClient = new HttpClient())
            //{
            //    httpClient.BaseAddress = new Uri(apiBaseUrl);
            //    httpClient.DefaultRequestHeaders.Accept.Clear();
            //    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    //GET Method 
            //    HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getRejectionEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
            //    if (response.IsSuccessStatusCode)
            //    {

            //        var apiResponse = response.Content.ReadAsStringAsync().Result;
            //        JArray a = JArray.Parse(apiResponse);
            //        foreach (var o in a)
            //        {
            //            RejectionEntryByShiftAndDateVM dto = o.ToObject<RejectionEntryByShiftAndDateVM>();

            //            rejList.Add(dto);
            //        }

            //    }
            //}

            //if (rejList.Count > 0)
            //{
            //    foreach (GetProductionEntryByShiftAndDateVM p in AllProductions)
            //    {
            //        p.RejectionReasonList = new List<RejectionEntryByShiftAndDateVM>();
            //        foreach (RejectionEntryByShiftAndDateVM c in rejList)
            //        {
            //            if (p.MachineID == c.MachineID)
            //            {

            //                p.RejectionReasonList.Add(c);
            //            }
            //        }

            //    }
            //}
            return AllProductions;
            // return new JsonResult(AllProductions);
        }

        [HttpGet]
        [Route("BreakDownEntryDataByShiftAndDatep")]
        public async Task<List<GetBreakDownEntryByShiftAndDateVM>> BreakDownEntryDataByShiftAndDatep([FromQuery] string ProdDate, string Shiftid, string organizationID)
        {

            int shiftid = Shiftid.ToInteger();
            int OrgId = organizationID.ToInteger();
            DateTime ProductionDate = new DateTime();
            DateTime.TryParse(ProdDate, out ProductionDate);
            List<GetBreakDownEntryByShiftAndDateVM> AllBreakdowns = new List<GetBreakDownEntryByShiftAndDateVM>();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(apiBaseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //GET Method  
                HttpResponseMessage response = await httpClient.GetAsync("/api/ProductionApi/getBreakDownEntryDataByShiftAndDate?ProdDate=" + ProductionDate + "&ShiftId=" + shiftid + "&OrganizationID=" + OrgId);
                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = response.Content.ReadAsStringAsync().Result;
                    JArray a = JArray.Parse(apiResponse);
                    foreach (var o in a)
                    {
                        GetBreakDownEntryByShiftAndDateVM dto = o.ToObject<GetBreakDownEntryByShiftAndDateVM>();

                        AllBreakdowns.Add(dto);
                    }
                }
            }

            return AllBreakdowns;
        }


    }
}