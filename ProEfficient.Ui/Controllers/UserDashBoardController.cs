﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.HomeDashBoardModels;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class UserDashBoardController : Controller
    {
        private readonly IConfiguration _configuration;
        private APIUser CurrentUserObj;
        private int CurrentOrgId;
        private int userID;
        private readonly ILogger<UserDashBoardController> _logger;

        public UserDashBoardController(IConfiguration configuration, ILogger<UserDashBoardController> logger)
        {
            _configuration = configuration;
            CurrentUserObj = UserUtility.CurrentUser;
            CurrentOrgId = CurrentUserObj.OrganizationId;
            userID = CurrentUserObj.Id;
            _logger = logger;
        }

        public IActionResult UserDashBoard()
        {
            FinalResultSetModel FinalResultset = new FinalResultSetModel();
            try
            {
                //Get Machine List 
                var AllstopageReason = new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList();

                var objMachinedata = new MachineService(_configuration).DataAccessLayer;
                var Machinelistdata = objMachinedata.GetMasterMachineList();

                var machines = Machinelistdata.Where(m => m.OrganizationId == CurrentOrgId).Select(m => new SelectListItem
                {
                    Text = m.MachineName,
                    Value = m.MachineID.ToString()
                }).ToList();

                FinalResultset.DatasetNames = machines;
                ViewBag.MachineList = JsonConvert.SerializeObject(Machinelistdata);
                ViewBag.BreakDownReasonList = AllstopageReason.Select(m => new SelectListItem
                {
                    Text = m.ReasonName.ToString(),
                    Value = m.ReasonId.ToString()
                }).ToList();

                LoadMqttConfigurationData();
                FinalResultset.ShiftList = UserUtility.AllShift;

                List<FinalARPRQROEEHome> lstperformance = new List<FinalARPRQROEEHome>();
                List<LossReportDashboardModel> lstLoss = new List<LossReportDashboardModel>();
                List<FinalWeekWiseOEE> lstWeek = new List<FinalWeekWiseOEE>();
                List<RejectionReasonReportModel> lstRejectionList = new List<RejectionReasonReportModel>();
                TypeofData typeData = new TypeofData();
                typeData = GetMomentRange();
                typeData.typeofdata = "DateWise";
                typeData.OrganizationID = CurrentOrgId.ToString();
                FinalResultset.MachineList = GetReportsConfig(typeData);
                lstperformance = GetPerformance(typeData);
                FinalResultset.PerformanceReportListHome = lstperformance;

                lstLoss = GetLossReport(typeData);
                FinalResultset.LossReportList = lstLoss;

                lstRejectionList = GetRejectionReport(typeData);
                FinalResultset.RejectionReportListHome = lstRejectionList;
                LoadMqttMapping();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{nameof(UserDashBoard)}.{nameof(UserDashBoard)} - Error occur..");
            }
            return View(FinalResultset);

        }
        private void LoadMqttConfigurationData()
        {
            ViewBag.PublishTopic = UtilityAll.PublishTopic;
            ViewBag.SubscribeTopic = UtilityAll.SubscribeTopic;
            ViewBag.MqttIpAddress = UtilityAll.MqttIpAddress;
            ViewBag.MqttPortNo = UtilityAll.MqttPortNo;
            ViewBag.ClientId = UtilityAll.ClientId;
        }
        private void LoadMqttMapping()
        {
            var objMachineService = new MachineProductService(_configuration).DataAccessLayer;
            var AllModule = objMachineService.GetUtilityModule(CurrentOrgId);

            if (AllModule != null)
            {
                ViewBag.IsMould = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Mould".ToLower()) == null ? false : true;
                ViewBag.IsDie = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Die".ToLower()) == null ? false : true;
                ViewBag.IsOperator = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Operator".ToLower()) == null ? false : true;
                ViewBag.IsOperation = AllModule.FirstOrDefault(i => i.ModuleName.ToLower() == "Operation".ToLower()) == null ? false : true;
            }


        }
        [HttpPost]
        public FinalResultSetModel FinalResult([FromBody] TypeofData typeData)
        {
            FinalResultSetModel FinalResultset = new FinalResultSetModel();
            List<FinalARPRQROEEHome> lstperformance = new List<FinalARPRQROEEHome>();
            List<LossReportDashboardModel> lstLoss = new List<LossReportDashboardModel>();
            List<FinalWeekWiseOEE> lstWeek = new List<FinalWeekWiseOEE>();
            List<RejectionReasonReportModel> lstRejectionList = new List<RejectionReasonReportModel>();
            var objMachinedata = new MachineService(_configuration).DataAccessLayer;
            var Machinelistdata = objMachinedata.GetMasterMachineList();
            var machines = Machinelistdata.Where(m => m.OrganizationId == CurrentOrgId).Select(m => new SelectListItem
            {
                Text = m.MachineName,
                Value = m.MachineID.ToString()
            }).ToList();

            FinalResultset.DatasetNames = machines;
            FinalResultset.MachineList = GetReportsConfig(typeData);
            typeData.OrganizationID = CurrentOrgId.ToString();
            lstperformance = GetPerformance(typeData);
            FinalResultset.PerformanceReportListHome = lstperformance;
            lstLoss = GetLossReport(typeData);
            FinalResultset.LossReportList = lstLoss;
            lstRejectionList = GetRejectionReport(typeData);
            FinalResultset.RejectionReportListHome = lstRejectionList;
            LoadMqttMapping();
            return FinalResultset;


        }

        //private FinalARPRQROEE CalculateARPRQR(List<ReportModel> CategoryList,string DataLevel)
        //  {
        //      double TotalWorkShiftTime = 0;
        //      double TotalShudownTime = 0;
        //      double TotalShiftBeakTime = 0;
        //      double TotalUtilisationLoss = 0;
        //      double TotalProductionTime = 0;
        //      double TotalQualtiyLoss = 0;

        //      double PlantAR = 0;
        //      double PlantPR = 0;
        //      double PlantQR = 0;
        //      double PlantOEE = 0;
        //      if (DataLevel == "Shift")
        //      {
        //          TotalWorkShiftTime = CategoryList.Where(s => s.shiftId == 1).Sum(a => a.WorkShiftTime).ToString().doubleTP();
        //          TotalShudownTime = CategoryList.Sum(a => a.ShutdownTime.doubleTP()).ToString().doubleTP();
        //          TotalShiftBeakTime = CategoryList.Sum(a => a.ShiftBreakTime.doubleTP()).ToString().doubleTP();
        //          TotalUtilisationLoss = CategoryList.Sum(a => a.Utliizationloss.doubleTP()).ToString().doubleTP();
        //          TotalProductionTime = CategoryList.Sum(a => a.TotalProduction.doubleTP()).ToString().doubleTP();
        //          TotalQualtiyLoss = CategoryList.Sum(a => a.Qualityloss.doubleTP()).ToString().doubleTP();

        //          PlantAR = Math.Round((TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime - TotalUtilisationLoss) /
        //                               (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
        //          PlantPR = Math.Round(TotalProductionTime / (TotalWorkShiftTime - TotalShudownTime - TotalShiftBeakTime), 2);
        //          PlantQR = Math.Round((TotalProductionTime - TotalQualtiyLoss) / (TotalProductionTime == 0 ? 1 : TotalProductionTime), 2);
        //          PlantOEE = Math.Round(PlantAR * PlantQR * PlantPR, 2);
        //      }

        //      return new FinalARPRQROEE { AR = PlantAR, PR = PlantPR, QR = PlantQR, OEE = PlantOEE, DataLevel = DataLevel };
        //  }
        [HttpPost]
        public bool SaveDashboardView([FromBody] UserDashboard userDashboard)
        {
            bool result = false;
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;
            if (userID > 0)
            {
                if (userDashboard != null)
                {
                    result = objCategoryDAL.SaveUserDashboardSearch(userDashboard.machineId, userID, userDashboard.dateRange);
                }
            }
            return result;
        }

        public List<FinalARPRQROEEHome> GetPerformance(TypeofData typeData)
        {

            var dates = GetStartEndDates(typeData);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            string strReturn = staretdate.ToString("yyyy-MM-dd") + " to " + endate.ToString("yyyy-MM-dd");
            int orgid = typeData.OrganizationID.intTP();
            int MachineID = typeData.MachineID.intTP();
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetReportHomeList(staretdate, endate, orgid, MachineID);

            return CategoryList;
        }

        public List<LossReportDashboardModel> GetLossReport(TypeofData typeData)
        {
            if (CurrentUserObj.HasBreakdown)
            {
                var dates = GetStartEndDates(typeData);
                DateTime staretdate = dates.Item1;
                DateTime endate = dates.Item2;
                int MachineID = typeData.MachineID.intTP();

                // ViewBag.rptDate = strReturn;
                var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                var CategoryList = objCategoryDAL.GetLossReport(staretdate, endate, CurrentOrgId, MachineID);
                var res = from element in CategoryList
                          group element by element.ReasonId
                      into groups
                          select groups.First();
                List<LossReportDashboardModel> lstParent = new List<LossReportDashboardModel>();

                foreach (var el in res)
                {
                    LossReportDashboardModel m = new LossReportDashboardModel();
                    //List<LossReportDashboardChildModel> lstchild = new List<LossReportDashboardChildModel>();
                    m.LossDetails = new List<LossReportDashboardChildModel>();
                    m.Reason = el.Reason;
                    m.Duration = el.Duration;
                    m.Freequency = el.Freequency;
                    // m.rptDate = strReturn;
                    foreach (var items in CategoryList)
                    {
                        if (el.ReasonId == items.ReasonId)
                        {
                            LossReportDashboardChildModel c = new LossReportDashboardChildModel();
                            c.MachineName = items.MachineName;
                            c.ProductionDate = items.ProductionDate;
                            c.SubReason = items.SubReason;
                            c.Remarks = items.Remarks;
                            c.TotalStoppagetime = items.TotalStoppagetime;
                            c.MAchineID = items.MAchineID;
                            c.SubReasonId = items.SubReasonId;
                            c.Reason = items.Reason;
                            // lstchild.Add(c);
                            m.LossDetails.Add(c);
                        }
                    }
                    lstParent.Add(m);
                }
                lstParent = lstParent.OrderByDescending(x => x.Duration).ToList();
                return lstParent;
                //string result = JsonConvert.SerializeObject(lstParent);
                //return result;
            }
            else
            {
                return new List<LossReportDashboardModel>();
            }
        }

        [HttpPost]
        public List<LossReportModel> GetLossDetails([FromBody] TypeofDataLoss typeData)
        {
            TypeofData typdata = GetTypeOfDataFromTypeOfLoss(typeData);
            var dates = GetStartEndDates(typdata);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            int orgid = CurrentOrgId;
            int MachineID = typeData.MachineID.intTP();

            // ViewBag.rptDate = strReturn;
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetLossReportDetails(staretdate, endate, orgid, MachineID, typeData.Reason);

            return CategoryList;

        }
        public List<RejectionReasonReportModel> GetRejectionReport(TypeofData typeData)
        {
            if (CurrentUserObj.HasRejection)
            {
                var dates = GetStartEndDates(typeData);
                DateTime staretdate = dates.Item1;
                DateTime endate = dates.Item2;
                string strReturn = staretdate.ToString("yyyy-MM-dd") + " to " + endate.ToString("yyyy-MM-dd");
                int orgid = typeData.OrganizationID.intTP();
                int MachineID = typeData.MachineID.intTP();
                var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
                var CategoryList = objCategoryDAL.GetRejectionReport(staretdate, endate, orgid, MachineID);

                return CategoryList;
            }
            else
            {
                return new List<RejectionReasonReportModel>();
            }
        }

        [HttpPost]
        public List<RejectionReasonDetailsModel> GetRejectionDetails([FromBody] TypeofDataLoss typeData)
        {
            TypeofData typdata = GetTypeOfDataFromTypeOfLoss(typeData);
            var dates = GetStartEndDates(typdata);
            DateTime staretdate = dates.Item1;
            DateTime endate = dates.Item2;
            int orgid = CurrentOrgId;
            int MachineID = typeData.MachineID.intTP();

            // ViewBag.rptDate = strReturn;
            var objCategoryDAL = new WeekMonthReportService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetRejectionReportDetails(staretdate, endate, orgid, MachineID, typeData.Reason);

            return CategoryList;

        }

        private List<SelectListItem> GetReportsConfig(TypeofData typeofData)
        {
            var objreportDAL = new UserMachineReportService(_configuration).DataAccessLayer;
            int userID = UserUtility.CurrentUser.Id;

            var Reportlistdata = objreportDAL.GetReportConfigList(userID);
            var reports = Reportlistdata.Select(m => new SelectListItem
            {
                Text = m.ReportName,
                Value = m.UserMachineReportID.ToString(),
                Selected = m.IsDefault
                //Selected = Reportlistdata.Exists(z => z.UserMachineReportID == Convert.ToInt32(typeofData.MachineID))
            }).ToList();

            reports.Add(new SelectListItem { Text = "+ Add New", Value = "Add New" });
            reports.Insert(0, new SelectListItem { Text = "All", Value = "0" });
            return reports;
        }
        private TypeofData GetMomentRange()
        {
            UserReportConfig userDashboard = new UserReportConfig();
            string preDefinedLabel = "";
            var objCategoryDAL = new UserMachineReportService(_configuration).DataAccessLayer;

            string strReturn = "";
            DateTime now = DateTime.Now;
            DateTime startDate;
            DateTime endDate;
            //userDashboard = objCategoryDAL.GetReportConfigList(userID).Where(m=> m.IsDefault==true).FirstOrDefault();
            //if (userDashboard != null)
            //{
            preDefinedLabel = userDashboard.DateRange;


            if (preDefinedLabel == "Today")
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            else if (preDefinedLabel == "Yesterday")
            {
                startDate = DateTime.Now.AddDays(-1);
                endDate = DateTime.Now.AddDays(-1);
            }
            else if (preDefinedLabel == "This Week")
            {
                DayOfWeek currentDay = now.DayOfWeek;
                int daysTillCurrentDay = currentDay - DayOfWeek.Monday;
                startDate = now.AddDays(-daysTillCurrentDay);
                endDate = now;
            }
            else if (preDefinedLabel == "Last 7 Days")
            {
                startDate = now.AddDays(-6);
                endDate = now;
            }
            else if (preDefinedLabel == "Last Week")
            {
                DateTime date = DateTime.Now.AddDays(-7);
                while (date.DayOfWeek != DayOfWeek.Monday)
                {
                    date = date.AddDays(-1);
                }

                startDate = date;
                endDate = date.AddDays(7);

            }
            else if (preDefinedLabel == "This Month")
            {
                startDate = new DateTime(now.Year, now.Month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }
            else if (preDefinedLabel == "Last Month")
            {
                var month = new DateTime(now.Year, now.Month, 1);
                startDate = month.AddMonths(-1);
                endDate = month.AddDays(-1);
            }
            else if (preDefinedLabel == "This Year")
            {
                int year = now.Year;
                startDate = new DateTime(year, 1, 1);
                endDate = now;
            }
            else
            {
                //startDate = new DateTime(now.Year, now.Month, 1);
                //endDate = startDate.AddMonths(1).AddDays(-1);
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            //}
            //else
            //{
            //    startDate = new DateTime(now.Year, now.Month, 1);
            //    endDate = startDate.AddMonths(1).AddDays(-1);
            //}

            strReturn = startDate.ToString("yyyy-MM-dd") + " to " + endDate.ToString("yyyy-MM-dd");

            // string strDateRange = GetMomentRange();
            TypeofData typeData = new TypeofData();
            if (!string.IsNullOrEmpty(strReturn))
            {

                var datearray = strReturn.Split("to");
                typeData.StartDate = datearray[0].Trim();
                typeData.EndDate = datearray[1].Trim();
                ViewBag.rptMonth = startDate.Month;
                ViewBag.rptDate = strReturn;
                if (userDashboard != null)
                {
                    ViewBag.rptMachine = userDashboard.UserMachineReportID;
                }
                else { ViewBag.rptMachine = 0; }
            }
            ViewBag.HasBreakdown = CurrentUserObj.HasBreakdown;
            ViewBag.HasRejection = CurrentUserObj.HasRejection;
            typeData.MachineID = ViewBag.rptMachine.ToString();
            return typeData;
        }
        (DateTime, DateTime) GetStartEndDates(TypeofData typeData)
        {
            DateTime staretdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            DateTime endate = DateTime.Now;
            if (typeData.typeofdata.ToLower() == "monthwise")
            {
                //Then Make Final Start and end Date
                int monnumber = typeData.monthnumber + 1;

                //Get last day of that month
                int curyear = DateTime.Now.Year;
                int lastday = DateTime.DaysInMonth(curyear, monnumber);
                staretdate = new DateTime(curyear, monnumber, 1);
                endate = new DateTime(curyear, monnumber, lastday);

            }
            else if (typeData.typeofdata.ToLower() == "datewise")
            {
                if (typeData.StartDate.dtTP().Year > 2001)
                {
                    staretdate = typeData.StartDate.dtTP();
                }
                if (typeData.EndDate.dtTP().Year > 2001)
                {
                    endate = typeData.EndDate.dtTP();
                }

            }
            return (staretdate, endate); // tuple literal
        }
        private TypeofData GetTypeOfDataFromTypeOfLoss(TypeofDataLoss typeData)
        {
            TypeofData typdata = new TypeofData();
            typdata.typeofdata = typeData.typeofdata;
            typdata.monthnumber = typeData.monthnumber;
            typdata.StartDate = typeData.StartDate;
            typdata.EndDate = typeData.EndDate;
            typdata.MachineID = typeData.MachineID;
            typdata.OrganizationID = typeData.OrganizationID;
            return typdata;
        }
    }
}