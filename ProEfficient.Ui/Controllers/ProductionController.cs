﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ProductionController : Controller
    {
        private readonly IConfiguration _configuration;

        public ProductionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Production
        public static int Accordionindex = 0;
        public static VwProductionBreakDownEntry VwProductionBreakDownModel;
        public static DateTime? Current_ProductionDate { get; set; }
        public static int CurrentMachineId { get; set; }
        public static int CurrentShiftId { get; set; }
        public ActionResult Index(String appId = "")
        {

            if (VwProductionBreakDownModel == null)
            {
                VwProductionBreakDownModel = new VwProductionBreakDownEntry();
                CurrentMachineId = 0;//ProEfficient.Utility.UserUtility.AllOrgMachine.FirstOrDefault().MachineID;
                CurrentShiftId = new ShiftService(_configuration).DataAccessLayer.GetShiftList().FirstOrDefault().ShiftId;

                VwProductionBreakDownModel.CurrentMachineId = CurrentMachineId;
                VwProductionBreakDownModel.CurrentShiftId = CurrentShiftId;
            }


            if (!string.IsNullOrEmpty(appId))
            {
                if ((appId.Split('=')[0]).ToLower() == "proddate")
                {
                    #region Production Date Selected
                    appId = (appId.Split('=')[1]);
                    var objProduction = new ProductionService(_configuration).DataAccessLayer;
                    ProductionEntry productionEntry = new ProductionEntry();
                    VwProductionBreakDownModel.AddedBreakDown = new BreakDownEntry();
                    VwProductionBreakDownModel.AddedProductionEntry = new ProductionEntry();

                    VwProductionBreakDownModel.AddedProductionEntry.ShiftId = CurrentShiftId;
                    VwProductionBreakDownModel.AddedProductionEntry.MachineID = CurrentMachineId;
                    VwProductionBreakDownModel.AddedBreakDown.ShiftId = CurrentShiftId;
                    VwProductionBreakDownModel.AddedBreakDown.MachineID = CurrentMachineId;
                    VwProductionBreakDownModel.CurrentShiftId = CurrentShiftId;
                    VwProductionBreakDownModel.CurrentMachineId = CurrentMachineId;
                    productionEntry.ProductionDate = DateTime.ParseExact(appId, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (productionEntry.ProductionDate == null || productionEntry.ProductionDate == DateTime.MinValue)
                    {
                        productionEntry.ProductionDate = DateTime.Now;
                        CurrentProductionDate = DateTime.Now;
                    }

                    VwProductionBreakDownModel.ListOfBreakDown = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryListDateTime(productionEntry.ProductionDate, UserUtility.CurrentUser.OrganizationId.ToInteger0());
                    VwProductionBreakDownModel.ListOfProduction = objProduction.GetProductionEntry(productionEntry, UserUtility.CurrentUser.OrganizationId.ToInteger0());
                    VwProductionBreakDownModel.ProductionDate = productionEntry.ProductionDate;



                    VwProductionBreakDownModel.OperatorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.SupervisorID = UserUtility.CurrentUser.UserId;

                    VwProductionBreakDownModel.AddedBreakDown.UserID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedBreakDown.OperatorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedBreakDown.SupervisorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedProductionEntry.OperatorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedProductionEntry.SupervisorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedProductionEntry.UserID = UserUtility.CurrentUser.UserId;

                    VwProductionBreakDownModel.AddedProductionEntry.StartTime = CurrentProductionDate;
                    VwProductionBreakDownModel.AddedProductionEntry.EndTime = CurrentProductionDate;

                    VwProductionBreakDownModel.AddedBreakDown.StartTime = CurrentProductionDate;
                    VwProductionBreakDownModel.AddedBreakDown.EndTime = CurrentProductionDate;

                    FillShift();
                    #endregion
                }
                else if ((appId.Split('=')[0]).ToLower() == "shift")
                {
                    #region Production Shift Selected
                    appId = (appId.Split('=')[1]);
                    CurrentShiftId = appId.ToInteger0();
                    if (VwProductionBreakDownModel.ProductionDate.ToDateTime() == DateTime.MinValue)
                    {
                        if (CurrentProductionDate == null)
                        {
                            VwProductionBreakDownModel.ProductionDate = DateTime.Now;
                        }
                        else
                        {
                            VwProductionBreakDownModel.ProductionDate = CurrentProductionDate;
                        }

                    }

                    appId = VwProductionBreakDownModel.ProductionDate.ToDateTime().ToString();
                    var objProduction = new ProductionService(_configuration).DataAccessLayer;
                    ProductionEntry productionEntry = new ProductionEntry();
                    VwProductionBreakDownModel.AddedBreakDown = new BreakDownEntry();
                    VwProductionBreakDownModel.AddedProductionEntry = new ProductionEntry();
                    VwProductionBreakDownModel.CurrentShiftId = CurrentShiftId;

                    VwProductionBreakDownModel.AddedProductionEntry.ShiftId = CurrentShiftId;
                    VwProductionBreakDownModel.AddedProductionEntry.MachineID = CurrentMachineId;
                    VwProductionBreakDownModel.AddedBreakDown.ShiftId = CurrentShiftId;
                    VwProductionBreakDownModel.AddedBreakDown.MachineID = CurrentMachineId;


                    productionEntry.ProductionDate = Convert.ToDateTime(appId);

                    VwProductionBreakDownModel.ListOfBreakDown = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryListDateTime(productionEntry.ProductionDate, UserUtility.CurrentUser.OrganizationId.ToInteger0());
                    VwProductionBreakDownModel.ListOfProduction = objProduction.GetProductionEntry(productionEntry, UserUtility.CurrentUser.OrganizationId.ToInteger0());
                    VwProductionBreakDownModel.ProductionDate = productionEntry.ProductionDate;

                    VwProductionBreakDownModel.AddedBreakDown.UserID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedBreakDown.OperatorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedBreakDown.SupervisorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.OperatorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.SupervisorID = UserUtility.CurrentUser.UserId;

                    VwProductionBreakDownModel.AddedProductionEntry.OperatorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedProductionEntry.SupervisorID = UserUtility.CurrentUser.UserId;
                    VwProductionBreakDownModel.AddedProductionEntry.UserID = UserUtility.CurrentUser.UserId;

                    VwProductionBreakDownModel.AddedProductionEntry.StartTime = CurrentProductionDate;
                    VwProductionBreakDownModel.AddedProductionEntry.EndTime = CurrentProductionDate;

                    VwProductionBreakDownModel.AddedBreakDown.StartTime = CurrentProductionDate;
                    VwProductionBreakDownModel.AddedBreakDown.EndTime = CurrentProductionDate;
                    FillShift();
                    #endregion
                }

            }
            else
            {
                var objProduction = new ProductionService(_configuration).DataAccessLayer;
                CurrentShiftId = 0;
                VwProductionBreakDownModel.AddedBreakDown = new BreakDownEntry();
                VwProductionBreakDownModel.AddedProductionEntry = new ProductionEntry();

                ProductionEntry productionEntry = new ProductionEntry();
                productionEntry.ProductionDate = DateTime.Now;
                VwProductionBreakDownModel.CurrentShiftId = CurrentShiftId;
                VwProductionBreakDownModel.ListOfProduction = objProduction.GetProductionEntry(productionEntry, UserUtility.CurrentUser.OrganizationId.ToInteger0());
                VwProductionBreakDownModel.ListOfBreakDown = new BreakDownService(_configuration).DataAccessLayer.GetBreakDownEntryListDateTime(productionEntry.ProductionDate, UserUtility.CurrentUser.OrganizationId.ToInteger0());


                VwProductionBreakDownModel.AddedProductionEntry.ShiftId = CurrentShiftId;
                VwProductionBreakDownModel.AddedProductionEntry.MachineID = CurrentMachineId;
                VwProductionBreakDownModel.AddedBreakDown.ShiftId = CurrentShiftId;
                VwProductionBreakDownModel.AddedBreakDown.MachineID = CurrentMachineId;


                VwProductionBreakDownModel.AddedBreakDown.UserID = UserUtility.CurrentUser.UserId;
                VwProductionBreakDownModel.AddedBreakDown.OperatorID = UserUtility.CurrentUser.UserId;
                VwProductionBreakDownModel.AddedBreakDown.SupervisorID = UserUtility.CurrentUser.UserId;
                VwProductionBreakDownModel.AddedProductionEntry.OperatorID = UserUtility.CurrentUser.UserId;
                VwProductionBreakDownModel.AddedProductionEntry.SupervisorID = UserUtility.CurrentUser.UserId;
                VwProductionBreakDownModel.AddedProductionEntry.UserID = UserUtility.CurrentUser.UserId;

                VwProductionBreakDownModel.OperatorID = UserUtility.CurrentUser.UserId;
                VwProductionBreakDownModel.SupervisorID = UserUtility.CurrentUser.UserId;


                VwProductionBreakDownModel.ProductionDate = DateTime.Now;

                VwProductionBreakDownModel.AddedProductionEntry.StartTime = CurrentProductionDate;
                VwProductionBreakDownModel.AddedProductionEntry.EndTime = CurrentProductionDate;

                VwProductionBreakDownModel.AddedBreakDown.StartTime = CurrentProductionDate;
                VwProductionBreakDownModel.AddedBreakDown.EndTime = CurrentProductionDate;

            }
            CurrentProductionDate = VwProductionBreakDownModel.ProductionDate;
            VwProductionBreakDownModel.ActiveIndex = Accordionindex;


            if (CurrentShiftId > 0)
            {
                if (VwProductionBreakDownModel.ListOfBreakDown == null)
                {
                    VwProductionBreakDownModel.ListOfBreakDown = new List<BreakDownEntry>();
                }
                if (VwProductionBreakDownModel.ListOfProduction == null)
                {
                    VwProductionBreakDownModel.ListOfProduction = new List<ProductionEntry>();
                }
                VwProductionBreakDownModel.ListOfBreakDown = VwProductionBreakDownModel.ListOfBreakDown.FindAll(i => i.ShiftId == CurrentShiftId) == null ? new List<BreakDownEntry>() : VwProductionBreakDownModel.ListOfBreakDown.FindAll(i => i.ShiftId == CurrentShiftId);
                VwProductionBreakDownModel.ListOfProduction = VwProductionBreakDownModel.ListOfProduction.FindAll(i => i.ShiftId == CurrentShiftId) == null ? new List<ProductionEntry>() : VwProductionBreakDownModel.ListOfProduction.FindAll(i => i.ShiftId == CurrentShiftId);
            }

            if (new ProductionService(_configuration).DataAccessLayer.IsProductionSubmitted(VwProductionBreakDownModel.ProductionDate.ToDateTime(), UserUtility.CurrentUser.OrganizationId.ToInteger0()))
            {
                VwProductionBreakDownModel.IsSubmitted = true;
                VwProductionBreakDownModel.AddedBreakDown.IsSaved = true;
                VwProductionBreakDownModel.AddedProductionEntry.IsSaved = true;
            }
            else
            {
                VwProductionBreakDownModel.IsSubmitted = false;
                VwProductionBreakDownModel.AddedBreakDown.IsSaved = false;
                VwProductionBreakDownModel.AddedProductionEntry.IsSaved = false;
            }
            LoadViewBags();
            return View("~/Views/ProductionPage/Index.cshtml", VwProductionBreakDownModel);
        }
        public ActionResult Submit(VwProductionBreakDownEntry modProduction)
        {
            if (new ProductionService(_configuration).DataAccessLayer.SubmitProduction(VwProductionBreakDownModel.ProductionDate.ToDateTime(), UserUtility.CurrentUser.OrganizationId.ToInteger0()))
            {
                VwProductionBreakDownModel.IsSubmitted = true;

                this.CreateObjectAlert("Production submitted");
            }
            else
            {
                this.ErrorMessage("Production submission failed");
            }

            return View("Index", VwProductionBreakDownModel);
        }
        //  public ActionResult Index()
        public void FillShift()
        {
            if (CurrentShiftId > 0)
            {
                var Shift = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = CurrentShiftId });
                VwProductionBreakDownModel.AddedProductionEntry.ProductionStartTime = Shift.StartTime.ToDecimal();
                VwProductionBreakDownModel.AddedProductionEntry.ProductionEndTime = Shift.EndTime.ToDecimal();

                VwProductionBreakDownModel.AddedBreakDown.BreakDownStartTime = Shift.StartTime.ToDecimal();
                VwProductionBreakDownModel.AddedBreakDown.BreakDownEndTime = Shift.EndTime.ToDecimal();
            }


        }
        public ActionResult EditProduction(int id)
        {
            VwProductionBreakDownModel.AddedProductionEntry = VwProductionBreakDownModel.ListOfProduction.FirstOrDefault(i => i.ProductionId == id);
            VwProductionBreakDownModel.AddedProductionEntry.ProductionStartTime = (VwProductionBreakDownModel.AddedProductionEntry.StartTime.ToDateTime().Hour.ToInteger0().ToStringWithNullEmpty() + "."
                + VwProductionBreakDownModel.AddedProductionEntry.StartTime.ToDateTime().Minute.ToInteger0().ToStringWithNullEmpty()).ToDecimal0();

            VwProductionBreakDownModel.AddedProductionEntry.ProductionEndTime = (VwProductionBreakDownModel.AddedProductionEntry.EndTime.ToDateTime().Hour.ToInteger0().ToStringWithNullEmpty() + "."
               + VwProductionBreakDownModel.AddedProductionEntry.EndTime.ToDateTime().Minute.ToInteger0().ToStringWithNullEmpty()).ToDecimal0();
            VwProductionBreakDownModel.ActiveIndex = 0;
            CurrentMachineId = VwProductionBreakDownModel.AddedProductionEntry.MachineID.ToInteger0();
            CurrentShiftId = VwProductionBreakDownModel.AddedProductionEntry.ShiftId.ToInteger0();
            VwProductionBreakDownModel.CurrentMachineId = CurrentMachineId;
            VwProductionBreakDownModel.CurrentShiftId = CurrentShiftId;
            LoadViewBags();
            return View("Index", VwProductionBreakDownModel);
        }
        public ActionResult EditBreakDown(int id)
        {
            VwProductionBreakDownModel.AddedBreakDown = VwProductionBreakDownModel.ListOfBreakDown.FirstOrDefault(i => i.BreakDownID == id);
            VwProductionBreakDownModel.ActiveIndex = 1;
            VwProductionBreakDownModel.CurrentShiftId = VwProductionBreakDownModel.AddedBreakDown.ShiftId.ToInteger0();
            VwProductionBreakDownModel.CurrentMachineId = VwProductionBreakDownModel.AddedBreakDown.MachineID.ToInteger0();

            CurrentMachineId = VwProductionBreakDownModel.CurrentMachineId;
            CurrentShiftId = VwProductionBreakDownModel.CurrentShiftId;

            LoadViewBags();
            return View("Index", VwProductionBreakDownModel);
        }

        public ActionResult DeleteProduction(int id)
        {
            new ProductionService(_configuration).DataAccessLayer.DeleteProductionEntry(id);
            VwProductionBreakDownModel.ActiveIndex = 0;
            return RedirectToAction("Index");
        }
        public ActionResult DeleteBreakDown(int id)
        {
            new BreakDownService(_configuration).DataAccessLayer.DeleteBreakDownEntry(id);
            VwProductionBreakDownModel.ActiveIndex = 1;
            return RedirectToAction("Index");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pProduction"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddProduction(VwProductionBreakDownEntry pProduction)
        {


            if (ModelState.IsValid)
            {
                //pProduction.AddedProductionEntry. = UserUtility.CurrentUser.UserId;
                pProduction.AddedProductionEntry.TimeOfEntry = DateTime.UtcNow;
                pProduction.AddedProductionEntry.ProductionDate = CurrentProductionDate.ToDateTime();
                pProduction.AddedProductionEntry.IsSaved = false;
                int AddDay = 0;
                if (pProduction.AddedProductionEntry.ProductionStartTime > pProduction.AddedProductionEntry.ProductionEndTime)
                {
                    AddDay = 1;
                }
                pProduction.AddedProductionEntry.StartTime = new DateTime(VwProductionBreakDownModel.ProductionDate.ToDateTime().Year,
                VwProductionBreakDownModel.ProductionDate.ToDateTime().Month, VwProductionBreakDownModel.ProductionDate.ToDateTime().Day,
                pProduction.AddedProductionEntry.ProductionStartTime.GetIntPart(), pProduction.AddedProductionEntry.ProductionStartTime.GetFractionPart(), 0);

                pProduction.AddedProductionEntry.EndTime = new DateTime(VwProductionBreakDownModel.ProductionDate.ToDateTime().Year,
                 VwProductionBreakDownModel.ProductionDate.ToDateTime().Month, VwProductionBreakDownModel.ProductionDate.ToDateTime().Day + AddDay,
                 pProduction.AddedProductionEntry.ProductionEndTime.GetIntPart(), pProduction.AddedProductionEntry.ProductionEndTime.GetFractionPart(), 0);


                CurrentShiftId = pProduction.AddedProductionEntry.ShiftId.ToInteger0();
                var _ShiftDataDetails = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = CurrentShiftId });
                var TimeOfShift = UtilityAll.getTimeDifference(_ShiftDataDetails.EndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), _ShiftDataDetails.StartTime.ToDecimal());
                var TimeOfProductionEntry = UtilityAll.getTimeDifference(pProduction.AddedProductionEntry.ProductionEndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), pProduction.AddedProductionEntry.ProductionStartTime.ToDecimal());


                CurrentMachineId = pProduction.AddedProductionEntry.MachineID.ToInteger0();
                //if (TimeOfShift != TimeOfProductionEntry)
                //{
                //    if (TimeOfShift < TimeOfProductionEntry)
                //    {
                //        this.ErrorMessage("Enter time is grater than the shift time.(" + (TimeOfProductionEntry - TimeOfShift).TotalMinutes.ToStringWithNullEmpty() + " Minutes difference)");
                //    }
                //    else
                //    {
                //        this.ErrorMessage("Enter time is lesser than the shift time.(" + (TimeOfShift - TimeOfProductionEntry).TotalMinutes.ToStringWithNullEmpty() + " Minutes difference)");
                //    }
                //    return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });
                //}
                if (CurrentMachineId <= 0)
                {
                    this.ErrorMessage("Please select a Machine");
                    return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });
                }
                //if (!IsValidToAddProductionDetails(pProduction))
                //{
                //    this.ErrorMessage("There is not enough shift Time exists");
                //    return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });
                //}
                new ProductionService(_configuration).DataAccessLayer.UpdateProductionEntry(pProduction.AddedProductionEntry);
                VwProductionBreakDownModel.AddedProductionEntry = pProduction.AddedProductionEntry;
                this.ShowSuccessMessage("Production details has been saved successfully.");
                Accordionindex = 0;
                return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });
            }
            else
            {
                String _ErrorMessage = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                this.ErrorMessage(_ErrorMessage);
            }
            return RedirectToAction("Index");
        }

        public Boolean IsValidToAddProductionDetails(VwProductionBreakDownEntry pProduction)
        {
            ProductionEntry productionEntry = new ProductionEntry();
            productionEntry.ProductionDate = pProduction.AddedProductionEntry.ProductionDate.ToDateTime();
            var LstAllProduction = new ProductionService(_configuration).DataAccessLayer.GetProductionEntry(productionEntry, UserUtility.CurrentUser.OrganizationId.ToInteger0());

            var ShiftDetails = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = CurrentShiftId });

            var TimeDifference = (ShiftDetails.EndTime.ToDecimal() - ShiftDetails.StartTime.ToDecimal());
            var TotalTimeInMinutes = TimeDifference.GetFractionPart() + (TimeDifference.GetIntPart() * 60);

            var AddedTime = (pProduction.AddedProductionEntry.EndTime.ToDateTime() - pProduction.AddedProductionEntry.EndTime.ToDateTime()).Minutes;
            if (LstAllProduction == null ||
                LstAllProduction.Count() <= 0 ||
                (LstAllProduction.FindAll(i => i.ShiftId == CurrentShiftId) == null) ||
                ((LstAllProduction.FindAll(i => i.ShiftId == CurrentShiftId).Count() == 0)))
            {
                return true;
            }
            else
            {
                var TimeinMinute = 0;
                foreach (var production in LstAllProduction)
                {
                    TimeinMinute += (production.EndTime.ToDateTime() - production.StartTime.ToDateTime()).Minutes;
                }

                if (TimeinMinute > TotalTimeInMinutes)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBreakdown"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddBreakdown(VwProductionBreakDownEntry pBreakdown)
        {

            if (ModelState.ContainsKey("AddedBreakDown.MachineName"))
                ModelState["AddedBreakDown.MachineName"].Errors.Clear();

            if (ModelState.IsValid)
            {
                pBreakdown.AddedBreakDown.AddBy = UserUtility.CurrentUser.UserId;
                pBreakdown.AddedBreakDown.UserID = UserUtility.CurrentUser.UserId;
                pBreakdown.AddedBreakDown.TimeOfEntry = DateTime.Now;
                pBreakdown.AddedBreakDown.ProductionDate = CurrentProductionDate.ToDateTime();
                pBreakdown.AddedBreakDown.IsActive = true;
                pBreakdown.AddedBreakDown.IsSaved = false;
                CurrentShiftId = pBreakdown.AddedBreakDown.ShiftId.ToInteger0();
                CurrentMachineId = pBreakdown.AddedBreakDown.MachineID.ToInteger0();


                //
                int AddDay = 0;
                if (pBreakdown.AddedBreakDown.BreakDownStartTime > pBreakdown.AddedBreakDown.BreakDownEndTime)
                {
                    AddDay = 1;
                }
                pBreakdown.AddedBreakDown.StartTime = new DateTime(VwProductionBreakDownModel.ProductionDate.ToDateTime().Year,
                VwProductionBreakDownModel.ProductionDate.ToDateTime().Month, VwProductionBreakDownModel.ProductionDate.ToDateTime().Day,
                pBreakdown.AddedBreakDown.BreakDownStartTime.GetIntPart(), pBreakdown.AddedBreakDown.BreakDownEndTime.GetFractionPart(), 0);

                pBreakdown.AddedBreakDown.EndTime = new DateTime(VwProductionBreakDownModel.ProductionDate.ToDateTime().Year,
                 VwProductionBreakDownModel.ProductionDate.ToDateTime().Month, VwProductionBreakDownModel.ProductionDate.ToDateTime().Day + AddDay,
                 pBreakdown.AddedBreakDown.BreakDownEndTime.GetIntPart(), pBreakdown.AddedBreakDown.BreakDownEndTime.GetFractionPart(), 0);

                //


                var _ShiftDataDetails = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = CurrentShiftId });
                var TimeOfShift = UtilityAll.getTimeDifference(_ShiftDataDetails.EndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), _ShiftDataDetails.StartTime.ToDecimal());
                var TimeOfProductionEntry = UtilityAll.getTimeDifference(pBreakdown.AddedBreakDown.BreakDownEndTime.ToDecimal() + (_ShiftDataDetails.isOverNight ? 24 : 0), pBreakdown.AddedBreakDown.BreakDownStartTime.ToDecimal());



                //if (TimeOfShift != TimeOfProductionEntry)
                //{
                //    if (TimeOfShift < TimeOfProductionEntry)
                //    {
                //        this.ErrorMessage("Enter time is grater than the shift time.(" + (TimeOfProductionEntry - TimeOfShift).TotalMinutes.ToStringWithNullEmpty() + " Minutes grater)");

                //        return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });
                //    }


                //}
                if (CurrentMachineId <= 0)
                {
                    this.ErrorMessage("Please select a Machine");
                    return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });
                }
                if (new BreakDownService(_configuration).DataAccessLayer.UpdateBreakDownEntry(pBreakdown.AddedBreakDown))
                {
                    this.ShowSuccessMessage("Breakdown time has been saved successfully.");

                }
                else
                {
                    this.ErrorMessage("Breakdown Information can not be saved");
                }
                Accordionindex = 1;
                return RedirectToAction("Index", new { appId = "proddate=" + CurrentProductionDate.ToDateTime().ToString("dd/MM/yyyy") });

            }
            else
            {
                String _ErrorMessage = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                this.ErrorMessage(_ErrorMessage);
            }
            Accordionindex = 1;
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetProductionBreakdownData(string pProductionDate)
        {
            try
            {
                var objProduction = new ProductionService(_configuration).DataAccessLayer;
                ProductionEntry productionEntry = new ProductionEntry();
                var VwProductionBreakDownModel = new VwProductionBreakDownEntry();


                //return View(objProduct.GetMasterProductList());



                return View(VwProductionBreakDownModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime? CurrentProductionDate
        {
            get
            {

                var CurrentProductionDate = HttpContext.Session.GetString("CurrentProductionDate");
                if (CurrentProductionDate == null)
                {
                    return null;
                }
                else
                {

                    try
                    {
                        return Convert.ToDateTime(CurrentProductionDate);
                    }
                    catch (Exception)
                    {

                        return null;
                    }
                }
            }
            set => HttpContext.Session.SetString("CurrentProductionDate", value.ToString());
        }

        [HttpGet]
        public string GetShiftTime(string ID)
        {

            Shift shft = (new ShiftService(_configuration).DataAccessLayer.GetShiftList().FirstOrDefault(x => x.ShiftId == int.Parse(ID)));
            string st = "00" + shft.StartTime.ToString();
            st = st.Substring(st.Length - 5);
            string et = "00" + shft.EndTime.ToString();
            et = et.Substring(et.Length - 5);
            decimal? stTime = shft.StartTime;
            if (stTime >= 17)
            {
                return DateTime.Now.ToString("yyyy/MM/dd" + " " + st) + "~" + DateTime.Now.AddDays(1).ToString("yyyy/MM/dd" + " " + et);
            }
            else
                return DateTime.Now.ToString("yyyy/MM/dd" + " " + st) + "~" + DateTime.Now.ToString("yyyy/MM/dd" + " " + et);
        }

        private void LoadViewBags()
        {
            ViewBag.CurrentShift = CurrentShiftId <= 0 ? null : new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = CurrentShiftId });

        }
    }
}