﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.Models.ViewModel;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Linq;
using System.Net;

namespace ProEfficient.Ui.Controllers
{
    public class ForgotPasswordController : Controller
    {
        private readonly IConfiguration _configuration;

        public ForgotPasswordController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: ForgotPassword

        public ActionResult Index()
        {
            var VwModel = new VwChangePassword();
            VwModel.EmailAddress = "";
            VwModel.IsCodeSend = false;
            HttpContext.Session.SetString("ChangeCode", null);
            HttpContext.Session.SetString("IsCodeSend", VwModel.IsCodeSend.ToString());

            return View(VwModel);
        }

        [HttpPost]
        public ActionResult Index(VwChangePassword changepasswordmodel)
        {
            var VwModel = new VwChangePassword();
            var isCodeSend = HttpContext.Session.GetString("IsCodeSend").ToBoolean();
            if (isCodeSend)
            {
                if (new UserManagementService(_configuration).DataAccessLayer.GetUserList().FindAll(i => i.IsActive.ToBoolean()).FirstOrDefault(k => k.EmailId == changepasswordmodel.EmailAddress) == null)
                {

                    VwModel.EmailAddress = "";
                    VwModel.IsCodeSend = false;
                    HttpContext.Session.SetString("IsCodeSend", VwModel.IsCodeSend.ToString());
                    HttpContext.Session.SetString("ChangeCode", null);
                    this.ErrorMessage("User Email is not exists");
                    return View(VwModel);
                }
                if (String.IsNullOrEmpty(changepasswordmodel.EmailAddress))
                {

                    VwModel.EmailAddress = "";
                    VwModel.IsCodeSend = false;
                    HttpContext.Session.SetString("ChangeCode", null);
                    ModelState.AddModelError("EmailAddress", "Please enter your valid email id");
                    return View(VwModel);
                }
                Random r = new Random();
                int n = r.Next(1500, 18000);
                HttpContext.Session.SetString("ChangeCode", n.ToString());
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                if (UtilityAll.SendEmail(changepasswordmodel.EmailAddress, "Varification code for update password", String.Format("Your code is {0}", n),""))
                {
                    this.ShowSuccessMessage(String.Format("A varification code has been sent to your email id {0}.Please verify and change the password", changepasswordmodel.EmailAddress));

                    changepasswordmodel.ConfirmPassword = "";
                    changepasswordmodel.Password = "";
                    changepasswordmodel.ChangeCode = "";
                    changepasswordmodel.IsCodeSend = true;
                    HttpContext.Session.SetString("IsCodeSend", changepasswordmodel.IsCodeSend.ToString());

                    return View(changepasswordmodel);
                }
                else
                {
                    this.ErrorMessage("Email Sending Failed");
                    VwModel.EmailAddress = "";
                    VwModel.IsCodeSend = false;

                    HttpContext.Session.SetString("IsCodeSend", VwModel.IsCodeSend.ToString());
                    HttpContext.Session.SetString("ChangeCode", null);
                    return View(VwModel);
                }
            }
            else
            {
                var changeCode = HttpContext.Session.GetString("ChangeCode");


                if (changeCode == null)
                {
                    this.ErrorMessage("Session Expired. Please retry");
                    VwModel.EmailAddress = "";
                    VwModel.IsCodeSend = false;

                    HttpContext.Session.SetString("IsCodeSend", VwModel.IsCodeSend.ToString());
                    HttpContext.Session.SetString("ChangeCode", null);

                    return View(VwModel);
                }
                else
                {

                    var isCodeSendChange = HttpContext.Session.GetString("IsCodeSend").ToBoolean();

                    changepasswordmodel.IsCodeSend = isCodeSendChange;
                    if (String.IsNullOrEmpty(changepasswordmodel.ChangeCode))
                    {
                        ModelState.AddModelError("ChangeCode", "Enter varification code.");
                        return View(changepasswordmodel);
                    }
                    if (String.IsNullOrEmpty(changepasswordmodel.Password))
                    {
                        ModelState.AddModelError("Password", "Enter Password.");
                        return View(changepasswordmodel);
                    }
                    if (String.IsNullOrEmpty(changepasswordmodel.ConfirmPassword))
                    {
                        ModelState.AddModelError("ConfirmPassword", "Enter Confirm Password.");
                        return View(changepasswordmodel);
                    }
                    var changeCodeChange = HttpContext.Session.GetString("ChangeCode");

                    if (changeCodeChange.ToStringWithNullEmpty() != changepasswordmodel.ChangeCode)
                    {
                        ModelState.AddModelError("ChangeCode", "Invalid varification code.");
                        return View(changepasswordmodel);
                    }
                    if (changepasswordmodel.ConfirmPassword != changepasswordmodel.Password)
                    {
                        ModelState.AddModelError("ConfirmPassword", "Confirm Password is not matching");
                        return View(changepasswordmodel);
                    }
                    else
                    {

                        var AllUser = new UserManagementService(_configuration).DataAccessLayer.GetUserList();
                        var _Email = AllUser.FirstOrDefault(i => i.EmailId == changepasswordmodel.EmailAddress);
                        _Email.Password = changepasswordmodel.Password;
                        new UserManagementService(_configuration).DataAccessLayer.UpdateUser(_Email);
                        this.ShowSuccessMessage("Password has been changed");
                        return RedirectToAction("Index", "Home");

                    }
                }

            }


        }
    }
}