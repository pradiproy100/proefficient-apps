﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Dal.Utility;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ImportMasterController : Controller
    {
        private readonly IConfiguration _configuration;
        public ImportMasterController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult UploadFile(string type)
        {
            int OrganizationId = UserUtility.CurrentUser.OrganizationId ;
            int currentUserId = UserUtility.CurrentUser.Id;
            string TableName = "";
            int colIndex = 0;
            if (type == "Machine")
            {
                TableName = "MasterMachineTemp";
                colIndex = 7;
            }
            else if (type == "Product")
            {
                TableName = "MasterProductTemp";
                colIndex = 8;
            }
            else if (type == "Machine-Product Mapping")
            {
                TableName = "Product_Machine_RelTemp";
                colIndex = 5;
            }
            string strMissingColumn = "";
            string filePath = string.Empty;
            IFormFile file = Request.Form.Files[0];
            //ExcelDataReader works on binary excel file
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            Stream stream = file.OpenReadStream();
            //We need to written the Interface.
            IExcelDataReader reader = null;
            if (file.FileName.EndsWith(".xls"))
            {
                //reads the excel file with .xls extension
                reader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (file.FileName.EndsWith(".xlsx"))
            {
                //reads excel file with .xlsx extension
                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }
            else
            {
                //Shows error if uploaded file is not Excel file
                ModelState.AddModelError("File", "This file format is not supported");
                return View();
            }
            //treats the first row of excel file as Coluymn Names
            var result = reader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                {
                    FilterColumn = (columnReader, columnIndex) => columnIndex < 7,                  
                    UseHeaderRow = true
                }
            });
            //Adding reader data to DataSet() 
            DataTable DT = RemoveEmptyRowsFromDataTable(result.Tables[0]);
           
            if (DT != null && DT.Rows.Count > 0)
            {

               // string AppData = AppDomain.CurrentDomain.GetData("DataDirectory") as string;
                //string XmlPath = AppData + "/"+type+".xml";
                //var doc = XDocument.Load(XmlPath);
                //var xmlColList = doc.Element("ColumnNames").Descendants("ColumnName").Select(d => d.Value);               
                //var dtColList = DT.Columns.Cast<DataColumn>().Select(x => x.ColumnName);
                //IEnumerable<string> onlyInFirstSet = xmlColList.Except(dtColList);
                //IEnumerable<string> onlyInSecondSet = dtColList.Except(xmlColList);
                //if (onlyInFirstSet.ToList().Count != 0 || onlyInSecondSet.ToList().Count != 0)
                //{
                //    strMissingColumn = "Invalid Data File. Please download "+type+" File from system or contact Administrator.";
                //}

                if (string.IsNullOrEmpty(strMissingColumn))
                {

                    string conString = _configuration.GetValue<string>("ConnectionString"); //"Data Source=SHAILENDRA\\SQLEXPRESS;Initial Catalog=pROeFFICIENTdb;User ID=SA;Password=123456";



                    using (SqlConnection conn = new SqlConnection(conString))
                    {
                        var bulkCopy = new SqlBulkCopy(conn);
                        bulkCopy.DestinationTableName = TableName;
                        conn.Open();
                        var schema = conn.GetSchema("Columns", new[] { null, null, TableName });
                        foreach (DataColumn sourceColumn in DT.Columns)
                        {
                            foreach (DataRow row in schema.Rows)
                            {
                                if (string.Equals(sourceColumn.ColumnName, (string)row["COLUMN_NAME"], StringComparison.OrdinalIgnoreCase))
                                {
                                    bulkCopy.ColumnMappings.Add(sourceColumn.ColumnName, (string)row["COLUMN_NAME"]);
                                    // Map the Excel columns with that of the database table
                                    //sqlBulkCopy.ColumnMappings.Add("Id", "CustomerId");
                                    //sqlBulkCopy.ColumnMappings.Add("Name", "Name");
                                    //sqlBulkCopy.ColumnMappings.Add("Country", "Country");

                                    break;
                                }
                            }
                        }
                        try { 
                            bulkCopy.WriteToServer(DT);
                            //if (type == "Machine" || type == "Product")
                            //{
                            //    var objImport = new ImportDataService(_configuration).DataAccessLayer;
                            //    bool res = objImport.ValidateImporteData(type, currentUserId, OrganizationId);
                            //    if (res)
                            //    { strMissingColumn = "Valid entries Saved Successfully"; }
                            //    else { strMissingColumn = "Some data validation error occured."; }
                            //}
                        }
                        catch (Exception ex) 
                        {
                            strMissingColumn = ex.Message;
                        }
                        
                        //try
                        //{
                            
                        //    var objImport = new ImportDataService(_configuration).DataAccessLayer;
                        //    bool res = objImport.ValidateImporteData(type);
                        //    if(res)
                        //    { strMissingColumn = "Saved Successfully"; }
                        //}
                        //catch (Exception ex) { strMissingColumn = ex.Message; }
                    }
                }
                else { return Content(strMissingColumn); }

            }
            if (string.IsNullOrEmpty(strMissingColumn))
            {
                return Content(strMissingColumn);
            }
            else { return Content(strMissingColumn); }
        }
        public List<String> CheckColumns(string type)
        {
            // bool isExists = false;
            string AppData = AppDomain.CurrentDomain.GetData("DataDirectory") as string;
            string XmlPath = AppData + "/"+type+".xml";
            var doc = XDocument.Load(XmlPath);
            // var results = doc.Element("ColumnNames").Descendants("ColumnName").Where(d => d.Value.Contains(ColName)).Select(d => d.Value).FirstOrDefault();
            var results = doc.Element("ColumnNames").Descendants("ColumnName").Select(d => d.Value).ToList();
            //if (!string.IsNullOrEmpty(results))
            //{ isExists = true; }
            //return isExists;
            return results;
        }
        DataTable RemoveEmptyRowsFromDataTable(DataTable dt)
        {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                if (dt.Rows[i][1] == DBNull.Value)
                    dt.Rows[i].Delete();
            }
            dt.AcceptChanges();
            return dt;
        }

    }
}
