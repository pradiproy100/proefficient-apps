﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;
using System.Threading;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class DieController : Controller
    {
        private readonly IConfiguration _configuration;

        public DieController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Die

        public ActionResult Index()
        {
            var objDie = new DieService(_configuration).DataAccessLayer;

            return View(objDie.GetMasterDieList(UserUtility.CurrentUser.OrganizationId)?.OrderByDescending(i => i?.DieId));

        }

        public ActionResult Create()
        {

            return View(new MasterDie() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });
        }
        [HttpPost]
        public ActionResult Create(MasterDie pDie)
        {
            if (ModelState.IsValid)
            {
                var AllOps = new DieService(_configuration).DataAccessLayer.GetMasterDieList(UserUtility.CurrentUser.OrganizationId);                
                pDie.IsActive = true;
                pDie.AddBy = UserUtility.CurrentUser.Id;         
                pDie.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new DieService(_configuration).DataAccessLayer.UpdateMasterDie(pDie);  
                this.CreateObjectAlert("Die");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Die Data is not valid");
                return View(pDie);
            }
        }
        public ActionResult Edit(int id)
        {

            var Die = new DieService(_configuration).DataAccessLayer.GetMasterDieById(UserUtility.CurrentUser.OrganizationId,id);

            if (Die == null)
            {
                return RedirectToAction("Index");
            }


            return View(Die);
        }
        [HttpPost]
        public ActionResult Edit(MasterDie pDie)
        {
            if (ModelState.IsValid)
            {               
                pDie.EditBy = UserUtility.CurrentUser.Id;
                pDie.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new DieService(_configuration).DataAccessLayer.UpdateMasterDie(pDie);
                this.UpdatedObjectAlert("Die");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Die Data is not valid");
                return View(pDie);
            }
        }
        public ActionResult Delete(int id)
        {


            var Die = new DieService(_configuration).DataAccessLayer.GetMasterDieById(UserUtility.CurrentUser.OrganizationId, id);
            if (Die != null)
            {
                Die.IsActive = !Die.IsActive;
                Die.EditBy = UserUtility.CurrentUser.Id;
                new DieService(_configuration).DataAccessLayer.UpdateMasterDie(Die);

            }
            return RedirectToAction("Index");
        }

    }
}