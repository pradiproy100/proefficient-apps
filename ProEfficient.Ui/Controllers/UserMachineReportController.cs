﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using ProEfficient.Ui.Entity;
using System.Collections.Generic;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class UserMachineReportController : Controller
    {
        private readonly ProefficientContext _context;
        private readonly IConfiguration _configuration;
        private int CurrentOrgId;
        public UserMachineReportController(ProefficientContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            CurrentOrgId = UserUtility.CurrentUser.OrganizationId;
        }

        // GET: Machine
        public SelectList GetFreequency()
        {
            List<SelectListItem> freequency = new List<SelectListItem>();
            freequency.Add(new SelectListItem() { Text = "Today", Value = "Today" });
            freequency.Add(new SelectListItem() { Text = "Yesterday", Value = "Yesterday" });
            freequency.Add(new SelectListItem() { Text = "This Week", Value = "This Week" });
            freequency.Add(new SelectListItem() { Text = "Last 7 Days", Value = "Last 7 Days" });
            freequency.Add(new SelectListItem() { Text = "Last Week", Value = "Last Week" });
            freequency.Add(new SelectListItem() { Text = "This Month", Value = "This Month" });
            freequency.Add(new SelectListItem() { Text = "Last Month", Value = "Last Month" });

          return new SelectList(freequency, "Value", "Text");
        }
        public ActionResult Index()
        {
            var objMachine = new UserMachineReportService(_configuration).DataAccessLayer;
           // ViewData["MachineIds"] = new SelectList(_context.MasterMachines, "MachineId", "MachineName");
            return View(objMachine.GetReportConfigList(UserUtility.CurrentUser.Id).OrderByDescending(i => i.UserMachineReportID));

        }

        public ActionResult Create()
        {
            ViewData["MachineIds"] = new SelectList(_context.MasterMachines.Where(m=>m.OrganizationId== CurrentOrgId), "MachineId", "MachineName");
            ViewData["UserIds"] = new SelectList(_context.AspNetUsers.Where(m => m.OrganizationId == CurrentOrgId), "Id", "UserName");
            ViewData["Freequency"] = GetFreequency();
            return View(new UserReportConfig());
        }
        [HttpPost]
        public ActionResult Create(UserReportConfig pMachine)
        {
            if (ModelState.IsValid)
            {
                if (pMachine.arrMachineIds != null) { pMachine.MachineIds = string.Join(",", pMachine.arrMachineIds); }
                if (pMachine.arrRecipients != null) { pMachine.MailTo = string.Join(",", pMachine.arrRecipients); }
                // pMachine.IsDefault = pMachine.IsDefault;
                pMachine.UserId = UserUtility.CurrentUser.Id;
                //if (new UserMachineReportService(_configuration).DataAccessLayer.GetMasterMachineList().FirstOrDefault(i => i.MachineCode.Trim() == pMachine.MachineCode.Trim() && i.OrganizationId == pMachine.OrganizationId) != null)
                //{
                //    ModelState.AddModelError("MachineCode", "Duplicate Machine Code For Same Organization");
                //    return View(pMachine);
                //}
                               
                new UserMachineReportService(_configuration).DataAccessLayer.UpdateReportConfig(pMachine);
                this.CreateObjectAlert("Report Group");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Data is not valid");
                return View(pMachine);
            }
        }
        public ActionResult Edit(int id)
        {

            var Machine = new UserMachineReportService(_configuration).DataAccessLayer.GetReportConfig(id);

            if (Machine == null)
            {
                return RedirectToAction("Index");
            }
            Machine.arrMachineIds = Machine.MachineIds.Split(',').Select(int.Parse).ToArray();
            Machine.arrRecipients = Machine.MailTo.Split(',').Select(int.Parse).ToArray();
            ViewData["MachineIds"] = new SelectList(_context.MasterMachines.Where(m => m.OrganizationId == CurrentOrgId), "MachineId", "MachineName");
            ViewData["UserIds"] = new SelectList(_context.AspNetUsers.Where(m => m.OrganizationId == CurrentOrgId), "Id", "UserName");
            ViewData["Freequency"] = GetFreequency();
            return View(Machine);
        }
        [HttpPost]
        public ActionResult Edit(UserReportConfig pMachine)
        {
            if (ModelState.IsValid)
            {
                //if (new UserMachineReportService(_configuration).DataAccessLayer.GetMasterMachineList().FirstOrDefault(i => i.MachineCode.Trim() == pMachine.MachineCode.Trim() && i.OrganizationId == pMachine.OrganizationId && i.MachineID != pMachine.MachineID) != null)
                //{
                //    ModelState.AddModelError("MachineCode", "Duplicate Machine Code For Same Organization");
                //    return View(pMachine);
                //}
                if (pMachine.arrMachineIds != null) { pMachine.MachineIds = string.Join(",", pMachine.arrMachineIds); }
                if (pMachine.arrRecipients != null) { pMachine.MailTo = string.Join(",", pMachine.arrRecipients); }
                new UserMachineReportService(_configuration).DataAccessLayer.UpdateReportConfig(pMachine);
                this.UpdatedObjectAlert("Report Group");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Data is not valid");
                return View(pMachine);
            }
        }
        public ActionResult Delete(int id)
        {
                new UserMachineReportService(_configuration).DataAccessLayer.DeleteReportConfig(id);
            this.DeletedObjectAlert("Report Group");
            return RedirectToAction("Index");
        }

    }
}