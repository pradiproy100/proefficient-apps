﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Dal.Utility;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class StoppageReasonController : Controller
    {
        private readonly IConfiguration _configuration;

        public StoppageReasonController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ActionResult Index()
        {

            return View(new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList());
        }

        public ActionResult Create()
        {
            LoadStoppageReasonTypeViewBag();
            return View(new StopageReason());
        }

        private void LoadStoppageReasonTypeViewBag()
        {
            var AllStoppageReasonType = new StopageReasonService(_configuration).DataAccessLayer.GetAllStoppageReasonTypes();
            if (AllStoppageReasonType != null)
            {
                ViewBag.AllStoppageReasonType = new SelectList(AllStoppageReasonType, "StoppageReasonTypeID", "StoppageReasonTypeName");
            }
        }
        [HttpPost]
        public ActionResult Create(StopageReason pStopageReason)
        {

            LoadStoppageReasonTypeViewBag();
            if (new StopageReasonService(_configuration).DataAccessLayer.GetStopageReasonList().FirstOrDefault(x => x.ReasonCode.Trim() == pStopageReason.ReasonCode.Trim()) != null)
            {
                ModelState.AddModelError("ReasonCode", "Duplicate Reason Code");
                return View(pStopageReason);
            }
            pStopageReason.IsActive = true;
            pStopageReason.AddBy = UserUtility.CurrentUser.Id;
            pStopageReason.EdittedBy = UserUtility.CurrentUser.Id;
            pStopageReason.OrganizationId = UserUtility.CurrentUser.OrganizationId;
            new StopageReasonService(_configuration).DataAccessLayer.UpdateStopageReason(pStopageReason);
            this.CreateObjectAlert("Stopage Reason");
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            LoadStoppageReasonTypeViewBag();

            var StopageReason = (new StopageReasonService(_configuration).DataAccessLayer.GetStopageReason(new StopageReason() { ReasonId = id }));
            if (StopageReason == null)
            {
                return RedirectToAction("Index");
            }
            return View(StopageReason);
        }


        [HttpPost]
        public ActionResult Edit(StopageReason pStopageReason)
        {
            LoadStoppageReasonTypeViewBag();
            pStopageReason.EdittedBy = UserUtility.CurrentUser.Id;
            new StopageReasonService(_configuration).DataAccessLayer.UpdateStopageReason(pStopageReason);
            this.UpdatedObjectAlert("Stopage Reason");
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {

            var StopageReason = new StopageReasonService(_configuration).DataAccessLayer.GetStopageReason(new StopageReason() { ReasonId = id });
            if (StopageReason != null)
            {
                StopageReason.IsActive = !StopageReason.IsActive;
                StopageReason.EdittedBy = UserUtility.CurrentUser.Id;
                new StopageReasonService(_configuration).DataAccessLayer.UpdateStopageReason(StopageReason);
            }
            return RedirectToAction("Index");
        }
    }
}