﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;
using System.Threading;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class OperatorController : Controller
    {
        private readonly IConfiguration _configuration;

        public OperatorController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Operator

        public ActionResult Index()
        {
            var objOperator = new OperatorService(_configuration).DataAccessLayer;

            return View(objOperator.GetMasterOperatorList(UserUtility.CurrentUser.OrganizationId)?.OrderByDescending(i => i?.OperatorId));

        }

        public ActionResult Create()
        {

            return View(new MasterOperator() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });
        }
        [HttpPost]
        public ActionResult Create(MasterOperator pOperator)
        {
            if (ModelState.IsValid)
            {
                var AllOps = new OperatorService(_configuration).DataAccessLayer.GetMasterOperatorList(UserUtility.CurrentUser.OrganizationId);
                if (AllOps!=null &&  AllOps.FirstOrDefault(i => i.EmployeeId.Trim() == pOperator.EmployeeId.Trim() && i.OrganizationId == pOperator.OrganizationId) != null)
                {
                    ModelState.AddModelError("EmployeeId", "Duplicate EmployeeId For Same Organization");
                    return View(pOperator);
                }
                pOperator.IsActive = true;
                pOperator.AddBy = UserUtility.CurrentUser.Id;         
                pOperator.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new OperatorService(_configuration).DataAccessLayer.UpdateMasterOperator(pOperator);  
                this.CreateObjectAlert("Operator");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Operator Data is not valid");
                return View(pOperator);
            }
        }
        public ActionResult Edit(int id)
        {

            var Operator = new OperatorService(_configuration).DataAccessLayer.GetMasterOperatorById(UserUtility.CurrentUser.OrganizationId,id);

            if (Operator == null)
            {
                return RedirectToAction("Index");
            }


            return View(Operator);
        }
        [HttpPost]
        public ActionResult Edit(MasterOperator pOperator)
        {
            if (ModelState.IsValid)
            {
                if (new OperatorService(_configuration).DataAccessLayer.GetMasterOperatorList(UserUtility.CurrentUser.OrganizationId).FirstOrDefault(i => i.EmployeeId.Trim() == pOperator.EmployeeId.Trim() && i.OrganizationId == pOperator.OrganizationId && i.OperatorId != pOperator.OperatorId) != null)
                {
                    ModelState.AddModelError("EmployeeId", "Duplicate EmployeeId For Same Organization");
                    return View(pOperator);
                }
                pOperator.EditBy = UserUtility.CurrentUser.Id;
                pOperator.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new OperatorService(_configuration).DataAccessLayer.UpdateMasterOperator(pOperator);
                this.UpdatedObjectAlert("Operator");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Operator Data is not valid");
                return View(pOperator);
            }
        }
        public ActionResult Delete(int id)
        {


            var Operator = new OperatorService(_configuration).DataAccessLayer.GetMasterOperatorById(UserUtility.CurrentUser.OrganizationId, id);
            if (Operator != null)
            {
                Operator.IsActive = !Operator.IsActive;
                Operator.EditBy = UserUtility.CurrentUser.Id;
                new OperatorService(_configuration).DataAccessLayer.UpdateMasterOperator(Operator);

            }
            return RedirectToAction("Index");
        }

    }
}