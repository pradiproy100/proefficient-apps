﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class ShiftController : Controller
    {
        private readonly IConfiguration _configuration;

        public ShiftController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ActionResult Index()
        {

            var objShift = new ShiftService(_configuration).DataAccessLayer;
            var Shifts = objShift.GetShiftList().Where(x => x.OrganizationId == UserUtility.CurrentUser.OrganizationId);
            string[] arrTime;
            foreach (var dr in Shifts)
            {
                arrTime = dr.StartTime.ToString().Split('.');
                dr.strStartTime = (arrTime[0].Count() == 1 ? "0" + arrTime[0] : arrTime[0]) + ":" + arrTime[1];
                dr.EndTime = dr.EndTime > (23.59).ToDecimal() ? dr.EndTime - 24 : dr.EndTime;
                arrTime = dr.EndTime.ToString().Split('.');
                dr.strEndTime = (arrTime[0].Count() == 1 ? "0" + arrTime[0] : arrTime[0]) + ":" + arrTime[1];

            }
            return View(Shifts);
        }

        public ActionResult Create()
        {
            if (!UserUtility.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            return View(new Shift());
        }

        [HttpPost]
        public ActionResult Create(Shift pShift)
        {
            string[] tempBreakInfo;
            BreakInfo objBreakInfo;
            var arrMain = pShift.ShiftInfoList.Split(new[] { "},{" }, StringSplitOptions.None);
            if (arrMain[0].Contains("BreakName"))
            {
                for (int x = 0; x < arrMain.Length; x++)
                {
                    arrMain[x] = arrMain[x].Replace("BreakName", string.Empty);
                    arrMain[x] = arrMain[x].Replace("StartTime", string.Empty);
                    arrMain[x] = arrMain[x].Replace("EndTime", string.Empty);
                    arrMain[x] = arrMain[x].Replace(":\"", string.Empty);
                    arrMain[x] = arrMain[x].Replace("\"", string.Empty);
                    arrMain[x] = arrMain[x].Replace("}]", string.Empty);
                    arrMain[x] = arrMain[x].Replace("[{", string.Empty);
                    tempBreakInfo = arrMain[x].Split(',');
                    objBreakInfo = new BreakInfo();
                    objBreakInfo.BreakName = tempBreakInfo[0];
                    objBreakInfo.strBreakStartTime = tempBreakInfo[1];
                    objBreakInfo.strBreakEndTime = tempBreakInfo[2];
                    objBreakInfo.StartTime = Convert.ToDecimal(tempBreakInfo[1].Replace(':', '.'));
                    objBreakInfo.EndTime = Convert.ToDecimal(tempBreakInfo[2].Replace(':', '.'));
                    objBreakInfo.IsActive = true;
                    objBreakInfo.EdittedBy = UserUtility.CurrentUser.Id;
                    objBreakInfo.AddBy = UserUtility.CurrentUser.Id;
                    pShift._BreakInfo.Add(objBreakInfo);
                }
            }

            if (new ShiftService(_configuration).DataAccessLayer.GetShiftList().FirstOrDefault(x => x.ShiftName.Trim().ToUpper() == pShift.ShiftName.Trim().ToUpper() && x.OrganizationId == pShift.OrganizationId) != null)
            {
                ModelState.AddModelError("ShiftName", "Duplicate Shift Name");
                return View(pShift);
            }

            var arrTime = pShift.strStartTime.Split(':');
            pShift.StartTime = (arrTime[0] + "." + arrTime[1]).ToDecimal();
            arrTime = pShift.strEndTime.Split(':');
            pShift.EndTime = (arrTime[0] + "." + arrTime[1]).ToDecimal();

            if (pShift.StartTime == pShift.EndTime)
            {
                ModelState.AddModelError("strEndTime", "Shift Start Time and Shift End Time can not be Same");
                return View(pShift);
            }
            if (pShift.StartTime > pShift.EndTime)
            {
                if (pShift.isOverNight == true)
                {
                    pShift.EndTime = pShift.EndTime + 24;
                }
                else
                {
                    ModelState.AddModelError("strEndTime", "Shift End Time Must be Greater than Shift Start Time");
                    return View(pShift);
                }
            }

            foreach (BreakInfo dr in pShift._BreakInfo)
            {
                if (dr.StartTime > dr.EndTime && pShift.isOverNight == false)
                {
                    ModelState.AddModelError("strEndTime", "Break End Time Must be Greater than Break Start Time");
                    return View(pShift);
                }
                if (pShift.isOverNight == true)
                {
                    if ((dr.StartTime < pShift.StartTime && (dr.StartTime + 24) > pShift.EndTime) || (dr.EndTime < pShift.StartTime && (dr.EndTime + 24) > pShift.EndTime))
                    {
                        ModelState.AddModelError("strEndTime", "Break Time Must be in between Shift Start Time and Shift End Time");
                        return View(pShift);
                    }
                }
                else
                {
                    if (dr.StartTime < pShift.StartTime || dr.EndTime > pShift.EndTime)
                    {
                        ModelState.AddModelError("strEndTime", "Break Time Must be in between Shift Start Time and Shift End Time");
                        return View(pShift);
                    }
                }
                if (dr.StartTime > dr.EndTime && pShift.isOverNight == true)
                {
                    dr.EndTime = dr.EndTime + 24;
                }
            }

            pShift.IsActive = true;
            pShift.AddBy = UserUtility.CurrentUser.Id;
            pShift.EdittedBy = UserUtility.CurrentUser.Id;
            Shift _shift;
            if (new ShiftService(_configuration).DataAccessLayer.UpdateShift(pShift) == true)
            {
                _shift = new ShiftService(_configuration).DataAccessLayer.GetShiftList().FirstOrDefault(x => x.ShiftName.Trim().ToUpper() == pShift.ShiftName.Trim().ToUpper());

                if (_shift != null)
                {
                    foreach (BreakInfo dr in pShift._BreakInfo)
                    {
                        dr.ShiftId = _shift.ShiftId;
                        new BreakInfoService(_configuration).DataAccessLayer.UpdateBreakInfo(dr);
                    }
                }
            }
            this.CreateObjectAlert("Shift details");
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var Shift = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = id });

            if (Shift == null)
            {
                return RedirectToAction("Index");
            }

            var arrTime = Shift.StartTime.ToString().Split('.');
            Shift.strStartTime = (arrTime[0].Count() == 1 ? "0" + arrTime[0] : arrTime[0]) + ":" + arrTime[1];
            Shift.EndTime = Shift.EndTime > (23.59).ToDecimal() ? Shift.EndTime - 24 : Shift.EndTime;
            arrTime = Shift.EndTime.ToString().Split('.');
            Shift.strEndTime = (arrTime[0].Count() == 1 ? "0" + arrTime[0] : arrTime[0]) + ":" + arrTime[1];
            Shift._BreakInfo = new BreakInfoService(_configuration).DataAccessLayer.USP_GetBreakInfoByShiftId(new BreakInfo() { ShiftId = id });

            foreach (BreakInfo dr in Shift._BreakInfo)
            {
                arrTime = dr.StartTime.ToString().Split('.');
                dr.strBreakStartTime = (arrTime[0].Count() == 1 ? "0" + arrTime[0] : arrTime[0]) + ":" + arrTime[1];
                dr.EndTime = dr.EndTime > (23.59).ToDecimal() ? dr.EndTime - 24 : dr.EndTime;
                arrTime = dr.EndTime.ToString().Split('.');
                dr.strBreakEndTime = (arrTime[0].Count() == 1 ? "0" + arrTime[0] : arrTime[0]) + ":" + arrTime[1];
            }

            return View(Shift);
        }

        [HttpPost]
        public ActionResult Edit(Shift pShift)
        {
            string[] tempBreakInfo;
            BreakInfo objBreakInfo;
            if (pShift.ShiftInfoList != null )
            {
                var arrMain = pShift.ShiftInfoList.Split(new[] { "},{" }, StringSplitOptions.None);
                if (arrMain[0].Contains("BreakName"))
                {
                    for (int x = 0; x < arrMain.Length; x++)
                    {
                        arrMain[x] = arrMain[x].Replace("BreakName", string.Empty);
                        arrMain[x] = arrMain[x].Replace("StartTime", string.Empty);
                        arrMain[x] = arrMain[x].Replace("EndTime", string.Empty);
                        arrMain[x] = arrMain[x].Replace(":\"", string.Empty);
                        arrMain[x] = arrMain[x].Replace("\"", string.Empty);
                        arrMain[x] = arrMain[x].Replace("}]", string.Empty);
                        arrMain[x] = arrMain[x].Replace("[{", string.Empty);
                        tempBreakInfo = arrMain[x].Split(',');
                        objBreakInfo = new BreakInfo();
                        objBreakInfo.BreakName = tempBreakInfo[0];
                        objBreakInfo.strBreakStartTime = tempBreakInfo[1];
                        objBreakInfo.strBreakEndTime = tempBreakInfo[2];
                        objBreakInfo.StartTime = Convert.ToDecimal(tempBreakInfo[1].Replace(':', '.'));
                        objBreakInfo.EndTime = Convert.ToDecimal(tempBreakInfo[2].Replace(':', '.'));
                        objBreakInfo.IsActive = true;
                        objBreakInfo.EdittedBy = UserUtility.CurrentUser.Id;
                        objBreakInfo.AddBy = UserUtility.CurrentUser.Id;
                        pShift._BreakInfo.Add(objBreakInfo);
                    }
                }
            }
            if (new ShiftService(_configuration).DataAccessLayer.GetShiftList().FirstOrDefault(x => x.ShiftName.Trim().ToUpper() == pShift.ShiftName.Trim().ToUpper() && x.ShiftId != pShift.ShiftId && x.OrganizationId == pShift.OrganizationId) != null)
            {
                ModelState.AddModelError("ShiftName", "Duplicate Shift Name");
                return View(pShift);
            }

            var arrTime = pShift.strStartTime.Split(':');
            pShift.StartTime = (arrTime[0] + "." + arrTime[1]).ToDecimal();
            arrTime = pShift.strEndTime.Split(':');
            pShift.EndTime = (arrTime[0] + "." + arrTime[1]).ToDecimal();

            if (pShift.StartTime == pShift.EndTime)
            {
                ModelState.AddModelError("strEndTime", "Shift Start Time and Shift End Time can not be Same");
                return View(pShift);
            }
            if (pShift.StartTime > pShift.EndTime)
            {
                if (pShift.isOverNight == true)
                {
                    pShift.EndTime = pShift.EndTime + 24;
                }
                else
                {
                    ModelState.AddModelError("strEndTime", "Shift End Time Must be Greater than Shift Start Time");
                    return View(pShift);
                }
            }

            foreach (BreakInfo dr in pShift._BreakInfo)
            {
                if (dr.StartTime > dr.EndTime && pShift.isOverNight == false)
                {
                    ModelState.AddModelError("strEndTime", "Break End Time Must be Greater than Break Start Time");
                    return View(pShift);
                }
                if (pShift.isOverNight == true)
                {
                    if ((dr.StartTime < pShift.StartTime && (dr.StartTime + 24) > pShift.EndTime) || (dr.EndTime < pShift.StartTime && (dr.EndTime + 24) > pShift.EndTime))
                    {
                        ModelState.AddModelError("strEndTime", "Break Time Must be in between Shift Start Time and Shift End Time");
                        return View(pShift);
                    }
                }
                else
                {
                    if (dr.StartTime < pShift.StartTime || dr.EndTime > pShift.EndTime)
                    {
                        ModelState.AddModelError("strEndTime", "Break Time Must be in between Shift Start Time and Shift End Time");
                        return View(pShift);
                    }
                }
                if (dr.StartTime > dr.EndTime && pShift.isOverNight == true)
                {
                    dr.EndTime = dr.EndTime + 24;
                }
            }

            pShift.IsActive = true;
            pShift.AddBy = UserUtility.CurrentUser.Id;
            pShift.EdittedBy = UserUtility.CurrentUser.Id;

            if (new ShiftService(_configuration).DataAccessLayer.UpdateShift(pShift) == true)
            {
                if (pShift._BreakInfo.Count > 0)
                {
                    for (int i = 0; i < pShift._BreakInfo.Count; i++)
                    {
                        if (i == 0)
                        {
                            pShift._BreakInfo[0].IsFirst = true;
                        }
                        pShift._BreakInfo[i].ShiftId = pShift.ShiftId;
                        new BreakInfoService(_configuration).DataAccessLayer.UpdateBreakInfo(pShift._BreakInfo[i]);
                    }
                }
                else
                {
                    BreakInfo tempBrkInf = new BreakInfo();
                    tempBrkInf.ShiftId = pShift.ShiftId;
                    tempBrkInf.IsFirst = true;
                    pShift._BreakInfo.Add(tempBrkInf);
                    new BreakInfoService(_configuration).DataAccessLayer.UpdateBreakInfo(pShift._BreakInfo[0]);
                }
            }

            this.UpdatedObjectAlert("Shift details");
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            if (!UserUtility.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            var Shift = new ShiftService(_configuration).DataAccessLayer.GetShift(new Shift() { ShiftId = id });
            if (Shift != null)
            {
                Shift.IsActive = !Shift.IsActive;
                Shift.EdittedBy = UserUtility.CurrentUser.Id;
                new ShiftService(_configuration).DataAccessLayer.UpdateShift(Shift);
            }
            return RedirectToAction("Index");
        }
    }
}