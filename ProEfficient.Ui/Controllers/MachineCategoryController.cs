﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class MachineCategoryController : Controller
    {
        private readonly IConfiguration _configuration;

        public MachineCategoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: MachineCategory
        public ActionResult Index()
        {


            var objCategoryDAL = new MachineCategoryService(_configuration).DataAccessLayer;
            var CategoryList = objCategoryDAL.GetMachineCategoryList().Where(x => x.OrganizationId == UserUtility.CurrentUser.OrganizationId);

            return View(CategoryList);

        }

        public ActionResult Create()
        {


            var newCat = new MachineCategory();
            newCat.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();

            return View();
        }

        [HttpPost]
        public ActionResult Create(MachineCategory pMachineCategory)
        {

            pMachineCategory.OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0();
            ModelState.Remove("OrganizationId");
            if (ModelState.IsValid)
            {

                if (new MachineCategoryService(_configuration).DataAccessLayer.UpdateMachineCategory(pMachineCategory))
                {

                    this.CreateObjectAlert("Machine Category");
                    return RedirectToAction("Index");
                }
                else
                {
                    this.ErrorMessage("Machine Category Save Error");
                    ModelState.AddModelError(string.Empty, "Machine Category is not saved");
                    return View(pMachineCategory);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Machine Category Data is not valid");
                return View(new MachineCategory());
                //this.ErrorMessage("Something is not right");
                //return View(new MachineCategory());
            }


        }

        public ActionResult Edit(int id)
        {

            var CateGory = new MachineCategoryService(_configuration).DataAccessLayer.GetMachineCategoryList().FirstOrDefault(i => i.CategoryId == id);

            if (CateGory == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(CateGory);
            }
        }

        [HttpPost]
        public ActionResult Edit(MachineCategory pMachineCategory)
        {


            if (ModelState.IsValid)
            {
                if (new MachineCategoryService(_configuration).DataAccessLayer.UpdateMachineCategory(pMachineCategory))
                {
                    this.ShowSuccessMessage("Machine Category Updated");
                    return RedirectToAction("Index");
                }
                else
                {
                    this.ErrorMessage("Machine Category Save Error");
                    ModelState.AddModelError(string.Empty, "Machine Category is not saved");
                    return View(pMachineCategory);
                }
            }
            else
            {

                this.ErrorMessage("Something is not right");
                ModelState.AddModelError(string.Empty, "Machine Category Data is not valid");
                return View(pMachineCategory);
            }
        }

        public ActionResult Assign(int id)
        {

            var CateGory = new MachineCategoryService(_configuration).DataAccessLayer.GetMachineCategoryList().FirstOrDefault(i => i.CategoryId == id);

            if (CateGory == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                var MachineList = new MachineService(_configuration).DataAccessLayer.GetMasterMachineList().FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId);
                MachineList = MachineList.FindAll(i => i.CategoryId == null || i.CategoryId == id || i.CategoryId == 0);

                HttpContext.Session.SetString("CurrentCategoryId", id.ToString());
                return View(MachineList);
            }
        }

        public ActionResult DoAssign(int id)
        {

            int CategoryId;
            var CurrentCategoryId = HttpContext.Session.GetString("CurrentCategoryId");
            if (CurrentCategoryId != null)
            {
                CategoryId = CurrentCategoryId.ToInteger0();
                var Machine = new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = id });
                Machine.CategoryId = CategoryId;

                if (new MachineService(_configuration).DataAccessLayer.UpdateMasterMachine(Machine))
                {
                    this.ShowSuccessMessage("Category Assigned");
                    return RedirectToAction("Assign", new { id = CategoryId });
                }
                else
                {
                    this.ErrorMessage("Something is wrong");
                    return RedirectToAction("Assign", new { id = CategoryId });
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

        }


        public ActionResult UnAssign(int id)
        {

            int CategoryId;
            var CurrentCategoryId = HttpContext.Session.GetString("CurrentCategoryId");

            if (CurrentCategoryId != null)
            {
                CategoryId = CurrentCategoryId.ToInteger0();
                var Machine = new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = id });
                Machine.CategoryId = null;

                if (new MachineService(_configuration).DataAccessLayer.UpdateMasterMachine(Machine))
                {
                    this.ShowSuccessMessage("Category assignment removed");
                    return RedirectToAction("Index");
                }
                else
                {
                    this.ErrorMessage("Something is wrong");
                    return RedirectToAction("Assign", new { id = CategoryId });
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

    }
}