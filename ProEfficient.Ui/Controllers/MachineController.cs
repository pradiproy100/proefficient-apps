﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class MachineController : Controller
    {
        private readonly IConfiguration _configuration;

        public MachineController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Machine

        public ActionResult Index()
        {
            var objMachine = new MachineService(_configuration).DataAccessLayer;

            return View(objMachine.GetMasterMachineList().FindAll(i => i.OrganizationId == UserUtility.CurrentUser.OrganizationId).OrderByDescending(i => i.MachineID));

        }

        public ActionResult Create()
        {

            return View(new MasterMachine() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });
        }
        [HttpPost]
        public ActionResult Create(MasterMachine pMachine)
        {
            if (ModelState.IsValid)
            {
                pMachine.IsActive = true;
                pMachine.AddBy = UserUtility.CurrentUser.Id;
                if (new MachineService(_configuration).DataAccessLayer.GetMasterMachineList().FirstOrDefault(i => i.MachineCode.Trim() == pMachine.MachineCode.Trim() && i.OrganizationId == pMachine.OrganizationId) != null)
                {
                    ModelState.AddModelError("MachineCode", "Duplicate Machine Code For Same Organization");
                    return View(pMachine);
                }

                pMachine.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new MachineService(_configuration).DataAccessLayer.UpdateMasterMachine(pMachine);
                this.CreateObjectAlert("Machine");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Machine Data is not valid");
                return View(pMachine);
            }
        }
        public ActionResult Edit(int id)
        {

            var Machine = new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = id });

            if (Machine == null)
            {
                return RedirectToAction("Index");
            }


            return View(Machine);
        }
        [HttpPost]
        public ActionResult Edit(MasterMachine pMachine)
        {
            if (ModelState.IsValid)
            {
                if (new MachineService(_configuration).DataAccessLayer.GetMasterMachineList().FirstOrDefault(i => i.MachineCode.Trim() == pMachine.MachineCode.Trim() && i.OrganizationId == pMachine.OrganizationId && i.MachineID != pMachine.MachineID) != null)
                {
                    ModelState.AddModelError("MachineCode", "Duplicate Machine Code For Same Organization");
                    return View(pMachine);
                }
                pMachine.EdittedBy = UserUtility.CurrentUser.Id;
                pMachine.OrganizationId = UserUtility.CurrentUser.OrganizationId;
                new MachineService(_configuration).DataAccessLayer.UpdateMasterMachine(pMachine);
                this.UpdatedObjectAlert("Machine");
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Machine Data is not valid");
                return View(pMachine);
            }
        }
        public ActionResult Delete(int id)
        {


            var Machine = new MachineService(_configuration).DataAccessLayer.GetMasterMachine(new MasterMachine() { MachineID = id });
            if (Machine != null)
            {
                Machine.IsActive = !Machine.IsActive;
                Machine.EdittedBy = UserUtility.CurrentUser.Id;
                new MachineService(_configuration).DataAccessLayer.UpdateMasterMachine(Machine);

            }
            return RedirectToAction("Index");
        }

    }
}