﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProEfficient.Business;
using ProEfficient.Core.Auth.DTO;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Controllers
{
    public class LoginController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly string apiBaseUrl;
        private readonly IMapper _mapper;
        public LoginController(IConfiguration configuration, UserManager<User> userManager, 
            SignInManager<User> signInManager, IMapper mapper)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
            _mapper = mapper;
            apiBaseUrl = _configuration.GetValue<string>("ApiUrls");
        }

        // GET: Login
        public ActionResult Login()
        {
            ViewBag.IsLoginPage = true;


            return View(new LoginModel());
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginModel pLogin)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ViewBag.IsLoginPage = true;
                    APIUser lstuser = new APIUser();
                    // lstuser = new UserManagementService(_configuration).DataAccessLayer.GetUserList();
                    var authenticationResult = IsAuthenticated(pLogin.Email, pLogin.Password);
                    if (authenticationResult != null)
                    {
                        lstuser = JsonConvert.DeserializeObject<APIUser>(JsonConvert.SerializeObject(authenticationResult));
                    }
                    var LoginUser = lstuser;
                    if (LoginUser == null || LoginUser.Id == 0)
                    {
                        ModelState.AddModelError(string.Empty, "User Id or Password is incorrect");
                        //  this.ErrorMessage("User Id or Password is incorrect");
                        return View(pLogin);
                    }
                    var CurrentOrganization = new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = LoginUser.OrganizationId.ToInteger0() });
                    string UserType = new Dal.DAL.UserManagementDAL(_configuration).GetAllUserType().FirstOrDefault(i => i.UserTypeID == LoginUser.UserTypeId).UserTypeName;
                    if (LoginUser != null && CurrentOrganization.IsActive.ToBoolean())
                    {
                        this.ShowSuccessMessage("Login Success,Welcome.");
                        LoginUser.OrganizationName = CurrentOrganization.OrganizationName; //new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = LoginUser.OrganizationId.ToInteger0() }).OrganizationName;
                        LoginUser.IsManualProductionEntry = CurrentOrganization.IsManualProductionEntry;
                        LoginUser.HasBreakdown = CurrentOrganization.HasBreakdown;
                        LoginUser.HasRejection = CurrentOrganization.HasRejection;
                        LoginUser.UserTypeName = UserType;
                        UserUtility.CurrentUser = LoginUser;


                        //Task.Run(() => UtilityAll.SendEmail(UtilityAll.AdminEmailId, "Information for Login in Proefficient", "<b>Hi,<b/> <br/> <p> <font color='red'> This is " + LoginUser.UserName +
                        //    " ,I am currently entering in proefficient.My Organization name is " + LoginUser.OrganizationName + ".</font> </p>"));

                        return RedirectToAction("UserDashboard", "UserDashboard");
                        //  return RedirectToAction("Index", "DataD");

                    }
                    else
                    {
                        if (!CurrentOrganization.IsActive.ToBoolean())
                        {
                            ModelState.AddModelError(string.Empty, "Your organization is not active now");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "User Id or Password is incorrect");

                        }

                        //this.ErrorMessage("User Id or Password is incorrect");
                        return View(pLogin);
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "User Id or Password is not valid");
                    return View(pLogin);
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }


        public  UserForListDTO IsAuthenticated(String email,String password)
        {
            var user = Task.Run(() => _userManager.FindByEmailAsync(email)).
                ConfigureAwait(true).GetAwaiter().
                GetResult();

            if (user != null)
            {
                var result = Task.Run(() => _signInManager.CheckPasswordSignInAsync(user, password, false)).
                    ConfigureAwait(true).GetAwaiter().GetResult();
                if (result.Succeeded)
                {
                    var appUser=  JsonConvert.DeserializeObject<UserForListDTO>(JsonConvert.SerializeObject(user));    
                    return appUser;
                }
            }
            return null;
        }

        //OLD Method
        //[HttpPost]
        //public async Task<ActionResult> Login(LoginModel pLogin)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ViewBag.IsLoginPage = true;
        //        APIUser lstuser = new APIUser();
        //        // lstuser = new UserManagementService(_configuration).DataAccessLayer.GetUserList();
        //        using (var httpClient = new HttpClient())
        //        {
        //            httpClient.BaseAddress = new Uri(apiBaseUrl);
        //            httpClient.DefaultRequestHeaders.Accept.Clear();
        //            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            //GET Method  
        //            var json = JsonConvert.SerializeObject(pLogin);
        //            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
        //            HttpResponseMessage response = await httpClient.PostAsync("/api/auth/login", stringContent);
        //            if (response.IsSuccessStatusCode)
        //            {
        //                var apiResponse = response.Content.ReadAsStringAsync().Result;
        //                var obj = JObject.Parse(apiResponse);

        //                var user = obj["user"].ToObject<APIUser>();

        //                lstuser = user;//JsonConvert.DeserializeObject<APIUser>(apiResponse);
        //                               //string stringJWT = response.Content.ReadAsStringAsync().Result;
        //                               //JWT jwt = JsonConvert.DeserializeObject<JWT>(stringJWT);

        //                //HttpContext.Session.SetString("token", jwt.Token);
        //            }
        //        }

        //        var LoginUser = lstuser;
        //        if (LoginUser == null || LoginUser.Id == 0)
        //        {
        //            ModelState.AddModelError(string.Empty, "User Id or Password is incorrect");
        //            //  this.ErrorMessage("User Id or Password is incorrect");
        //            return View(pLogin);
        //        }
        //        var CurrentOrganization = new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = LoginUser.OrganizationId.ToInteger0() });
        //        string UserType = new Dal.DAL.UserManagementDAL(_configuration).GetAllUserType().FirstOrDefault(i => i.UserTypeID == LoginUser.UserTypeId).UserTypeName;
        //        if (LoginUser != null && CurrentOrganization.IsActive.ToBoolean())
        //        {
        //            this.ShowSuccessMessage("Login Success,Welcome.");
        //            LoginUser.OrganizationName = CurrentOrganization.OrganizationName; //new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = LoginUser.OrganizationId.ToInteger0() }).OrganizationName;
        //            LoginUser.IsManualProductionEntry= CurrentOrganization.IsManualProductionEntry;
        //            LoginUser.HasBreakdown = CurrentOrganization.HasBreakdown;
        //            LoginUser.HasRejection = CurrentOrganization.HasRejection;
        //            LoginUser.UserTypeName = UserType;
        //            UserUtility.CurrentUser = LoginUser;


        //            //Task.Run(() => UtilityAll.SendEmail(UtilityAll.AdminEmailId, "Information for Login in Proefficient", "<b>Hi,<b/> <br/> <p> <font color='red'> This is " + LoginUser.UserName +
        //            //    " ,I am currently entering in proefficient.My Organization name is " + LoginUser.OrganizationName + ".</font> </p>"));

        //            return RedirectToAction("UserDashboard", "UserDashboard");
        //          //  return RedirectToAction("Index", "DataD");

        //        }
        //        else
        //        {
        //            if (!CurrentOrganization.IsActive.ToBoolean())
        //            {
        //                ModelState.AddModelError(string.Empty, "Your organization is not active now");
        //            }
        //            else
        //            {
        //                ModelState.AddModelError(string.Empty, "User Id or Password is incorrect");

        //            }

        //            //this.ErrorMessage("User Id or Password is incorrect");
        //            return View(pLogin);
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "User Id or Password is not valid");
        //        return View(pLogin);
        //    }
        //}

        public ActionResult ChangeOrganization(int id,string controllername,string actionname)
        {
            if (UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger()))
            {
                bool isChanged = UserUtility.ChangeOrg(id);
                if (isChanged)
                {
                    return RedirectToAction(actionname, controllername);
                }
               
            }
            UserUtility.CurrentUser = null;
            return RedirectToAction("Login", "Login");
        }
        public ActionResult Logout()
        {
            UserUtility.CurrentUser = null;
            //return View();
            return RedirectToAction("Login", "Login");
        }
    }
}