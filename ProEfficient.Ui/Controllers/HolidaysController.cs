﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProEfficient.Ui.Entity;
using ProEfficient.Dal.Utility;
using ProEfficient.Core.CustomAuthorization;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class HolidaysController : Controller
    {
        private readonly ProefficientContext _context;
        private int CurrentOrgId;
        public HolidaysController(ProefficientContext context)
        {
            _context = context;
            CurrentOrgId = UserUtility.CurrentUser.OrganizationId;
        }

        // GET: Holidays
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetEvents()
        {
           
                var events = _context.Holidays.Where(h=>h.OrganizationId==CurrentOrgId).ToList();
                return new JsonResult(new { data = events });

        }

        [HttpPost]
        public JsonResult SaveEvent(Holidays e)
        {
            var status = false;
            if (e.ThemeColor == null) { e.ThemeColor = "red"; }
                if (e.HolidayID > 0)
                {
                    //Update the event
                    var v = _context.Holidays.Where(a => a.HolidayID == e.HolidayID).FirstOrDefault();
                    if (v != null)
                    {
                        v.HolidayName = e.HolidayName;
                        v.Start = e.Start;
                        v.End = e.End;
                        v.HolidayDescription = e.HolidayDescription;
                        v.IsFullDay = e.IsFullDay;
                        v.ThemeColor = e.ThemeColor;
                    v.OrganizationId = CurrentOrgId;
                    }
                }
                else
                {
                e.OrganizationId = CurrentOrgId;
                    _context.Holidays.Add(e);
                }

            _context.SaveChanges();
                status = true;


            return Json(new { data = new { status = status } });
        }

        [HttpPost]
        public JsonResult DeleteEvent(int eventID)
        {
            var status = false;
            
                var v = _context.Holidays.Where(a => a.HolidayID == eventID).FirstOrDefault();
                if (v != null)
                {
                _context.Holidays.Remove(v);
                _context.SaveChanges();
                    status = true;
                }

            return Json(new { data = new { status = status } });
        }
    }
}
