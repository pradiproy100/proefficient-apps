﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProEfficient.Business;
using ProEfficient.Core.CustomAuthorization;
using ProEfficient.Core.Models;
using ProEfficient.Core.Utility;
using ProEfficient.Dal.Utility;
using System.Linq;

namespace ProEfficient.Ui.Controllers
{
    [ProEfficientAuthorization]
    public class OrganizationController : Controller
    {
        private readonly IConfiguration _configuration;

        public OrganizationController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: Organization
        public ActionResult Index()
        {
            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            var objOrganization = new OrganizationService(_configuration).DataAccessLayer;

            return View(objOrganization.GetOrganizationList().OrderByDescending(i => i.OrganizationId));
        }

        public ActionResult Create()
        {

            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            return View(new Organization());
        }
        [HttpPost]
        public ActionResult Create(Organization pOrganization)
        {


            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            if (new OrganizationService(_configuration).DataAccessLayer.GetOrganizationList().FirstOrDefault(x => x.OrganizationName.Trim() == pOrganization.OrganizationName.Trim()) != null)
            {
                ModelState.AddModelError("OrganizationName", "Duplicate Organization Name");
                return View(pOrganization);
            }

            pOrganization.IsActive = true;
            new OrganizationService(_configuration).DataAccessLayer.UpdateOrganization(pOrganization);
            this.CreateObjectAlert("Organization Details");
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }

            var Organization = new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = id });

            if (Organization == null)
            {
                return RedirectToAction("Index");
            }


            return View(Organization);
        }
        [HttpPost]
        public ActionResult Edit(Organization pOrganization)
        {

            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            if (new OrganizationService(_configuration).DataAccessLayer.GetOrganizationList().FirstOrDefault(x => x.OrganizationName.Trim() == pOrganization.OrganizationName.Trim() && x.OrganizationId != pOrganization.OrganizationId) != null)
            {
                ModelState.AddModelError("OrganizationName", "Duplicate Organization Name");
                return View(pOrganization);
            }
            pOrganization.EdittedBy = UserUtility.CurrentUser.UserId;
            new OrganizationService(_configuration).DataAccessLayer.UpdateOrganization(pOrganization);
            this.UpdatedObjectAlert("Organization Details");
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {

            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            var Organization = new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = id });
            if ((Organization.OrganizationName.Trim().ToLower() == UserUtility.CurrentUser.OrganizationName.Trim().ToLower()) && ((!Organization.IsActive) == false))
            {
                this.ErrorMessage("Current Organization can not be deactivated.");
                return RedirectToAction("Index");
            }
            if (Organization != null)
            {
                Organization.IsActive = !Organization.IsActive;
                Organization.EdittedBy = UserUtility.CurrentUser.UserId;
                new OrganizationService(_configuration).DataAccessLayer.UpdateOrganization(Organization);
                if (!Organization.IsActive.ToBoolean())
                    this.ShowSuccessMessage("Organization has been deactivated.");
                else
                    this.ShowSuccessMessage("Organization has been activated.");

            }
            return RedirectToAction("Index");
        }

        public ActionResult ChangeOrganization()
        {

            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            var Organization = new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });
            return View(Organization);
        }
        [HttpPost]
        public ActionResult ChangeOrganization(Organization pOrganization)
        {

            if (!UserUtility.IsSuperAdmin(UserUtility.CurrentUser.UserTypeId.ToInteger0()))
            {
                return RedirectToAction("Login", "Login");
            }
            UserUtility.CurrentUser.OrganizationId = pOrganization.OrganizationId;
            var Organization = new OrganizationService(_configuration).DataAccessLayer.GetOrganization(new Organization() { OrganizationId = UserUtility.CurrentUser.OrganizationId.ToInteger0() });

            UserUtility.CurrentUser.OrganizationName = Organization.OrganizationName;
            MessageUtility.ShowSuccessMessage(this, "Organization has been Changed to " + Organization.OrganizationName);
            return RedirectToAction("Index", "Home");
        }
    }
}