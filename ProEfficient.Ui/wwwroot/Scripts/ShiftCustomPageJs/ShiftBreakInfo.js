﻿$(document).ready(function () {
    var startTime = '00:00';    
    if ($('#strStartTime').val() != "") {
        startTime = $('#strStartTime').val();
    }
    var endTime = '00:00';
    if ($('#strEndTime').val() != "") {
        endTime = $('#strEndTime').val();
    }   
    $('#strStartTime').timepicker({
        'timeFormat': 'H:i',
        'step': 5,
        'disableTextInput': true
    }).val(startTime);
    $('#strEndTime').timepicker({
        'timeFormat': 'H:i',
        'step': 5,
        'disableTextInput': true
    }).val(endTime);
    $('#txtStartTime').timepicker({
        'timeFormat': 'H:i',
        'step': 5,
        'disableTextInput': true
    }).val('00:00');
    $('#txtEndTime').timepicker({
        'timeFormat': 'H:i',
        'step': 5,
        'disableTextInput': true
    }).val('00:00');
    
    
    $("body").on("click", "#btnAdd", function () {
        //Reference the Name and Country TextBoxes.
        var txtBreakName = $("#txtBreakName");
        var txtStartTime = $("#txtStartTime");
        var txtEndTime = $("#txtEndTime");

        if (txtBreakName.val() == "") {
            alert("Please Enter Break Name");
            return;
        }
        if (txtStartTime.val() == "") {
            alert("Please Enter Start Time");
            return;
        }
        if (txtEndTime.val() == "") {
            alert("Please Enter End Time");
            return;
        }        
        //if (txtStartTime.val() >= txtEndTime.val()) {
        //    alert("Break End Time Must be Greater than Break Start Time");
        //    return;
        //}

        //Get the reference of the Table's TBODY element.
        var tBody = $("#tblBreakInfo > TBODY")[0];

        //Add Row.
        var row = tBody.insertRow(-1);

        //Add Name cell.
        var cell = $(row.insertCell(-1));
        cell.html(txtBreakName.val());

        //Add Start Time cell.
        cell = $(row.insertCell(-1));
        cell.html(txtStartTime.val());

        //Add End Time cell.
        cell = $(row.insertCell(-1));
        cell.html(txtEndTime.val());

        //Add Remove Button cell.
        cell = $(row.insertCell(-1));
        var btnRemove = $("<input />");
        btnRemove.attr("type", "button");
        btnRemove.attr("class", "btn btn-remove-time");
        btnRemove.attr("onclick", "Remove(this);");
        btnRemove.val("Remove");
        cell.append(btnRemove);

        //Clear the TextBoxes.             
        txtBreakName.val("");
        txtStartTime.val("");
        txtEndTime.val("");
        //addToList();
    });

    $("body").on("click", "#btnSave", function () {
        //var startTime = $('#strStartTime').val();
        //var EndTime = $('#strEndTime').val();       
        //alert(startTime);
        //alert(EndTime);
        //if (startTime >= EndTime) {         
        //    alert ("Shift End Time Must be Greater than Shift Start Time");
        //    return;
        //}

        //Loop through the Table rows and build a JSON array.             
        var shifts = new Array();       
        $("#tblBreakInfo TBODY TR").each(function () {
            var row = $(this);
            var shift = {};
            shift.BreakName = row.find("TD").eq(0).html();
            shift.StartTime = row.find("TD").eq(1).html();
            shift.EndTime = row.find("TD").eq(2).html();
            shifts.push(shift);
        });
        $("#ShiftInfoList").val(JSON.stringify(shifts));
        $("form").submit();
    });
});
function Remove(button) {
    //Determine the reference of the Row using the Button.
    var row = $(button).closest("TR");
    var name = $("TD", row).eq(0).html();
    if (confirm("Do You Want to Delete: " + name)) {
        //Get the reference of the Table.
        var table = $("#tblBreakInfo")[0];

        //Delete the Table row using it's Index.
        table.deleteRow(row[0].rowIndex);
        //addToList();
    }
};
