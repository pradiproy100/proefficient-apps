﻿/// <reference path="../jquery-1.12.4.intellisense.js" />



var RejectionTextBox;
var ProductionId;
$(document).ready(function () {
    loadrejectionreasonlist();

    if (($("#Rejection") === undefined) || ($("#Rejection") === null)) {
        RejectionTextBox = (".rej-quantity");
    }
    else {
        RejectionTextBox = ("#Rejection");
    }

    
})

function KedownPop(prodid) {
    ProductionId = prodid;
    if (event.keyCode === 13) {

        var pid = $("#ProductionId").val();
        if ((prodid === '') || (prodid === undefined)) {
            pid = 0;           
        }
        showrejectionreasonlist(prodid);
    }
}

function loadrejectionreasonlist() {
    debugger;
    $("#productionRejectionReason").dialog({
        modal: true,
        title: 'Rejection reasons and their quantity',
        autoopen: false,
        width: 800,
        buttons: {
            ok: function () {
                if (checkQuantityValidation() == true) {
                    $(this).dialog("close");
                }
               
            }
        }
    });
    $('#productionRejectionReason').dialog('close');
}
function showrejectionreasonlist(prodid) {
    debugger;
    ProductionId = prodid;
    var RejectionQ = $(RejectionTextBox).val();
    if ((RejectionQ === '') || (RejectionQ === undefined) || (RejectionQ === 0)) {
        //do nothing
    }
    else {
        $('#productionRejectionReason').dialog('open');
        getRejectionReasonQuantity(ProductionId);
    }


}
function hiderejectionreasonlist() {

    if (checkQuantityValidation() == true) {
        $('#productionRejectionReason').dialog('close');
    }
}

function getRejectionReasonQuantity(productionid = 0) {
    var RejectionQuantity = $(RejectionTextBox).val();
    $("#divRejQuantity").html(RejectionQuantity);
    $.get("/ProductionPage/getRejectionReasonQuantityList/" + productionid).then(function (result) {
        //  alert(JSON.stringify(result));
        var HtmlRows = "";
        debugger;
        $(result).each(function (i, item) {
            var innerstring = "<tr id='" + item.ReasonId + "'><td>" + item.ReasonName + "</td><td> <input value='" + item.ReasonQuantity + "' /></td></tr>";
            HtmlRows += innerstring;
        });
        $("#rejectionReasonBody").html(HtmlRows);
    })
}

function checkQuantityValidation() {
    debugger;
    var RejectionQuan = $(RejectionTextBox).val();
    if ((RejectionQuan === '') || (RejectionQuan === undefined) || (RejectionQuan === 0)) {
        RejectionQuan = 0;
    }
    

    var TotalCount = 0;
    $('#rejectionReasonBody  > tr').each(function (index, tr) {
        console.log(index);
        console.log(tr);      
        var singlevalue = $(tr).find("td:nth-child(2) input").val();
        if (singlevalue !== undefined && singlevalue !== '' && singlevalue !== '0') {
            TotalCount += parseInt(singlevalue);
        }
    });

    if (TotalCount != RejectionQuan) {
        alert("Please assign all quantity properly");
        return false;
    }
    else {
        return true;
    }
}

function SaveRejectionReasons() {

    var ReasonString = "";
    $('#rejectionReasonBody  > tr').each(function (index, tr) {
        console.log(index);
        console.log(tr);
        var reasonquantity = 0;
        var singlevalue = $(tr).find("td:nth-child(2) input").val();
        if (singlevalue !== undefined && singlevalue !== '' && singlevalue !== '0') {
            reasonquantity=   parseInt(singlevalue);
        }
        ReasonString = ReasonString + ($(tr).attr("id") + "-" + reasonquantity+",");
    });

    $("#RejectionReasons").val(ReasonString);
}