﻿/// <reference path="../jquery-ui-1.12.1.js" />
/// <reference path="../jquery-1.12.4.js" />

$(document).ready(function () {
    $("#CurrentMachineId").change(function () {
        $(".current_machine").val($("#CurrentMachineId").val());
    });

    $("#CurrentMachineId").change(function () {
        $(".current_machine").val($("#CurrentMachineId").val());
    });

    $(".MasterOperator").change(function () {

        $(".Operator").val($(".MasterOperator").val());
    })

    $(".MasterSupervisor").change(function () {
        $(".Supervisor").val($(".MasterSupervisor").val());
    })
    $("#CurrentShiftId").change(function () {

        window.location.href = "/Production/Index/?appId=" + encodeURIComponent("shift=" + $("#CurrentShiftId").val());
    })

});


function ProdSubmitCheck() {
    var returnval = true;
    var Message = "";
    if (($("#CurrentMachineId").val() == 0) || ($("#CurrentMachineId").val() == "")) {

        returnval = false;
        Message = "Please select a Machine.";
    }
    if (($("#CurrentShiftId").val() == 0) || ($("#CurrentShiftId").val() == "")) {
        if (Message != "") {
            Message = Message + "\n Please select a Shift.";
        }
        else {
            Message = "Please select a Shift.";
        }
        returnval = false;
    }
    if (returnval == false) {
        ShowAlertMessage(Message);
    }
    return returnval;
}

function CalculateTotalBreakDownTime() {
    calculateStoppageTime('AddedBreakDown_BreakDownStartTime', 'AddedBreakDown_BreakDownEndTime','AddedBreakDown_TotalStoppageTime');
}
function calculateStoppageTime(StartTimeControl, EndTimeControl, ResultControl) {


    try {
        var StartTimeVal = $('#' + StartTimeControl).val();
        var EndTimeVal = $('#' + EndTimeControl).val();

        var StartHour = parseInt(StartTimeVal.split(".")[0]);
        var StartMinute = parseInt(StartTimeVal.split(".")[1]);


        var EndHour = parseInt(EndTimeVal.split(".")[0]);
        var EndMinute = parseInt(EndTimeVal.split(".")[1]);

        var TotalStartMinute = (StartHour * 60) + StartMinute;
        var TotalEndMinute = (EndHour * 60) + EndMinute;


        var TotalDifference = TotalEndMinute - TotalStartMinute;
        if (TotalDifference < 0) {
            TotalDifference = parseInt($("#overNightValue").val()) + TotalDifference;
        }
        $('#' + ResultControl).val(TotalDifference);
    }
    catch (err) {
        document.getElementById("demo").innerHTML = err.message;
    }
   
}