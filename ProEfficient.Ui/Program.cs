using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.IO;

namespace ProEfficient.Ui
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {

                Log.Logger = new LoggerConfiguration()
                                   .Enrich.FromLogContext()
                                   .WriteTo.Console(LogEventLevel.Information)
                                   .WriteTo.File("Logs/Log.log", LogEventLevel.Information, rollingInterval: RollingInterval.Day)
                                   .CreateLogger();

                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"{nameof(Program)}.{nameof(Main)} - Error occured..");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
