﻿using ProEfficient.Core.Models;
using System.Threading.Tasks;

namespace ProEfficient.Ui.Data
{
    public interface IAuthRepository
    {
        Task<User> Register(User user, string password);
        Task<User> Login(string username, string password);
        Task<bool> IsUserExists(string username);
    }
}