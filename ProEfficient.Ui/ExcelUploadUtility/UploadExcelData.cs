﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ProEfficient.Ui.ExcelUploadUtility
{
    public class UploadExcelData
    {
        public static SqlConnection SqlConnection = new SqlConnection();
      
        public static string sqlAddressserver= "Data Source=DESKTOP-K0BHCJ8;Initial Catalog=ProEfficientJKL;Integrated Security=True";
        public void UploadDataToServer(string type,DataTable data)
        {
            string TableName = "";
            string CheckColname = "MachineName";
            if (type == "machine")
            {
                TableName = "dbo.MasterMachine";
                CheckColname = "";
            }
            else if (type == "product")
            {
                TableName = "dbo.MasterProduct";
                CheckColname = "ProductName";
            }
            string insertvar = "";
            for (int col = 0; col < data.Columns.Count; col++)
            {
                if (insertvar == "")
                    insertvar = "`" + data.Columns[col] + "`";
                else
                    insertvar = insertvar + "`" + data.Columns[col] + "`";
            }

            string InsertHead = "Insert into "+ TableName + " (" + insertvar + ") Values () ";
            string insertbody = "";

            for (int j = 0; j < data.Rows.Count; j++)
            {

                DataTable DataExcists = getDataFromSql("select * from "+TableName+" where "+ CheckColname + "='"+data.Rows[j][CheckColname]+"'");

                if (DataExcists.Rows.Count == 0)
                {
                    for (int col = 0; col < data.Columns.Count; col++)
                    {
                        if (insertbody == "")
                            insertbody = data.Rows[j][data.Columns[col]].ToString();
                        else
                            insertbody = insertbody + "," + data.Rows[j][data.Columns[col]].ToString();
                    }
                    if (j % 50 == 0)
                    {
                        //Excecute Qry Inset Data
                        execQuerySQL(InsertHead + insertbody);
                        insertbody = "";
                    }
                }


            }

            if (insertbody != "")
            {
                //Excecute Qry Inset Data
                execQuerySQL(InsertHead + insertbody);
                insertbody = "";
            }
        }







        public static DataTable getDataFromSql(string query)
        {
            DataTable DT = new DataTable();
            bool ConnFound = ConnectionSql();


            try
            {
                if (ConnFound)
                {
                    String sql = query;

                    SqlDataAdapter Adpter = new SqlDataAdapter(sql, SqlConnection);
                    Adpter.SelectCommand.CommandTimeout = 300;
                    DataSet DataSet = new DataSet();
                    Adpter.Fill(DataSet);

                    if (DataSet.Tables.Count == 0 || DataSet.Tables[0].Rows.Count == 0)
                    {
                        SqlConnection.Close();
                        return DT;
                    }
                    DT = DataSet.Tables[0];
                    SqlConnection.Close();
                    return DT;

                }
                else
                {
                    return DT;
                }
            }
            catch (Exception e)
            {
                SqlConnection.Close();
                return DT;
            }
            finally { SqlConnection.Close(); }

        }
        public static bool ConnectionSql()
        {
            try
            {
                SqlConnection = new SqlConnection(sqlAddressserver);
                SqlConnection.Open();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Connection error");
                return false;
            }
        }
        public static bool execQuerySQL(string sqlinsert)
        {
            // bool ConnFound = ConnectionMS();

            string sql = sqlinsert;
            try
            {
                //if (ConnFound)
                if (SqlConnection.State != ConnectionState.Open)
                    ConnectionSql();
                {
                    SqlCommand Cmd = new SqlCommand(sql, SqlConnection);
                    if (Cmd.ExecuteNonQuery() > 0)
                    {
                        SqlConnection.Close();
                        return true;
                    }
                    else
                    {
                        SqlConnection.Close();
                        return false;
                    }
                }

            }
            catch (Exception e)
            {
                SqlConnection.Close();
                return false;
            }
            finally { SqlConnection.Close(); }

        }

    }
}
