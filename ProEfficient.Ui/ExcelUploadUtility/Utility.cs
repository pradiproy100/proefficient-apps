﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading;

namespace ProEfficient.Ui.ExcelUploadUtility
{
    public static class Utility
    {


        public static SelectList GetSelectList<T>(this List<T> list, string valueProp, string textProp)
        {

            var SelectList = new SelectList(list, valueProp, textProp);
            return SelectList;

        }



    }

    public class EmailUtility
    {

        private IConfiguration Configuration;

        public EmailUtility(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public Boolean SendMail(String ToEmail, String Subject, String EmailMessageBody, String AttachmentPath)
        {
            try
            {


                // var BackUppath = (Email.BackupExcelPath + DateTime.Now.ToString("yyyyMMddHHmmssFFF")) + ".xlsx";

                if (!String.IsNullOrEmpty(AttachmentPath))
                {


                    MailMessage Msg = new MailMessage();
                    Msg.From = new MailAddress(FromEmailId);
                    if (ToEmail.Contains(","))
                    {
                        var EmailArray = ToEmail.Split(',');
                        if (EmailArray.Length > 0)
                        {
                            foreach (var em in EmailArray)
                            {
                                if (!String.IsNullOrEmpty(em))
                                {
                                    Msg.To.Add(em);
                                }
                            }
                        }
                    }
                    else
                    {
                        Msg.To.Add(ToEmail.Trim());
                    }


                    Msg.Subject = Subject;
                    Msg.IsBodyHtml = true;
                    //Msg.Body = EmailMessage;
                    Msg.Body = EmailMessageBody;
                    // your remote SMTP server IP.

                    SmtpClient smtp = new SmtpClient(FromEmailsmtpClient);
                    smtp.UseDefaultCredentials = false;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    // smtp.Host = FromEmailsmtpClient;
                    smtp.Port = FromEmailsmtpClientPort;
                    smtp.Credentials = new NetworkCredential(FromEmailId, FromEmailPassword);
                    smtp.EnableSsl = true;
                    Attachment attachment = new Attachment(AttachmentPath);
                    Msg.Attachments.Add(attachment);
                    smtp.Send(Msg);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public String FromEmailsmtpClient => Configuration.GetValue<string>("FromEmailsmtpClient");

        public String FromEmailId => Configuration.GetValue<string>("FromEmailId");

        public String FromEmailPassword => Configuration.GetValue<string>("FromEmailPassword");

        public int FromEmailsmtpClientPort => Configuration.GetValue<int>("FromEmailsmtpClientPort");
        public String BccEmail => Configuration.GetValue<string>("BccEmail");
        public String ReplaceToToBCC => Configuration.GetValue<string>("ReplaceToToBCC");


    }
}
